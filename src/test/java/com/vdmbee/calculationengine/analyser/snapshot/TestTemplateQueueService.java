package com.vdmbee.calculationengine.analyser.snapshot;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.vdmbee.TestBase;
import com.vdmbee.calculationengine.analyser.snapshot.service.TemplateQueueService;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;

public class TestTemplateQueueService extends TestBase{
	@Autowired 
	private TemplateQueueService templateQueueService;
	
  	@DataProvider(name = "templateProvider")
	public static Object[][] getTemplates(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("dataFilePath");
		//System.out.println("data file path" + dataPath);
		Object instanceData[][] = new Object[][] {};
		JSONObject jsonObj= loadDataFromJSONFile(dataPath);
		List<JSONObject> templateList = (List<JSONObject>) jsonObj.get("templates");
		for (int i = 0; i < templateList.size(); i++) {
			instanceData = appendValue(instanceData, templateList.get(i));
		}
		return instanceData;
  	}
  	@Test(groups = { "find"}, dataProvider = "templateProvider", description = "Find template. Takes input id, email, tenantid",dependsOnMethods = {"testCreateTemplate"})
	void testFindTemplate(ITestContext context, JSONObject tempObj) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject response = templateQueueService.getTemplate(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "createpython"}, dataProvider = "templateProvider", description = "create python template, this generates single script")
	void testCreatePythonTemplate(ITestContext context, JSONObject tempObj) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject response = templateQueueService.createPythonTemplate(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "create"}, dataProvider = "templateProvider", description = "create template")
	void testCreateTemplate(ITestContext context, JSONObject tempObj) throws Exception {
  		JSONObject data = (JSONObject) tempObj.get("data");
		String tenantId = (String)data.get("tenantId");
  		//MongoTenantContext.setTenant(tenantId);
		JSONObject response = templateQueueService.createTemplate(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "update"}, dataProvider = "templateProvider", description = "Updatetemplate")
	void testUpdateTemplate(ITestContext context, JSONObject tempObj) throws Exception {
		JSONParser parser = new JSONParser();
		JSONObject response = templateQueueService.createTemplate(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "read"},description = "read templates")
	void testReadTemplates(ITestContext context) throws Exception {
  		JSONObject templateRequest = new JSONObject();
  		templateRequest.put("tenantId", "VDMBee");
  		templateRequest.put("data", new JSONObject());
		JSONObject response = templateQueueService.getTemplates(templateRequest);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "delete"}, dataProvider = "templateProvider", description = "Updatetemplate",dependsOnMethods = {"testCreateTemplate"})
	void testDeleteTemplate(ITestContext context, JSONObject tempObj) throws Exception {
		templateQueueService.deleteTemplateFromQueue(tempObj);
		JSONParser parser = new JSONParser();
		JSONObject response = templateQueueService.getTemplate(tempObj);
		assertEquals(response, null);
	}
  	
  	@Test(groups = { "deleteDB"}, dataProvider = "templateProvider", description = "droptemplate")
	void testDropDB(ITestContext context, JSONObject tempObj) throws Exception {
  		JSONObject data = (JSONObject) tempObj.get("data");
		String tenantId = (String)data.get("tenantId");
  		MongoTenantContext.setTenant(tenantId);
  		ApplicationTransaction transaction = ApplicationTransactionManager.getApplicationTransaction();
		 transaction.initializeMongoTransaction();	
		templateQueueService.dropTenant(tempObj);
		transaction.commitTransaction();
		//JSONParser parser = new JSONParser();
		//JSONObject response = templateQueueService.getTemplate(tempObj);
		//assertEquals(response, null);
	}
  	
}
