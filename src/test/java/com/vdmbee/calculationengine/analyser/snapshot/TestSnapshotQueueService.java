package com.vdmbee.calculationengine.analyser.snapshot;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.xml.sax.SAXException;

import com.vdmbee.TestBase;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotQueueService;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.analyser.snapshot.service.TemplateService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;

public class TestSnapshotQueueService extends TestBase {
	@Autowired 
	private SnapshotQueueService snapshotQueueService;
	
	@Autowired
	private TemplateService templateService;
	@Autowired
	private SnapshotService snapshotService;
	
	private static Object[][] getFileData(String dataPath,String dataType ) throws FileNotFoundException, IOException, ParseException{
		//System.out.println("data file path" + dataPath);
		Object instanceData[][] = new Object[][] {};
		JSONObject jsonObj= loadDataFromJSONFile(dataPath);
		List<JSONObject> templateList = (List<JSONObject>) jsonObj.get(dataType);
		for (int i = 0; i < templateList.size(); i++) {
			instanceData = appendValue(instanceData, templateList.get(i));
		}
		return instanceData;
	}
  	@DataProvider(name = "templateProvider")
	public static Object[][] getTemplates(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("templateDataFilePath");
	  	return getFileData(dataPath, "templates");
  	}
  	
  	@DataProvider(name = "snapshotProvider")
	public static Object[][] getSnapshots(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("dataFilePath");
	  	return getFileData(dataPath, "snapshots");
  	}
  	@DataProvider(name = "snapshotProviderSkipInputs")
	public static Object[][] getSnapshotsWithOptionalInputs(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("skipOptionalDataFilePath");
	  	return getFileData(dataPath, "snapshots");
  	}
  	@DataProvider(name = "snapshotProviderInvertSign")
	public static Object[][] getSnapshotsWithSignInversion(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("invertSignDataFilePath");
	  	return getFileData(dataPath, "snapshots");
  	}
  	@DataProvider(name = "snapshotProviderUpdateDataset")
	public static Object[][] getSnapshotsWithUpdateDataset(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("updateDatasetDataFilePath");
	  	return getFileData(dataPath, "snapshots");
  	}
  	@DataProvider(name = "rawSnapshotProvider")
	public static Object[][] getRawSnapshots(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("rawDataFilePath");
	  	return getFileData(dataPath, "snapshots");
  	}
  	@DataProvider(name = "snapshotRubricProvider")
	public static Object[][] getSnapshotRubrics(ITestContext context)
			throws ParserConfigurationException, IOException, SAXException, ParseException {
	  	String dataPath = context.getCurrentXmlTest().getParameter("rubricsDataFilePath");
	  	return getFileData(dataPath, "snapshotrubrics");
  	}
  	@Test(groups = { "find"}, dataProvider = "snapshotProvider", description = "Find snapshot. Takes input id, email, tenantid",dependsOnMethods = {"testCreateSnapshot"})
	void testFindSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
		JSONObject response = snapshotQueueService.getSnapshot(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "create","createpython"}, dataProvider = "snapshotProvider", description = "create snapshot")
	void testCreateSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
  		System.out.println("Start time:" + new Date());
  		JSONObject data = (JSONObject) tempObj.get("data");
		String tenantId = (String)data.get("tenantId");
  		//MongoTenantContext.setTenant(tenantId);
		//MongoTenantContext.setTenant("018730F5-264B-AAD8-5550-DCAFF89E1164");
		JSONObject response = snapshotQueueService.createSnapshot(tempObj);
		//System.out.println(response.toJSONString());
		System.out.println("End Time:" + new Date());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "update"}, dataProvider = "snapshotProvider", description = "Updatesnapshot")
	void testUpdateSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
		JSONObject response = snapshotQueueService.createSnapshot(tempObj);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "updateatomicoptional"}, dataProvider = "snapshotProviderSkipInputs", description = "Updatesnapshot",dependsOnMethods = "testCreateSnapshot")
	void testUpdateOptionalSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
  		JSONObject data= (JSONObject)tempObj.get("data");
  		JSONObject snapshot = snapshotService.fetchSnapshot((String)data.get("id"), (String)data.get("tenantId"), (String)tempObj.get("email"));
		JSONObject response = snapshotQueueService.updateSnapshot(tempObj);
		Template template = templateService.getTemplateObject((String)(String)data.get("templateId"), (String)data.get("tenantId"), (String)tempObj.get("email"));
		List<JSONObject> templateFileContent = template.getFileContent();
		List<JSONObject> snapshotFileContent = (List<JSONObject>)snapshot.get("fileContent");
		List<JSONObject> responseFileContent = (List<JSONObject>)response.get("fileContent");
		for(int i=0;i<templateFileContent.size();i++) {
			JSONObject node = templateFileContent.get(i);
			if(node.get("ValueType").equals("Input")) {
				String nodeId = (String)node.get("id");
				for(int j=0;j<snapshotFileContent.size();j++) {
					JSONObject snapshotNode = snapshotFileContent.get(j);
					if(snapshotNode.get("id").equals(nodeId)) {
						Double oldValue =(Double)snapshotNode.get("Value");
						if(oldValue != null) {
							for(int k=0;k<responseFileContent.size();k++) {
								JSONObject responseNode = responseFileContent.get(k);
								if(responseNode.get("id").equals(nodeId)) {
									Double newValue =(Double)responseNode.get("Value");
									assertEquals(oldValue,newValue);
									break;
								}
							}
						}
						break;
					}
				}
			}
		}
		
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	
  	@Test(groups = { "invertsign"}, dataProvider = "snapshotProviderInvertSign", description = "Invert Sign",dependsOnMethods = "testCreateSnapshot")
	void testInvertSignSnapshot(ITestContext context, JSONObject tempObj) throws Exception {  		JSONObject data= (JSONObject)tempObj.get("data");
  		JSONObject snapshot = snapshotService.fetchSnapshot((String)data.get("id"), (String)data.get("tenantId"), (String)tempObj.get("email"));
		JSONObject response = snapshotQueueService.updateSnapshot(tempObj);
		Template template = templateService.getTemplateObject((String)(String)data.get("templateId"), (String)data.get("tenantId"), (String)tempObj.get("email"));
		List<JSONObject> templateFileContent = template.getFileContent();
		List<JSONObject> snapshotFileContent = (List<JSONObject>)snapshot.get("fileContent");
		List<JSONObject> responseFileContent = (List<JSONObject>)response.get("fileContent");
		List<JSONObject> requestFileContent = (List<JSONObject>)data.get("fileContent");
		for(int i=0;i<templateFileContent.size();i++) {
			JSONObject node = templateFileContent.get(i);
			if(node.get("ValueType").equals("Input")) {
				String nodeId = (String)node.get("id");
				for(int l=0;l<requestFileContent.size();l++) {
					JSONObject requestNode = requestFileContent.get(l);
					String reqNodeId = (String)requestNode.get("id");
					if(reqNodeId.equals(nodeId) && (requestNode.get("invertSign") != null) && (boolean)requestNode.get("invertSign") ){
						for(int j=0;j<snapshotFileContent.size();j++) {
							JSONObject snapshotNode = snapshotFileContent.get(j);
							String snodeId = (String)snapshotNode.get("id");
							if(snapshotNode.get("id").equals(nodeId)) {
								Object oldValue =snapshotNode.get("Value");
								Double oldVal = null;
								if(oldValue instanceof Long) {
									oldVal = ((Long) oldValue).doubleValue();
								}else if(oldValue instanceof Double) {
									oldVal = ((Double) oldValue).doubleValue();
								}
								if(oldValue != null) {
									for(int k=0;k<responseFileContent.size();k++) {
										JSONObject responseNode = responseFileContent.get(k);
										if(responseNode.get("id").equals(nodeId)) {
											Object newValue =responseNode.get("Value");
											Double newVal = null;
											if(newValue instanceof Long) {
												newVal = ((Long) newValue).doubleValue() * (-1);
											}else if(newValue instanceof Double) {
												newVal = ((Double) newValue).doubleValue() * (-1);
											}
											assertEquals(oldVal,newVal);
											break;
										}
									}
								}
								break;
							}
						}
					}
				}
			}
		}
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "updateDataset"}, dataProvider = "snapshotProviderUpdateDataset", description = "UpdateDataset",dependsOnMethods = "testCreateSnapshot")
	void testUpdateDatasetSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
  		JSONObject data= (JSONObject)tempObj.get("data");
		JSONObject response = snapshotQueueService.updateSnapshot(tempObj);
		Template template = templateService.getTemplateObject((String)(String)data.get("templateId"), (String)data.get("tenantId"), (String)tempObj.get("email"));
		List<JSONObject> templateFileContent = template.getFileContent();
		List<JSONObject> responseFileContent = (List<JSONObject>)response.get("fileContent");
		List<JSONObject> requestFileContent = (List<JSONObject>)data.get("fileContent");
		for(int i=0;i<templateFileContent.size();i++) {
			JSONObject node = templateFileContent.get(i);
			if(node.get("ValueType").equals("Input")) {
				String nodeId = (String)node.get("id");
				for(int l=0;l<requestFileContent.size();l++) {
					JSONObject requestNode = requestFileContent.get(l);
					String reqNodeId = (String)requestNode.get("id");
					if(reqNodeId.equals(nodeId) && (requestNode.get("updateDataset") != null) && !(boolean)requestNode.get("updateDataset") ){
						for(int k=0;k<responseFileContent.size();k++) {
							JSONObject responseNode = responseFileContent.get(k);
							if(responseNode.get("id").equals(nodeId)) {
								Object newValue =responseNode.get("Value");
								assertEquals(newValue,null);
								break;
							}
						}
					}
				}
			}
		}
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "read"},description = "read snapshots")
	void testReadSnapshots(ITestContext context) throws Exception {
  		JSONObject snapshotRequest = new JSONObject();
  		snapshotRequest.put("tenantId", "VDMBee");
  		snapshotRequest.put("data", new JSONObject());
		JSONObject response = snapshotQueueService.getSnapshots(snapshotRequest);
		//System.out.println(response.toJSONString());
		assertNotEquals(response, null);
	}
  	@Test(groups = { "delete"}, dataProvider = "snapshotProvider", description = "Updatesnapshot",dependsOnMethods = {"testCreateSnapshot"})
	void testDeleteSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
		snapshotQueueService.deleteSnapshot(tempObj);
		JSONParser parser = new JSONParser();
		JSONObject response = snapshotQueueService.getSnapshot(tempObj);
		if(response != null) {
			assertNotEquals(response.get("error_404"),null);
		}else {
			assertEquals(response, null);	
		}
	}
  	
  	@Test(groups = { "find"}, dataProvider = "templateProvider", description = "Find snapshots by template id",dependsOnMethods = {"testCreateSnapshot"})
  	public void getSnapshotsByTemplateId(ITestContext context,JSONObject snapshotRequest) throws ParseException, AccessDeniedException {
  		snapshotQueueService.getSnapshotsByTemplates(snapshotRequest);
  	}
  	@Test(groups = { "findRubrics"}, dataProvider = "snapshotRubricProvider", description = "Updatesnapshot",dependsOnMethods = {"testCreateSnapshot"})
  	public void getRubricValueInSnapshots(ITestContext context,JSONObject snapshotRequest) throws  ParseException, AccessDeniedException {
  		snapshotQueueService.getRubricValueInSnapshots(snapshotRequest);
  	}
  	@Test(groups = {"createpythonraw"}, dataProvider = "rawSnapshotProvider", description = "create raw snapshot")
	void testCreateRawSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
		JSONObject response = snapshotQueueService.createSnapshot(tempObj);
		response = snapshotQueueService.getSnapshot(tempObj);
		//System.out.println(response.toJSONString());
		assertEquals(response.get("templateId"), null);
	}
  	
  	@Test(groups = { "deleteraw"}, dataProvider = "rawSnapshotProvider", description = "delete raw snapshot",dependsOnMethods = {"testCreateRawSnapshot"})
	void testDeleteRawSnapshot(ITestContext context, JSONObject tempObj) throws Exception {
		snapshotQueueService.deleteSnapshot(tempObj);
		JSONParser parser = new JSONParser();
		JSONObject response = snapshotQueueService.getSnapshot(tempObj);
		if(response != null) {
			assertNotEquals(response.get("error_404"),null);
		}else {
			assertEquals(response, null);	
		}
	}
  	
  	

}
