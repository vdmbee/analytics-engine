package com.vdmbee;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;

import com.vdmbee.calculationengine.Application;
import com.vdmbee.calculationengine.multitenant.MongoDBCredentials;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;

@SpringBootTest(classes = Application.class)
public class TestBase extends AbstractTestNGSpringContextTests{
	private static Logger logger = LogManager.getLogger(TestBase.class);
	
	@Autowired
	public MongoDBCredentials mongoDBCredentials;
	
	protected static JSONObject loadDataFromJSONFile(String path) throws FileNotFoundException, IOException, ParseException {
		File jsonFile = new File(path);
		JSONParser parser = new JSONParser();
		JSONObject jsonObj = (JSONObject)parser.parse(new FileReader(jsonFile));
		return jsonObj;
	}

	protected static Object[][] appendValue(Object[][] obj, Object newObj) {
		Object[][] newObjArray = new Object[1][1];
		newObjArray[0][0] = newObj;
		Object[][] result = new Object[obj.length + newObjArray.length][];
		System.arraycopy(obj, 0, result, 0, obj.length);
		System.arraycopy(newObjArray, 0, result, obj.length, newObjArray.length);
		return result;
	}
	

	@AfterSuite(alwaysRun = true)
	public void afterSuite() {
		try {
			if(mongoDBCredentials == null) {
				mongoDBCredentials = SnapshotBeanUtil.getBean(MongoDBCredentials.class);
			}
			//String mongoUrl = mongoDBCredentials.getPath() + "/" + MongoTenantContext.getTenant();
			//MongoClient mongo = MongoClients.create(mongoUrl );
			/*MongoClient mongoClient = mongoDBCredentials.getMongoClient(MongoTenantContext.getTenant(), null);
			MongoDatabase db = mongoClient.getDatabase(MongoTenantContext.getTenant());
			db.drop();*/
		}catch(Exception e) {
			logger.error(e);
			e.printStackTrace();
		}
	}
	
	@BeforeMethod(alwaysRun = true)
    public void initialiseTransaction() {
        System.out.println("Initializing transaction...");
        ApplicationTransactionManager.getApplicationTransaction().initializeMongoTransaction();	
    }
	
	@AfterTest(alwaysRun = true)
    public void closeTransaction() {
        System.out.println("Commiting transaction...");
        ApplicationTransactionManager.getApplicationTransaction().commitTransaction();
    }
}

