package client;
/*

import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.remoting.client.AmqpProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.vdmbee.server.common.ICalculationEngineFacade;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.core.env.Environment;

import java.util.Objects;

@Configuration
public class SpringConfig {

    @Autowired
    private Environment env;

    @Bean
    public ConnectionFactory connectionFactory() {
    	CachingConnectionFactory factory = new CachingConnectionFactory("localhost", 5672);
    	factory.setCloseTimeout(6000000);
    	factory.setUsername(Objects.requireNonNull(env.getProperty("spring.rabbitmq.username")));
    	factory.setPassword(Objects.requireNonNull(env.getProperty("spring.rabbitmq.password")));
        return factory;
    }
    @Bean
    public RabbitTemplate template() {
        RabbitTemplate template = new RabbitTemplate(connectionFactory());
        template.setExchange("rpc");
        return template;
    }

    @Bean
    public AmqpProxyFactoryBean proxy(RabbitTemplate template) {
        AmqpProxyFactoryBean proxy = new AmqpProxyFactoryBean();
        proxy.setAmqpTemplate(template);
        proxy.setServiceInterface(ICalculationEngineFacade.class);
        proxy.setRoutingKey(ICalculationEngineFacade.class.getSimpleName());

        return proxy;
    }
}
*/
