package com.vdmbee.calculationengine.errorhandler;


import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class tenantError extends Exception {
	private static final Logger logger = LogManager.getLogger(tenantError.class);
    public tenantError(Class clazz, String... searchParamsMap) {
        super(tenantError.generateMessage(clazz.getSimpleName(), searchParamsMap[0]));
    }

    
	private static String generateMessage(String entity, String searchParams) {
		String message = StringUtils.capitalize(entity) +
                " was not found for parameters " +
                searchParams;
		logger.info("Info: "+message);
		
		
        return message;
    }

   

}
