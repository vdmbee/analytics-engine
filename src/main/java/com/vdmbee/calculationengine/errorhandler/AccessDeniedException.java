package com.vdmbee.calculationengine.errorhandler;


import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AccessDeniedException extends Exception {
	
	private static final Logger logger = LogManager.getLogger(AccessDeniedException.class);
    public AccessDeniedException(Class clazz, String... searchParamsMap) {
        super(AccessDeniedException.generateMessage(clazz.getSimpleName(), toMap(String.class, String.class, searchParamsMap)));
    }

    
	private static String generateMessage(String entity, Map<String, String> searchParams) {
		String message = StringUtils.capitalize(entity) +
                " was not found for parameters " +
                searchParams;
		logger.info(message);
        return message;
    }

    private static <K, V> Map<K, V> toMap(
            Class<K> keyType, Class<V> valueType, Object... entries) {
        if (entries.length % 2 == 1)
            throw new IllegalArgumentException("Invalid entries");
        return IntStream.range(0, entries.length / 2).map(i -> i * 2)
                .collect(HashMap::new,
                        (m, i) -> m.put(keyType.cast(entries[i]), valueType.cast(entries[i + 1])),
                        Map::putAll);
    }

}

