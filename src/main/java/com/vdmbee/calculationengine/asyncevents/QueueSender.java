package com.vdmbee.calculationengine.asyncevents;

import org.json.simple.JSONObject;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;


@Component
@ConditionalOnProperty(prefix="vmp.queue", name = "useRabbitmq", havingValue = "true")
public class QueueSender implements CommandLineRunner {

	@Autowired
    private final RabbitTemplate rabbitTemplate;
	@Autowired
    private final QueueReceiver receiver;

    public QueueSender(QueueReceiver receiver, RabbitTemplate rabbitTemplate) {
        this.receiver = receiver;
        this.rabbitTemplate = rabbitTemplate;
    }

    @Override
    public void run(String... args) throws Exception {
        /*System.out.println("Sending message...");
        rabbitTemplate.convertAndSend(QueueConfig.topicExchangeName, "bms.caseevent", "{\"name\":\"rajender\"}");
        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);*/
    }
    public void sendMessage(String routingKey,JSONObject message,String corrleationId) throws InterruptedException {
        //System.out.println("Sending message...");
        //rabbitTemplate.convertAndSend(VisionPlannerApplication.topicExchangeName, "bms.caseevent", "{\"name\":\"rajender\"}");
        CorrelationData correlationData = null;
        if(corrleationId != null) {
        	correlationData = new CorrelationData(corrleationId);
        }
        rabbitTemplate.convertAndSend(QueueConfig.topicExchangeName, QueueConfig.writeRoutingKey, message.toJSONString(),correlationData);
        //receiver.getLatch().await(0, TimeUnit.MILLISECONDS);
    }

}
