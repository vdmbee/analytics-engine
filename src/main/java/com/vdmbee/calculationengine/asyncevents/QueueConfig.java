package com.vdmbee.calculationengine.asyncevents;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.remoting.service.AmqpInvokerServiceExporter;
import org.springframework.amqp.support.converter.SimpleMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.vdmbee.server.common.ICalculationEngineFacade;

@Configuration
@ConditionalOnProperty(prefix="vmp.queue", name = "useRabbitmq", havingValue = "true")
public class QueueConfig {
	@Autowired
	private Environment env;
	
	@Value("${vmp.queue.concurrency}")
	private String vmpQueueConcurrency;

	public static String topicExchangeName= null;
	public static String rpcExchangeName= null;
	public static String readRoutingKey = null;
	public static String readQueueName = null;
	public static String writeRoutingKey = null;
	public static String writeQueueName = null;

	public static String queueName= null;
	//public static String deadQueueName = null;
	public static String routingKey = null;
	//public static String deadRoutingKey = null;
	
   // public static final String NODE_ID_PROPERTY = "node";
    //public static final String NODE_ID_PROPERTY_EL = "#{systemProperties[" + NODE_ID_PROPERTY + "]}";
  //  public static final String NODE_ID_PROPERTY_EL = "1";

    /*@Bean
    public ConnectionFactory connectionFactory() {
    	CachingConnectionFactory factory = new CachingConnectionFactory("localhost", 5672);
    	factory.setCloseTimeout(6000000);
    	factory.setUsername("mrajender@gmail.com");
    	factory.setPassword("rajender123");
        return factory;
    }*/
	@Bean
	Queue queue() {
		QueueConfig.topicExchangeName = env.getProperty("vmp.queue.topicExchangeName");
		QueueConfig.rpcExchangeName = env.getProperty("vmp.queue.vmpExchangeName");
		QueueConfig.queueName = env.getProperty("vmp.queue.vmpQueueName");
		//QueueConfig.deadQueueName = env.getProperty("vmp.queue.deadQueueName");
		QueueConfig.routingKey = env.getProperty("vmp.queue.vmpRoutingKey");
		QueueConfig.readRoutingKey = env.getProperty("vmp.queue.readRoutingKey");
		QueueConfig.readQueueName = env.getProperty("vmp.queue.readQueueName");
		QueueConfig.writeRoutingKey = env.getProperty("vmp.queue.writeRoutingKey");
		QueueConfig.writeQueueName = env.getProperty("vmp.queue.writeQueueName");
		//QueueConfig.deadRoutingKey = env.getProperty("vmp.queue.deadRoutingKey");
		return new Queue(QueueConfig.queueName);
	}
	
	@Bean
	TopicExchange exchange() {
		if(topicExchangeName == null) {
			QueueConfig.topicExchangeName = env.getProperty("vmp.queue.topicExchangeName");
		}
		return new TopicExchange(topicExchangeName);
	}
	
	@Bean
	Queue readQueue() {
		return new Queue(QueueConfig.readQueueName, true);
	}

	@Bean
	Binding readBinding(Queue readQueue, TopicExchange exchange) {
		if(readRoutingKey == null) {
			QueueConfig.readRoutingKey = env.getProperty("vmp.queue.readRoutingKey");
		}
		return BindingBuilder.bind(readQueue).to(exchange).with(QueueConfig.readRoutingKey);
	}
	
	@Bean
	Queue writeQueue() {
		return new Queue(QueueConfig.writeQueueName, true);
	}
	
	@Bean
	Binding writeBinding(Queue writeQueue, TopicExchange exchange) {
		if(writeRoutingKey == null) {
			QueueConfig.writeRoutingKey = env.getProperty("vmp.queue.writeRoutingKey");
		}
		return BindingBuilder.bind(writeQueue).to(exchange).with(QueueConfig.writeRoutingKey);
	}

	/*@Bean
	DirectExchange exchange() {
		if(topicExchangeName == null) {
			QueueConfig.topicExchangeName = env.getProperty("vmp.queue.topicExchangeName");
		}
		return new DirectExchange(QueueConfig.topicExchangeName);
	}

	@Bean
	Binding binding(Queue queue, Exchange exchange) {
		if(routingKey == null) {
			QueueConfig.routingKey = env.getProperty("vmp.queue.routingKey");
		}
		return BindingBuilder.bind(queue).to(exchange).with(QueueConfig.routingKey).noargs();
	}*/
	
	/*@Bean
	Binding deadBinding(Queue deadQueue, Exchange exchange) {
		if(deadRoutingKey == null) {
			QueueConfig.deadRoutingKey = env.getProperty("vmp.queue.deadRoutingKey");
		}
		return BindingBuilder.bind(deadQueue).to(exchange).with(QueueConfig.deadRoutingKey).noargs();
	}*/
	
	/*@Bean
	com.rabbitmq.client.ConnectionFactory createConnection() throws KeyManagementException, NoSuchAlgorithmException, URISyntaxException, IOException, TimeoutException {
		com.rabbitmq.client.ConnectionFactory factory = new com.rabbitmq.client.ConnectionFactory();
		factory.setUsername("krishna@vdmbee.com");
		factory.setPassword("krishna");
		factory.setVirtualHost("/");
		factory.setHost("localhost");
		factory.setPort(5672);

		Connection conn = factory.newConnection();
		//connectionFactory.getRabbitConnectionFactory().setUri("amqps://guest:guest@localhost:5671/virtualhost");
		return factory;
	}*/

  /*  @Bean
    public RabbitAdmin admin(ConnectionFactory factory) {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(factory);

        return rabbitAdmin;
    }

    @Bean
    public RabbitTemplate template(ConnectionFactory factory) {
        RabbitTemplate template = new RabbitTemplate(factory);

        return template;
    }*/
    @Bean
    public AmqpInvokerServiceExporter exporter(AmqpTemplate template, ICalculationEngineFacade service) {
        AmqpInvokerServiceExporter exporter = new AmqpInvokerServiceExporter();
        exporter.setAmqpTemplate(template);
        exporter.setService(service);
        exporter.setServiceInterface(ICalculationEngineFacade.class);
        SimpleMessageConverter ss = new SimpleMessageConverter();
        ss.addAllowedListPatterns(new String[]{"org.springframework.remoting.support.RemoteInvocation","java.lang.Boolean","org.json.simple.JSONArray","java.lang.String","org.json.simple.JSONObject","java.util.HashMap","java.util.ArrayList"});
        exporter.setMessageConverter(ss);
        return exporter;
    }
    

    @Bean
    public SimpleMessageListenerContainer container(ConnectionFactory connectionFactory, Queue queue, AmqpInvokerServiceExporter exporter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setQueues(queue);
        container.setMessageListener(exporter);
        container.setConcurrency(vmpQueueConcurrency);
        container.setPrefetchCount(1);
        return container;
    }

    @Bean
    SimpleMessageListenerContainer asyncContainer(ConnectionFactory connectionFactory,
    		Queue queue,MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(QueueConfig.readQueueName);
        container.setConcurrency(vmpQueueConcurrency);
        container.setMessageListener(listenerAdapter);
        container.setPrefetchCount(1);
        return container;
    }

	@Bean
	MessageListenerAdapter listenerAdapter(QueueReceiver receiver) {
		return new MessageListenerAdapter(receiver);
	}
}
