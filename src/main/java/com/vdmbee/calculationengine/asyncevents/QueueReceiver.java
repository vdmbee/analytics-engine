package com.vdmbee.calculationengine.asyncevents;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.retry.RecoveryCallback;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.mongodb.MongoWriteException;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;

@Component
@ConditionalOnProperty(prefix="vmp.queue", name = "useRabbitmq", havingValue = "true")
public class QueueReceiver implements MessageListener{
	@Autowired
	private Environment env;
	
	@Autowired
	@Lazy
	QueueSender queueSender;
	
    @Autowired
    RetryTemplate retryTemplate;
    
    @Value("${retry.maxAttempts}")
    Integer maxAttempts = 1;

	private static final Logger logger = LogManager.getLogger(QueueReceiver.class);
	
    //private CountDownLatch latch = new CountDownLatch(1);

    public void receiveMessage(String message) {
    	logger.info("Received <" + message + ">");
      //  latch.countDown();
    }

	/*
	 * public CountDownLatch getLatch() { return latch; }
	 */
	@Override
	public void onMessage(Message message) {
		String request = new String(message.getBody());
		//System.out.println("thread start:" + Thread.currentThread().toString()+" date:"+new Date());
		Date before = new Date();
		JSONParser parser = new JSONParser();
		
		try {
			
			//request = request.replace("'", "\"");
			JSONObject requestObj = (JSONObject)parser.parse(request);
			requestObj = (JSONObject)requestObj.get("data");
			if(requestObj == null) {
				return;
			}
			
			String componentName = (String)requestObj.get("component");
			logger.debug("received:" + componentName + new Date());
			if(componentName != null){
				this.handleServiceRequest(message,requestObj);
			}else {
				logger.error("Component Name missing, Unsupported Handler for request");
				//throw new RuntimeException ("Unsupported Handler for request");
			}
			/*Date after = new Date();
			Timming.addHandlingTime(after.getTime() - before.getTime());
			
			if(requestObj.get("responseMethod") != null && requestObj.get("responseMethod").equals("saveBenchmarkMeasurements")) {
				Timming.printStatistics("Benchmark");
			}else {
				Timming.incrementUploads();
			}*/
			//System.out.println("thread end:" + Thread.currentThread().toString()+" date:"+new Date());
			
			//transaction.threadCount();
		}
		catch(Exception ex){
			StringWriter sw = new StringWriter();
			ex.printStackTrace(new PrintWriter(sw));
	        String expString = sw.toString();
	        String result = expString.substring(0, Math.min(expString.length(), 800));
			logger.error(result);
			if(message.getMessageProperties().getRedelivered() == true) {
				logger.error("Skipping message after retry:" + request);
				return;
			}else {
				logger.error("RunTime Exception:" + ex.getMessage());
				throw new RuntimeException(ex);
			}
		}
	}
	
	/*@SuppressWarnings("unchecked")
	private void handleHttpRequest(String caseInstanceId, Message message,JSONObject requestObj) {
		Hashtable<String, String> methodHeaders = new Hashtable<String, String>();
		JSONParser parser = new JSONParser();
		String authorizationHeader = null;
		String url = (String)requestObj.get("url");
		String method = (String)requestObj.get("method");
		if(method == null) {
			return;
		}
		JSONObject headers = (JSONObject)requestObj.get("headers");
		if(headers == null) {
			return;
		}
		Iterator<String> keys = headers.keySet().iterator();
		JSONObject body = (JSONObject)requestObj.get("body");
		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpRequestBase requestMethod = this.getHttpMethod(url,method);
		try {
			if(requestMethod instanceof HttpEntityEnclosingRequestBase) {
				StringEntity entity = new StringEntity(body.toJSONString());
				((HttpEntityEnclosingRequestBase)requestMethod).setEntity(entity);
			}
			while(keys.hasNext()) {
				String key = keys.next();
				String hvalue = (String)headers.get(key);
				if(key.equals("Authorization")) {
					authorizationHeader = hvalue;
				}
				methodHeaders.put(key, hvalue);
				requestMethod.setHeader(key, hvalue);
			}
			 
	        HttpResponse response = client.execute(requestMethod);
	        int status = response.getStatusLine().getStatusCode();
	        if(status > 400) {
	        	//String reason = response.getStatusLine().getReasonPhrase();
	        	logger.error("Status:" + status + " , for message: " + requestObj.toJSONString());
	        }
	        if(caseInstanceId != null && authorizationHeader != null) {
	        	StringBuffer result = new StringBuffer();
		        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		        String line = "";
		        while ((line = rd.readLine()) != null) {
		            result.append(line);
		        }
		        JSONObject resultObj = (JSONObject)parser.parse(result.toString());
		        this.sendResponse(authorizationHeader,caseInstanceId, status, resultObj);
	        }
	        client.close();
		}catch(IllegalArgumentException ia) {
			String requestStr = new String(message.getBody());
			logger.error(ia.getMessage() + " for message:" + requestStr);
			if(caseInstanceId != null && authorizationHeader != null) {
				try {
					JSONObject errorObj = new JSONObject();
					errorObj.put("error", ia.toString());
					this.sendResponse(authorizationHeader,caseInstanceId, 400, errorObj);	
				}catch(Exception e) {
					JSONObject eventObj = new JSONObject();
					eventObj.put("caseInstanceId", caseInstanceId);
					eventObj.put("message", requestStr);
					eventObj.put("error", e.getMessage());
					logger.error("Unable to update case instance for message:" + requestStr );
					try {
						queueSender.sendMessage(Application.deadRoutingKey, eventObj);	
					}catch(InterruptedException ie) {
						
					}
					
				}
				
			}
			return;
		}
		catch(Exception ex){
			String requestStr = new String(message.getBody());
			logger.error(ex.getMessage());
			if(message.getMessageProperties().getRedelivered() == true) {
				logger.error("Skipping message after retry:" + requestStr);
				if(caseInstanceId != null && authorizationHeader != null) {
					try {
						JSONObject errorObj = new JSONObject();
						errorObj.put("error", ex.toString());
						this.sendResponse(authorizationHeader,caseInstanceId, 400, errorObj);	
					}catch(Exception e) {
						JSONObject eventObj = new JSONObject();
						eventObj.put("caseInstanceId", caseInstanceId);
						eventObj.put("message", requestStr);
						eventObj.put("error", e.getMessage());
						logger.error("Unable to update case instance for message:" + requestStr );
						try {
							queueSender.sendMessage(Application.deadRoutingKey, eventObj);	
						}catch(InterruptedException ie) {
							
						}
					}
				}
				return;
			}else {
				throw new RuntimeException(ex);
			}
		}
	}
	*/
	
	private void handleServiceRequest(Message message, JSONObject requestObj) throws Exception {
        retryTemplate.execute(retryContext -> {
            try {
                executeAndHandleMethod(message, requestObj);
            } catch(Exception ex) {
            	Throwable targetException = ex;
                if (ex instanceof InvocationTargetException) {
                    targetException = ((InvocationTargetException) ex).getCause();
                }
                if (targetException instanceof MongoWriteException && retryContext.getRetryCount() < maxAttempts - 1) {
                	logger.error("Retrying...Handling MongoWriteException");
                     throw (MongoWriteException) targetException;
                } else {
					String requestStr = new String(message.getBody());
					StringWriter sw = new StringWriter();
					ex.printStackTrace(new PrintWriter(sw));
			        String expString = sw.toString();
			        String result = expString.substring(0, Math.min(expString.length(), 800));
					logger.error(result);
					//if(message.getMessageProperties().getRedelivered() == true) { // as these are handling errors ...and throwing error retries..commenting out
					String respComponentName = (String)requestObj.get("responceComponent");
					String respMethodName = (String)requestObj.get("responseMethod");
					
					JSONObject body = (JSONObject)requestObj.get("body");
					String tenantId = (String)body.get("tenantId");
					JSONObject dataObj = (JSONObject)body.get("data");
					String objectId = null;
					if(dataObj != null) {
						objectId = (String)dataObj.get("id");
					}
					logger.error("Error Processing message for:" + objectId + " " + ex.getLocalizedMessage());
					if(respComponentName != null && respMethodName != null) {
						try {
							JSONObject errorObj = new JSONObject();
							errorObj.put("id", objectId);
							errorObj.put("errorCode", ex.toString());
							if(dataObj.containsKey("context") && dataObj.get("context") != null) {
								errorObj.put("context",(String)dataObj.get("context"));
							}
							this.sendResponse(tenantId,respComponentName,respMethodName, 400, errorObj,message.getMessageProperties().getCorrelationId());	
						}catch(Exception e) {
							JSONObject eventObj = new JSONObject();
							eventObj.put("component", respComponentName);
							eventObj.put("methodName", respMethodName);
							eventObj.put("errorCode", e.getMessage());
							logger.error("Unable to update reponse for message queue:" + requestStr );
						}
					}
                }
				//return;
			  } finally {
				  ApplicationTransactionManager.getApplicationTransaction().rollbackTransaction();
			  }
            return null;
        });
    }
	
	@SuppressWarnings("unchecked")
	private void executeAndHandleMethod(Message message,JSONObject requestObj) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InterruptedException {
		String componentName = (String)requestObj.get("component");
		String methodName = (String)requestObj.get("method");
		String respComponentName = (String)requestObj.get("responceComponent");
		String respMethodName = (String)requestObj.get("responseMethod");
		
		JSONObject body = (JSONObject)requestObj.get("body");
		String tenantId = (String)body.get("tenantId");
		JSONObject dataObj = (JSONObject)body.get("data");
		String objectId = null;
		if(dataObj != null) {
			objectId = (String)dataObj.get("id");
		}
		//String authorizationHeader =(String)requestObj.get("Authorization");
			  Object compInst = SnapshotBeanUtil.getBean(Class.forName(componentName));
			  Method method = compInst.getClass().getMethod(methodName, new Class[]{JSONObject.class});
			  
			  String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
 			  if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				  if(StringUtils.hasText(tenantId)) {	
						MongoTenantContext.setTenant(tenantId);
				  }
 			  }
 			  //transaction.threadCount();
 			  ApplicationTransaction transaction = ApplicationTransactionManager.getApplicationTransaction();
 			  transaction.initializeMongoTransaction();	
			  JSONObject ret = (JSONObject)method.invoke(compInst, body);
			  
			  if(respComponentName != null && respMethodName != null) {
				  String correlationId  = message.getMessageProperties().getCorrelationId();
				  //System.out.print("corrleationid received:" + correlationId);
				  logger.info("sending message to "+respComponentName+"for tenantId:"+tenantId+new Date());
				  this.sendResponse(tenantId,respComponentName,respMethodName, 200, ret,correlationId);  
			  }
			  transaction.commitTransaction();
			  //transaction.threadCount();
	}

	/*private UserNamePassword getEmailPasswordFromAuthorization(String authHeader) {
		UserNamePassword up = new UserNamePassword();
		if (authHeader != null && authHeader.indexOf("Basic") >=0 ) {
		    String encodedUsernamePassword = authHeader.substring("Basic ".length()).trim();
		    //Encoding encoding = Encoding.getEncoding("iso-8859-1");
		    String userNamePassword = new String(Base64.getDecoder().decode(encodedUsernamePassword)); 

		    int seperatorIndex = userNamePassword.indexOf(':');

		    up.userName = userNamePassword.substring(0, seperatorIndex);
		    up.password = userNamePassword.substring(seperatorIndex + 1);
		} else {
		    //Handle what happens if that isn't the case
		    throw new RuntimeException("The authorization header is either empty or isn't Basic.");
		}
		return up;
	}*/
	@SuppressWarnings("unchecked")
	private void sendResponse(String organizationId, String component,String methodName,int status,JSONObject response, String corrleationId) throws InterruptedException {
		
		JSONObject reqObj = new JSONObject();
		JSONObject dataObj = new JSONObject();
		reqObj.put("data", dataObj);
		dataObj.put("organizationId", organizationId);
		dataObj.put("component", component);
		dataObj.put("method", methodName);
		dataObj.put("status", status);
		JSONObject bodyObj = new JSONObject();
		bodyObj.put("organizationId", organizationId);
		dataObj.put("body", bodyObj);
		//if(status == 200) {
			bodyObj.put("data", response);
			try {
				queueSender.sendMessage(QueueConfig.readRoutingKey, reqObj,corrleationId);	
			} catch(InterruptedException ie){
				logger.error("Unable to update case instance for message:" + ie.getMessage());
	    		throw ie;
	    	}catch(Exception ie) {
				logger.error("Unable to update case instance for message:" + ie.getMessage());
			}
		/*}else {
			bodyObj.put("data", response);
			try {
				queueSender.sendMessage(QueueConfig.writeRoutingKey, reqObj);
				queueSender.sendMessage(QueueConfig.deadRoutingKey, reqObj);	
			}catch(InterruptedException ie) {
				
			}
		}*/
	}
	
	/*@SuppressWarnings("unchecked")
	private String getBatavJWTToken(UserNamePassword up) throws UnsupportedEncodingException,ClientProtocolException,IOException{
		CloseableHttpClient client = HttpClientBuilder.create().build();
		String url = env.getProperty("caseengine.server.url") + "/identity/login";
		HttpRequestBase requestMethod = this.getHttpMethod(url,"POST");

		JSONObject requestObj = new JSONObject();
		requestObj.put("username", up.userName);
		requestObj.put("password", up.password);
		requestObj.put("context", "BMS");
		if(requestMethod instanceof HttpEntityEnclosingRequestBase) {
			StringEntity entity = new StringEntity(requestObj.toJSONString());
			((HttpEntityEnclosingRequestBase)requestMethod).setEntity(entity);
		}
		requestMethod.setHeader("Content-Type","application/json");
        HttpResponse caseResponse = client.execute(requestMethod);
        //int caseStatus = caseResponse.getStatusLine().getStatusCode();
        Header[] headers = caseResponse.getHeaders("BATAV-JWT-TOKEN");
        return headers[0].getValue();
	}
	private HttpRequestBase getHttpMethod(String url,String method) {
		if(method.equalsIgnoreCase("post")) {
			return new HttpPost(url);
		}else if(method.equalsIgnoreCase("get")) {
			return new HttpGet(url);
		}else if(method.equalsIgnoreCase("put")) {
			return new HttpPut(url);
		}else if(method.equalsIgnoreCase("delete")) {
			return new HttpDelete(url);
		}
		return new HttpPatch(url);
	}*/
	
}
/*class UserNamePassword{
	String userName;
	String password;
};*/
