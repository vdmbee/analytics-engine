package com.vdmbee.calculationengine.asyncevents;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.mongodb.MongoWriteException;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;
import com.vdmbee.server.common.ICalculationEngineFacade;

@Component
public class CalculationEngineImpl implements ICalculationEngineFacade{
	
	@Autowired
	private Environment env;
	
    @Value("${retry.maxAttempts}")
    Integer maxAttempts = 1;
    
    @Autowired
    RetryTemplate retryTemplate;
	
	private static Logger logger = LoggerFactory.getLogger(CalculationEngineImpl.class);
	//private final int nodeId;

    public CalculationEngineImpl() {
    	//logger.info("Node with ID: {} started", nodeId);
       // this.nodeId = nodeId;
    }

	@Override
	public JSONObject handleCalculateEngineRequest(JSONObject request) {
		return retryTemplate.execute(retryContext -> {
            try {
	            JSONObject requestObj = (JSONObject) request.get("data");
	            if (requestObj == null) {
	                return null;
	            }
	            logger.debug("received:" + new Date());
	            
	            String componentName = (String) requestObj.get("component");
	            if (componentName != null) {
	            	JSONObject ret = this.handleServiceRequest(requestObj);                
	                return ret;
	            } else {
	                logger.error("Component Name missing, unsupported Handler for request");
	                throw new Exception("Component Name missing, unsupported Handler for request");
	            }
	        } catch (Exception ex) {
	        	Throwable targetException = ex;
                if (ex instanceof InvocationTargetException) {
                    targetException = ((InvocationTargetException) ex).getCause();
                }
                if (targetException instanceof MongoWriteException && retryContext.getRetryCount() < maxAttempts - 1) {
                	logger.error("Retrying...Handling MongoWriteException");
                     throw (MongoWriteException) targetException;
                } else {
                	//String requestStr = new String(message.getBody());
                    //if(message.getMessageProperties().getRedelivered() == true) { // as these are handling errors ...and throwing error retries..commenting out
                    //logger.error("Error Processing message for:" + objectId + " " + ex.getLocalizedMessage() + " " + ex.getMessage(), ex);
                    //JSONObject body = (JSONObject) request.get("body");
                    //String tenantId = (String) body.get("tenantId");
                    JSONObject dataObj = null;//(JSONObject) body.get("data");
                    if (dataObj == null) {
                        dataObj = new JSONObject();
                    }
                    if (ex.getCause() != null) {
                        logger.error(ex.getCause().getMessage());
                        dataObj.put("error", ex.getCause().getMessage());
                    } else {
                        dataObj.put("error", ex.getMessage());
                    }
		            logger.error(ex.getMessage(), ex);
		            request.put("error", ex.getMessage());
		            return dataObj;
                }
			 } finally {
				ApplicationTransactionManager.getApplicationTransaction().rollbackTransaction();
			 }
		});
    }
	
	
	@SuppressWarnings("unchecked")
	private JSONObject handleServiceRequest(JSONObject requestObj) throws ClassNotFoundException, NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, InterruptedException {
        String componentName = (String) requestObj.get("component");
        String methodName = (String) requestObj.get("method");

        JSONObject body = (JSONObject) requestObj.get("body");
        String tenantId = (String) body.get("tenantId");
        JSONObject dataObj = (JSONObject) body.get("data");
        String objectId = null;
        if (dataObj != null) {
            objectId = (String) dataObj.get("id");
        }
        Object compInst = SnapshotBeanUtil.getBean(Class.forName(componentName));
        Method method = compInst.getClass().getMethod(methodName, new Class[] { JSONObject.class });

        String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
        if (useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
            if (StringUtils.hasText(tenantId)) {
                MongoTenantContext.setTenant(tenantId);
            }
        }
        ApplicationTransaction transaction = ApplicationTransactionManager.getApplicationTransaction();
    	transaction.initializeMongoTransaction();
        JSONObject ret = (JSONObject) method.invoke(compInst, body);
        transaction.commitTransaction();
        return ret;
    }

}
