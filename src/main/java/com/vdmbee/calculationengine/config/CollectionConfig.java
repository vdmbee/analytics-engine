package com.vdmbee.calculationengine.config;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.index.Index;

import com.vdmbee.calculationengine.multitenant.ExtendedMongoTemplate;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;

@Configuration
@DependsOn("mongoTemplate")
public class CollectionConfig {

	/*@Autowired
    private MongoTenantTemplate mongoTemplate;

    @PostConstruct
    public void initIndexes() {
    	mongoTemplate.indexOps("documents") // collection name string or .class
        .ensureIndex(
            new Index().on("refdoc", Sort.Direction.ASC)
        		);
    }
    
    public static void createIndex(ExtendedMongoTemplate mongoTemplate) {
    	mongoTemplate.indexOps("documents") // collection name string or .class
        .ensureIndex(
            new Index().on("refdoc", Sort.Direction.ASC)
        		);
    }*/
}