package com.vdmbee.calculationengine.config;


import java.io.Serializable;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {

    //@Autowired
    //private IAuthenticationFacade authenticationFacade;
    
	@Override
	public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
		// TODO Auto-generated method stub
        if ((authentication == null) || (targetDomainObject == null) || !(permission instanceof String)){
            return false;
        }
        //SecurityContextHolder.getContext().setAuthentication(authentication);
        return hasPrivilege(authentication,null, (String)targetDomainObject, (String)permission);
	}

	@Override
	public boolean hasPermission(Authentication authentication, Serializable targetId, String targetType,
			Object permission) {
		// TODO Auto-generated method stub
        if ((authentication == null) || (targetType == null) || !(permission instanceof String)){
            return false;
        }			
        //SecurityContextHolder.getContext().setAuthentication(authentication);
        return hasPrivilege(authentication,targetId.toString(), targetType,(String)permission);
	}
	private boolean hasPrivilege(Authentication auth,String targetId, String targetType, String permission) {
		//IAuthenticationFacade authenticationFacade = new AuthenticationFacade();
		//return authenticationFacade.hasPrivilege(targetId, targetType, permission);
		return true;
	}
}
