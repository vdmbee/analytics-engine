package com.vdmbee.calculationengine.config;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {
	
	@Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	/*if (username != null || username != "") {
    		List<IUser> users = authenticationFacade.getLocalUsersByEmail(username);
    		if(users.size() < 0) {
    			throw new UsernameNotFoundException(username);
    		}
    	}*/
        return new org.springframework.security.core.userdetails.User(username, createHashCode(username), getAuthorities());
    }
    
	private Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>(1);
		authorities.add(new SimpleGrantedAuthority("ADMIN"));
		return authorities;
	}
	
	private String createHashCode(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}
}