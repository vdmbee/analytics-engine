package com.vdmbee.calculationengine.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryCallback;
import org.springframework.retry.RetryContext;
import org.springframework.retry.listener.RetryListenerSupport;

import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;


public class DefaultListenerSupport extends RetryListenerSupport {

    private static final Logger logger = LoggerFactory.getLogger(DefaultListenerSupport.class);

    @Override
    public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        logger.info("onClose");
        ApplicationTransactionManager.getApplicationTransaction().rollbackTransaction();
        super.close(context, callback, throwable);
    }

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        logger.error(
                String.format("onError in retry handler, retryCount '%s', last error: %s, error: %s", context.getRetryCount(),
                        (context.getLastThrowable() != null ? context.getLastThrowable().getMessage() : ""),
                        (throwable != null ? throwable.getMessage() : "")), throwable);
        ApplicationTransactionManager.getApplicationTransaction().rollbackTransaction();
    }

    @Override
    public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
        logger.info("onOpen");
        return super.open(context, callback);
    }
}
