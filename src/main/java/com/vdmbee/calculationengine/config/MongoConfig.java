package com.vdmbee.calculationengine.config;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.domain.EntityScanner;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.annotation.Persistent;
import org.springframework.data.mapping.model.FieldNamingStrategy;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.google.gson.Gson;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoIterable;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Tag;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.multitenant.SnapshotMongoClientDatabaseFactory;
import com.vdmbee.calculationengine.multitenant.TenantContext;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;


@Configuration
@EnableMongoRepositories(basePackages = {"com.vdmbee"})
public class MongoConfig {
	
    @Autowired
    private ApplicationContext applicationContext;
    
    @Autowired
    MongoProperties properties;
    
    @Value("${spring.data.mongodb.uri}")
    private String connectionStr;
    @Value("${mongodb.connectionpool.maxwaittime}")
    private long maxWaitTime;
    @Value("${mongodb.connectionpool.minsize}")
    private int minSize;
    @Value("${mongodb.connectionpool.maxsize}")
    private int maxSize;
    @Value("${mongodb.connectionpool.connectiontimeout}")
    private int timeout;
    @Value("${mongodb.connectionpool.maxidletime}")
    private long maxIdle;
    @Value("${transaction.lifetime.limit.seconds}")
    private int transactionLifetimeLimitSeconds;
    
    
    public int getTransactionLifetimeLimitSeconds() {
		return transactionLifetimeLimitSeconds;
	}


	public void setTransactionLifetimeLimitSeconds(int transactionLifetimeLimitSeconds) {
		this.transactionLifetimeLimitSeconds = transactionLifetimeLimitSeconds;
	}
	private static final Logger LOGGER = LoggerFactory.getLogger(MongoConfig.class);

    @Bean(name = "mongoTransactionManager")
    MongoTransactionManager mongoTransactionManager(MongoDatabaseFactory dbFactory) {
    	TransactionOptions transactionOptions = TransactionOptions.builder()
    			 .readPreference(ReadPreference.primary())
    			 .readConcern(ReadConcern.LOCAL)
    			 .writeConcern(WriteConcern.MAJORITY).maxCommitTime(3L,TimeUnit.MINUTES).build();
        return new MongoTransactionManager(dbFactory, transactionOptions);
    }

    
	@Bean
    public MappingMongoConverter getDefaultMongoConverter(MongoDatabaseFactory mongoDbFactory) throws ClassNotFoundException{
    	//MongoMappingContext mappingContext = new MongoMappingContext();
		MongoMappingContext mappingContext = mongoMappingContext();
    	DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
        MappingMongoConverter converter = new MappingMongoConverter(dbRefResolver, mappingContext);
        MongoCustomConversions conversions =customConversions();
        mappingContext.setSimpleTypeHolder(conversions.getSimpleTypeHolder());
        converter.setCustomConversions(conversions);
        //mappingContext.setAutoIndexCreation(false);TODO
        return converter;
    }
	
	public MongoClientSettings getMongoClientSettings(ConnectionString connectionString) {
		return MongoClientSettings.builder()
		        .applyConnectionString(connectionString)
		        .codecRegistry(getCodecRegistry())
		        .applyToConnectionPoolSettings(connPoolBuilder ->
		                connPoolBuilder
		                        .maxWaitTime(maxWaitTime, TimeUnit.MILLISECONDS)
		                        .minSize(minSize)
		                        .maxConnectionIdleTime(maxIdle, TimeUnit.MILLISECONDS)
		                        .maxConnectionLifeTime(10, TimeUnit.MINUTES)
		                        .maxSize(maxSize))
		        .applyToSocketSettings(socketBuilder ->
		                socketBuilder
		                        .connectTimeout(timeout, TimeUnit.MILLISECONDS))
		        .applyToClusterSettings(clusterBuilder ->
		                clusterBuilder
		                        .applyConnectionString(connectionString))
		        .build();
    }
	
	public CodecRegistry getCodecRegistry() {
        return CodecRegistries.fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
        		CodecRegistries.fromProviders(PojoCodecProvider.builder().automatic(true).build()));
    }

	
    @Bean
    public MongoTenantTemplate mongoTenantTemplate() throws Exception {
    	//MongoDatabaseFactory dbFactory;
    	//dbFactory = SnapshotBeanUtil.getBean(MongoDatabaseFactory.class);
    	String dataBase = TenantContext.getCurrentTenant();
    	if(dataBase == null) {
    		dataBase = "snapshot";
    	}
    	ConnectionString connectionString = new ConnectionString(properties.getUri());
    	
    	MongoClient mc = MongoClients.create(this.getMongoClientSettings(connectionString));
        
		/*try {			
			MongoIterable<String> dbNames = mc.listDatabaseNames();
			boolean emptyDatabase = true;
			for (String name : dbNames) {
				if(name.equalsIgnoreCase("admin")) {
					emptyDatabase = false;
					break;
				}
			}
			if(!emptyDatabase) {
				MongoDatabase adminDatabase = mc.getDatabase("admin");
				Map<String, Object> doc = new HashMap<String, Object>();
				doc.put("setParameter", 1);
				doc.put("transactionLifetimeLimitSeconds", transactionLifetimeLimitSeconds);
				Bson command = new org.bson.Document(doc);
				adminDatabase.runCommand(command);
			}
		}catch(Exception e) {
			LOGGER.error("Error while running transactionLifetimeLimitSeconds : " + e.getMessage());
			e.printStackTrace();
		}*/
		
    	SnapshotMongoClientDatabaseFactory dbFactory=new SnapshotMongoClientDatabaseFactory(mc,dataBase);
    	//dbFactory = new SnapshotMongoClientDatabaseFactory(mongoClient,dataBase);
    	MongoTenantTemplate mongoTemplate = new MongoTenantTemplate(dbFactory);
        return mongoTemplate;
    }
	
    /*@Bean(name = "mongoTransactionManager")
    MongoTransactionManager mongoTransactionManager(MongoDatabaseFactory dbFactory) {
        TransactionOptions transactionOptions = TransactionOptions.builder().readConcern(ReadConcern.LOCAL).writeConcern(WriteConcern.W1).build();
        return new MongoTransactionManager(dbFactory, transactionOptions);
    }*/
    
	@Bean
	@ConditionalOnMissingBean
	public MongoMappingContext mongoMappingContext() throws ClassNotFoundException {
		PropertyMapper mapper = PropertyMapper.get().alwaysApplyingWhenNonNull();
		MongoMappingContext context = new MongoMappingContext();
		context.setInitialEntitySet(new EntityScanner(applicationContext).scan(Document.class, Persistent.class));
		Class<?> strategyClass = properties.getFieldNamingStrategy();
		if (strategyClass != null) {
			context.setFieldNamingStrategy((FieldNamingStrategy) BeanUtils.instantiateClass(strategyClass));
		}
		//context.setSimpleTypeHolder(conversions.getSimpleTypeHolder());
		return context;
	}
	
    @Bean
    public MongoCustomConversions customConversions() {
        List<Converter<?, ?>> converterList = new ArrayList<Converter<?, ?>>();
        converterList.add(new MongoSnapshotToStringConverter());
        converterList.add(new MongoTemplateToStringConverter());
        converterList.add(new MongoTagToStringConverter());
        converterList.add(new LinkedHashMapToStringConverter());
        converterList.add(new MongoDocumentsToStringConverter());
        //converterList.add(new ArrayListToStringConverter());
        return new MongoCustomConversions(converterList);
    }
    /*@Autowired
    void conversionService(GenericConversionService genericConversionService) {
    	genericConversionService.addConverter(new MongoSnapshotToStringConverter());
    	genericConversionService.addConverter(new MongoTemplateToStringConverter());
    	genericConversionService.addConverter(new MongoTagToStringConverter());
    	genericConversionService.addConverter(new LinkedHashMapToStringConverter());
    	genericConversionService.addConverter(new ArrayListToStringConverter());
    }*/
    private static final class LinkedHashMapToStringConverter implements Converter<LinkedHashMap,String> {
		@Override
		public String convert(LinkedHashMap source) {
			return JSONObject.toJSONString(source) ;
		}
    }
    private static final class ArrayListToStringConverter implements Converter<ArrayList,String> {
		@Override
		public String convert(ArrayList source) {
			return new Gson().toJson(source);
		}
    }

    private static final class MongoTagToStringConverter implements Converter<Tag,String> {
		@Override
		public String convert(Tag source) {
			return source.toString();
		}
    }
    private static final class MongoSnapshotToStringConverter implements Converter<Snapshot,String> {
		@Override
		public String convert(Snapshot source) {
			return source.toString();
		}
    }
    private static final class MongoTemplateToStringConverter implements Converter<Template,String> {
		@Override
		public String convert(Template source) {
			return source.toString();
		}
    }
    private static final class MongoDocumentsToStringConverter implements Converter<Documents,String> {
		@Override
		public String convert(Documents source) {
			return source.toString();
		}
    }
   /* @Autowired
    private MongoTenantTemplate mongoTenantTemplate;
    @Autowired
    private MongoConverter mongoConverter;
    
    @EventListener(ApplicationReadyEvent.class)
    public void initIndicesAfterStartup() {//TODO

        //log.info("Mongo InitIndicesAfterStartup init");
        long init = System.currentTimeMillis();

        MongoMappingContext mappingContext = (MongoMappingContext) this.mongoConverter.getMappingContext();

        if (mappingContext instanceof MongoMappingContext) {
            MongoMappingContext mongoMappingContext = (MongoMappingContext) mappingContext;
            for (BasicMongoPersistentEntity<?> persistentEntity : mongoMappingContext.getPersistentEntities()) {
                Class clazz = persistentEntity.getType();
                if (clazz.isAnnotationPresent(Document.class)) {
                	MongoPersistentEntityIndexResolver resolver = new MongoPersistentEntityIndexResolver(mongoMappingContext);

                    IndexOperations indexOps = mongoTenantTemplate.indexOps(clazz);
                    ClassTypeInformation<?> info;
                    resolver.resolveIndexFor(ClassTypeInformation.from(clazz)).forEach(indexOps::ensureIndex);
                }
            }
        }

        //log.info("Mongo InitIndicesAfterStartup take: {}", (System.currentTimeMillis() - init));
    }*/
    /*@Bean
    public MongoClient mongoClient() {
        ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017/test");
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
          .applyConnectionString(connectionString)
          .build();
        
        return MongoClients.create(mongoClientSettings);
    }*/
}
