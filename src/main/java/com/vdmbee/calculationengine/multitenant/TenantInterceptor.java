package com.vdmbee.calculationengine.multitenant;

import static java.lang.String.format;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.vdmbee.calculationengine.util.SnapshotBeanUtil;

@Component
public class TenantInterceptor implements HandlerInterceptor {
	//private final String TENANT_REQ_PARAM = "tenantId";
	
	private static Logger log = LogManager.getLogger(TenantInterceptor.class);
	@Autowired
	private Environment env;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) 
			throws Exception {
		if(env == null) {
			env = SnapshotBeanUtil.getBean(Environment.class);	
		}
		String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
		if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
			String tenantId = checkTenantParameter(request);
			//log.info(format("[!] Setting TeantContext to '%s'!", tenantId));
	
			if(StringUtils.hasText(tenantId)) {	
				MongoTenantContext.setTenant(tenantId);
			}
		}

		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {
		String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
		if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
	
			TenantContext.clear();
			MongoTenantContext.clear();
			//UserContext.clear();
		}
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		// your code
	}

	private String checkTenantParameter(HttpServletRequest request) {
		if(env == null) {
			env = SnapshotBeanUtil.getBean(Environment.class);	
		}
		String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
		if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
			Enumeration<?> e = request.getParameterNames();
			if (e != null) {
				while (e.hasMoreElements()) {
					String curr = (String) e.nextElement();
					if (curr.contains("tenantId")) {
						return request.getParameter("tenantId");
					}
				}
			}
		}
		return null;
	}
}
