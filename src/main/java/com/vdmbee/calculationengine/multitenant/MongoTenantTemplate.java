package com.vdmbee.calculationengine.multitenant;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.data.geo.GeoResults;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.BulkOperations.BulkMode;
import org.springframework.data.mongodb.core.CollectionCallback;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.CursorPreparer;
import org.springframework.data.mongodb.core.DbCallback;
import org.springframework.data.mongodb.core.DocumentCallbackHandler;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoAction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.MongoWriter;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.IndexOperations;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;
import org.springframework.data.mongodb.core.mapreduce.MapReduceOptions;
import org.springframework.data.mongodb.core.mapreduce.MapReduceResults;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.NearQuery;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.UpdateDefinition;
import org.springframework.data.util.CloseableIterator;
import org.springframework.lang.Nullable;
import org.springframework.util.StringUtils;

import com.mongodb.ConnectionString;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.CountOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;
import com.vdmbee.calculationengine.config.MongoConfig;

@Configuration
@ConfigurationProperties(prefix="spring.data.mongodb")
public class MongoTenantTemplate extends MongoTemplate {
	private static Map<String, ExtendedMongoTemplate> tenantTemplates = new HashMap<String, ExtendedMongoTemplate>();
	private static Map<String, Date> tenantLastAccess = new HashMap<String, Date>();

	@Autowired
	public MongoDBCredentials mongoDBCredentials;
	
	@Value("${useAzure}")
	private String useAzure;
	
	@Value("${offerThroughput}")
	private String offerThroughput;
	
	@Value("${maxThroughput}")
	private String maxThroughput;
	
	@Autowired
	public MongoConfig MongoConfig;
	
	@Autowired
	private MongoConverter mongoConverter;
	
	private static Logger logger = LogManager.getLogger(MongoTenantTemplate.class);

	public MongoTenantTemplate(MongoDatabaseFactory mongoDbFactory) {
		super(mongoDbFactory);
		tenantTemplates.put(mongoDbFactory.getMongoDatabase().getName(), new ExtendedMongoTemplate(mongoDbFactory));
	}
	
	public MongoTenantTemplate(MongoDatabaseFactory mongoDbFactory,MongoConverter mongoConverter) {
		super(mongoDbFactory,mongoConverter);
		tenantTemplates.put(mongoDbFactory.getMongoDatabase().getName(), new ExtendedMongoTemplate(mongoDbFactory,mongoConverter));
	}
    
	public ExtendedMongoTemplate getTenantMongoTemplate(String tenant) {
		ExtendedMongoTemplate mongoTemplate = tenantTemplates.get(tenant);
		if (mongoTemplate == null) {
			String queryString = mongoDBCredentials.getUri().substring(mongoDBCredentials.getUri().indexOf("?"));
			String mongoUrl=mongoDBCredentials.getPath()+"/"+tenant+ queryString;
			//String mongoUrl=mongoDBCredentials.getPath()+"/"+tenant+"?authSource="+mongoDBCredentials.getDefaultDatabaseName()+"&ssl=true";//;
			//System.out.println("setting new tenant "+mongoUrl);
			logger.info("----setting new tenant----- "+mongoUrl);
			/*SimpleMongoClientDatabaseFactory mongoDbFactory = new SimpleMongoClientDatabaseFactory(
					MongoClients.create(mongoUrl), tenant);
			mongoTemplate = new ExtendedMongoTemplate(mongoDbFactory);
			tenantTemplates.put(tenant, mongoTemplate);*/
			mongoTemplate = getTemplateAndCreateDB(mongoUrl,tenant);
		}
		tenantLastAccess.put(tenant, new Date());
		return mongoTemplate;
	}

	public ExtendedMongoTemplate getTemplateAndCreateDB(String url,String tenant) {
		ConnectionString connectionString = new ConnectionString(url);
		SnapshotMongoClientDatabaseFactory mongoDbFactory=new SnapshotMongoClientDatabaseFactory(MongoClients.create(MongoConfig.getMongoClientSettings(connectionString)),
				tenant);
		ExtendedMongoTemplate mongoTemplate = new ExtendedMongoTemplate(mongoDbFactory,mongoConverter);
		//ExtendedMongoTemplate mongoTemplate = new ExtendedMongoTemplate(mongoDbFactory);
		if(useAzure != null && useAzure.equalsIgnoreCase("true") && tenant != null && !tenant.equalsIgnoreCase("Snapshot")) {
			logger.error("Using azure Cosmo Database Creation");
			createAzureDatabase(url,tenant);
		}
		tenantTemplates.put(tenant, mongoTemplate);
		MongoTenantContext.setTenant(tenant);
		//this.createIndex(mongoTemplate);
		return mongoTemplate;
	}
	
    /*public boolean isIndexExists(String collectionName, String indexName,ExtendedMongoTemplate mongoTemplate) {
    	return mongoTemplate.indexOps(collectionName).getIndexInfo().stream()
                .map(IndexInfo::getName)
                .anyMatch(name -> name.equals(indexName));
    }
    
    public void createIndex(ExtendedMongoTemplate mongoTemplate) {
    	String collectionName = "documents";
		String fieldName = "refdoc";
    	if(mongoTemplate.collectionExists(collectionName) && !isIndexExists(collectionName,fieldName,mongoTemplate)) {
    		this.ensureIndex(collectionName, fieldName, mongoTemplate);
    	}
    }*/
    
    public void ensureIndex(String collectionName, String fieldName,ExtendedMongoTemplate mongoTemplate) {
        IndexOperations indexOps = mongoTemplate.indexOps(collectionName);
        indexOps.ensureIndex(new Index().on(fieldName, Sort.Direction.ASC));
    }
	
	public void createAzureDatabase(String uriStr,String tenant) { 
		MongoClient mongoClient = null;
		try {
			//uriStr = "mongodb://vmpadmin-we-tst:wGin750IWay5dnCCKqDwl9mZIu5WZSrp41ENVk31qbwKpeWfzL5etbCF9lnJTbvsI52Gcsddr5ekS1PuYwDAng==@vmpadmin-we-tst.mongo.cosmos.azure.com:10255/vmpAdmin?ssl=true&replicaSet=globaldb&retrywrites=false&maxIdleTimeMS=120000&appName=@vmpadmin-we-tst@";
			mongoClient = MongoClients.create(uriStr);

			MongoDatabase mydb = mongoClient.getDatabase(tenant);

			String dbExists = mydb.listCollectionNames().first();
			if (StringUtils.isEmpty(dbExists)) {
				Map<String, Object> doc = new HashMap<String, Object>();
				doc.put("customAction", "CreateDatabase");// UpdateCollection
				doc.put("offerThroughput", Integer.parseInt(offerThroughput));
				Bson command = new Document(doc);
				mydb.runCommand(command);
		        
				if(!StringUtils.isEmpty(maxThroughput)) {
					Bson snapshotCollectionCommand = createCollection(Integer.parseInt(maxThroughput),"snapshot");
			        mydb.runCommand(snapshotCollectionCommand);
			        Bson templateCollectionCommand = createCollection(Integer.parseInt(maxThroughput),"template");
			        mydb.runCommand(templateCollectionCommand);
				}
			}
	        
		}catch(Exception e) {
        	logger.error(e.getMessage().toString());
        } finally {
        	if (mongoClient != null) {
        		mongoClient.close();
        	}
        }
	}
	
	private Bson createCollection(Integer maxThroughput,String collectionName) {
		Map<String, Object> docCollection = new HashMap<String, Object>();
        docCollection.put("customAction", "CreateCollection");
        docCollection.put("collection", collectionName);
        docCollection.put("shardKey", "_id");
        docCollection.put("autoScaleSettings", new Document("maxThroughput", maxThroughput));
        Bson collectionCommand = new Document(docCollection);
        return collectionCommand;
	}

	public static void addTemplate(String tenant,ExtendedMongoTemplate mongoTemplate) {
		tenantTemplates.put(tenant, mongoTemplate);
	}
	@Override
	public String getCollectionName(Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).getCollectionName(entityClass);
	}

	@Override
	public Document executeCommand(String jsonCommand) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).executeCommand(jsonCommand);
	}

	/*@Override
	public CommandResult executeCommand(DBObject command) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).executeCommand(command);
	}

	@Override
	@SuppressWarnings("deprecation")
	public CommandResult executeCommand(DBObject command, int options) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).execute(command, options);
	}

	@Override
	public CommandResult executeCommand(DBObject command, ReadPreference readPreference) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).executeCommand(command, readPreference);
	}*/

	@Override
	public void executeQuery(Query query, String collectionName, DocumentCallbackHandler dch) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).executeQuery(query, collectionName, dch);

	}

	@Override
	public <T> T execute(DbCallback<T> action) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).execute(action);
	}

	@Override
	public <T> T execute(Class<?> entityClass, CollectionCallback<T> action) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).execute(entityClass, action);
	}

	@Override
	public <T> T execute(String collectionName, CollectionCallback<T> action) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).execute(collectionName, action);
	}

	/*@SuppressWarnings("deprecation")
	@Override
	public <T> T executeInSession(DbCallback<T> action) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).executeInSession(action);
	}*/

	@Override
	public <T> CloseableIterator<T> stream(Query query, Class<T> entityType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).stream(query, entityType);
	}

	@Override
	public <T> MongoCollection<Document> createCollection(Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).createCollection(entityClass);
	}

	@Override
	public <T> MongoCollection<Document> createCollection(Class<T> entityClass, CollectionOptions collectionOptions) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).createCollection(entityClass, collectionOptions);
	}

	@Override
	public MongoCollection<Document> createCollection(String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).createCollection(collectionName);
	}

	@Override
	public MongoCollection<Document> createCollection(String collectionName, CollectionOptions collectionOptions) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).createCollection(collectionName, collectionOptions);
	}

	@Override
	public Set<String> getCollectionNames() {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).getCollectionNames();
	}

	@Override
	public MongoCollection<Document> getCollection(String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).getCollection(collectionName);
	}

	@Override
	public <T> boolean collectionExists(Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).collectionExists(entityClass);
	}

	@Override
	public boolean collectionExists(String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).collectionExists(collectionName);
	}

	@Override
	public <T> void dropCollection(Class<T> entityClass) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).dropCollection(entityClass);
	}

	@Override
	public void dropCollection(String collectionName) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).dropCollection(collectionName);

	}

	@Override
	public IndexOperations indexOps(String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).indexOps(collectionName);
	}

	@Override
	public IndexOperations indexOps(Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).indexOps(entityClass);
	}

	/*@Override
	public ScriptOperations scriptOps() {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).scriptOps();
	}*/

	@Override
	public BulkOperations bulkOps(BulkMode mode, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).bulkOps(mode, collectionName);
	}

	@Override
	public BulkOperations bulkOps(BulkMode mode, Class<?> entityType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).bulkOps(mode, entityType);
	}

	@Override
	public BulkOperations bulkOps(BulkMode mode, Class<?> entityType, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).bulkOps(mode, entityType, collectionName);
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAll(entityClass);
	}

	@Override
	public <T> List<T> findAll(Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAll(entityClass, collectionName);
	}

	/*@Override
	public <T> GroupByResults<T> group(String inputCollectionName, GroupBy groupBy, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).group(inputCollectionName, groupBy, entityClass);
	}

	@Override
	public <T> GroupByResults<T> group(Criteria criteria, String inputCollectionName, GroupBy groupBy,
			Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).group(criteria, inputCollectionName, groupBy,
				entityClass);
	}*/

	@Override
	public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, String collectionName,
			Class<O> outputType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).aggregate(aggregation, outputType);
	}

	@Override
	public <O> AggregationResults<O> aggregate(TypedAggregation<?> aggregation, Class<O> outputType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).aggregate(aggregation, outputType);
	}

	@Override
	public <O> AggregationResults<O> aggregate(Aggregation aggregation, Class<?> inputType, Class<O> outputType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).aggregate(aggregation, inputType, outputType);
	}

	@Override
	public <O> AggregationResults<O> aggregate(Aggregation aggregation, String collectionName, Class<O> outputType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).aggregate(aggregation, collectionName, outputType);
	}

	@Override
	public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
			Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).mapReduce(inputCollectionName, mapFunction,
				reduceFunction, entityClass);
	}

	@Override
	public <T> MapReduceResults<T> mapReduce(String inputCollectionName, String mapFunction, String reduceFunction,
			MapReduceOptions mapReduceOptions, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).mapReduce(inputCollectionName, mapFunction,
				reduceFunction, mapReduceOptions, entityClass);
	}

	@Override
	public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
			String reduceFunction, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).mapReduce(query, inputCollectionName, mapFunction,
				reduceFunction, entityClass);
	}

	@Override
	public <T> MapReduceResults<T> mapReduce(Query query, String inputCollectionName, String mapFunction,
			String reduceFunction, MapReduceOptions mapReduceOptions, Class<T> entityClass) {
		return mapReduce(query, inputCollectionName, mapFunction, reduceFunction, mapReduceOptions, entityClass);
	}

	@Override
	public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).geoNear(near, entityClass);
	}

	@Override
	public <T> GeoResults<T> geoNear(NearQuery near, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).geoNear(near, entityClass, collectionName);
	}

	@Override
	public <T> T findOne(Query query, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findOne(query, entityClass);
	}

	@Override
	public <T> T findOne(Query query, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findOne(query, entityClass, collectionName);
	}

	@Override
	public boolean exists(Query query, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).exists(query, collectionName);
	}

	@Override
	public boolean exists(Query query, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).exists(query, entityClass);
	}

	@Override
	public boolean exists(Query query, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).exists(query, entityClass, collectionName);
	}

	@Override
	public <T> List<T> find(Query query, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).find(query, entityClass);
	}

	@Override
	public <T> List<T> find(Query query, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).find(query, entityClass, collectionName);
	}

	@Override
	public <T> T findById(Object id, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findById(id, entityClass);
	}

	@Override
	public <T> T findById(Object id, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findById(id, entityClass, collectionName);
	}

	@Override
	public <T> T findAndModify(Query query, UpdateDefinition update, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndModify(query, update, entityClass);
	}

	@Override
	public <T> T findAndModify(Query query, UpdateDefinition update, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndModify(query, update, entityClass,
				collectionName);
	}

	@Override
	public <T> T findAndModify(Query query, UpdateDefinition update, FindAndModifyOptions options, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndModify(query, update, options, entityClass);
	}

	@Override
	public <T> T findAndModify(Query query, UpdateDefinition update, FindAndModifyOptions options, Class<T> entityClass,
			String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndModify(query, update, options, entityClass,
				collectionName);
	}

	@Override
	public <T> T findAndRemove(Query query, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndRemove(query, entityClass);
	}

	@Override
	public <T> T findAndRemove(Query query, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAndRemove(query, entityClass, collectionName);
	}

	@Override
	public long count(Query query, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).count(query, entityClass);
	}

	@Override
	public long count(Query query, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).count(query, collectionName);
	}

	@Override
	public long count(Query query, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).count(query, entityClass, collectionName);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object insert(Object objectToSave) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).insert(objectToSave);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object insert(Object objectToSave, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).insert(objectToSave, collectionName);
	}

/*	@Override
	public void insert(Collection<? extends Object> batchToSave, Class<?> entityClass) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).insert(batchToSave, entityClass);
	}

	@Override
	public void insert(Collection<? extends Object> batchToSave, String collectionName) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).insert(batchToSave, collectionName);
	}

	@Override
	public Object insertAll(Collection<? extends Object> objectsToSave) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).insertAll(objectsToSave);
	}
*/
	@SuppressWarnings("unchecked")
	@Override
	public Object save(Object objectToSave) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).save(objectToSave);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object save(Object objectToSave, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).save(objectToSave, collectionName);
	}

	@Override
	public UpdateResult upsert(Query query, UpdateDefinition update, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).upsert(query, update, entityClass);
	}

	@Override
	public UpdateResult upsert(Query query, UpdateDefinition update, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).upsert(query, update, collectionName);
	}

	@Override
	public UpdateResult upsert(Query query, UpdateDefinition update, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).upsert(query, update, entityClass, collectionName);
	}

	@Override
	public UpdateResult updateFirst(Query query, UpdateDefinition update, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateFirst(query, update, entityClass);
	}

	@Override
	public UpdateResult updateFirst(Query query, UpdateDefinition update, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateFirst(query, update, collectionName);
	}

	@Override
	public UpdateResult updateFirst(Query query, UpdateDefinition update, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateFirst(query, update, entityClass,
				collectionName);
	}

	@Override
	public UpdateResult updateMulti(Query query, UpdateDefinition update, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateMulti(query, update, entityClass);
	}

	@Override
	public UpdateResult updateMulti(Query query, UpdateDefinition update, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateMulti(query, update, collectionName);
	}

	@Override
	public UpdateResult updateMulti(Query query, UpdateDefinition update, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).updateMulti(query, update, entityClass,
				collectionName);
	}

	@Override
	public DeleteResult remove(Object object) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).remove(object);
	}

	@Override
	public DeleteResult remove(Object object, String collection) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).remove(object, collection);
	}

	@Override
	public DeleteResult remove(Query query, Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).remove(query, entityClass);
	}

	@Override
	public DeleteResult remove(Query query, Class<?> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).remove(query, entityClass, collectionName);
	}

	@Override
	public DeleteResult remove(Query query, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).remove(query, collectionName);
	}

	@Override
	public <T> List<T> findAllAndRemove(Query query, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAllAndRemove(query, collectionName);
	}

	@Override
	public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAllAndRemove(query, entityClass);
	}

	@Override
	public <T> List<T> findAllAndRemove(Query query, Class<T> entityClass, String collectionName) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).findAllAndRemove(query, entityClass, collectionName);
	}

	@Override
	public MongoConverter getConverter() {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).getConverter();
	}

	@Override
	public MongoDatabase getDb() {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).getDb();
	}

	@Override
	protected MongoDatabase doGetDatabase() {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoGetDatabase();
	}
	
	@Override
	protected <T> CloseableIterator<T> doStream(Query query, final Class<?> entityType, String collectionName,
			Class<T> returnType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoStream(query, entityType,collectionName,returnType);
	}
	@Override
	protected void executeQuery(Query query, String collectionName, DocumentCallbackHandler documentCallbackHandler,
			@Nullable CursorPreparer preparer) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedExecuteQuery(query, collectionName, documentCallbackHandler, preparer);
	}
	
	@Override
	protected long doCount(String collectionName, Document filter, CountOptions options) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoCount(collectionName, filter, options);
	}
	
	@Override
	protected void ensureNotIterable(@Nullable Object o) {
		getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedEnsureNotIterable(o);
	}
	
	@Override
	protected MongoCollection<Document> prepareCollection(MongoCollection<Document> collection) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedPrepareCollection(collection);
	}
	@Override
	protected WriteConcern prepareWriteConcern(MongoAction mongoAction) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedPrepareWriteConcern(mongoAction);
	}
	@Override
	protected <T> T doInsert(String collectionName, T objectToSave, MongoWriter<T> writer) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoInsert(collectionName, objectToSave, writer);
	}
	@Override
	protected <T> Collection<T> doInsertAll(Collection<? extends T> listToSave, MongoWriter<T> writer) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoInsertAll(listToSave, writer);
	}
	@Override
	protected <T> Collection<T> doInsertBatch(String collectionName, Collection<? extends T> batchToSave,
			MongoWriter<T> writer) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoInsertBatch(collectionName, batchToSave, writer);
	}
	
	@Override
	protected <T> T doSave(String collectionName, T objectToSave, MongoWriter<T> writer) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoSave(collectionName, objectToSave, writer);
	}
	
	@Override
	protected Object insertDocument(final String collectionName, final Document document, final Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedInsertDocument(collectionName, document, entityClass);
	}
	@Override
	protected List<Object> insertDocumentList(final String collectionName, final List<Document> documents) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedInsertDocumentList(collectionName, documents);
	}
	@Override
	protected Object saveDocument(final String collectionName, final Document dbDoc, final Class<?> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedSaveDocument(collectionName, dbDoc, entityClass);
	}
	@Override
	protected UpdateResult doUpdate(final String collectionName, final Query query, final UpdateDefinition update,
			@Nullable final Class<?> entityClass, final boolean upsert, final boolean multi) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoUpdate(collectionName, query, update, entityClass, upsert, multi);
	}
	@Override
	protected <T> DeleteResult doRemove(final String collectionName, final Query query,
			@Nullable final Class<T> entityClass, boolean multi) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoRemove(collectionName, query, entityClass, multi);
	}
	@Override
	protected <T> List<T> doFindAndDelete(String collectionName, Query query, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFindAndDelete(collectionName, query, entityClass);
	}
	@Override
	protected <O> AggregationResults<O> aggregate(Aggregation aggregation, String collectionName, Class<O> outputType,
			@Nullable AggregationOperationContext context) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedAggregate(aggregation, collectionName, outputType, context);
	}
	@Override
	protected <O> AggregationResults<O> doAggregate(Aggregation aggregation, String collectionName, Class<O> outputType,
			AggregationOperationContext context) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoAggregate(aggregation, collectionName, outputType, context);
	}
	@Override
	protected <O> CloseableIterator<O> aggregateStream(Aggregation aggregation, String collectionName,
			Class<O> outputType, @Nullable AggregationOperationContext context) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedAggregateStream(aggregation, collectionName, outputType, context);
	}
	@Override
	protected String replaceWithResourceIfNecessary(String function) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedReplaceWithResourceIfNecessary(function);
	}
	@Override
	protected MongoDatabase prepareDatabase(MongoDatabase database) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedPrepareDatabase(database);
	}
	@Override
	protected <E extends MongoMappingEvent<T>, T> E maybeEmitEvent(E event) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedMaybeEmitEvent(event);
	}
	@Override
	protected MongoCollection<Document> doCreateCollection(final String collectionName,
			final Document collectionOptions) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoCreateCollection(collectionName, collectionOptions);
	}
	@Override
	protected <T> T doFindOne(String collectionName, Document query, Document fields, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFindOne(collectionName, query, fields, entityClass);
	}
	@Override
	protected <T> List<T> doFind(String collectionName, Document query, Document fields, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFind(collectionName, query, fields, entityClass);
	}
	@Override
	protected <T> List<T> doFind(String collectionName, Document query, Document fields, Class<T> entityClass,
			CursorPreparer preparer) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFind(collectionName, query, fields, entityClass, preparer);
	}
	@Override
	protected Document convertToDocument(@Nullable CollectionOptions collectionOptions, Class<?> targetType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedConvertToDocument(collectionOptions, targetType);
	}
	@Override
	protected Document convertToDocument(@Nullable CollectionOptions collectionOptions) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedConvertToDocument(collectionOptions);
	}
	@Override
	protected <T> T doFindAndRemove(String collectionName, Document query, Document fields, Document sort,
			@Nullable Collation collation, Class<T> entityClass) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFindAndRemove(collectionName, query, fields, sort, collation, entityClass);
	}
	@Override
	protected <T> T doFindAndModify(String collectionName, Document query, Document fields, Document sort,
			Class<T> entityClass, UpdateDefinition update, @Nullable FindAndModifyOptions options) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFindAndModify(collectionName, query, fields, sort, entityClass, update, options);
	}
	@Override
	protected <T> T doFindAndReplace(String collectionName, Document mappedQuery, Document mappedFields,
			Document mappedSort, @Nullable com.mongodb.client.model.Collation collation, Class<?> entityType,
			Document replacement, FindAndReplaceOptions options, Class<T> resultType) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedDoFindAndReplace(collectionName, mappedQuery, mappedFields, mappedSort, collation, entityType, replacement, options, resultType);
	}
	@Override
	protected <T> T populateIdIfNecessary(T savedObject, Object id) {
		return getTenantMongoTemplate(MongoTenantContext.getTenant()).extendedPopulateIdIfNecessary(savedObject, id);
	}
	
	public static void checkTenants(long databaseCleaningInterval) throws Exception {
		//System.out.println("Cleaning the tenant template that was accessed before 30min.");
		Set<String> tenantKeys = new HashSet<>(tenantTemplates.keySet());
		long currentTime = System.currentTimeMillis();
		//long thirtyMinutesInMillis = 30 * 60 * 1000;
		logger.error("Cleaning the tenant template that was accessed before "+databaseCleaningInterval+"millisec");
		for (String tenant : tenantKeys) {
	        Date lastAccess = tenantLastAccess.get(tenant);
	        if (lastAccess != null && (currentTime - lastAccess.getTime()) > databaseCleaningInterval) {
	        	ExtendedMongoTemplate extTemplate = tenantTemplates.get(tenant);
	        	logger.error("Cleaning the tenant "+tenant);
	        	if (extTemplate != null) {
	                MongoDatabaseFactory mongoFactory = extTemplate.getMongoDatabaseFactory();
	                SnapshotMongoClientDatabaseFactory mongoDbFactory = (SnapshotMongoClientDatabaseFactory) mongoFactory;
	                mongoDbFactory.closeClient();
	                extTemplate = null;
	            }
	            tenantTemplates.remove(tenant);
	            tenantLastAccess.remove(tenant);
	        }
		}
	}
}
