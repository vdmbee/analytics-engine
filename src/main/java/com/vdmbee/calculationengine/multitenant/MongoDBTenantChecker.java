package com.vdmbee.calculationengine.multitenant;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class MongoDBTenantChecker {

	private static Logger logger = LogManager.getLogger(MongoDBTenantChecker.class);
	
    @Value("${database.cleaning.interval}")
    private long databaseCleaningInterval;
    
	@Scheduled(fixedRate = 30*60*1000)
    public void performTask() throws Exception {
		logger.error("Performing MongoDB tenent checker task");
		/*long databaseCleaningIntervalLong = 1800000;
		if(NumberUtils.isDigits(databaseCleaningInterval)) {
			databaseCleaningIntervalLong = Long.parseLong(databaseCleaningInterval);
    	}*/
		MongoTenantTemplate.checkTenants(databaseCleaningInterval);
    }
}
