package com.vdmbee.calculationengine.multitenant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.stereotype.Component;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClients;
import com.vdmbee.calculationengine.config.MongoConfig;


@Component
public class LoadTenantDataSource {

	//private static Logger logger = LoggerFactory.getLogger(LoadTenantDataSource.class);

	@Autowired
	public MongoDBCredentials mongoDBCredentials;
	
	@Autowired
	public MongoConfig MongoConfig;
	
	@Primary
	@Bean
	public MongoTemplate mongoTemplate() {
		ConnectionString connectionString = new ConnectionString(mongoDBCredentials.getUri());
		MongoTemplate template = new MongoTenantTemplate(
				new SnapshotMongoClientDatabaseFactory(MongoClients.create(MongoConfig.getMongoClientSettings(connectionString)),
						mongoDBCredentials.getDefaultDatabaseName()));
		return template;
	}

}
