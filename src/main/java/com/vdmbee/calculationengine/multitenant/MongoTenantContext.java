package com.vdmbee.calculationengine.multitenant;

import org.springframework.util.StringUtils;

public class MongoTenantContext {
	
	
	private static final String DEFAULT_TENANT = "snapshot";
	
	private static final ThreadLocal<String> tenant = new ThreadLocal<String>();
	
	public static final String getTenant() {
		String tenantCode = tenant.get();
		if(StringUtils.isEmpty(tenantCode)) {
			tenant.set(DEFAULT_TENANT);
			tenantCode = tenant.get();
		}
		return tenantCode;
	}
	
	public static final void setTenant(String tenantCode) {
		tenant.set(tenantCode);
	}
	
	public static void clear() {
		tenant.remove();
		//tenant.set(DEFAULT_TENANT);
	}

}
