package com.vdmbee.calculationengine.multitenant;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;

@Component
@ConfigurationProperties(prefix="spring.data.mongodb")
public class MongoDBCredentials {

	private String uri;

	//private String defaultDatabaseName;

	//private String path;

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getDefaultDatabaseName() {
		//String queryString = this.getUri().substring(this.getUri().indexOf("?"));
		String queryString = this.getUri().substring(this.getUri().lastIndexOf("/")+1, this.getUri().lastIndexOf("?"));
		//String queryString = StringUtils.substringBetween(this.getUri(), "/", "?");
		return queryString;
	}

//	public void setDefaultDatabaseName(String defaultDatabaseName) {
//		this.defaultDatabaseName = defaultDatabaseName;
//	}

	public String getPath() {
		String queryString = this.getUri().substring(0,this.getUri().lastIndexOf(getDefaultDatabaseName()+"?")-1);
		return queryString;
	}

	public MongoClient getMongoClient(String tenantId, String queryString) {
		if(tenantId != null) {
			if (queryString != null) {
				return MongoClients.create(getPath() + "/" + tenantId + queryString);
			} else {
				return MongoClients.create(getPath() + "/" + tenantId);
			}
		}
		else{
			if (queryString != null) {
				return MongoClients.create(getPath() + "/" + MongoTenantContext.getTenant() + queryString);
			} else {
				return MongoClients.create(getPath() + "/" + MongoTenantContext.getTenant());
			}
		}
	}
//	public void setPath(String path) {
//		this.path = path;
//	}


}
