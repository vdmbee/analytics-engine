package com.vdmbee.calculationengine.multitenant;

import java.util.Collection;
import java.util.List;

import org.bson.Document;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.CollectionOptions;
import org.springframework.data.mongodb.core.CursorPreparer;
import org.springframework.data.mongodb.core.DocumentCallbackHandler;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.MongoAction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperationContext;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.convert.MongoConverter;
import org.springframework.data.mongodb.core.convert.MongoWriter;
import org.springframework.data.mongodb.core.mapping.event.MongoMappingEvent;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.UpdateDefinition;
import org.springframework.data.util.CloseableIterator;
import org.springframework.lang.Nullable;

import com.mongodb.WriteConcern;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.CountOptions;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.UpdateResult;

public class ExtendedMongoTemplate extends MongoTemplate {

	public ExtendedMongoTemplate(MongoClient mongoClient, String databaseName) {
		super(mongoClient, databaseName);
	}

	public ExtendedMongoTemplate(MongoDatabaseFactory mongoDbFactory) {
		super(mongoDbFactory);
	}
	
	public ExtendedMongoTemplate(MongoDatabaseFactory mongoDbFactory,MongoConverter mongoConverter) {
		super(mongoDbFactory,mongoConverter);
	}

	public MongoDatabase extendedDoGetDatabase() {
		return doGetDatabase();
	}
	
	public <T> CloseableIterator<T> extendedDoStream(Query query, final Class<?> entityType, String collectionName,
			Class<T> returnType) {
		return doStream(query, entityType, collectionName,returnType);
	}
	public void extendedExecuteQuery(Query query, String collectionName, DocumentCallbackHandler documentCallbackHandler,
			@Nullable CursorPreparer preparer) {
		executeQuery(query, collectionName, documentCallbackHandler,preparer);
	}
	
	public long extendedDoCount(String collectionName, Document filter, CountOptions options) {
		return doCount(collectionName, filter, options);
	}
	public void extendedEnsureNotIterable(@Nullable Object o) {
		ensureNotIterable(o);
	}
	public MongoCollection<Document> extendedPrepareCollection(MongoCollection<Document> collection) {
		return prepareCollection(collection);
	}
	
	public WriteConcern extendedPrepareWriteConcern(MongoAction mongoAction) {
		return prepareWriteConcern(mongoAction);
	}
	
	public <T> T extendedDoInsert(String collectionName, T objectToSave, MongoWriter<T> writer) {
		return doInsert(collectionName, objectToSave, writer);
	}
	public <T> Collection<T> extendedDoInsertAll(Collection<? extends T> listToSave, MongoWriter<T> writer) {
		return doInsertAll(listToSave, writer);
	}
	
	public <T> Collection<T> extendedDoInsertBatch(String collectionName, Collection<? extends T> batchToSave,
			MongoWriter<T> writer) {
		return doInsertBatch(collectionName, batchToSave, writer);
	}
	public <T> T extendedDoSave(String collectionName, T objectToSave, MongoWriter<T> writer) {
		return doSave(collectionName, objectToSave, writer);
	}
	
	public Object extendedInsertDocument(final String collectionName, final Document document, final Class<?> entityClass) {
		return insertDocument(collectionName, document, entityClass);
	}
	public List<Object> extendedInsertDocumentList(final String collectionName, final List<Document> documents) {
		return insertDocumentList(collectionName, documents);
	}
	public Object extendedSaveDocument(final String collectionName, final Document dbDoc, final Class<?> entityClass) {
		return saveDocument(collectionName, dbDoc, entityClass);
	}
	
	public UpdateResult extendedDoUpdate(final String collectionName, final Query query, final UpdateDefinition update,
			@Nullable final Class<?> entityClass, final boolean upsert, final boolean multi) {
		return doUpdate(collectionName, query, update, entityClass, upsert, multi);
	}
	
	public <T> DeleteResult extendedDoRemove(final String collectionName, final Query query,
			@Nullable final Class<T> entityClass, boolean multi) {
		return doRemove(collectionName, query, entityClass, multi);
	}
	
	public <T> List<T> extendedDoFindAndDelete(String collectionName, Query query, Class<T> entityClass) {
		return doFindAndDelete(collectionName, query, entityClass);
	}
	
	public <O> AggregationResults<O> extendedAggregate(Aggregation aggregation, String collectionName, Class<O> outputType,
			@Nullable AggregationOperationContext context) {
		return aggregate(aggregation, collectionName, outputType,context);
	}
	
	public <O> AggregationResults<O> extendedDoAggregate(Aggregation aggregation, String collectionName, Class<O> outputType,
			AggregationOperationContext context) {
		return doAggregate(aggregation, collectionName, outputType, context);
	}
	public <O> CloseableIterator<O> extendedAggregateStream(Aggregation aggregation, String collectionName,
			Class<O> outputType, @Nullable AggregationOperationContext context) {
		return aggregateStream(aggregation, collectionName, outputType, context);
	}
	
	public String extendedReplaceWithResourceIfNecessary(String function) {
		return replaceWithResourceIfNecessary(function);
	}
	
	public MongoDatabase extendedPrepareDatabase(MongoDatabase database) {
		return prepareDatabase(database);
	}
	
	public <E extends MongoMappingEvent<T>, T> E extendedMaybeEmitEvent(E event) {
		return maybeEmitEvent(event);
	}
	
	public MongoCollection<Document> extendedDoCreateCollection(final String collectionName,
			final Document collectionOptions) {
		return doCreateCollection(collectionName, collectionOptions);
	}
	public <T> T extendedDoFindOne(String collectionName, Document query, Document fields, Class<T> entityClass) {
		return doFindOne(collectionName, query, fields, entityClass);
	}
	public <T> List<T> extendedDoFind(String collectionName, Document query, Document fields, Class<T> entityClass) {
		return doFind(collectionName, query, fields, entityClass);
	}
	
	public <T> List<T> extendedDoFind(String collectionName, Document query, Document fields, Class<T> entityClass,
			CursorPreparer preparer) {
		return doFind(collectionName, query, fields, entityClass, preparer);
	}
	
	//Not able to override as DocumentCallback is not visible
	/*public <S, T> List<T> extendedDoFind(String collectionName, Document query, Document fields, Class<S> entityClass,
			@Nullable CursorPreparer preparer, DocumentCallback<T> objectCallback) {
		return doFind(collectionName, query, fields, entityClass, preparer, objectCallback);
	}*/
	
	public Document extendedConvertToDocument(@Nullable CollectionOptions collectionOptions, Class<?> targetType) {
		return convertToDocument(collectionOptions, targetType);
	}
	
	public Document extendedConvertToDocument(@Nullable CollectionOptions collectionOptions) {
		return convertToDocument(collectionOptions);
	}
	
	public <T> T extendedDoFindAndRemove(String collectionName, Document query, Document fields, Document sort,
			@Nullable Collation collation, Class<T> entityClass) {
		return doFindAndRemove(collectionName, query, fields, sort, collation, entityClass);
	}
	
	public <T> T extendedDoFindAndModify(String collectionName, Document query, Document fields, Document sort,
			Class<T> entityClass, UpdateDefinition update, @Nullable FindAndModifyOptions options) {
		return doFindAndModify(collectionName, query, fields, sort, entityClass, update, options);
	}
	public <T> T extendedDoFindAndReplace(String collectionName, Document mappedQuery, Document mappedFields,
			Document mappedSort, @Nullable com.mongodb.client.model.Collation collation, Class<?> entityType,
			Document replacement, FindAndReplaceOptions options, Class<T> resultType) {
		return doFindAndReplace(collectionName, mappedQuery, mappedFields, mappedSort, collation, entityType, replacement, options, resultType);
	}
	public <T> T extendedPopulateIdIfNecessary(T savedObject, Object id) {
		return populateIdIfNecessary(savedObject, id);
	}
}
