package com.vdmbee.calculationengine.multitenant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TenantContext {

	private static Logger logger = LogManager.getLogger(TenantContext.class.getName());

	private static ThreadLocal<String> currentTenant = new ThreadLocal<>();
	//private static ThreadLocal<DependencyTaskExecutor> currentTaskExecutor = new ThreadLocal<>();
	
	public static void setCurrentTenant(String tenant) {
		logger.debug("Setting tenant to " + tenant);
		currentTenant.set(tenant);
	}

	public static String getCurrentTenant() {
		return currentTenant.get();
	}

	/*public static void setTaskExecutor(DependencyTaskExecutor taskExecutor) {
		currentTaskExecutor.set(taskExecutor);
	}
	public static DependencyTaskExecutor getCurrentTaskExecutor() {
		return currentTaskExecutor.get();
	}*/
	public static void clear() {
		currentTenant.remove();
		//currentTenant.set(null);
	}

}
