package com.vdmbee.calculationengine.document.controller;


import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.document.entity.Permission;

public class PermissionResourceAssembler extends RepresentationModelAssemblerSupport<Permission, PermissionResource> {

	public PermissionResourceAssembler() {
		super(Permission.class, PermissionResource.class);
		
	}

	@Override
	public PermissionResource toModel(Permission entity) {
		PermissionResource resource = createModelWithId(entity.getId(), entity);
		resource.setGroup(entity.getGroup());
		resource.setUserIds(entity.getUserIds());
		resource.setAccessType(entity.getAccessType().getValue());
		resource.setForEveryone(entity.isForEveryone());
		return resource;
	}

}