package com.vdmbee.calculationengine.document.controller;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;

public class DocumentResource extends RepresentationModel<DocumentResource> {
	
	private String name;
	
	private String refdoc;
	
	private String type;
	
	private List<Documents> childDocuments;
	
	private Documents parentDocument;
	
	private List<Permission> permission;

	public String getRefdoc() {
		return refdoc;
	}

	public void setRefdoc(String refdoc) {
		this.refdoc = refdoc;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Documents> getChildDocuments() {
		return childDocuments;
	}

	public void setChildDocuments(List<Documents> childDocuments) {
		this.childDocuments = childDocuments;
	}

	public Documents getParentDocument() {
		return parentDocument;
	}

	public void setParentDocument(Documents parentDocument) {
		this.parentDocument = parentDocument;
	}

	public List<Permission> getPermission() {
		return permission;
	}

	public void setPermission(List<Permission> permission) {
		this.permission = permission;
	}
}