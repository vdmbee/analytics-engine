package com.vdmbee.calculationengine.document.controller;

import java.util.List;

import org.springframework.hateoas.RepresentationModel;

import com.vdmbee.calculationengine.document.entity.Documents;

public class PermissionResource extends RepresentationModel<PermissionResource> {
	
	private int accessType;
	
	private List<String> group;
	
	private Documents document;
	
	private String[] userIds;
	
	private boolean forEveryone;

	public int getAccessType() {
		return accessType;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public List<String> getGroup() {
		return group;
	}

	public void setGroup(List<String> group) {
		this.group = group;
	}

	public Documents getDocument() {
		return document;
	}

	public void setDocument(Documents document) {
		this.document = document;
	}

	public String[] getUserIds() {
		return userIds;
	}

	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}

	public boolean isForEveryone() {
		return forEveryone;
	}

	public void setForEveryone(boolean forEveryone) {
		this.forEveryone = forEveryone;
	}

}