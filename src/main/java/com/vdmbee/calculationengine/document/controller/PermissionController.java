package com.vdmbee.calculationengine.document.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.repository.PermissionRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class PermissionController {
	
	@Autowired
	PermissionRepository permissionRepository;
	@Autowired
	MongoTenantTemplate mongoTemplate;
	@Autowired
	DocumentsRepository documentRepository;
	@Autowired
	TemplateRepository templateRepository;
	@Autowired
	private PermissionService permissionService;
	
	@RequestMapping(path = "/permission", method = RequestMethod.POST, consumes = "application/json")
	@ResponseBody
	public PermissionResource addNewPermission(PermissionResourceAssembler permissionAssembler, @RequestBody Permission permission, @RequestParam("templateId") String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, templateId, 1, null)) {
			//get template's document
			Query getDocument = new Query();
			getDocument.addCriteria(Criteria.where("refdoc").is(templateId));
			List<Documents> documentFromQuery = mongoTemplate.find(getDocument, Documents.class);
			String documentId = documentFromQuery.get(0).getId();
			//permission check
			if(permissionService.checkPermission(tenantId, templateId, 1, null)) {
				if(permission.getId() == null) {
					permission.setId(UUID.randomUUID().toString());
				}else {
					permission.setId(permission.getId());
				}
				
				Optional<Template> tempObj = templateRepository.findById(templateId);
				if(tempObj.isPresent()) {
					Template template = tempObj.get();
					String tenantIdForUsers = template.getTenantId();
					List<String> userIdsList = permissionService.getUserIdsFromEmails(permission.getUserIds(), tenantIdForUsers);
					if(userIdsList == null) {	//user not found
						return null;
					}
					String[] userIdsArray = userIdsList.toArray(new String[0]);
					permission.setUserIds(userIdsArray);
						
					/*if document exists, add in permission
					if(documentId != null) {
						Documents tempDoc = new Documents();
						tempDoc.setId(documentFromQuery.get(0).getId());
						tempDoc.setName(documentFromQuery.get(0).getName());
						tempDoc.setRefdoc(documentFromQuery.get(0).getRefdoc());
						tempDoc.setType(documentFromQuery.get(0).getType());
						permission.setDocument(tempDoc);
					}
					permissionRepository.save(permission);*/
					
					//add permission in document
					if(documentId != null) {
						String docId = documentId;
						Documents document = documentRepository.getDocumentById(docId);
						List<Permission> permissionList = document.getPermission();
						permissionList.add(permission);
						document.setPermission(permissionList);
						documentRepository.save(document);
					}
					return permissionAssembler.toModel(permission);
				}
			}
			return null;
		}
		return null;
	}
	
	@RequestMapping(path = "/permission/{id}", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	public PermissionResource getPermission(PermissionResourceAssembler permissionAssembler, @PathVariable("id") String id, @RequestParam("templateId") String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, templateId, 0, null)) {
			//Permission p = permissionRepository.getPermissionById(id);
			Permission p = new Permission();
			Query getPermissionFromDoc = new Query();
			getPermissionFromDoc.addCriteria(Criteria.where("permission.id").is(id));
			List<Documents> documentsWithPermission = mongoTemplate.find(getPermissionFromDoc, Documents.class);
			List<Permission> permissionList = documentsWithPermission.get(0).getPermission();
			for(int i = 0; i < permissionList.size(); i++) {
				if(permissionList.get(i).getId().equalsIgnoreCase(id)) {
					p = permissionList.get(i);
				}
			}
			return permissionAssembler.toModel(p);
		}
		return null;
	}
	
	/*@RequestMapping(value = "/permission/groups/{id}", method = RequestMethod.GET,consumes = "application/json")
	@ResponseBody
	@PreAuthorize("hasPermission(#id,'LegalEntity_grouplist', 'read')")
	public PagedResources<GroupResource> getGroupsByLegalEntity(GroupResourceAssembler groupResourceAssembler, Pageable pageable, @PathVariable("id") String id) throws EntityNotFoundException{
		LegalEntity entity = legalEntityRepository.findById(id).orElse(null);
	    Page<Group> groups = groupRepository.findByLegalEntity(pageable, entity);	       
	    PagedResources<GroupResource> pagedGroupResource = pagedAssembler.toModel(groups,groupResourceAssembler);
	    return pagedGroupResource;
	}*/
	
	@RequestMapping(path = "/permission/permissionsByRefdoc/{templateId}/{tenantId}", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	public Page<Permission> getPermissionByTemplate(Pageable pageable, @PathVariable("templateId") String refDoc, @PathVariable("tenantId") String tenantId) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, refDoc, 1, null)) {
			Query getDocument = new Query();
			getDocument.addCriteria(Criteria.where("refdoc").is(refDoc));
			List<Documents> document = mongoTemplate.find(getDocument, Documents.class);
			List<Permission> permissions = document.get(0).getPermission();
			List<String> permissionIds = new ArrayList<String>();
			//List<String> guidIds=new ArrayList<String>();
			for( Permission p : permissions) {
				permissionIds.add(p.getId());				
			}
			
			Optional<Template> tempObj = templateRepository.findById(refDoc);
			if(tempObj.isPresent()) {
				Template template = tempObj.get();	
				String tenantIdForUsers = template.getTenantId();
				/*String[] array = permissionIds.toArray(new String[0]);
				Page<Permission> templetePermissions = permissionRepository.findByIds(array, pageable);
				List<Permission> permissionObj = templetePermissions.getContent();*/
								
				for( Permission p : permissions) {
					if(p.getUserIds().length != 0) {
						for(int i = 0; i < p.getUserIds().length; i++) {
							String email = permissionService.getEmailFromGuidId(p.getUserIds()[i], tenantIdForUsers, null);
							p.getUserIds()[i] = email;
						}
					} else if(p.getGroup() != null) {
						List<String> group = p.getGroup();
						//manipulating output to show group names in datatable
						List<String> groupNames = new ArrayList<String>();
						/*for(String g : group) {
							groupNames.add(authenticationFacade.findOneGroup(g).getName());
						}*/
						p.setUserIds(groupNames.toArray(new String[0]));
					}
				}
				int start = (int) pageable.getOffset();
				int end = (start + pageable.getPageSize()) > permissions.size() ? permissions.size() : (start + pageable.getPageSize());
				Page<Permission> templetePermissions = new PageImpl<Permission>(permissions.subList(start, end), pageable, permissions.size());
				return templetePermissions;
			}
		}
		return null;
	};
	
	/*@RequestMapping(path = "/permission/update", method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody PersistentEntityResource updatePermissionForPlan( PersistentEntityResourceAssembler resourceAssembler, @RequestParam String id, @RequestParam(value = "templateId", required = false) String templateId,@RequestBody Permission permission){
		if(permissionService.checkForAdmin() || permissionService.checkPermission(templateId, 2)) { //only if the user is the permission creator can he edit permissions
			Permission existingPermission = new Permission();
			
			//get the permission from a document 
			Query getPermission = new Query();
			getPermission.addCriteria(Criteria.where("permission.id").is(id));
			List<Documents> documentWithPermission = mongoTemplate.find(getPermission, Documents.class);
			List<Permission> permissionList = documentWithPermission.get(0).getPermission();
			for(Permission p : permissionList) {
				if(p.getId().equals(id)) {
					existingPermission = p;
					break;
				}
			}
			
			//Permission existingPermission = permissionRepository.findOne(id);
			Template template = templateRepository.findTemplateById(templateId);
			String tenantId = template.getTenantId();
			List<String> userIdsList = permissionService.getUserIdsFromEmails(permission.getUserIds(), tenantId);
			if(userIdsList == null) {	//user not found
				return null;
			}
			String[] userIdsArray = userIdsList.toArray(new String[0]);
			
			//Update permission
			existingPermission.setUserIds(userIdsArray);
			existingPermission.setAccessType(permission.getAccessType());
			existingPermission.setGroup(permission.getGroup());
			existingPermission.setForEveryone(permission.isForEveryone());			
				//permissionRepository.save(existingPermission);
			// updating the permission in document
			permissionService.updateDocument(templateId, existingPermission);						
			return resourceAssembler.toModel(existingPermission);
		}
		return null;
    }*/
		
	@RequestMapping(value = "/permission/{id}", method = RequestMethod.DELETE, consumes = "application/json")
	@ResponseBody
	public void deletePermission(@PathVariable("id") String id, @RequestParam(value = "templateId", required = false) String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
	   if(permissionService.checkPermission(tenantId, templateId, 2, null))  {	    
		   permissionService.deletePermissionFromDocument(templateId, id);
	   }
	}	    
		
	 /*@RequestMapping(path = "/permission/getLocalUser", method = RequestMethod.GET, consumes = "application/json")
	 @ResponseBody
	 public List<IUser> getLocalUsers() {
	 	List<IUser> user = permissionService.getLocalUser(null);
	 	return user;
	 }*/
	 
	 
	@RequestMapping(path = "/permission/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public PermissionResource putPermission(PermissionResourceAssembler permissionAssembler, @PathVariable("id") String id, @RequestBody Permission permission, @RequestParam(value = "templateId", required = false) String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, templateId, 1, null)) {
			Query getDocumentsWithPermission = new Query();
			getDocumentsWithPermission.addCriteria(Criteria.where("permission.id").is(id));
			List<Documents> documentsWithPermissionList = mongoTemplate.find(getDocumentsWithPermission, Documents.class);
			for(Documents d : documentsWithPermissionList) {
				List<Permission> individualPermissionLists = d.getPermission();
				for(Permission p : individualPermissionLists) {
					if(p.getId().equals(id)) {
						p.setAccessType(permission.getAccessType());
						p.setForEveryone(permission.isForEveryone());
						p.setGroup(permission.getGroup());
						p.setUserIds(permissionService.getUserIdsFromEmails(permission.getUserIds()).toArray(new String[0]));
						break;
					}
				}
				documentRepository.save(d);
			}
			Permission p = new Permission();
			p.setId(id);
			p.setAccessType(permission.getAccessType());
			p.setForEveryone(permission.isForEveryone());
			p.setGroup(permission.getGroup());
			p.setUserIds(permission.getUserIds());
			//permissionRepository.save(p);
			
			return permissionAssembler.toModel(p);
		}
		return null;
	}
	
	@RequestMapping(value = "/permission", method = RequestMethod.GET, consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'permission', null)")
	@ResponseBody
	public void getAllPermissions() {}
	
	@RequestMapping(value = "/permission/{id}", method = RequestMethod.PATCH, consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'permission', null)")
	@ResponseBody
	public void patchPermission(@PathVariable("id") String id) {}
	
}