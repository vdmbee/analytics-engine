package com.vdmbee.calculationengine.document.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class DocumentsController {
	
	/*@Autowired
	DocumentsRepository documentsRepository;
	@Autowired
	private PermissionService templateService;
	@RequestMapping(path = "/documents/{id}", method = RequestMethod.GET, consumes = "application/json")
	@ResponseBody
	public DocumentResource getDocument(DocumentsResourceAssembler documentAssembler, @PathVariable("id") String id, @RequestParam("templateId") String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		if(templateService.checkPermission(tenantId, templateId, 0, null)) {	//edit
			Documents document = documentsRepository.getDocumentById(id);
			return documentAssembler.toModel(document);
		}
		return null;
	}
	
	@RequestMapping(value = "/documents/{id}", method = RequestMethod.PUT, consumes = "application/json")
	@ResponseBody
	public DocumentResource putDocument(DocumentsResourceAssembler documentAssembler, @PathVariable("id") String id, @RequestBody Documents document, @RequestParam("templateId") String templateId, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		if(templateService.checkPermission(tenantId, templateId, 1, null)) {
			Documents doc = documentsRepository.getDocumentById(id);
			
			doc.setName(document.getName());
			doc.setPermission(document.getPermission());
			doc.setChildDocuments(document.getChildDocuments());
			doc.setParentDocument(document.getParentDocument());
			doc.setRefdoc(document.getRefdoc());
			doc.setType(document.getType());
			
			documentsRepository.save(doc);
			return documentAssembler.toModel(doc);
		}
		
		return null;
	}
	
	@RequestMapping(value = "/documents", method = RequestMethod.POST, consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'documents', null)")
	@ResponseBody
	public void createDocument() {}
	
	@RequestMapping(value = "/documents/{id}", method = RequestMethod.PATCH, consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'documents', null)")
	@ResponseBody
	public void patchDocument(@PathVariable("id") String id) {}
	
	@RequestMapping(value = "/documents", method = RequestMethod.GET, consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'documents', null)")
	@ResponseBody
	@PageableAsQueryParam
	public void getAllDocument(@Parameter(hidden = true) Pageable pageable) {}*/
	
}