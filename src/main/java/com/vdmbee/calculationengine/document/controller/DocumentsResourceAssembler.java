package com.vdmbee.calculationengine.document.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.document.entity.Documents;

public class DocumentsResourceAssembler extends RepresentationModelAssemblerSupport<Documents, DocumentResource >{

	public DocumentsResourceAssembler() {
		super(Documents.class, DocumentResource.class);
	}

	@Override
	public DocumentResource toModel(Documents entity) {

		DocumentResource resource = createModelWithId(entity.getId(), entity);
		resource.setName(entity.getName());
		resource.setRefdoc(entity.getRefdoc().toString());
		resource.setType(entity.getType());
		resource.setChildDocuments(entity.getChildDocuments());
		resource.setParentDocument(entity.getParentDocument());
		resource.setPermission(entity.getPermission());
		return resource;
	}
	
}