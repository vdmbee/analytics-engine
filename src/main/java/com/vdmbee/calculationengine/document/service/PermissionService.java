package com.vdmbee.calculationengine.document.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.LookupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Category;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoDBCredentials;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;

@Service
public class PermissionService {

	@Value("${skipAuthorization}")
	private boolean skipAuthorization;
	@Autowired
	private MongoDBCredentials mongoDBCredentials;
	
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	@Autowired
	private TemplateRepository templateRepository;
	@Autowired
	private DocumentsRepository documentsRepository;
	@Autowired
	private Environment env;
	
	private static Logger logger = LogManager.getLogger(PermissionService.class);

	
	/*private List<String> getGroupsWithUserIds(List<IUser> users) {
		List<IGroup> groups = new ArrayList<IGroup>();
		List<String> groupIds = new ArrayList<String>();
		for(IUser user: users) {
			//groups.addAll(authenticationFacade.getUserGroups(user));
		}
		for(IGroup g: groups) {
			if(!groupIds.contains(g.getId())) {
				groupIds.add(g.getId());
			}
		}
		return groupIds;
	}*/
	

	public Page<Template> getAllTemplates(String tenantId, Pageable pageable) {
		Page<Template> TemplateList = templateRepository.getTemplatesWithTenantId(tenantId, pageable);
		return TemplateList;
	}

	public boolean checkPermission(String tenantId, String refdoc, int permissionNumber, String email)throws AccessDeniedException {
		// refdoc is the id of the file for which permissions need to be checked.
		if(tenantId == null || tenantId.equals("")) {
			logger.error("Invalid Tenent");
			throw new AccessDeniedException(Permission.class,"id","Invalid Tenant");
		}
		if(skipAuthorization) {
			return true;
		}
		return true;
		//this.switchTenant(tenantId);
		/*List<IUser> users = null;
		if(email != null && email != "") {
			users = authenticationFacade.getLocalUsersByLegalEntity(tenantId,email);
		}else {
			users = authenticationFacade.getLocalUsersByLegalEntity(tenantId);
		}
		//if( authenticationFacade.isMemberOf(RoleType.VMP_ADMIN,true) || authenticationFacade.isMemberOf(RoleType.VMP_USER,true)){
		if(email == null && authenticationFacade.hasAdminRole(true)) {
			return true;
		}
		for(IUser u : users ) {
			String userId = u.getId();
			
			//MatchOperation directUser = Aggregation.match(Criteria.where("permission.userIds").in(userId));
			MatchOperation directUser = Aggregation.match(new Criteria().orOperator(
			Criteria.where("permission.userIds").in(userId),
			Criteria.where("permission.forEveryone").in(true)));
			MatchOperation directAccessType = Aggregation
					.match(Criteria.where("permission.accessType").gte(permissionNumber));
			MatchOperation directRefdoc = Aggregation.match(Criteria.where("refdoc").is(refdoc));
			Aggregation dirAggregation = Aggregation.newAggregation(directUser, directAccessType, directRefdoc)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
			List<Documents> directResults = mongoTemplate.aggregate(dirAggregation, "documents", Documents.class)
					.getMappedResults();
			if (directResults.size() != 0) {
				return true;
			}
			
			//get group ids
			List<String> groupIds = new ArrayList<String>();
			List<IGroup> groupList = new ArrayList<IGroup>();//authenticationFacade.getUserGroups(u);
			for(IGroup g: groupList) {
				if(!groupIds.contains(g.getId())) {
					groupIds.add(g.getId());
				}
			}
				
			MatchOperation groupUser = Aggregation.match(Criteria.where("permission.group").in(groupIds));	//match groups
			MatchOperation groupAccessType = Aggregation
					.match(Criteria.where("permission.accessType").gte(permissionNumber));
			MatchOperation groupRefdoc = Aggregation.match(Criteria.where("refdoc").is(refdoc));
			Aggregation grpAggregation = Aggregation.newAggregation(groupUser, groupAccessType, groupRefdoc)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
			List<Documents> groupResults = mongoTemplate.aggregate(grpAggregation, "documents", Documents.class)
					.getMappedResults();
			if (groupResults.size() > 0) {
				return true;
			}
	
			
		}
		Documents doc = documentsRepository.getDocumentByRefdoc(refdoc);
		if(doc != null) {
			logger.error(new Date()+":No permission for user "+email+" in tenant:"+tenantId);
			throw new AccessDeniedException(Permission.class,"email",""+new Date()+":No permission for user "+email+" in tenant:"+tenantId);
		}else {
			logger.info(String.format("No Document: %s in tenant: %s", refdoc, tenantId));
			return false;
		}*/
	}

	//public boolean checkForAdmin() {
	//	return authenticationFacade.hasAdminRole(true);
	//}

	public String checkIsLocalUser(String userId, String tenantId, String email) {
		/*List<IUser> users = getLocalUser(email);
		for (IUser u : users) {
			if (u.getLegalEntity().getId().equals(tenantId)) {
				List<IUser> userList = u.getLegalEntity().getUserList();
				Iterator<IUser> iterator = userList.iterator();
				while (iterator.hasNext()) {
					IUser user = iterator.next();
					IGlobalUser globalUser = user.getGlobalUser();
					List<IUserIdentity> identities = globalUser.getIdentities();
					Iterator<IUserIdentity> idIterator = identities.iterator();
					while (idIterator.hasNext()) {
						if (idIterator.next().getEmail().equals(userId)) {
							return user.getId();
						}
					}
				}
			}
		}*/
		return null;
	}

	public void addDocument(Documents document, String email) throws AccessDeniedException {
		/*if(userList.size() == 0) {
			throw new NoSuchElementException("User not found.");
		}*/
		//if(skipAuthorization) {
            return;
        //}
		/*List<IUser> userList = getLocalUser(email);
		//documentsRepository.save(document);
		if(userList.size() > 0) {
			List<Permission> permissionList = new ArrayList<Permission>();
			Permission p = new Permission();
			String[] tempUser = { userList.get(0).getId() };
			p.setId(UUID.randomUUID().toString());
			p.setUserIds(tempUser);
			p.setAccessType(AccessType.DELETE);
			String documentDefaultAccess= env.getProperty("documentDefaultAccess");
			if((documentDefaultAccess == null || !documentDefaultAccess.equalsIgnoreCase("everyOne"))) {
				p.setForEveryone(false);	
			}else {
				p.setForEveryone(true);
			}*/
			
			// permissionRepository.save(p);
	
			//String id = document.getId();
			//Documents d = documentsRepository.getDocumentById(id);
	
		/*	permissionList.add(p);
			document.setPermission(permissionList);
			document.setId(UUID.randomUUID().toString());
			documentsRepository.save(document);
		}else {
			logger.error("No User for "+email);
			throw new AccessDeniedException(Permission.class,"email","No User for "+email);
		}*/
	}

	public void deleteDocument(String refdocId) {
		if(skipAuthorization) {
            return;
        }
		Query getDocument = new Query();
		getDocument.addCriteria(Criteria.where("refdoc").is(refdocId));

		Documents doc = documentsRepository.getDocumentByRefdoc(refdocId);
		if (doc != null) {
			if (doc.getChildDocuments().size() != 0) { // delete children
				List<Documents> childDocuments = doc.getChildDocuments();
				Iterator<Documents> childIterator = childDocuments.iterator();
				while (childIterator.hasNext()) {
					Documents child = childIterator.next();
					deleteDocument(child.getRefdoc());
				}
			}
			List<Permission> permissionList = doc.getPermission();
			List<String> permissionIds = new ArrayList<String>();
			for (Permission p : permissionList) {
				permissionIds.add(p.getId());
			}
			/*
			 * Query permissionDeletion = new Query();
			 * permissionDeletion.addCriteria(Criteria.where("_id").in(permissionIds));
			 * WriteResult groupResults = mongoTemplate.remove(permissionDeletion,
			 * Permission.class);
			 */
			DeleteResult documentDeletion = mongoTemplate.remove(getDocument, Documents.class);

			//System.out.println(documentDeletion);
		}
	}

	public void updateDocument(String refdocId, Permission permission) {
		if(skipAuthorization) {
            return;
        }
		Documents doc = documentsRepository.getDocumentByRefdoc(refdocId);
		if (doc != null) {
			List<Permission> permissionList = doc.getPermission();
			List<Permission> updatedPermissionList = new ArrayList<Permission>();
			for (Permission p : permissionList) {
				if (permission.getId().equals(p.getId())) {
					p.setUserIds(permission.getUserIds());
					p.setAccessType(permission.getAccessType());
					p.setForEveryone(permission.isForEveryone());
					p.setGroup(permission.getGroup());
				}
				updatedPermissionList.add(p);
			}
			doc.setPermission(updatedPermissionList);
			documentsRepository.save(doc);
		}
	}

	public void deletePermissionFromDocument(String refdocId, String permissionId) {
		if(skipAuthorization) {
            return;
        }
		Documents doc = documentsRepository.getDocumentByRefdoc(refdocId);
		if (doc != null) {
			List<Permission> permissionList = doc.getPermission();
			int permissionIndex = 0;
			for (Permission p : permissionList) {
				if (p.getId().equals(permissionId)) {
					permissionList.remove(permissionIndex);
					break;
				}
				permissionIndex++;
			}
			doc.setPermission(permissionList);
			documentsRepository.save(doc);
		}
	}
	
	/*public void updateGroupsInDocuments(String groupId,Group group) {
		// get Documents
		Query getDocumentsWithGroup = new Query();
		getDocumentsWithGroup.addCriteria(Criteria.where("permission.group.id").is(groupId));
		List<Documents> documentsWithGroup = mongoTemplate.find(getDocumentsWithGroup, Documents.class);			
		for(Documents doc : documentsWithGroup) {
			List<Permission> permissionList = doc.getPermission();
			List<Permission> updatedPermissionList = new ArrayList<Permission>();
			for(Permission p : permissionList) {
				List<String> groupList = p.getGroup();
				if(groupList!=null) {
					for(String g : groupList) {
						if(g.equals(groupId)) {
							Query getGrpById = new Query();
							getGrpById.addCriteria(Criteria.where("_id").is(g));
							List<Group> groups = mongoTemplate.find(getGrpById, Group.class);
							Group updatedGroup = groups.get(0);
							updatedGroup.setName(group.getName());
						}
					}
				}
				updatedPermissionList.add(p);
			}
			doc.setPermission(updatedPermissionList);
			documentsRepository.save(doc);
		}
	}*/
	
	/*public void updateGroupPermissionsInDocuments(String groupId, String permissionId) {
		// get Documents
		Query getDocumentsWithGroup = new Query();
		getDocumentsWithGroup.addCriteria(Criteria.where("permission.group.id").is(groupId));
		List<Documents> documentsWithGroup = mongoTemplate.find(getDocumentsWithGroup, Documents.class);			
		for(Documents doc : documentsWithGroup) {
			List<Permission> permissionList = doc.getPermission();
			List<Permission> updatedPermissionList = new ArrayList<Permission>();
			for(Permission p : permissionList) {				
				if(p.getId().equals(permissionId)) {
					List<String> groupList = p.getGroup();
					for(String g : groupList) {
						if(g.equals(groupId)) {
							groupList.remove(g);
							break;
						}
					}
				}				
				updatedPermissionList.add(p);
			}
			doc.setPermission(updatedPermissionList);
			documentsRepository.save(doc);
		}
	}*/

	public List<JSONObject> getRubricValueInSnapshots(String rubricId,List<String> snapshotsIds,String tenantId, String email) throws ParseException {
		this.switchTenant(tenantId);
		MatchOperation userFilter = null;
		if(!this.skipAuthorization) {
			List<String> userIds = new ArrayList<String>();
			/*List<IUser> users = getLocalUser(email);
			for (IUser u : users) {
				userIds.add(u.getId());
			}*/
			List<String> groupsWithUserIds = new ArrayList<String>();
			//get group's Id's with userIds
			//groupsWithUserIds = getGroupsWithUserIds(users);
			userFilter = Aggregation.match(new Criteria().orOperator(
					Criteria.where("permission.group").in(groupsWithUserIds), Criteria.where("permission.userIds").in(userIds),
					Criteria.where("permission.forEveryone").in(true)));
		}
		LookupOperation lookupFilter = Aggregation.lookup("snapshot", "refdoc", "_id", "refdoc");
		MatchOperation snapshotsFilter = Aggregation.match(Criteria.where("refdoc._id").in(snapshotsIds));
		//ProjectionOperation projectStage = Aggregation.project("fileContent");
		Aggregation aggregation = null;
		if(userFilter != null){
			aggregation = Aggregation.newAggregation(userFilter, lookupFilter, snapshotsFilter)
					//Aggregation aggregation = Aggregation.newAggregation(userFilter, lookupFilter, snapshotsFilter,projectStage)
							.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		}else {
			aggregation = Aggregation.newAggregation(snapshotsFilter)
					//Aggregation aggregation = Aggregation.newAggregation(userFilter, lookupFilter, snapshotsFilter,projectStage)
							.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		}
		
		List<Documents> results = mongoTemplate.aggregate(aggregation, "documents", Documents.class).getMappedResults();

		List<JSONObject> snapshotList = new ArrayList<JSONObject>();
		for (Documents d : results) {
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(d.getRefdoc());
			//String jsonString = jsonObj.toString();
			ArrayList<JSONObject> fc = (ArrayList<JSONObject>)jsonObj.get("fileContent");
			for(int i=0;i<fc.size();i++) {
				JSONObject fcObj = fc.get(i);
				if(fcObj.get("id").equals(rubricId)) {
					JSONObject retObj = new JSONObject();
					retObj.put("id", jsonObj.get("_id"));
					retObj.put("Value", fcObj);
					snapshotList.add(retObj);
					break;
				}
			}
		}
		return snapshotList;
	}
	
	public List<JSONObject> getRubricValueInSnapshots2(String rubricId,List<String> snapshotsIds,String tenantId, String email) throws ParseException {
		this.switchTenant(tenantId);
		MatchOperation userFilter = null;
		LookupOperation lookupFilter = null;
		if(!this.skipAuthorization) {
			List<String> userIds = new ArrayList<String>();
			/*List<IUser> users = getLocalUser(email);
			for (IUser u : users) {
				userIds.add(u.getId());
			}*/
			List<String> groupsWithUserIds = new ArrayList<String>();
			//get group's Id's with userIds
			//groupsWithUserIds = getGroupsWithUserIds(users);
			userFilter = Aggregation.match(new Criteria().orOperator(
					Criteria.where("refdoc.permission.group").in(groupsWithUserIds), Criteria.where("refdoc.permission.userIds").in(userIds),
					Criteria.where("refdoc.permission.forEveryone").in(true)));
			lookupFilter = Aggregation.lookup("documents", "_id", "refdoc", "refdoc");
		}
		
		MatchOperation snapshotsFilter = Aggregation.match(Criteria.where("_id").in(snapshotsIds));
		Aggregation aggregation  =null;
		if(userFilter != null){
			aggregation = Aggregation.newAggregation(snapshotsFilter,lookupFilter,userFilter)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		}else {
			aggregation = Aggregation.newAggregation(snapshotsFilter)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		}
		List<Snapshot> results = mongoTemplate.aggregate(aggregation, "snapshot", Snapshot.class).getMappedResults();

		List<JSONObject> snapshotList = new ArrayList<JSONObject>();
		for (Snapshot s : results) {
			List<JSONObject> fc = s.getFileContent();
			for(int i=0;i<fc.size();i++) {
				JSONObject fcObj = fc.get(i);
				if(fcObj.get("id").equals(rubricId)) {
					JSONObject retObj = new JSONObject();
					retObj.put("id", s.getId());
					retObj.put("Value", fcObj);
					snapshotList.add(retObj);
					break;
				}
			}
		}
		return snapshotList;
	}
	public Page<JSONObject> getSnapshotsByTemplateId(String templateId,String tenantId, Pageable pageable, String email) throws ParseException {
		this.switchTenant(tenantId);
		MatchOperation userFilter = null;
		LookupOperation lookupFilter = null;
		MatchOperation templateFilter = null;
		if(!skipAuthorization) {
			List<String> userIds = new ArrayList<String>();
			/*List<IUser> users = getLocalUser(email);
			for (IUser u : users) {
				userIds.add(u.getId());
			}*/
			List<String> groupsWithUserIds = new ArrayList<String>();
			//get group's Id's with userIds
			//groupsWithUserIds = getGroupsWithUserIds(users);
			userFilter = Aggregation.match(new Criteria().orOperator(
					Criteria.where("permission.group").in(groupsWithUserIds), Criteria.where("permission.userIds").in(userIds),
					Criteria.where("permission.forEveryone").in(true)));
			lookupFilter = Aggregation.lookup("snapshot", "refdoc", "_id", "refdoc");
			templateFilter = Aggregation.match(Criteria.where("refdoc.templateId").is(templateId));
		}else {
			templateFilter = Aggregation.match(Criteria.where("templateId").is(templateId));
		}
		Aggregation aggregation = null;
		if(userFilter != null) {
			aggregation = Aggregation.newAggregation(userFilter, lookupFilter, templateFilter)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
			List<Documents>results = mongoTemplate.aggregate(aggregation, "documents", Documents.class).getMappedResults();
		}else {
			aggregation = Aggregation.newAggregation( templateFilter)
					.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		}
		List results = null;
		if(!skipAuthorization) {
			results = mongoTemplate.aggregate(aggregation, "documents", Documents.class).getMappedResults();
		}else {
			results = mongoTemplate.aggregate(aggregation, "snapshot", Snapshot.class).getMappedResults();
		}

		List templateList = new ArrayList<JSONObject>();
		for (Object d : results) {
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = null;
			if(!skipAuthorization) {
				jsonObj = (JSONObject) parser.parse(((Documents)d).getRefdoc());
			}else {
				String dStr = d.toString();
				dStr = dStr.substring(1,dStr.length() -1);
				jsonObj = (JSONObject) parser.parse(dStr);
			}
			templateList.add(jsonObj);
		}
		return new PageImpl<JSONObject>(templateList, pageable, templateList.size());
	}
	
	public List<String> getUserIdsFromEmails(String[] emailIds) {
		List<String> userIds = new ArrayList<String>();
		/*List<IUser> users=getLocalUser(null);
		for (String email : emailIds) {
			// get user id from each email
			//List<IUser> users = getLocalUser(email);
			String tempUserId = null;
			for (IUser u : users) {
					List<IUser> userList = u.getLegalEntity().getUserList();
					Iterator<IUser> iterator = userList.iterator();
					while (iterator.hasNext()) {
						IUser user = iterator.next();
						IGlobalUser globalUser = user.getGlobalUser();
						List<IUserIdentity> identities = globalUser.getIdentities();
						Iterator<IUserIdentity> idIterator = identities.iterator();
						while (idIterator.hasNext()) {
							if (idIterator.next().getEmail().equals(email)) {
								tempUserId = user.getId();
								break;
							}
						}
						if (tempUserId != null) {
							break;
						}
					}				
				if (tempUserId != null) {
					break;
				}
			}

			if (tempUserId != null) {
				userIds.add(tempUserId);
			} else {
				return null; // local user not found
			}
		}*/
		return userIds;
	}

	public List<String> getUserIdsFromEmails(String[] emailIds, String tenantId) {
		List<String> userIds = new ArrayList<String>();
		/*List<IUser> users = getLocalUser(null);
		for (String email : emailIds) {
			// get user id from each email
			//List<IUser> users = getLocalUser(email);
			String tempUserId = null;
			for (IUser u : users) {
				if (u.getLegalEntity().getId().equals(tenantId)) {
					List<IUser> userList = u.getLegalEntity().getUserList();
					Iterator<IUser> iterator = userList.iterator();
					while (iterator.hasNext()) {
						IUser user = iterator.next();
						IGlobalUser globalUser = user.getGlobalUser();
						List<IUserIdentity> identities = globalUser.getIdentities();
						Iterator<IUserIdentity> idIterator = identities.iterator();
						while (idIterator.hasNext()) {
							if (idIterator.next().getEmail().equals(email)) {
								tempUserId = user.getId();
								break;
							}
						}
						if (tempUserId != null) {
							break;
						}
					}
				}
				if (tempUserId != null) {
					break;
				}
			}

			if (tempUserId != null) {
				userIds.add(tempUserId);
			} else {
				return null; // local user not found
			}
		}*/
		return userIds;
	}

	/*public List<String> getEmailsFromGuid(List<String> guids, String tenantId) {
		List<String> userEmailIds = new ArrayList<String>();
		for (String id : guids) {
			List<User> users = authenticationFacade.getLocalUsers();
			String tempUserId = null;
			for (User u : users) {
				if (u.getLegalEntity().getId().equals(tenantId)) {
					List<User> userList = u.getLegalEntity().getUserList();
					Iterator<User> iterator = userList.iterator();
					while (iterator.hasNext()) {
						User user = iterator.next();
						if (user.getId().equals(id)) {
							GlobalUser globalUser = user.getGlobalUser();
							List<Identity> identities = globalUser.getIdentities();
							Iterator<Identity> idIterator = identities.iterator();
							while (idIterator.hasNext()) {
								tempUserId = idIterator.next().getEmail();//TODO check all emails
								break;
							}
						}
					}
				}
				if (tempUserId != null) {
					break;
				}
			}

			if (tempUserId != null) {
				userEmailIds.add(tempUserId);
			} else {
				return null; // local user not found
			}
		}
		return userEmailIds;
	}*/

	public String getEmailFromGuidId(String guid) {
		//List<User> users = authenticationFacade.getLocalUsers();
		String tempUserId = null;
		/*for (User u : users) {
			//if (u.getLegalEntity().getId().equals(tenantId)) {
				List<User> userList = u.getLegalEntity().getUserList();
				Iterator<User> iterator = userList.iterator();
				while (iterator.hasNext()) {
					User user = iterator.next();
					if (user.getId().equals(guid)) {
						GlobalUser globalUser = user.getGlobalUser();
						List<Identity> identities = globalUser.getIdentities();
						Iterator<Identity> idIterator = identities.iterator();
						while (idIterator.hasNext()) {
							tempUserId = idIterator.next().getEmail();//TODO check all emails
							break;
						}
					}
				}
			//}
			if (tempUserId != null) {
				break;
			}
		}*/
		return tempUserId;
	}
	
	public String getEmailFromGuidId(String guid, String tenantId, String email) {
		//List<IUser> users = getLocalUser(email);
		String tempUserId = null;
		/*for (IUser u : users) {
			if (u.getLegalEntity().getId().equals(tenantId)) {
				List<IUser> userList = u.getLegalEntity().getUserList();
				Iterator<IUser> iterator = userList.iterator();
				while (iterator.hasNext()) {
					IUser user = iterator.next();
					
					//if (user.getId().equals(guid)) {
						IGlobalUser globalUser = user.getGlobalUser();
						List<IUserIdentity> identities = globalUser.getIdentities();
						Iterator<IUserIdentity> idIterator = identities.iterator();
						while (idIterator.hasNext()) {
							tempUserId = idIterator.next().getEmail();//TODO check all emails
							break;
						}
					//}
				}
			}
			if (tempUserId != null) {
				break;
			}
		}*/
		return tempUserId;
	}

	/*public List<IUser> getLocalUser(String email) {
		List<IUser> users = null;
		if(email != null && email != "") {
			users = authenticationFacade.getLocalUsersByEmail(email);
		}else {
			users = authenticationFacade.getLocalUsers();
		}
		return users;
	}*/
	public List<JSONObject> getdocsWithOutPermissions(String tenantId, String collection) throws ParseException{
		MatchOperation tenantFilter = Aggregation.match(Criteria.where("tenantId").is(tenantId));
		Aggregation aggregation  = null;
		aggregation = Aggregation.newAggregation(tenantFilter)
				.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		
		List results = null;
		if(collection.equals("template")) {
			results = getTemplates(aggregation);
		}else if(collection.equals("category")) {
			results = getCategories(aggregation);
		}else if(collection.equals("snapshot")) {
			results = getSnapshots(aggregation);
		}

		List<JSONObject> templateList = new ArrayList<JSONObject>();
		for (Object d : results) {
			JSONParser parser = new JSONParser();
			String jsonArrStr = d.toString();// not sure why we add [<json string>]
			String jsonStr = (jsonArrStr != null) ? jsonArrStr.substring(1,jsonArrStr.length() -1): "{}";
			JSONObject jsonObj = (JSONObject) parser.parse(jsonStr);
			templateList.add(jsonObj);
		}
		return templateList;
	}
	private String getQueryString(String uriStr) {
		if(uriStr.lastIndexOf("?") > 0) {
			return uriStr.substring(uriStr.lastIndexOf("?"));	
		}else {
			return "";
		}
		
	}
	public List<JSONObject> getDocsWitoutPermissions2(String tenantId, String collection,Pageable pageable) throws ParseException{
		List<JSONObject> ret = new ArrayList<JSONObject>();
		if(mongoDBCredentials == null) {
			mongoDBCredentials = SnapshotBeanUtil.getBean(MongoDBCredentials.class);	
		}
		//String uriStr = mongoDBCredentials.getPath() + "/" + MongoTenantContext.getTenant() + this.getQueryString(this.mongoDBCredentials.getUri());
		//MongoClient mongoClient = MongoClients.create(uriStr);
		MongoClient mongoClient = mongoDBCredentials.getMongoClient(MongoTenantContext.getTenant(), this.getQueryString(this.mongoDBCredentials.getUri()));
		String databaseStr = MongoTenantContext.getTenant();
		MongoDatabase db = mongoClient.getDatabase(databaseStr);
		Iterator iter = null;
		JSONParser parser = new JSONParser();
		if(pageable != null) {
			Boolean isEmpty = pageable.getSort().isEmpty();
			
			JSONObject sortJson = new JSONObject();
			if(!isEmpty) {
				Iterator<Order> orderIter = pageable.getSort().iterator();	
				while(orderIter.hasNext()) {
					Order order = orderIter.next();
					sortJson.put(order.getProperty(), (order.getDirection() == Sort.Direction.ASC)? 1 :-1);
				}
			}else {
				sortJson.put("_id", 1);
			}
			int pageNumber = pageable.getPageNumber();
			Consumer<? super Document> collObj;
			iter = db.getCollection(collection).find()
				.sort( Document.parse(sortJson.toJSONString()))
				 .skip( pageNumber > 0 ? ( ( pageNumber - 1 ) * pageable.getPageSize() ) : 0 )
				 .limit(pageable.getPageSize())
				 .iterator();
			
		}else {
			iter = db.getCollection(collection).find().iterator();
		}
		while(iter.hasNext()) {
			Object doc = iter.next();
			String jsonArrStr = doc.toString();// not sure why we add [<json string>]
			JSONObject jsonObj = (JSONObject) parser.parse(((Document)doc).toJson());
			ret.add(jsonObj);
		}
		return ret;
	}
	private List<Template> getTemplates(Aggregation aggregation){
		return mongoTemplate.aggregate(aggregation, "template", Template.class).getMappedResults();
	}
	private List<Category> getCategories(Aggregation aggregation){
		return mongoTemplate.aggregate(aggregation, "category", Category.class).getMappedResults();
	}
	private List<Snapshot> getSnapshots(Aggregation aggregation){
		return mongoTemplate.aggregate(aggregation, "snapshot", Snapshot.class).getMappedResults();
	}
	
	public List<JSONObject> getdocs(String tenantId, String collection, String email,Pageable pageable)
			throws ParseException {
		if(skipAuthorization) {
			return this.getDocsWitoutPermissions2(tenantId, collection, pageable);
		}
		MatchOperation userFilter= null;
		String tenantName = null;
		List<String> userIds = new ArrayList<String>();
		/*List<IUser> users = getLocalUser(email);
		for (IUser u : users) {
			userIds.add(u.getId());
			if(u.getLegalEntity().getId().equals(tenantId)) {
				tenantName=u.getLegalEntity().getName();
			}
		}*/
		List<String> groupsWithUserIds = new ArrayList<String>();
		//get group's Id's with userIds
		//groupsWithUserIds = getGroupsWithUserIds(users);
		userFilter = Aggregation.match(new Criteria().orOperator(
				Criteria.where("permission.group").in(groupsWithUserIds), Criteria.where("permission.userIds").in(userIds),
				Criteria.where("permission.forEveryone").is(true)));
		
		LookupOperation lookupFilter = Aggregation.lookup(collection, "refdoc", "_id", "refdoc");
		MatchOperation tenantFilter = Aggregation.match(Criteria.where("refdoc.tenantId").is(tenantId));
		Aggregation aggregation  = null;
		aggregation = Aggregation.newAggregation(userFilter, lookupFilter, tenantFilter)
				.withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		
		List<Documents> results = mongoTemplate.aggregate(aggregation, "documents", Documents.class).getMappedResults();

		List<JSONObject> docList = new ArrayList<JSONObject>();
		for (Documents d : results) {
			JSONParser parser = new JSONParser();
			JSONObject jsonObj = (JSONObject) parser.parse(d.getRefdoc());
			//JSONObject jsonObj = new JSONObject(d.getRefdoc());
			jsonObj.put("tenantName", tenantName);
			//String jsonString = jsonObj.toString();
			docList.add(jsonObj);
		}
		return docList;

	}
	
	

	public void switchTenant(String tenantId) {
		String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
		if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
			//authenticationFacade.setCurrentTenant(tenantId);	
			//MongoTenantContext.setTenant(tenantId);
		}
	}
	
	public boolean isMemberOfTenant(String tenantId,String email) {
		this.switchTenant(tenantId);
		if(skipAuthorization) {
			return true;
		}
		//ILegalEntity entity = authenticationFacade.findOneLegalEntity(tenantId);
		//if(entity == null) {
		//	return false;
		//}else {
			return false;//authenticationFacade.isUserOfLegalEntity(tenantId,email);
		//}
	}
	
}
