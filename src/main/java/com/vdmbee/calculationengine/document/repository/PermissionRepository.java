package com.vdmbee.calculationengine.document.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.vdmbee.calculationengine.document.entity.Permission;

@RepositoryRestResource(collectionResourceRel = "permission", path = "permission")
public interface PermissionRepository extends MongoRepository<Permission, String> {
	
	@Query("{'_id' : ?0}")
	public Permission getPermissionById(String id);
	
	@Query("{ '_id': {'$in' : ?0 } }")
	Page<Permission> findByIds(String [] IdList, Pageable pageable);
}