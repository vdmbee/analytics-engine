package com.vdmbee.calculationengine.document.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.vdmbee.calculationengine.document.entity.Documents;

@RepositoryRestResource(collectionResourceRel = "documents", path = "documents")
public interface DocumentsRepository extends MongoRepository<Documents, String> {
	
	@Query("{'refdoc': ?0}")
	public Documents getDocumentByRefdoc(String refDocId);
	
	@Query("{'_id' : ?0}")
	public Documents getDocumentById(String id);
}
