package com.vdmbee.calculationengine.document.entity;

import java.util.List;

import javax.persistence.Id;

import com.vdmbee.calculationengine.permission.model.AccessType;

public class Permission {
	
	@Id
    private String id;
	
	private int accessType;
	
	private List<String> group;
		
	private String[] userIds;
	
	private boolean forEveryone;
	
	public AccessType getAccessType() {
		return AccessType.values()[accessType];
	}

	public void setAccessType(AccessType accessType) {
		this.accessType = accessType.getValue();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getGroup() {
		return group;
	}

	public void setGroup(List<String> group) {
		this.group = group;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public String[] getUserIds() {
		return userIds;
	}

	public void setUserIds(String[] userIds) {
		this.userIds = userIds;
	}

	public boolean isForEveryone() {
		return forEveryone;
	}

	public void setForEveryone(boolean forEveryone) {
		this.forEveryone = forEveryone;
	}
}