package com.vdmbee.calculationengine.document.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "documents")
public class Documents {

	@Id
	private String id;

	private String name;

	@Indexed
	private String refdoc;

	private String type;

	private List<Documents> childDocuments = new ArrayList<Documents>();

	private Documents parentDocument;

	private List<Permission> permission;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Documents> getChildDocuments() {
		return childDocuments;
	}

	public void setChildDocuments(List<Documents> childDocuments) {
		this.childDocuments = childDocuments;
	}

	public List<Permission> getPermission() {
		return permission;
	}

	public void setPermission(List<Permission> permission) {
		this.permission = permission;
	}

	public Documents getParentDocument() {
		return parentDocument;
	}

	public void setParentDocument(Documents parentDocument) {
		this.parentDocument = parentDocument;
	}

	public String getRefdoc() {
		return refdoc;
	}

	public void setRefdoc(String refdoc) {
		this.refdoc = refdoc;
	}

}
