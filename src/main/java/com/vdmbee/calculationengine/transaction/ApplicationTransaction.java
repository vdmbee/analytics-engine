package com.vdmbee.calculationengine.transaction;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.MongoTransactionManager;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.mongodb.ClientSessionOptions;
import com.mongodb.ReadConcern;
import com.mongodb.ReadPreference;
import com.mongodb.TransactionOptions;
import com.mongodb.WriteConcern;
import com.mongodb.client.MongoDatabase;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.config.MongoConfig;
import com.vdmbee.calculationengine.multitenant.ExtendedMongoTemplate;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;


public class ApplicationTransaction {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationTransaction.class);


    private MongoDatabaseFactory dbFactory;
	private MongoTransactionManager mongoTransactionManager;
    private TransactionStatus mongoStatus;
    
	@Autowired
	public MongoConfig mongoConfig;
    
    @Autowired
	public MongoTenantTemplate mongoTenantTemplate;
    
	
	public ExtendedMongoTemplate checkAndCreateTemplate(String tenant) {
		if(mongoTenantTemplate == null) {
			mongoTenantTemplate = BeanLoaderUtil.getBean(MongoTenantTemplate.class);
		}
		return mongoTenantTemplate.getTenantMongoTemplate(tenant);
	}

    public MongoDatabaseFactory getDbFactory() {
		return dbFactory;
	}

	public void setDbFactory(MongoDatabaseFactory dbFactory) {
		this.dbFactory = dbFactory;
	}

	public MongoTransactionManager getMongoTransactionManager() {
		return mongoTransactionManager;
	}

	public void setMongoTransactionManager(MongoTransactionManager mongoTransactionManager) {
		this.mongoTransactionManager = mongoTransactionManager;
	}

	public TransactionStatus getMongoStatus() {
		return mongoStatus;
	}

	public void setMongoStatus(TransactionStatus mongoStatus) {
		this.mongoStatus = mongoStatus;
	}

    public ApplicationTransaction() {
    	ExtendedMongoTemplate extendedTemplate = this.checkAndCreateTemplate(MongoTenantContext.getTenant());
        this.dbFactory = extendedTemplate.getMongoDatabaseFactory();
    }

	public void initializeMongoTransaction() {
		if(this.mongoStatus == null || this.mongoStatus.isCompleted()) {
			this.mongoTransactionManager = new MongoTransactionManager(dbFactory);
			TransactionOptions transactionOptions = TransactionOptions.builder().readPreference(ReadPreference.primary())
					.readConcern(ReadConcern.LOCAL).writeConcern(WriteConcern.MAJORITY).maxCommitTime(5L,TimeUnit.MINUTES).build();
			this.mongoTransactionManager.setOptions(transactionOptions);
			DefaultTransactionDefinition def =  new DefaultTransactionDefinition();
			if(mongoConfig == null) {
				mongoConfig = BeanLoaderUtil.getBean(MongoConfig.class);
			}
			def.setTimeout(mongoConfig.getTransactionLifetimeLimitSeconds());
            this.mongoStatus = this.mongoTransactionManager.getTransaction(def);
		}
	}

	public MongoDatabase getCurrentDatabase() {
		return this.dbFactory.getMongoDatabase();
    }


    public boolean commitTransaction() {
		try {
            if(this.mongoStatus != null && !this.mongoStatus.isCompleted()) {
            	this.mongoTransactionManager.commit(mongoStatus);
            	ClientSessionOptions options = ClientSessionOptions.builder()
            	        .causallyConsistent(true)
            	        .build();
            	this.dbFactory.getSession(options).close();
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("Error while committing transaction : " + e.getMessage() + ", attempting to rollback the transaction.", e);
            rollbackTransaction();
            throw e;
        }finally {
        	this.mongoTransactionManager = null;
        	this.mongoStatus = null;
        	ApplicationTransactionManager.clearTransaction();
        }
    }

    public boolean rollbackTransaction() {
        try {
            if(this.mongoStatus != null && !this.mongoStatus.isCompleted()) {
            	this.mongoTransactionManager.rollback(mongoStatus);
            	LOGGER.error("rolling back the transaction");
            	ClientSessionOptions options = ClientSessionOptions.builder()
            	        .causallyConsistent(true)
            	        .build();
            	this.dbFactory.getSession(options).close();
            }
            return true;
        } catch (Exception e) {
            LOGGER.error("Error while rolling back the transaction  : " + e.getMessage(), e);
            throw e;
        }finally {
        	this.mongoTransactionManager = null;
        	this.mongoStatus = null;
        	ApplicationTransactionManager.clearTransaction();
        }
    }
    
    public void threadCount() {
        int totalThreadCount = Thread.activeCount();
        System.out.println("Total active threads: " + totalThreadCount);

        // Print details of each thread
        /* Thread[] allThreads = new Thread[totalThreadCount];
        Thread.enumerate(allThreads);

        System.out.println("Thread details:");
        for (Thread thread : allThreads) {
            System.out.println("Thread ID: " + thread.getId() +
                    " Name: " + thread.getName() +
                    " State: " + thread.getState());
        }*/
    }

}
