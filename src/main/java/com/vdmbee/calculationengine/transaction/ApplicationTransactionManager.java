package com.vdmbee.calculationengine.transaction;

import org.springframework.stereotype.Component;


@Component
public class ApplicationTransactionManager {
	
	
    private static ThreadLocal<ApplicationTransaction> sessionTransaction = new ThreadLocal<>();

    public static ApplicationTransaction getApplicationTransaction() {
    	//MongoDatabaseFactory dbFactory;
    	//MongoClient mongoClient = SnapshotBeanUtil.getBean(MongoClient.class);
    	//dbFactory = SnapshotBeanUtil.getBean(MongoDatabaseFactory.class);
        ApplicationTransaction transaction = sessionTransaction.get();		
        if (transaction == null) {
        	//dbFactory = new SimpleMongoClientDatabaseFactory(mongoClient,MongoTenantContext.getTenant());
            transaction = new ApplicationTransaction();
            sessionTransaction.set(transaction);
        }
        return transaction;
    }
    
    public static void clearTransaction() {
    	sessionTransaction.remove();
    	//sessionTransaction.set(null);
    }
}
