
package com.vdmbee.calculationengine;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;
import javax.persistence.PersistenceException;

import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.retry.backoff.FixedBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.mongodb.MongoWriteException;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.config.DefaultListenerSupport;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;

import io.micrometer.core.instrument.binder.commonspool2.CommonsObjectPool2Metrics;

@EnableScheduling
@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
/*@ComponentScan(basePackages = {"com.visionplanner","com.vdmbee"})
@EnableJpaRepositories(basePackages = {"com.visionplanner","com.vdmbee"})
@EnableMongoRepositories (basePackages = {"com.visionplanner","com.vdmbee"})
@EntityScan(basePackages = {"com.visionplanner","com.vdmbee"})*/
public class Application {//implements CommandLineRunner{
	
	@Value("${spring.data.mongodb.uri}")
    private String connectionStr;
	
    @Autowired
    MongoProperties properties;
	
    @Value("${retry.maxAttempts}")
    Integer maxAttempts = 1;
    
	@PostConstruct
    public void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
       // System.out.println("Date in UTC: " + new Date().toString());
    }
	
	@Bean
	CommonsObjectPool2Metrics dataSourceStatusProbe() {
		return new CommonsObjectPool2Metrics();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
    @Bean
    public RetryTemplate retryTemplate() {
        Map<Class<? extends Throwable>, Boolean> exceptionsToRetry = new HashMap<>();
        exceptionsToRetry.put(MongoWriteException.class, true);
        //exceptionsToRetry.put(InvocationTargetException.class, true);
        //exceptionsToRetry.put(PersistenceException.class, true);
        //exceptionsToRetry.put(IOException.class, true);
        SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(maxAttempts, exceptionsToRetry);

        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        backOffPolicy.setBackOffPeriod(1000); // 1 second

        RetryTemplate template = new RetryTemplate();
        template.setRetryPolicy(retryPolicy);
        template.setBackOffPolicy(backOffPolicy);

        template.registerListener(new DefaultListenerSupport());

        return template;
    }

	/*@Override
	public void run(String... args) throws Exception {
		try {
			GenericObjectPoolConfig config = new GenericObjectPoolConfig();
			Integer minIdle = 50;
	        Integer maxTotal = 50;//default
	        
			String executorPoolsStatic = System.getenv("VMP_EXECUTOR_POOLS");
	        if(NumberUtils.isDigits(executorPoolsStatic)) {
	        	maxTotal = Integer.parseInt(executorPoolsStatic);
	    	}
	        String minIdleExecutorPools = System.getenv("VMP_MIN_IDLE_EXECUTOR_POOLS");
	        if(NumberUtils.isDigits(minIdleExecutorPools)) {
	        	minIdle = Integer.parseInt(minIdleExecutorPools);
	    	}
	        System.out.println("max total"+minIdleExecutorPools);
	        System.out.println("max total"+maxTotal);
	        config.setMinIdle(minIdle);
	        config.setMaxTotal(maxTotal);
	        config.setTestOnBorrow(true);
	        config.setTestOnReturn(true);
	        PythonScriptExecutor ex = new PythonExecutorPool(new PythonExecutorFactory(), config).borrowObject();
	        String[] args1 = {};
	        //Properties properties = null;
			//PythonInterpreter.initialize(System.getProperties(), properties, args1);
			//PythonInterpreter interpretor = new PythonInterpreter();
			//String baseScript = "from statistics import stdev \nimport sys \n" + 
			//		 "import json";
	       // interpretor.exec(baseScript);
	        
	        //PythonScriptExecutor ex = pool.borrowObject();
			if(!ex.isInitialized()) {
				try {
				    //String JYTHON_HOME = System.getenv("JYTHON_HOME") != null ? System.getenv("JYTHON_HOME") : "E:\\jython2.7.2";
				    String JYTHON_HOME = "E:\\jython2.7.2";
				    String JYTHON_LIB = JYTHON_HOME + File.separator + "Lib";
				    System.out.println("jythonlocation:"+JYTHON_HOME);
				    System.out.println("jythonlocationenv:"+JYTHON_HOME);
				    System.out.println("jythonlocationlib:"+JYTHON_LIB);
			        Properties properties = new Properties();
			        properties.setProperty("python.home", JYTHON_HOME);
			        properties.setProperty("python.path", JYTHON_LIB);
			        properties.put("python.console.encoding", "UTF-8"); // Used to prevent: console: Failed to install '': java.nio.charset.UnsupportedCharsetException: cp0.
			        //properties.put("python.security.respectJavaAccessibility", "false"); //don't respect java accessibility, so that we can access protected members on subclasses
			        //properties.put("python.import.site","false");
			        
			        ex.setInitProperties(properties);
			        ex.init();
			        String content = getBufferedString("E:/pythonscript.py");
			        ex.getTemplateInstanceForBigScripts(TemplateType.class, "Template_e6012d1e_806e_437f_82ca_61dc59dedec9",content);
			        System.out.println("it works");
				}catch(Exception e) {
					//logger.error("Initialization of Jython failed");
					//System.out.println("Initialization of Jython failed");
					e.printStackTrace();
					//throw e;
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getBufferedString(String path) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
		 BufferedReader reader = new BufferedReader(new FileReader(path));
	        String line = null;
	        String ls = System.getProperty("line.separator");
	        while ((line = reader.readLine()) != null) {
	        	stringBuilder.append(line);
	        	stringBuilder.append(ls);
	        	
	        }
	        // delete the last new line separator
	        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
	        reader.close();

	        
		}catch(Exception e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}*/
    
	/*@Override
	public void run(String... args) throws Exception {
		ApplicationTransactionManager.getApplicationTransaction().initializeMongoTransaction();
		ApplicationTransaction mongoTransactionManager = ApplicationTransactionManager.getApplicationTransaction();
		String entityId = "017F25CD-9DDC-CC80-C938-900752B6EAFD";
		SnapshotRepository snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
		Optional<Snapshot> optionalEntity = snapshotRepository.findById(entityId);
		Thread.sleep(80000);
        try {            
            if (optionalEntity.isPresent()) {
            	Snapshot entity = optionalEntity.get();
                System.out.println("Found entity: " + entity);
                snapshotRepository.save(entity);
            } else {
                System.out.println("Entity with ID " + entityId + " not found.");
            }
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
		ApplicationTransactionManager.getApplicationTransaction().commitTransaction();
		System.out.println("completed entity: ");
	}*/
}
