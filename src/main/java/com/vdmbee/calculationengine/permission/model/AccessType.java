package com.vdmbee.calculationengine.permission.model;

public enum AccessType {
	READ(0), WRITE(1), DELETE(2);
	
	private final int value;

	AccessType(final int newValue) {
        value = newValue;
    }

    public int getValue() { return value; }
    
    public AccessType getType() {
    	int val = getValue();
    	return AccessType.values()[val];
    }
}