package com.vdmbee.calculationengine.analyser.snapshot.repository;

import org.json.simple.JSONObject;
import org.springframework.hateoas.RepresentationModel;

public class SnapshotEventResource extends RepresentationModel<SnapshotEventResource> {
	private String tenantId;

	private String eventType;
	
	private String entityType;
	
	private String eventId;
	
	private JSONObject data ;

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}

	public JSONObject getData() {
		return data;
	}

	public void setData(JSONObject data) {
		this.data = data;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}
}
