package com.vdmbee.calculationengine.analyser.snapshot.model;

import java.util.Date;

import org.json.simple.JSONObject;

import lombok.Data;

@Data
public class SnapshotEventModel {

    private String id;

	private String tenantId;
	
	private String entityType;

	private String eventType;
	
	private String eventId;
	
	private JSONObject data ;
	
	private Date eventDate;

}

