package com.vdmbee.calculationengine.analyser.snapshot.entity;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="Category")
public class Category {

		@Id
	    private String id;

		
		private String tenantId;
		private String name;
		private CategoryType categoryType;
		private String parent;
		private double upperBound;
		private double lowerBound;
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTenantId() {
			return tenantId;
		}
		public void setTenantId(String tenantId) {
			this.tenantId = tenantId;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public CategoryType getCategoryType() {
			return categoryType;
		}
		public void setCategoryType(CategoryType categoryType) {
			this.categoryType = categoryType;
		}
		public String getParent() {
			return parent;
		}
		public void setParent(String parent) {
			this.parent = parent;
		}
		public double getUpperBound() {
			return upperBound;
		}
		public void setUpperBound(double upperBound) {
			this.upperBound = upperBound;
		}
		public double getLowerBound() {
			return lowerBound;
		}
		public void setLowerBound(double lowerBound) {
			this.lowerBound = lowerBound;
		}
		@SuppressWarnings("unchecked")
		@Override
		public String toString() {
			JSONObject obj= new JSONObject();
			obj.put("id",id);
			obj.put("tenantId",tenantId);
			obj.put("name",name);
			obj.put("categoryType",this.getCategoryType().toString());
			obj.put("parent", this.getParent());
			obj.put("upperBoundary", this.getUpperBound());
			obj.put("lowerBoundary", this.getLowerBound());
			return "[" + obj.toJSONString() + "]";
		}
		
		
}
