package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="tag")
public class Tag {

		@Id
	    private String id;

		
		private String tenantId;
		
		private String name;
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTenantId() {
			return tenantId;
		}
		public void setTenantId(String tenantId) {
			this.tenantId = tenantId;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public List<String> getValues() {
			return values;
		}
		public void setValues(List<String> values) {
			this.values = values;
		}
		private String type;
		private List<String> values;
		
		@SuppressWarnings("unchecked")
		@Override
		public String toString() {
			JSONObject obj= new JSONObject();
			obj.put("id",id);
			obj.put("tenantId",tenantId);
			obj.put("name",name);
			obj.put("type",type);
			obj.put("values", this.getValues());
			return "[" + obj.toJSONString() + "]";
		}
}
