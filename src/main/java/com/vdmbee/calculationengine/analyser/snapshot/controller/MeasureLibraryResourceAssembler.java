package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.MeasureLibrary;


public class MeasureLibraryResourceAssembler extends RepresentationModelAssemblerSupport<MeasureLibrary, MeasureLibraryResource> {

		public MeasureLibraryResourceAssembler() {
			super(MeasureLibraryController.class, MeasureLibraryResource.class);
		}
		 @Override
		 public MeasureLibraryResource toModel(MeasureLibrary library) {
		    MeasureLibraryResource resource = createModelWithId(library.getId(), library);
		    resource.setLibraryId(library.getLibraryId());
		    resource.setTenantId(library.getTenantId());
		    resource.setFileContent(library.getFileContent());
		    return resource;
		 }	

}
