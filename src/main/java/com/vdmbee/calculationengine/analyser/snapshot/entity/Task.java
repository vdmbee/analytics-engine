package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.util.ArrayList;
import java.util.List;


public interface Task {
    String name();
    List<Task> dependencies();
    void execute();
    void checkDependencies(ArrayList<Task> tasks);
}
