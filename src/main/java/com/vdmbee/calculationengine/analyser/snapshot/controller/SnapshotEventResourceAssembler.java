package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.SnapshotEvent;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotEventResource;

public class SnapshotEventResourceAssembler extends RepresentationModelAssemblerSupport<SnapshotEvent, SnapshotEventResource> {

	public SnapshotEventResourceAssembler() {
		super(SnapshotEventController.class, SnapshotEventResource.class);
	}
	 @Override
	 public SnapshotEventResource toModel(SnapshotEvent snapshotEvent) {
	
	    SnapshotEventResource resource = createModelWithId(snapshotEvent.getId(), snapshotEvent);
	    resource.setTenantId(snapshotEvent.getTenantId());
	    resource.setEventType(snapshotEvent.getEventType());
	    resource.setEventId(snapshotEvent.getEventId());
	    resource.setData(snapshotEvent.getData());
	    resource.setEntityType(snapshotEvent.getEntityType());
	    return resource;
	 }	

}
