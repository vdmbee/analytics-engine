package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Category;


public class CategoryResourceAssembler extends RepresentationModelAssemblerSupport<Category, CategoryResource> {

		public CategoryResourceAssembler() {
			super(CategoryController.class, CategoryResource.class);
		}
		 @Override
		 public CategoryResource toModel(Category category) {
		    CategoryResource resource = createModelWithId(category.getId(), category);
		    // … do further mapping
		    resource.setName(category.getName());
		    resource.setTenantId(category.getTenantId());
		    resource.setCategoryType(category.getCategoryType());
		    resource.setLowerBound(category.getLowerBound());
		    resource.setUpperBound(category.getUpperBound());
		    resource.setParent(category.getParent());
		    return resource;
		 }	

}
