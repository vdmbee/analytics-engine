package com.vdmbee.calculationengine.analyser.snapshot.entity;


public enum CategoryType {

	BUSINESS_CLASSIFICATION(0), RUBRIC_DIMENSION(1), CLASSIFICATION_CATEGORY(2),
	RUBRIC_CATEGORY(3);
	
	int categoryType;
	
	CategoryType(int CategoryType) {
		this.categoryType = CategoryType;
	}
	
	int getCategoryType() {
		return categoryType;
	}

	public static CategoryType getCategoryType(String roleName) {
		if(roleName.equals("Business Classification")) {
			return BUSINESS_CLASSIFICATION;
		}else if(roleName.equals("Rubric Dimension")) {
			return RUBRIC_DIMENSION;
		} else if(roleName.equals("Classification Category")) {
			return CLASSIFICATION_CATEGORY;
		} else if(roleName.equals("Rubric Category")) {
			return RUBRIC_CATEGORY;
		} 
		return null;

	}
}
