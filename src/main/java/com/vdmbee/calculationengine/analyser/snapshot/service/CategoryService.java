package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.client.result.DeleteResult;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Category;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.model.CategoryModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.CategoryRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.repository.PermissionRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;


@Service
public class CategoryService {
	
	@Autowired
	private Environment env;
	
	@Autowired 
	private CategoryRepository categoryRepository;
	
	
	@Autowired
	private DocumentsRepository documentRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	
	@Autowired
	private PermissionService templateService;

	@Autowired
	private PermissionService permissionService;
	
	public boolean checkAccessToTenant(String tenantId, String email) {
		if(tenantId != null) {
			boolean canAccess = permissionService.isMemberOfTenant(tenantId,email);
			if(!canAccess) {
				return false;
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				return false;
			}
		}
		return true;
	}
	public boolean checkAccessForDocument(String tenantId,String id, String email) throws AccessDeniedException {
		if(tenantId != null) {
			boolean canAccess = permissionService.checkPermission(tenantId, id, 0, email);
			if(!canAccess) {
				return false;
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				return false;
			}
		}
		return true;
	}
	
	public Category getCategoryObject(String tenantId, String id) throws AccessDeniedException {
		if(!checkAccessForDocument(tenantId,id, null)) {
			return null;
		}
		Optional<Category> categoryObj = categoryRepository.findById(id);
		Category category = null;
		if(categoryObj.isPresent()) {
			category = categoryObj.get();
		}
		return category;
	}
	
	public Category saveCategoryObjects(CategoryModel categoryModel, String tenantId,String email) throws AccessDeniedException {
		if(!this.checkAccessToTenant(tenantId,email)) {
			return null;
		}
		Category existingCategory = null;
		if(categoryModel.getId() != null) {
			Optional<Category> existCategoryObj = categoryRepository.findById(categoryModel.getId());
			if(existCategoryObj.isPresent()) {
				existingCategory = existCategoryObj.get();
			}
		}
		/*else {
			existingCategory = categoryRepository.findOneByTenantIdAndContext(category.getTenantId(),category.getContext());	
		}*/
		
		if(existingCategory != null) {
			if(permissionService.checkPermission(tenantId, categoryModel.getId(), 1, null)) {
				existingCategory.setName(categoryModel.getName());
				existingCategory.setCategoryType(categoryModel.getCategoryType());
				existingCategory.setParent(categoryModel.getParent());
				existingCategory.setLowerBound(categoryModel.getLowerBound());
				existingCategory.setUpperBound(categoryModel.getUpperBound());
				categoryRepository.save(existingCategory);
				return existingCategory;
			}else {
				return null;
			}
		}else {
			ModelMapper mapper = new ModelMapper();
			Category category = mapper.map(categoryModel, Category.class);
			if(category.getId() == null) {
				category.setId(UUID.randomUUID().toString());
			}else {
				category.setId(category.getId());
			}
			
			Documents document = new Documents();
			document.setRefdoc(category.getId());
			document.setType("category");
			templateService.addDocument(document,email);
			
			categoryRepository.save(category);
			return category;
		}
	}
	public void deleteCategory(String id, String tenantId) throws AccessDeniedException {
    	if(tenantId == null || (tenantId != null && permissionService.checkPermission(tenantId, id, 2, null))) {
	    	Query getSnapshots = new Query();
	    	getSnapshots.addCriteria(Criteria.where("categoryId").is(id));
	    	List<Snapshot> snapshotList = mongoTemplate.find(getSnapshots, Snapshot.class);
	    	for(Snapshot s : snapshotList) {
	    		templateService.deleteDocument(s.getId());
	    	}
	    	DeleteResult deletedSnapshots = mongoTemplate.remove(getSnapshots, Snapshot.class);
	    	//System.out.println(deletedSnapshots);
	    	
	    	//category deletion
	    	templateService.deleteDocument(id);
	    	Query categoryDelete = new Query();
	    	categoryDelete.addCriteria(Criteria.where("_id").is(id));
	    	DeleteResult deletedCategory = mongoTemplate.remove(categoryDelete, Category.class);
	    	//System.out.println(deletedCategory);
    	}
	}
	public Permission createNewPermission(String id, String tenantId, Permission permission,String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, id, 1, null)) {
			Documents document;
			String isLocalUserGuideId="";
			Documents doc = documentRepository.getDocumentByRefdoc(id);
	
			Category category = categoryRepository.findCategoryById(id);
			
			String tenantIdForUsers = category.getTenantId();
			String[] userIds = permission.getUserIds();
			for(int i=0;i<userIds.length ;i++) {
				isLocalUserGuideId = templateService.checkIsLocalUser(userIds[i],tenantIdForUsers,email);
				if(isLocalUserGuideId == null) {			
					return null;
				}
			}
			 
			if(doc == null) {		
				document = new Documents();
				document.setId(UUID.randomUUID().toString());
				document.setRefdoc(id);
				//document.setType(type);
				document.setType("category");
				document.setName(category.getName());
				documentRepository.save(document);
				//resourceAssembler.toResource(document);
			}else {
				document = doc;
			}
			
			if(permission.getId() == null) {
				permission.setId(UUID.randomUUID().toString());
			}else {
				permission.setId(permission.getId());
			}
			
			String[] userGuideId = {isLocalUserGuideId};
			permission.setUserIds(userGuideId);
			//permission.setUserId(email);
			//permission.setDocument(doc);
			permissionRepository.save(permission);
			return permission;
		}else {
			return null;
		}
	}
	
}
