package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vdmbee.calculationengine.analyser.snapshot.entity.SnapshotEvent;
import com.vdmbee.calculationengine.analyser.snapshot.model.SnapshotEventModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotEventRepository;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotEventResource;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class SnapshotEventController {
	private static Logger logger = LogManager.getLogger(SnapshotEventController.class);
	@Autowired
	private PagedResourcesAssembler<SnapshotEvent> pagedAssembler;
	@Autowired 
	private SnapshotEventRepository snapshotEventRepository;
	
	@GetMapping(path = "/snapshotevent/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
    //@PreAuthorize("hasPermission('Address', 'create')")
	public SnapshotEventResource getSnapshotEvent(SnapshotEventResourceAssembler snapshotEventResourceAssembler, @PathVariable("id") String id,@RequestParam("tenantId") String tenantId) {
		SnapshotEventResource resource = null;
		Optional<SnapshotEvent> snapshotEventObj = snapshotEventRepository.findById(id);
		SnapshotEvent snapshotEvent = null;
		if(snapshotEventObj.isPresent()) {
			snapshotEvent = snapshotEventObj.get();
		}
		if(snapshotEvent != null) {
			resource = snapshotEventResourceAssembler.toModel(snapshotEvent);
		}
		return resource;
	}
	
	@GetMapping(path = "/snapshotevent",produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
    //@PreAuthorize("hasPermission('Address', 'create')")
	public PagedModel<SnapshotEventResource> getAllSnapshotEvent(SnapshotEventResourceAssembler snapshotEventResourceAssembler,Pageable pageable,@RequestParam("tenantId") String tenantId) {
    	List<SnapshotEvent> snapshotEvents = snapshotEventRepository.findAll();
    	return pagedAssembler.toModel(new PageImpl<SnapshotEvent>(snapshotEvents, pageable, snapshotEvents.size()),new Assembler());
	}
	class Assembler extends RepresentationModelAssemblerSupport<SnapshotEvent, SnapshotEventResource> {

		public Assembler() {
			super(SnapshotEventController.class, SnapshotEventResource.class);
		}

		@Override
		public SnapshotEventResource toModel(SnapshotEvent auditRecord) {
			try {
				return createModelWithId(auditRecord.getId(), auditRecord);
			}
			catch (IllegalStateException e) {
				logger.warn("Failed to create StreamDefinitionResource. " + e.getMessage());
			}
			return null;
		}

		@Override
		public SnapshotEventResource instantiateModel(SnapshotEvent snapshotEvent) {
			final SnapshotEventResource resource = new SnapshotEventResource();
			resource.setEventId(snapshotEvent.getId());
			resource.setEntityType(snapshotEvent.getEventType());
			resource.setData(snapshotEvent.getData());
			resource.setEventType(snapshotEvent.getEventType());
			resource.setTenantId(snapshotEvent.getTenantId());
			return resource;
		}

	}
	@PostMapping(path = "/snapshotevent", consumes = "application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
    public @ResponseBody PersistentEntityResource addNewSnapshotEvent( PersistentEntityResourceAssembler resourceAssembler,@RequestBody SnapshotEventModel snapshotEventModel,@RequestParam("tenantId") String tenantId){
		SnapshotEvent existingSnapshotEvent = null;
		if(snapshotEventModel.getId() != null) {
			existingSnapshotEvent = snapshotEventRepository.findOneByTenantIdAndId(snapshotEventModel.getTenantId(),snapshotEventModel.getId());
		}else if(snapshotEventModel.getEventId() != null) {
			existingSnapshotEvent = snapshotEventRepository.findOneByTenantIdAndEventId(snapshotEventModel.getTenantId(),snapshotEventModel.getEventId());	
		}
		if(existingSnapshotEvent != null) {
			existingSnapshotEvent.setData(snapshotEventModel.getData());
			existingSnapshotEvent.setEventDate(new Date());
			snapshotEventRepository.save(existingSnapshotEvent);
			return resourceAssembler.toModel(existingSnapshotEvent);
		}else {
			ModelMapper mapper = new ModelMapper();
			mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			SnapshotEvent snapshotEvent = mapper.map(snapshotEventModel, SnapshotEvent.class);
			if(snapshotEvent.getId() == null) {
				snapshotEvent.setId(UUID.randomUUID().toString());
			}else {
				snapshotEvent.setId(snapshotEvent.getId());
			}
			snapshotEventRepository.save(snapshotEvent);
			return resourceAssembler.toModel(snapshotEvent);
		}
    }
	
	/*@RequestMapping(path = "/snapshotevent/generate", method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody PersistentEntityResource generateSnapshotBasedOnEventsForSales( PersistentEntityResourceAssembler resourceAssembler,@RequestBody SnapshotConfiguration config, @RequestParam(value="tenantId", required=false) String tenantId, @RequestParam(value="templateId", required=false) String templateId) {
		Snapshot snapshot= null;
		if(config.getProviderConfig().equals("JDBCProvider")) {
			snapshot = dataRepository.generateSnapshotBasedOnEvents(config, tenantId, templateId);
		}
		return resourceAssembler.toModel(snapshot);
    }*/

}
