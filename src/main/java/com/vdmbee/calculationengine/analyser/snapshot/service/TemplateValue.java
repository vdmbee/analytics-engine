package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;
import org.springframework.util.StringUtils;

public class TemplateValue {
	private boolean calculatedValue;
	private List<String> dependentValues;
	private List<String> rubricsToCalculate;
	private String script;
	private List<String> measures;
	private String id;
	
	public List<String> getMeasures() {
		return measures;
	}

	public void setMeasures(List<String> measures) {
		this.measures = measures;
	}

	public TemplateValue(JSONObject valueNode){
		this.id = (String)valueNode.get("id");
		this.calculatedValue = valueNode.get("ValueType").equals("Calculated");
		this.measures = (List<String>)valueNode.get("measures");
		if(calculatedValue) {
			this.dependentValues = new ArrayList<String>();
			JSONObject formulaNode = (JSONObject) valueNode.get("Formula");
			if(formulaNode.containsKey("dependentValues")){
				this.dependentValues.addAll((List<String>)formulaNode.get("dependentValues"));
			}
			if(((String)formulaNode.get("expression")).contains("pythonExecutor")) {
				this.script = (String)formulaNode.get("pythonScript");	
			}
			if(formulaNode.containsKey("rubricsToCalculate")){
				this.rubricsToCalculate = new ArrayList<String>();
				this.rubricsToCalculate.addAll((List<String>)formulaNode.get("rubricsToCalculate"));
			}
		}
	}
	
	public static LinkedHashMap<String,TemplateValue> getNodes(List<JSONObject> fileContent){
		LinkedHashMap<String,TemplateValue> nodes = new LinkedHashMap<String, TemplateValue>();
		for(int i=0;i<fileContent.size();i++) {
			TemplateValue node = new TemplateValue((JSONObject)fileContent.get(i));
			nodes.put(node.getId(), node);
		}
		return nodes;
	}

	public static String getSingleScript(String templateId, LinkedHashMap<String,TemplateValue> nodes) {
		StringBuffer calFunScript = new StringBuffer();
		StringBuffer calFunCallsScript = new StringBuffer();
		HashMap<String,String> shortVars = new HashMap<String,String>();
		Iterator<String> keys = nodes.keySet().iterator();
		LinkedHashMap<String,TemplateValue> calculatedNodes = new LinkedHashMap<>();
		LinkedHashMap<String,TemplateValue> usedNodes = new LinkedHashMap<>();
		while(keys.hasNext())
		{
			String key = keys.next(); 
			TemplateValue valueNode = nodes.get(key);
			if(!valueNode.isCalculatedValue()) {
				usedNodes.put(valueNode.getId(), valueNode);
			}else {
				calculatedNodes.put(valueNode.getId(), valueNode);
			}
		}
		//StringBuffer inputsScript = new StringBuffer();
		Iterator<String> inputDeps =  getInputDependentNodes(nodes).iterator();
		int counter = 0;
		int fnCounter = 0;
		while(inputDeps.hasNext()) {
			if(counter % 500 == 0) {
				calFunCallsScript.insert(0,"\t\tTemplate.hc(self,self.__class__.setInstanceValuesf" + fnCounter + ",\"setInstanceValuesf" + fnCounter + "\",\"setInstanceValuesf" + fnCounter + "\")\n");
				calFunScript.append("\n\tdef setInstanceValuesf"+fnCounter+"(self):\n");
				fnCounter++;
			}
			TemplateValue valueNode = nodes.get(inputDeps.next());
			addScriptForInputNode(valueNode,calFunScript,shortVars);
			counter++;
		}
		Iterator<String> calculatedNodesIter = calculatedNodes.keySet().iterator();
		while(calculatedNodesIter.hasNext()) {
			String calValKey = calculatedNodesIter.next();
			if(!usedNodes.containsKey(calValKey)) {
				getCalculatedScript(calValKey,nodes,usedNodes,calFunScript,shortVars,calFunCallsScript);
			}
		}
		//String executeValueFormulaScript = script.toString().replaceAll("[\n]", "\n\t\t");
		StringBuffer baseScript = new StringBuffer(); 
		generateStandardPart(templateId,baseScript);
		//baseScript.append(inputsScript);
		baseScript.append(calFunCallsScript);
		baseScript.append("\n");
		baseScript.append("\t\t" + "return self.returnValues"  );
		baseScript.append("\n");
		baseScript.append(calFunScript);
		//baseScript.append("\n\t\t" + script);
		
		return baseScript.toString(); 
	}
	private static Set<String> getInputDependentNodes(HashMap<String,TemplateValue> nodes){
		Set<String> ret = new HashSet<String>();
		Iterator<String> nodeIteraator = nodes.keySet().iterator();
		while(nodeIteraator.hasNext()) {
			String nodeKey = nodeIteraator.next();
			TemplateValue node = nodes.get(nodeKey);
			List<String> dependents = node.getDependentValues();
			if(dependents != null) {
				for(int i=0;i<dependents.size();i++) {
					String depKey = dependents.get(i);
					if(!ret.contains(depKey) && nodes.get(depKey) != null && !nodes.get(depKey).isCalculatedValue()) {
						ret.add(depKey);
					}
				}
			}
		}
		return ret;
	}
	
	private static StringBuffer generateStandardPart(String templateId,StringBuffer script) {
		script.append("import org.json.simple.JSONObject\n");
		script.append("import json\n");
		script.append("import com.vdmbee.calculationengine.analyser.formula.PythonExecutor\n");
		script.append("\n");
		//String escKey = templateId.replaceAll("[#@-]", "_");
		script.append("class " + templateId + "(Template):\n");
		script.append("\tdef executeValueFormula(self,executor):\n");
		return script;
	}
	private static StringBuffer addScriptForInputNode(TemplateValue inputValue, StringBuffer script,HashMap<String,String> shortVars) {
		String key = inputValue.getId();
		String var = shortVars.get(key);
		if(var == null) {
			var = "v" + shortVars.size();
			shortVars.put(key, var);
		}
		String escKey = "self.val_" + key.replaceAll("[#@-]", "_");
		script.append("\t\tself." + var + " = " + escKey + "\n");
		return script;
	}
	private static StringBuffer getCalculatedScript(String key ,HashMap<String,TemplateValue> nodes,HashMap<String,TemplateValue> usedNodes,StringBuffer script,HashMap<String,String> shortVars,StringBuffer calFunCallsScript) {
		TemplateValue calculatedNode = nodes.get(key);
		if(calculatedNode != null) {
			usedNodes.put(key, calculatedNode);
			List<String> dependencies = calculatedNode.getDependentValues();
			//String escKey = "val_" + key.replaceAll("[#@-]", "_");
			String var = shortVars.get(key);
			if(var == null) {
				if(key.equalsIgnoreCase("benchmark")) {
					var = "vb";
					shortVars.put(key, var);
				}else {
					var = "v" + shortVars.size();
					shortVars.put(key, var);
				}
			}
			
			String nodeScript = calculatedNode.getScript();
			int measureSeparator = calculatedNode.getId().indexOf("_");
			String measure = null;
			if(measureSeparator > 0) {
				measure = calculatedNode.getId().substring(0, measureSeparator);
			}
			
			for(int i=0;i<dependencies.size();i++) {
				String dependentNodeKey = dependencies.get(i);
				String depVar = "";
				if(!StringUtils.isEmpty(measure)) {
					depVar = shortVars.get(measure+"_"+dependentNodeKey);
					dependentNodeKey = measure+"_"+dependentNodeKey;
				}else {
					depVar = shortVars.get(dependentNodeKey);
				}
				if(depVar == null) {
					if(key.equalsIgnoreCase("benchmark")) {
						depVar = "vb";
						shortVars.put(dependentNodeKey, depVar);
					}else {
						depVar = "v" + shortVars.size();
						shortVars.put(dependentNodeKey, depVar);
					}
				}
				//String escDepNodeKey = "val_" + dependentNodeKey.replaceAll("[#@-]", "_");
				/*TemplateValue depNode = nodes.get(dependentNodeKey);
				if(!depNode.isCalculatedValue()) {
					depVar = "self." + depVar;
				}*/
				nodeScript = nodeScript.replaceAll("returnValues\\[\"" + dependentNodeKey + "\"\\]", "self." + shortVars.get(dependentNodeKey) );
				nodeScript = nodeScript.replaceAll(dependentNodeKey, "self." + depVar);
				if(usedNodes.containsKey(dependentNodeKey)) {
					continue;
				}
				getCalculatedScript(dependentNodeKey, nodes, usedNodes, script,shortVars,calFunCallsScript);
			}
			script.append("\tdef f" + var + "(self):\n");
			//Template.hc(self,ChildTemplate.f1,"a")
			if(!key.equalsIgnoreCase("benchmark")) {
				calFunCallsScript.append("\t\tTemplate.hc(self,self.__class__.f" + var + ",\"" + key + "\",\"" + var + "\")\n");	
			}
			
			nodeScript = nodeScript.toString().replaceAll("[\n]", "\n\t\t");
			//nodeScript = nodeScript.replaceAll("\\binvalidDatasets\\b", "self.invalidDs" );
			//nodeScript = nodeScript.replaceAll("\\bdatasets\\b", "self.ds");
			nodeScript = nodeScript.replaceAll("\\breturnValues\\b", "self." + var );
			nodeScript = nodeScript.replaceAll("\\breturnValue\\b", "self." + var );
			//nodeScript = nodeScript.replaceAll("\\\\n ", System.lineSeparator());
			script.append("\t\t" + nodeScript);
			script.append("\n");
			script.append("\t\tself.returnValues[\"" + key + "\"] = self." + var + "\n");
			if(key.equalsIgnoreCase("benchmark")) {
				Iterator<String> iterShortVars = shortVars.keySet().iterator();
				int counter = 0;
				int fnCounter = 0;
				while(iterShortVars.hasNext()) {
					String shortKey = iterShortVars.next();
					if(!shortKey.equalsIgnoreCase("benchmark")) {
						if(counter % 10 == 0) {
							calFunCallsScript.insert(0,"\t\tTemplate.hc(self,self.__class__.setInstanceValuesf" + fnCounter + ",\"setInstanceValuesf" + fnCounter + "\",\"setInstanceValuesf" + fnCounter + "\")\n");
							script.append("\n\tdef setInstanceValuesf"+fnCounter+"(self):\n");
							fnCounter++;
						}
						script.append("\t\tself." + shortVars.get(shortKey) +"=get_key(self."+var+",\"" + shortKey + "\")  \n");
						counter++;
					}
				}
				calFunCallsScript.insert(0,"\t\tTemplate.hc(self,self.__class__.f" + var + ",\"" + key + "\",\"" + var + "\")\n");
			}
			script.append("\n");
		}
		return script;
	}
	
	public boolean isCalculatedValue() {
		return calculatedValue;
	}

	public void setCalculatedValue(boolean calculatedValue) {
		this.calculatedValue = calculatedValue;
	}

	public List<String> getDependentValues() {
		return dependentValues;
	}

	public void setDependentValues(List<String> dependentValues) {
		this.dependentValues = dependentValues;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getRubricsToCalculate() {
		return rubricsToCalculate;
	}

	public void setRubricsToCalculate(List<String> rubricsToCalculate) {
		this.rubricsToCalculate = rubricsToCalculate;
	}
	
	
}
