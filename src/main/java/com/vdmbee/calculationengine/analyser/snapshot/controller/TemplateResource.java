package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.hateoas.RepresentationModel;

public class TemplateResource extends RepresentationModel<TemplateResource> {
	private String tenantId;
	private String context;
	private String name;
	private List<JSONObject> tags;
	
    public String getName() {
		return name;
	}
	public List<JSONObject> getTags() {
		return tags;
	}
	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getContext() {
		return context;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public void setContext(String context) {
		this.context = context;
	}
	private List<JSONObject> fileContent;
	public List<JSONObject> getFileContent() {
		return fileContent;
	}
	public void setFileContent(List<JSONObject> fileContent) {
		this.fileContent = fileContent;
	}
}
