package com.vdmbee.calculationengine.analyser.snapshot.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Tag;

@Component
@RepositoryRestResource(collectionResourceRel = "tag", path = "tag")
public interface TagRepository extends MongoRepository<Tag, String> {

	Tag findTagById(String id);
	
	@Query("{ '_id' : { '$in': ?0 }}")
	Page<Tag> getTagsByDocument(String[] docIds, Pageable pageable);
	
	@Query("{ 'tenantId' : ?0 }")
	Page<Tag> getTagWithTenantId(String tenantId, Pageable pageable);
	
}
