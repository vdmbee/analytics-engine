package com.vdmbee.calculationengine.analyser.snapshot.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Category;

@Component
@RepositoryRestResource(collectionResourceRel = "category", path = "category")
public interface CategoryRepository extends MongoRepository<Category, String> {

	Category findCategoryById(String id);
	
	@Query("{ '_id' : { '$in': ?0 }}")
	Page<Category> getCategorysByDocument(String[] docIds, Pageable pageable);
	
	@Query("{ 'tenantId' : ?0 }")
	Page<Category> getCategoryWithTenantId(String tenantId, Pageable pageable);
	
}
