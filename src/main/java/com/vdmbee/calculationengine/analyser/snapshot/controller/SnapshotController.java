package com.vdmbee.calculationengine.analyser.snapshot.controller;


import java.io.IOException;
import java.util.List;
import java.util.Optional;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.xml.sax.SAXException;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.model.SnapshotModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class SnapshotController {

	/*@Autowired
	private PagedResourcesAssembler<Snapshot> pagedAssembler;
	@Autowired 
	private SnapshotRepository snapshotRepository;
	@Autowired 
	private TemplateRepository templateRepository;
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	
	@Autowired
	private SnapshotService snapshotService;
	
	@Autowired
	private PermissionService permissionService;
	
	
	@GetMapping(path = "/snapshot/{id}", consumes = "application/json")
	public @ResponseBody SnapshotResource getSnapshot(SnapshotResourceAssembler snapshotResourceAssembler, @PathVariable("id") String id,@RequestParam(value = "tenantId") String tenantId)throws AccessDeniedException {
		if(snapshotService.checkAccessForDocument(tenantId,id,null)) {
			
		
		Optional<Snapshot> snapshotObj = snapshotRepository.findById(id);
		Snapshot snapshot = null;
		if(snapshotObj.isPresent()) {
			snapshot = snapshotObj.get();
		}
		return snapshot != null ? snapshotResourceAssembler.toModel(snapshot):null;
		} else {
			return null;
		}
	}*/
	
	
	/*@RequestMapping(path = "/snapshot", method = RequestMethod.GET,consumes = "application/json",produces = MediaType.APPLICATION_JSON_VALUE)
	public PagedResources<SnapshotResource> getAllSnapshot(SnapshotResourceAssembler snapshotResourceAssembler,Pageable pageable,@RequestParam(value= "tenantId",required= false) String tenantId) {
		Page<Snapshot> snapshots = snapshotRepository.findAll(pageable);
    	PagedResources<SnapshotResource> pagedSnapshotResource = pagedAssembler.toModel(snapshots,snapshotResourceAssembler);
        return pagedSnapshotResource;
	}*/
	
	/*@GetMapping(path = "/snapshot", consumes = "application/json")
	@ResponseBody
	public Page<JSONObject> getSnapshots(@RequestParam("tenantId") String tenantId, Pageable pageable) throws  ParseException {
		List<JSONObject> templateList = permissionService.getdocs(tenantId, "snapshot", null,pageable);
		if(templateList.size()>0) {
			int start = (int) pageable.getOffset();
			int end = (start + pageable.getPageSize()) > templateList.size() ? templateList.size()
					: (start + pageable.getPageSize());
			return new PageImpl<JSONObject>(templateList.subList(start, end), pageable, templateList.size());
		}else {
			return null;
		}
	}*/
	
	/*@RequestMapping(path = "/snapshot", method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody PersistentEntityResource addNewSnapshot( PersistentEntityResourceAssembler resourceAssembler,@RequestBody Snapshot snapshot){
		snapshot.setId(UUID.randomUUID().toString());
		snapshotRepository.save(snapshot);
		return resourceAssembler.toModel(snapshot);
    }*/
	
	/*@RequestMapping(path = "/snapshot/sales/generate", method = RequestMethod.POST,consumes = "application/json")
    public @ResponseBody PersistentEntityResource generateNewSnapshot( PersistentEntityResourceAssembler resourceAssembler,@RequestBody SnapshotConfiguration config){
		Snapshot snapshot= null;
		if(config.getProviderConfig().equals("JDBCProvider")) {
			snapshot = dataRepository.generateSnapshot(config);
		}
		return resourceAssembler.toModel(snapshot);
    }*/
	

	/*@PostMapping(path = "/snapshot", consumes = "application/json")
    public @ResponseBody PersistentEntityResource createSnapshotForPlan( PersistentEntityResourceAssembler resourceAssembler,@RequestBody SnapshotModel snapshotModel, @RequestParam("tenantId") String tenantId) throws Exception{
		if(permissionService.checkPermission(tenantId, snapshotModel.getTemplateId(), 1, null)) {
			Snapshot newSnapshot = snapshotService.saveSnapshot(snapshotModel,null,null);
			return newSnapshot != null?resourceAssembler.toModel(newSnapshot):null;
		}
		return null;
    }
	
	@PutMapping(path = "/snapshot/{id}",consumes = "application/json")
    public @ResponseBody PersistentEntityResource updateSnapshotForPlan( PersistentEntityResourceAssembler resourceAssembler,@PathVariable("id") String id,@RequestBody SnapshotModel snapshotModel, @RequestParam("tenantId") String tenantId) throws ParseException,AccessDeniedException, InterruptedException{
		if(permissionService.checkPermission(tenantId, id, 1, null)) {
			
			Snapshot newSnapshot = snapshotService.updateSnapshot(id,snapshotModel);
			return newSnapshot != null?resourceAssembler.toModel(newSnapshot):null;
		}else {
        	return null;
        }
    }
	
	@DeleteMapping(path = "/snapshot/{id}", consumes = "application/json")
	@ResponseBody
	public void deleteSnapshot(@PathVariable("id") String id, @RequestParam("tenantId") String tenantId)throws AccessDeniedException {
		snapshotService.deleteSnapshot(id, tenantId, null);	
	}
	


	@PostMapping(path = "/snapshot/xbrl/{id}", consumes = "application/json")
	public @ResponseBody PersistentEntityResource createSnapshotForXBRL( PersistentEntityResourceAssembler resourceAssembler,@PathVariable("id") String id,@RequestBody XBRLTransformation xbrlConfig,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException,TransformerException, IOException, ParserConfigurationException, SAXException, ParseException, InterruptedException{
		if(permissionService.checkPermission(tenantId, xbrlConfig.getTemplateId(), 1, null)) {
			Snapshot snapshot = snapshotService.saveSnapshotForXBRL(xbrlConfig,id,null);
			return resourceAssembler.toModel(snapshot);
		}else {
			return null;
		}
    }
	
	@GetMapping(path = "snapshot/byTemplate/{id}/{tenantId}", consumes = "application/json")
	@ResponseBody
	public Page<JSONObject> getSnapshotsByTemplates(@PathVariable("id") String id, Pageable pageable,@PathVariable(value = "tenantId") String tenantId) throws  ParseException, AccessDeniedException {
		return permissionService.getSnapshotsByTemplateId(id,tenantId, pageable,null);
	}
	
	
	@PostMapping(path = "snapshot/byRubricInSnapshots", consumes = "application/json")
	@ResponseBody
	public List<JSONObject> getRubricValueInSnapshots(@RequestBody RubricSnapshotsModel rubricSnapshotModel, @RequestParam(value = "tenantId") String tenantId) throws  ParseException, AccessDeniedException {
		return permissionService.getRubricValueInSnapshots2(rubricSnapshotModel.getRubricId(),rubricSnapshotModel.getSnapshots(),tenantId,null);
	}
	@PatchMapping(path = "/snapshot/{id}", consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'snapshot', null)")
	@ResponseBody
	public void patchSnapshot(@PathVariable("id") String id) {};*/
	
	
}
