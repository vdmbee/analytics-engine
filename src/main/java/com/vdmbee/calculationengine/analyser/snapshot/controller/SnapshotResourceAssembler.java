package com.vdmbee.calculationengine.analyser.snapshot.controller;


import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;


public class SnapshotResourceAssembler extends RepresentationModelAssemblerSupport<Snapshot, SnapshotResource> {

	public SnapshotResourceAssembler() {
		super(SnapshotController.class, SnapshotResource.class);
	}
	 @Override
	 public SnapshotResource toModel(Snapshot snapshot) {
	
	    SnapshotResource resource = createModelWithId(snapshot.getId(), snapshot);
	    // … do further mapping
	    resource.setContext(snapshot.getContext());
	    resource.setTenantId(snapshot.getTenantId());
	    resource.setName(snapshot.getName());
	    resource.setPeriod(snapshot.getPeriod());
	    resource.setUnit(snapshot.getUnit());
	    resource.setTags(snapshot.getTags());
	    resource.setFileContent(snapshot.getFileContent());
	    resource.setDataParameters(snapshot.getDataParameters());
	    if(snapshot.getTemplate() != null) {
	    	resource.setTemplateId(snapshot.getTemplate().getId());	
	    }
	    
	    return resource;
	 }	
}