package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityNotFoundException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.result.DeleteResult;
import com.vdmbee.calculationengine.analyser.snapshot.controller.XBRLTransformation;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.model.SnapshotModel;
import com.vdmbee.calculationengine.analyser.snapshot.model.TagModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.util.Timming;

@Service
public class SnapshotService {
	
	private static final String AGG_METHOD_SUM = "SUM";
	private static final String AGG_METHOD_AVERAGE = "AVERAGE";
	private static final String AGG_METHOD_LATEST = "LATEST";
	
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	@Autowired 
	private TemplateRepository templateRepository;
	//@Autowired
	//private ILegalEntityRepository legalEntityRepository;
	//@Autowired
	//private DocumentsRepository documentsRepository;
	@Autowired
	private PermissionService permissionService;
	@Autowired
	private SnapshotRepository snapshotRepository;
	
	/*
	 * @Autowired private DataRepository dataRepository;
	 */
	@Autowired
	private Environment env;
	@Autowired
	private TemplateService templateService;
	
	private static Logger logger = LogManager.getLogger(SnapshotService.class);

	
	public List<Snapshot> getSnapshotsWithTags(List<JSONObject> tags) {
		MatchOperation tagsFilter = Aggregation.match(Criteria.where("tags").all(tags));
		Aggregation aggregation = Aggregation.newAggregation(tagsFilter).withOptions(Aggregation.newAggregationOptions().cursor(new Document()).build());
		List<Snapshot> snapshots= mongoTemplate.aggregate(aggregation, "snapshot", Snapshot.class).getMappedResults();
		return snapshots;
	}
	
	public void updateTagsInSnapshot(TagModel tag) {
		List<Template> templateList = templateRepository.getTemplatesWithTag(tag.getId());
		//System.out.println(templateList);
		for(Template t: templateList) {
			List<JSONObject> tagList = t.getTags();
			for(JSONObject obj: tagList) {
				if(obj.get("id").equals(tag.getId())) {
					obj.put("name", tag.getName());
					obj.put("values", tag.getValues());
				}
			}
			templateRepository.save(t);
		}
	}
	
	public void editTemplateId(String id, String templateId, String snapshotId) {
		//old snapshot deletion and legal entity chart id updation
		/*ILegalEntity entity = authenticationFacade.findOneLegalEntity(id);
		String entitySnapshotId = entity.getChartTemplateId();
		if(entitySnapshotId != null) {
			Documents doc = documentsRepository.getDocumentById(entitySnapshotId);
			if(doc != null) {
				Snapshot snapshot = snapshotRepository.getSnapshotById(doc.getRefdoc());
				if(snapshot != null) {
					snapshotRepository.delete(snapshot);
				}
				documentsRepository.delete(doc);
			}
		}
		Documents newDoc = documentsRepository.getDocumentByRefdoc(snapshotId);
		entity.setChartTemplateId(newDoc.getId());
		//authenticationFacade.saveLegalEntity(entity);
		
		//old template deletion
		Query getChartTemplate = new Query();
		getChartTemplate.addCriteria(new Criteria().andOperator(Criteria.where("tenantId").is(entity.getName()), Criteria.where("name").is("legal entity charts")));
		List<Template> templates = mongoTemplate.find(getChartTemplate, Template.class);
		for(Template t: templates) {
			if(!(t.getId().equals(templateId))) {
				mongoTemplate.remove(t, "template");
				permissionService.deleteDocument(t.getId());
			}
		}*/
	}
	
	
	public boolean checkAccessForDocument(String tenantId,String id, String email) throws AccessDeniedException {
		if(tenantId != null) {
			boolean canAccess = permissionService.checkPermission(tenantId, id, 0, email);
			if(!canAccess) {
				throw new AccessDeniedException(Snapshot.class,"id","Invalid Tenant");
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				throw new AccessDeniedException(Snapshot.class,"id","Invalid Tenant");
			}
		}
		return true;
	}
	
	private static String getValue(String tag, Element element) {
		NodeList tagNodes = element.getElementsByTagName(tag);
		if(tagNodes != null && tagNodes.getLength()>0) {
			NodeList nodes = element.getElementsByTagName(tag).item(0).getChildNodes();
			Node node = (Node) nodes.item(0);
			return node.getNodeValue();
		}else {
			return null;
		}
	}

	public Snapshot saveSnapshotForXBRL(XBRLTransformation xbrlConfig,String tenantId,String email) throws ParserConfigurationException, TransformerException, ParseException, SAXException, IOException, InterruptedException, AccessDeniedException {
		Optional<Template> templateObj = templateRepository.findById(xbrlConfig.getTemplateId());
		Template template = null;
		if(templateObj.isPresent()) {
			template = templateObj.get();
		}
		
        if(template != null) {
    		Snapshot snapshot = new Snapshot();
    		if(xbrlConfig.getId() == null) {
    			snapshot.setId(UUID.randomUUID().toString());
    		}else {
    			snapshot.setId(xbrlConfig.getId());
    		}
    		
            TransformerFactory factory = TransformerFactory.newInstance();
            factory.setFeature(javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING, true);

            // Additional configurations to prevent XXE attacks
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");

            Source xslt = new StreamSource(new StringReader(xbrlConfig.getXsltDocument()));
            Transformer transformer = factory.newTransformer(xslt);
            String xsltParamStr = xbrlConfig.getXsltParameters();
            JSONObject xsltParams;
            if(xsltParamStr != null && !xsltParamStr.equals("")) {
            	JSONParser parser = new JSONParser();
            	xsltParams = (JSONObject) parser.parse(xsltParamStr);
            	Iterator keys = xsltParams.keySet().iterator();
            	while(keys.hasNext()) {
            		String param  = (String)keys.next();
            		transformer.setParameter(param, xsltParams.get(param));
            	}
            }
            Source xbrl = new StreamSource(new StringReader(xbrlConfig.getXbrlDocument()));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Result result = new StreamResult(baos);
            
            transformer.transform(xbrl, result);
            String resultStr = new String(baos.toByteArray());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            dbFactory.setFeature(javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING, true);
            dbFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            dbFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            dbFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            dbFactory.setNamespaceAware(true);
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            org.w3c.dom.Document doc = dBuilder.parse(new ByteArrayInputStream(resultStr.getBytes(StandardCharsets.UTF_8)));
            doc.getDocumentElement().normalize();
            List<JSONObject> fileContentObj = new ArrayList();
            
            NodeList nodes = doc.getElementsByTagName("value");
            List<JSONObject> templateContent = template.getFileContent();
            for (int i = 0; i < nodes.getLength(); i++) {
            	Node node = nodes.item(i);
            	JSONObject valueObj = new JSONObject();
            	if (node.getNodeType() == Node.ELEMENT_NODE) {
            		Element element = (Element) node;
	            	valueObj.put("ValueName",getValue("ValueName", element));
	            	valueObj.put("Value",getValue("Value", element));
	            	valueObj.put("Unit",getValue("Unit", element));
	            	//String elementId = getValue("id",element);
	            	String valueId = null;
	            	/*if(elementId != null) {
	            		valueId = getValue("id", element);
	            		valueObj.put("id",valueId);	
	            	}else {*/
	            		//Iterator<String> keys = templateContent.keySet().iterator();
	            		//while(keys.hasNext()) {
	            		for(int j=0;j<templateContent.size();j++) {
	            			JSONObject templateValue = new JSONObject((Map)templateContent.get(i));
	            			if(((String)templateValue.get("ValueName")).equals((String)valueObj.get("ValueName"))) {
	            				valueId = (String)templateValue.get("id");
	            				valueObj.put("id",valueId);
	            				valueObj.put("ValueType", (String)templateValue.get("ValueType"));
	            				if(valueObj.get("Unit") == null) {
	            					valueObj.put("Unit", (String)templateValue.get("Unit"));	
	            				}
	            				break;
	            			}
	            		}
	            	//}
	            	if(valueId != null) {
	            		fileContentObj.add(valueObj);	
	            	}
            	}
           }
            snapshot.setTenantId(tenantId);
            snapshot.setTemplateId(xbrlConfig.getTemplateId());
            snapshot.setFileContent(fileContentObj);
            snapshot.setTags(xbrlConfig.getTags());
            NodeList nameNodes = doc.getElementsByTagName("name");
            if(nameNodes.getLength() >0) {
            	Node nameNode = nameNodes.item(0);
            	snapshot.setName(nameNode.getNodeValue() + " snapshot:" + xbrlConfig.getName());
            }else {
            	snapshot.setName(xbrlConfig.getName());	
            }
            
            snapshot.setPeriod(xbrlConfig.getPeriod());
            snapshot.setUnit(xbrlConfig.getUnit());
            snapshot.setContext(xbrlConfig.getContext());
            snapshot.update(templateRepository,null,null,true,null,true);
           
    		snapshotRepository.save(snapshot);
    		Documents document = new Documents();
    		document.setRefdoc(snapshot.getId());
    		document.setType("snapshot");
    		permissionService.addDocument(document,email);
    		return snapshot;
    		
        }else {
        	logger.error("Template not found");
        	throw new EntityNotFoundException("Template not found");
        }
		
	}
	
	public JSONObject saveSnapshotFromQueue(JSONObject jsonObj) throws Exception {
		try {
			String email = (String) jsonObj.get("email");
			long lastCalculated = 0;
			if(jsonObj.get("lastCalculated") != null) {
				lastCalculated = (long) jsonObj.get("lastCalculated");
			}
			JSONObject data = (JSONObject) jsonObj.get("data");
			SnapshotModel snapshotObj = fillSnapshotObject(data);
			if(permissionService.checkPermission((String)data.get("tenantId"), snapshotObj.getTemplateId(), 1, email)) {
				Snapshot snapshot = saveSnapshot(snapshotObj, email, lastCalculated);
		        
		        JSONObject jsonResponse = getJsonResponseSnapshotObject(snapshot);
		        return jsonResponse;
			}
			return null;
		}catch(Exception e) {
			logger.error("Error while performing saveSnapshotFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public SnapshotModel fillSnapshotObject(JSONObject data) {
		SnapshotModel snapshotObj = new SnapshotModel();
		if(!StringUtils.isEmpty(data.get("tags"))) {//for vdmbee
			List<JSONObject> tagList = new ArrayList<JSONObject>();
			tagList.addAll((List<JSONObject>)data.get("tags"));
			snapshotObj.setTags(tagList);
		}
		snapshotObj.setId((String) data.get("id"));
		snapshotObj.setTenantId((String) data.get("tenantId"));
		snapshotObj.setName((String) data.get("name"));
		snapshotObj.setUnit((String) data.get("unit"));
		if(!StringUtils.isEmpty(data.get("period"))) {
			snapshotObj.setPeriod(data.get("period").toString());
		}
		if(!StringUtils.isEmpty(data.get("persistSnapshot")) && data.get("persistSnapshot") instanceof Boolean) {
			snapshotObj.setPersistSnapshot((Boolean) data.get("persistSnapshot"));
		}
		snapshotObj.setContext((String) data.get("context"));
		snapshotObj.setTemplateId((String) data.get("templateId"));
		if(!StringUtils.isEmpty(data.get("dataParameters"))) {
			snapshotObj.setDataParameters((JSONObject) data.get("dataParameters"));
		}
		snapshotObj.setValidationTemplateId((String) data.get("validationTemplateId"));
		snapshotObj.setContext((String) data.get("context"));
		snapshotObj.setFileContent((List<JSONObject>) data.get("fileContent"));
		Object useExistingDataObj = data.get("useExistingData");
		boolean useExistingData = useExistingDataObj != null ? (Boolean) useExistingDataObj : false;
		snapshotObj.setUseExistingData(useExistingData);
		int i=0;
		if(data.get("rescaleFactor") != null) {
			snapshotObj.setRescaleFactor(convertToDouble(data.get("rescaleFactor")));	
		}
		if(data.get("rescaleRubric") != null) {
			snapshotObj.setRescaleRubric((String)data.get("rescaleRubric"));	
		}
		return snapshotObj;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJsonResponseSnapshotObject(Snapshot snapshot) {
		  JSONObject jsonResponse = new JSONObject();
		  //jsonResponse.put("template", snapshot.getTemplate());
		  if(!StringUtils.isEmpty(snapshot.getErrorCode())) {
			  jsonResponse.put("errorCode", snapshot.getErrorCode());
			  jsonResponse.put("errorMessage", snapshot.getErrorMessage());
		  }
	      jsonResponse.put("period", snapshot.getPeriod());
	      jsonResponse.put("templateId", snapshot.getTemplateId());
	      jsonResponse.put("dataParameters", snapshot.getDataParameters());
	      jsonResponse.put("tags", snapshot.getTags());
	      jsonResponse.put("unit", snapshot.getUnit());
	      jsonResponse.put("tenantId", snapshot.getTenantId());
	      jsonResponse.put("context", snapshot.getContext());
	      jsonResponse.put("name", snapshot.getName());
	      //jsonResponse.put("modifiedDate", snapshot.getModifiedDate());
	      jsonResponse.put("modifiedBy", snapshot.getModifiedBy());
	      jsonResponse.put("id", snapshot.getId());
	      jsonResponse.put("lastModified", snapshot.getLastModified());
	      jsonResponse.put("fileContent", snapshot.getFileContent());
	      jsonResponse.put("validationTemplateId", snapshot.getValidationTemplateId());
	      jsonResponse.put("contentChange", snapshot.getContentChange());
	      if(snapshot.getAggregationsByAdmin() != null) {
	    	  JSONObject aggregationsByAdmin = new JSONObject();
	    	  Iterator<String> averagesByAdminObj = snapshot.getAggregationsByAdmin().keySet().iterator();
	    	  while(averagesByAdminObj.hasNext()) {
	    		  String adminId = averagesByAdminObj.next();
	    		  aggregationsByAdmin.put(adminId, snapshot.getAggregationsByAdmin().get(adminId));
	    	  }
	    	  jsonResponse.put("aggregationsByAdmin", aggregationsByAdmin);
	      }
	      return jsonResponse;
	}
	
	/*public JSONObject createMultipleSnapshotsFromQueue(JSONObject jsonObj) {
		try {
			String templateData = (String) jsonObj.get("templateData");
			String tenantId = (String) jsonObj.get("tenantId");
			String email = (String) jsonObj.get("email");
			Template template = null;
			if(templateData != null) {
				String jsonStr = templateData.toString();
				ObjectMapper objectMapper = new ObjectMapper();
				template = objectMapper.readValue(jsonStr, Template.class);
				Optional<Template> templateObj = templateRepository.findById(template.getId());
				if(templateObj.isPresent()) {
					Template existedTemplate = templateObj.get();
					template = templateService.updateTemplateObject(existedTemplate, tenantId, existedTemplate.getId(), email);
				}else {
					template = templateService.saveTemplateObject(template, email);
				}
			}
			
			List<JSONObject> dataList = (List<JSONObject>)jsonObj.get("snapshots");
			List<JSONObject> snapshotsList = new ArrayList<JSONObject>();
			Iterator<JSONObject> itr = dataList.iterator();
			while(itr.hasNext()) {
				JSONObject data=itr.next();
				String jsonStr1 = data.toString();
				ObjectMapper objectMapper1 = new ObjectMapper();
				Snapshot snapshotObj = objectMapper1.readValue(jsonStr1, Snapshot.class);
				Snapshot snapshot = null;
				if(snapshotObj.getId() != null) {
					Optional<Snapshot> existignSnapshotObj = snapshotRepository.findById(snapshotObj.getId());
					Snapshot existingSnapshot = null;
					if(existignSnapshotObj.isPresent()) {
						existingSnapshot = existignSnapshotObj.get();
						snapshot = this.updateSnapshot(existingSnapshot.getId(), existingSnapshot);
					}else {
						snapshot = saveSnapshot(snapshotObj, email, null);
					}
				}else {
					snapshot = saveSnapshot(snapshotObj, email, null);
				}
				
		        String jsonString = objectMapper1.writeValueAsString(snapshot);
		        JSONParser parser = new JSONParser();
		        JSONObject jsonResponse = (JSONObject) parser.parse(jsonString);
				snapshotsList.add(jsonResponse);
			}
			
			JSONObject snapshotResponse = new JSONObject();
			snapshotResponse.put("snapshotsList", snapshotsList);
			return snapshotResponse;
		}catch(Exception e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return null;
	}*/
	
	public JSONObject updateSnapshotFromQueue(JSONObject jsonObj) throws Exception {//currently unsed for bms
		try {
			String email=(String) jsonObj.get("email");
			JSONObject data = (JSONObject) jsonObj.get("data");
			String tenantId = (String) data.get("tenantId");
			if(permissionService.checkPermission(tenantId, (String)data.get("id"), 1, email)) {
				String jsonStr = data.toString();
				ObjectMapper objectMapper = new ObjectMapper();
				SnapshotModel snapshotObj = objectMapper.readValue(jsonStr, SnapshotModel.class);
				Snapshot snapshot = updateSnapshot(snapshotObj.getId(),snapshotObj);
				snapshotObj = null;
		        String jsonString = objectMapper.writeValueAsString(snapshot);
		        JSONParser parser = new JSONParser();
		        JSONObject jsonResponse = (JSONObject) parser.parse(jsonString);
		        return jsonResponse;
			}
			return null;
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Error while performing updateSnapshotFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}
	
	public Snapshot saveSnapshot(SnapshotModel snapshotModel, String email, Long lastCalculated) throws Exception {
		String unit = snapshotModel.getUnit();
		String period = snapshotModel.getPeriod();
		String context = snapshotModel.getContext();
		JSONObject dataParameters = snapshotModel.getDataParameters();
		Snapshot existingSnapshot = null;
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Snapshot snapshot = mapper.map(snapshotModel, Snapshot.class); 
		Boolean persistSnapshot = true;
		if(Boolean.FALSE.equals(snapshotModel.getPersistSnapshot())) {
			persistSnapshot = false;
		}
		if(!StringUtils.isEmpty(snapshotModel.getId())) {
			Date before = new Date();
			Optional<Snapshot> existignSnapshotObj = snapshotRepository.findById(snapshotModel.getId());
			Date after = new Date();
			Timming.addReadTime(after.getTime() - before.getTime());
			if(existignSnapshotObj.isPresent()) {
				existingSnapshot = existignSnapshotObj.get();
			}
		}
		/*else if(context != null && unit != null && period != null) {
			existingSnapshot = snapshotRepository.findOneByTenantIdAndContextAndUnitAndPeriod(snapshot.getTenantId(),snapshot.getContext(), snapshot.getUnit(), snapshot.getPeriod());	
		}else if(context != null && (unit == null || period == null)) {
			existingSnapshot = snapshotRepository.findOneByTenantIdAndContext(snapshot.getTenantId(),snapshot.getContext());
		}*/
		//snapshot.update(templateRepository,null);

		if(existingSnapshot != null) {
			List validSnapshotIdsList = new ArrayList<>();
			List inValidSnapshotIdsList = new ArrayList<>();
			if(dataParameters != null && dataParameters.get("snapshotIds") != null) {
				validSnapshotIdsList = (List)dataParameters.get("snapshotIds");
			}
			if(dataParameters != null && dataParameters.get("invalidSnapshotIds") != null) {
				inValidSnapshotIdsList = (List)dataParameters.get("invalidSnapshotIds");
			}
			if((dataParameters == null) || (validSnapshotIdsList.size() > 0 && validSnapshotIdsList.get(0) instanceof String ||
					(inValidSnapshotIdsList.size() > 0 && inValidSnapshotIdsList.get(0) instanceof String))) {
				Boolean calculateSnapshot = loadDataParametersSnapshots(existingSnapshot,dataParameters,false);
				List<JSONObject> oldFileContent = existingSnapshot.getFileContent();
				String lastModifiedString = existingSnapshot.getLastModified();
				long snapshotLastModified = 0;
				if(lastModifiedString != null) {
					snapshotLastModified = Long.parseLong(lastModifiedString);
				}
				if (calculateSnapshot) {
					snapshot.update(templateRepository, null, existingSnapshot, snapshotModel.isUseExistingData(),null,true);
				} else {
					if(snapshot.getTemplateId() != null) {
						snapshot.setFileContent(new ArrayList<>());	
					}
				}
				existingSnapshot.setName(snapshotModel.getName());
				List<JSONObject> newFileContent = snapshot.getFileContent();//BMS LAST CALCUTE < getLastModified takes 6sec
				if((oldFileContent== null) || (lastCalculated == null || lastCalculated == 0) || snapshotLastModified == 0 || lastCalculated > snapshotLastModified ||(oldFileContent != null && (oldFileContent.size() != newFileContent.size() || !oldFileContent.equals(newFileContent)))) {
					existingSnapshot.setTemplateId(snapshot.getTemplateId());
					existingSnapshot.setFileContent(newFileContent);
					existingSnapshot.setTags(snapshotModel.getTags());
					if(lastCalculated != null && lastCalculated != 0) {
						existingSnapshot.setLastModified("" + lastCalculated);
					}else {
						existingSnapshot.setLastModified(""+new Date().getTime());	
					}
					
					existingSnapshot.setErrorCode(snapshotModel.getErrorCode());
					existingSnapshot.setErrorMessage(snapshotModel.getErrorMessage());
					existingSnapshot.setDataParameters(null);
					existingSnapshot.setContext(context);
					if(!StringUtils.isEmpty(unit)) {
						existingSnapshot.setUnit(unit);
					}
					if(!StringUtils.isEmpty(period)) {
						existingSnapshot.setPeriod(period);
					}
					//Date before = new Date();
					if(persistSnapshot) {
						snapshotRepository.save(existingSnapshot);
					}
					//Date after = new Date();
					//Timming.addUpdateTime(after.getTime() - before.getTime());
					existingSnapshot.setContentChange(true);
				}else {
					existingSnapshot.setDataParameters(null);
					existingSnapshot.setContext(context);
					existingSnapshot.setContentChange(false);
					logger.info("not persisted as there is no change for "+snapshotModel.getId()+":"+period);
				}
				snapshotModel = null;
				oldFileContent = null;
				newFileContent = null;
				return existingSnapshot;
			} else {
				logger.debug("skipping average benchmark calculating as this case does not arise");
				return existingSnapshot;
			}
		}else {
			if(snapshot.getId() == null) {
				snapshot.setId(UUID.randomUUID().toString());
			}else {
				snapshot.setId(snapshot.getId());
			}
			List validSnapshotIdsList = new ArrayList<>();
			List inValidSnapshotIdsList = new ArrayList<>();
			if(dataParameters != null && dataParameters.get("snapshotIds") != null) {
				validSnapshotIdsList = (List)dataParameters.get("snapshotIds");
			}
			if(dataParameters != null && dataParameters.get("invalidSnapshotIds") != null) {
				inValidSnapshotIdsList = (List)dataParameters.get("invalidSnapshotIds");
			}
			if(dataParameters == null || (validSnapshotIdsList.size() > 0 && validSnapshotIdsList.get(0) instanceof String ||
					(inValidSnapshotIdsList.size() > 0 && inValidSnapshotIdsList.get(0) instanceof String))) {
				//default case
				Boolean calculateSnapshot = loadDataParametersSnapshots(snapshot,dataParameters,false);
				if (calculateSnapshot) {
					snapshot.update(templateRepository,null,null,false,null,true);
				} 
			}else{
				if(dataParameters != null) {
					//average calculation case
					persistSnapshot = false;	
				}
				//calculate average by administration Id
				HashMap<String, List<JSONObject>> aggregationByAdmin = new HashMap<>();
				this.getAggregationByAdminstration(snapshot, aggregationByAdmin, validSnapshotIdsList, inValidSnapshotIdsList);

				//List<String> measures = new ArrayList<>();
				JSONObject parameters = new JSONObject();
				//use averages to calculate bechmark
				ArrayList includeSnapshotIds = new ArrayList<>();
				for(int i=0;i<validSnapshotIdsList.size();i++) {
					JSONObject adminstrationObj = (JSONObject)validSnapshotIdsList.get(i);
					if(adminstrationObj.get("includeInBenchmark") == null || adminstrationObj.get("includeInBenchmark").equals(true)) {
						includeSnapshotIds.add(adminstrationObj.get("administrationId"));
					}
				}
				parameters.put("snapshotIds",includeSnapshotIds);
				parameters.put("invalidSnapshotIds",new ArrayList());
				if(aggregationByAdmin.size() > 0) {
					snapshot.setDataParameters(parameters);	
				}
				snapshot.update(templateRepository,null,null,false,null,true);
				snapshot.setAggregationsByAdmin(aggregationByAdmin);
			}
			
			if(snapshot.getUnit()==null) { 
				snapshot.setUnit("");
			}
			if(snapshot.getPeriod()==null) {
				snapshot.setPeriod("");
			}
			if(lastCalculated != null && lastCalculated != 0) {
				snapshot.setLastModified("" + lastCalculated);
			}else {
				snapshot.setLastModified(""+new Date().getTime());	
			}
			snapshot.setContext(context);
			snapshot.setDataParameters(null);
			if(persistSnapshot) {
				Documents document = new Documents();
				document.setRefdoc(snapshot.getId());
				document.setType("snapshot");
				permissionService.addDocument(document,email);
				snapshotRepository.save(snapshot);
			}
			//Date after = new Date();
			//Timming.addUpdateTime(after.getTime() - before.getTime());
			snapshot.setContentChange(true);
			snapshotModel = null;
			return snapshot;
		}
	}
	private void getAggregationByAdminstration(Snapshot snapshot,HashMap<String, List<JSONObject>> aggregationByAdmin,List validSnapshotIdsList,List inValidSnapshotIdsList) throws Exception {
		for(int i=0;i<validSnapshotIdsList.size();i++) {
			JSONObject parameters = new JSONObject();
			JSONObject validJSON = (JSONObject)validSnapshotIdsList.get(i);
			parameters.put("snapshotIds", validJSON.get("snapshots"));
			parameters.put("invalidSnapshotIds", new ArrayList<>());
			for(int j=0;j<inValidSnapshotIdsList.size();j++) {
				JSONObject invalidJSON = (JSONObject)inValidSnapshotIdsList.get(j);
				if(invalidJSON != null && invalidJSON.get("administrationId").equals(validJSON.get("administrationId"))) {
					parameters.put("invalidSnapshotIds", invalidJSON.get("snapshots"));
					break;
				}
			}
			Boolean calculateSnapshot = loadDataParametersSnapshots(snapshot,parameters,true);
			if (calculateSnapshot) {
				Optional<Template> datasetTemplateObj = Optional.empty();
				Template datesetTemplate = null;
				List<String> snapshots = (List<String>)parameters.get("snapshotIds");
				Snapshot aggDatasetSnapshot = null;
				if(snapshots.size() > 0) {
					aggDatasetSnapshot = Snapshot.getSnapshotsCache(snapshot.getId(), snapshots.get(0));
					if(aggDatasetSnapshot == null) {
						throw new Exception("Snapshot not present with id:" + snapshots.get(0));
					}
					datasetTemplateObj = templateRepository.findById(aggDatasetSnapshot.getTemplateId());
					datesetTemplate = datasetTemplateObj.isPresent() ? datasetTemplateObj.get() : null;
					if(datesetTemplate != null) {
						this.aggregateAndrescaleRubrics(datesetTemplate, snapshot, aggDatasetSnapshot, snapshots);
					}
					aggDatasetSnapshot.update(templateRepository, null, null, false, null, false);
					String adminId =(String)validJSON.get("administrationId"); 
					aggregationByAdmin.put(adminId, aggDatasetSnapshot.getFileContent());
					aggDatasetSnapshot.setId(adminId);
					Snapshot.putSnapshotsCache(snapshot.getId(),aggDatasetSnapshot.getId(),aggDatasetSnapshot);
				} else {
					logger.error("snapshotIds empty for SnapshotId:"+ snapshot.getId());
				}
			} 
		}
	}
	private void aggregateAndrescaleRubrics(Template datesetTemplate,Snapshot snapshot,Snapshot aggDatasetSnapshot,List<String> snapshots) throws Exception {
		List<JSONObject> tempalteFileContentJsonObjects = datesetTemplate.getFileContent();
		for(int j=1;j< tempalteFileContentJsonObjects.size();j++) {
			JSONObject templateNode = tempalteFileContentJsonObjects.get(j);
			//if(templateNode.get("ValueType").equals("Input")) {
				this.updateAggregationSnapshotDataForTemplateNode(snapshot,templateNode,aggDatasetSnapshot,snapshots);	
			//}
		}
		if(StringUtils.hasText(snapshot.getRescaleRubric())) {
			String rescaleRubric = snapshot.getRescaleRubric();
			Double rescaleFactor = snapshot.getRescaleFactor();
			JSONObject rescaleNode = this.getSnapshotNode(aggDatasetSnapshot.getFileContent(), rescaleRubric);
			if(rescaleNode == null) {
				throw new Exception("Rescale Rubric not present in template");
			}
			Double rescaleValue = convertToDouble(rescaleNode.get("Value"));
			if(rescaleFactor == null  || rescaleFactor == 0.0) {
				throw new Exception("Rescale factor doesn't have valid value");
			} else if(rescaleValue == 0.0 || rescaleValue == null) {
				throw new Exception("Rescale Rubric doesn't have valid value");
			}
			Double datasetRescaleFactor = rescaleFactor/rescaleValue;
			
			for(int j=1;j< tempalteFileContentJsonObjects.size();j++) {
				JSONObject templateNode = tempalteFileContentJsonObjects.get(j);
				if(templateNode.get("ValueType").equals("Input")) {
					//JSONObject aggrNode = this.getSnapshotNode(tempalteFileContentJsonObjects, (String)templateNode.get("id"));
					JSONObject aggrNode = this.getSnapshotNode(aggDatasetSnapshot.getFileContent(), (String)templateNode.get("id"));
					if(aggrNode != null) {
						Double value = datasetRescaleFactor * convertToDouble(aggrNode.get("Value"));
						aggrNode.put("Value", value);
					}
				}
			}
			
		}
	}
	private void updateAggregationSnapshotDataForTemplateNode(Snapshot benchmarkSnapshot,JSONObject templateNode,Snapshot aggregationSnapshot,List<String> snapshots) {
		//HashMap<String,JSONObject> avgNodes = new HashMap<>();
		String id = (String)templateNode.get("id");
		JSONObject  aggNode = this.getSnapshotNode(aggregationSnapshot.getFileContent(), id);
		if(aggNode == null) {
			aggNode = new JSONObject();
			aggNode.put("id", id);
			aggNode.put("Value", 0);
			aggregationSnapshot.getFileContent().add(aggNode);
		}else {
			Object val = aggNode.get("Value");
			if(val == null) {
				aggNode.put("Value", 0.0);
			}
		}
		int totalSnapshots =snapshots.size();
		String aggregationMethod = AGG_METHOD_SUM;
		aggregationMethod = this.getAggregationMethodFromTemplate(templateNode, id);
		for(int i =0;i<totalSnapshots;i++) {
			if(aggregationMethod != null && aggregationMethod.equals(AGG_METHOD_LATEST) && (i< snapshots.size() - 1)) {
				continue;
			}
			Snapshot datasetSnapshot = Snapshot.getSnapshotsCache(benchmarkSnapshot.getId(), snapshots.get(i));
			JSONObject datasetSnapshotNode = this.getSnapshotNode(datasetSnapshot.getFileContent(), id);
			if(aggregationMethod != null) {
				Double value  = 0.0;
				if(datasetSnapshotNode != null) {
					value = convertToDouble(datasetSnapshotNode.get("Value"));
				}
				Double aggVal = convertToDouble(aggNode.get("Value"));
				if(i > 0 && aggregationMethod.equals(AGG_METHOD_SUM)) {
					aggNode.put("Value", aggVal + value);
				}else if(i > 0 && aggregationMethod.equals(AGG_METHOD_AVERAGE)) {
					aggNode.put("Value", aggVal + value);
				}else if(i== snapshots.size() - 1) {
					aggNode.put("Value", aggVal);
				}
			}
		}
		if(aggregationMethod != null && aggregationMethod.equals(AGG_METHOD_AVERAGE)) {
			aggNode.put("Value", convertToDouble(aggNode.get("Value"))/totalSnapshots);
		}
		
	}
	
	private static double convertToDouble(Object value) {
	    if (value instanceof Double) {
	        return (double) value;
	    } else if (value instanceof Integer) {
	        return (double) ((int) value);
	    }else if ( value instanceof Long) {
	    	 return (double) ((long) value);
	    }
	    else if (value instanceof String) {
	        try {
	            return Double.parseDouble((String) value);
	        } catch (NumberFormatException e) {
	            // Handle the case where the string cannot be parsed as a double
	            return 0.0; // or any default value you prefer
	        }
	    }
	    return 0.0; // or any default value you prefer for unsupported types
	}
	
	private JSONObject getSnapshotNode(List<JSONObject> snapshotFileContent,String id) {
		for(int i=0;i<snapshotFileContent.size();i++) {
			JSONObject snapshotNode = snapshotFileContent.get(i);
			String nodeId = (String)snapshotNode.get("id");
			if(nodeId.equals(id)) {
				return snapshotNode;
			}
		}
		return null;
	}
	private String getAggregationMethodFromTemplate(JSONObject templateNode,String id) {
		String nodeId = (String)templateNode.get("id");
		if(nodeId.equals(id)) {
			Object valueType = templateNode.get("aggregationMethod");
			if(valueType == null) {
				return "SUM";
			}else {
				return (String)templateNode.get("aggregationMethod");
			}
		}
		return null;
	}
	private Boolean loadDataParametersSnapshots(Snapshot snapshot,JSONObject dataParameters,boolean skipRandomization) {
		if(snapshot.getTemplateId() == null) {
			return false;
		}
		Boolean calulateSnapshot = true;
		List<String> validSnapshotIdsList = new ArrayList<>();
		List<String> inValidSnapshotIdsList = new ArrayList<>();
		if(dataParameters != null && dataParameters.get("snapshotIds") != null) {
    		validSnapshotIdsList = (List<String>)dataParameters.get("snapshotIds");
    		Date before = new Date();
    		logger.error(snapshot.getName()+" valid:"+validSnapshotIdsList.size());
    		List<String> randomList = addToSnapshotCache(validSnapshotIdsList,snapshot,skipRandomization);
    		Date after = new Date();
			Timming.addReadTime(after.getTime() - before.getTime());
    		dataParameters.put("snapshotIds",randomList);
		}
		if(dataParameters != null && dataParameters.get("invalidSnapshotIds") != null) {
			inValidSnapshotIdsList = (List<String>)dataParameters.get("invalidSnapshotIds");
    		logger.error(snapshot.getName()+" Invalid:"+inValidSnapshotIdsList.size());
    		List<String> randomList = addToSnapshotCache(inValidSnapshotIdsList,snapshot,skipRandomization);
    		dataParameters.put("invalidSnapshotIds",randomList);
			if (validSnapshotIdsList.size() == 0 && inValidSnapshotIdsList.size() == 0) {
				calulateSnapshot = false;
			}
		}
		snapshot.setDataParameters(dataParameters);
		return calulateSnapshot;
	}

	private List<String> addToSnapshotCache(List<String> snapshotList, Snapshot currentSnapshot,boolean skipRandomization) {
		int inputLength = snapshotList.size();
		int maxSampleSize = 100000;
		int chunkSize = 5000;
		List<String> randomList= null;
		if(inputLength > maxSampleSize) {
			randomList = new LinkedList<String>(snapshotList);
			if(!skipRandomization) {
				Collections.shuffle(randomList);	
			}
		}else {
			randomList = snapshotList;
		}
		String currentSnapshotId = currentSnapshot.getId();
		int length = randomList.size();
		int index = 0;
		
		while(index<length) {
			List<String> cursorSnapshotList = new ArrayList<String>();
			if(index + chunkSize < length) {
				cursorSnapshotList = randomList.subList(index, index+chunkSize);
			}else {
				cursorSnapshotList = randomList.subList(index, length);
			}
			List<Snapshot> existedSnapshotObjList = snapshotRepository.findByIdIn(cursorSnapshotList);//takes 45sec for 250 records
			for(int i=0;i<existedSnapshotObjList.size();i++) {
				Snapshot.putSnapshotsCache(currentSnapshotId, existedSnapshotObjList.get(i).getId(), existedSnapshotObjList.get(i));
			}
			index = index+chunkSize;
			if(index >=maxSampleSize) {
				break;	//TODO
			}
		}
		if(length > maxSampleSize) {
			randomList = randomList.subList(0, maxSampleSize);
		}
		return randomList;
	}
	
	public JSONObject fetchSnapshotFromQueue(JSONObject jsonObj) throws Exception{
		try{
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			String tenantId = (String)data.get("tenantId");
			String snapshotId = (String)data.get("id");
			//Long timestamp = (Long)data.get("timestamp");
			return fetchSnapshot(snapshotId, tenantId, email);
		}catch(Exception e) {
			logger.error("Error while performing fetchSnapshotFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}

	public JSONObject fetchSnapshotsByRubricExternalIdFromQueue(JSONObject jsonObj) throws Exception {
		try {
			String email = (String) jsonObj.get("email");
			JSONObject data = (JSONObject) jsonObj.get("data");
			String tenantId = (String) data.get("tenantId");
			String rubricExternalId = (String) data.get("rubricExternalId");
			List<JSONObject> snapshotsJson = fetchSnapshots(rubricExternalId, tenantId, email);
			JSONObject responseJson = new JSONObject();
			responseJson.put("snapshots", snapshotsJson);
			return responseJson;
		} catch (Exception e) {
			logger.error("Error while performing fetchSnapshotFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}

	public JSONObject fetchSnapshot(String id, String tenantId, String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, id, 2, email)) {	
			Optional<Snapshot> existignSnapshotObj = snapshotRepository.findById(id);
			Snapshot existingSnapshot = null;
			if(existignSnapshotObj.isPresent()) {
				existingSnapshot = existignSnapshotObj.get();
				//if(timestamp == null || timestamp > Long.parseLong(existingSnapshot.getLastModified())) {
				return getJsonResponseSnapshotObject(existingSnapshot);
				/*}else {
					JSONObject jsonResponse = new JSONObject();
					jsonResponse.put("error_204","timestamp in snapshot greater than given timestamp "+id);
					return jsonResponse;
				}*/
			}else {//retry again in bms
				 JSONObject jsonResponse = new JSONObject();
				 jsonResponse.put("error_404","No Snapshot with id "+id);
				return jsonResponse;
			}
		}else {//recheck/reject in bms
			JSONObject jsonResponse = new JSONObject();
			jsonResponse.put("error_201","permission failed for email "+email+" with id "+id);
			return jsonResponse;
		}
	}

	public List<JSONObject> fetchSnapshots(String rubricExternalId, String tenantId, String email) throws AccessDeniedException {
		List<JSONObject> snapshotsJson = new ArrayList<>();
		if (permissionService.checkPermission(tenantId, rubricExternalId, 2, email)) {
			snapshotsJson = snapshotRepository.getSnapshotsByRubricExternalId(rubricExternalId);
		}
		return snapshotsJson;
	}
	
	public void deleteSnapshotFromQueue(JSONObject jsonObj) throws Exception{
		try{
			String parentTenantId = (String)jsonObj.get("tenantId");
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			String snapshotId = (String)data.get("id");
			String tenantId = (String) data.get("tenantId");
			deleteSnapshot(snapshotId, tenantId, email);
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Error while performing deleteSnapshotFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}

	public void deleteSnapshot(String id, String tenantId, String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, id, 2, email)) {	
			permissionService.deleteDocument(id);
			
			Query deleteSnapshot = new Query();
			deleteSnapshot.addCriteria(Criteria.where("_id").is(id));
			DeleteResult result = mongoTemplate.remove(deleteSnapshot, Snapshot.class);	
		}
	}

	public Snapshot updateSnapshot(String id, SnapshotModel snapshotModel) throws ParseException, InterruptedException {
		Optional<Snapshot> existignSnapshotObj = snapshotRepository.findById(id);
		Snapshot existingSnapshot = null;
		if(existignSnapshotObj.isPresent()) {
			existingSnapshot = existignSnapshotObj.get();
		}
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		Snapshot snapshot = mapper.map(snapshotModel, Snapshot.class); 
		if(existingSnapshot != null) {
			snapshot.update(templateRepository,null,existingSnapshot,snapshotModel.isUseExistingData(),null,true);
			existingSnapshot.setContext(snapshot.getContext());
			existingSnapshot.setPeriod(snapshot.getPeriod());
			existingSnapshot.setUnit(snapshot.getUnit());
			existingSnapshot.setName(snapshot.getName());
			existingSnapshot.setTags(snapshot.getTags());
			existingSnapshot.setFileContent(snapshot.getFileContent());
			if(snapshot.getLastModified() != null) {
				existingSnapshot.setLastModified(snapshot.getLastModified());
			}else {
				existingSnapshot.setLastModified(new Long(new Date().getTime()).toString());
			}
			snapshotRepository.save(existingSnapshot);
		}
		snapshotModel = null;
		return existingSnapshot;
	}
	public Page<JSONObject> getAccessableSnapshots(String tenantId, Pageable pageable) throws ParseException {
		List<JSONObject> templateList = permissionService.getdocs(tenantId, "snapshot", null,pageable);
		int start = (int) pageable.getOffset();
		int end = (start + pageable.getPageSize()) > templateList.size() ? templateList.size()
				: (start + pageable.getPageSize());
		return new PageImpl<JSONObject>(templateList.subList(start, end), pageable, templateList.size());
    }
}
