package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vdmbee.calculationengine.analyser.snapshot.controller.TemplateResource;
import com.vdmbee.calculationengine.analyser.snapshot.controller.TemplateResourceAssembler;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.model.TemplateModel;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;

@Service
public class TemplateQueueService {
	@Autowired
	private TemplateService templateService;
	
	

	public JSONObject getTemplate(JSONObject body) throws Exception{
		JSONObject dataObj = (JSONObject) body.get("data");
		String templateId = null;
		if (dataObj != null) {
			templateId = (String) dataObj.get("id");
        }
		if(templateId == null) {
			throw new Exception("Template Id can not be null");
		}
		String tenantId = (String) dataObj.get("tenantId");
		Template template = templateService.getTemplateObject(templateId, tenantId, (String)body.get("email"));
		return getTemplateResponse(template);
	}
	private JSONObject getTemplateResponse(Template template ) throws JsonProcessingException, ParseException {
		TemplateResourceAssembler templateResourceAssembler = new TemplateResourceAssembler();
		TemplateResource tempResource =  template != null ? templateResourceAssembler.toModel(template): null;
		JSONParser parser = new JSONParser();
		ObjectMapper objectMapper = new ObjectMapper();
		return tempResource != null ? (JSONObject)parser.parse(objectMapper.writeValueAsString(tempResource)):null;
	}

	public JSONObject createTemplate(JSONObject templateRequest) throws Exception{
		TemplateModel templateModel = new TemplateModel((JSONObject)templateRequest.get("data"));
		//TemplateModel templateModel = templateService.fillTemplateObject((JSONObject)templateRequest.get("data"));// not needed as createPythonTemplate exists for single template creation
		Template template =  templateService.saveTemplateObject(templateModel, (String)templateRequest.get("email"));
		return getTemplateResponse(template);
	}
	

	public JSONObject createPythonTemplate(JSONObject templateRequest) throws Exception{
		return templateService.saveTemplateFromQueue(templateRequest);
	}
	

	public JSONObject updateTemplate(JSONObject templateRequest) throws Exception{//not used
		TemplateModel templateModel = new TemplateModel((JSONObject)templateRequest.get("data"));
		String templateId = null;
		String tenantId = null;
		if (templateModel != null) {
			templateId = (String) templateModel.getId();
        }
		if(templateId == null) {
			throw new Exception("Template Id can not be null");
		}
		tenantId = (String) templateModel.getTenantId();
		Template newTemplate = templateService.updateTemplateObject(templateModel,tenantId,templateId,null);
		return getTemplateResponse(newTemplate);
	}
	

	public JSONObject getTemplates(JSONObject templateRequest) throws ParseException {
		JSONObject data = (JSONObject)templateRequest.get("data");
		String tenantId = (String) data.get("tenantId");
		JSONObject page = (JSONObject)data.get("page");
		PageRequest pageReq = getPagable(page);
		JSONObject ret = new JSONObject();
		List<JSONObject> templates = new ArrayList<JSONObject>();
		Page<JSONObject> responsePage = templateService.getAccessableTemplates(tenantId, pageReq);
		Iterator<JSONObject> retIter = responsePage.get().iterator();
		while(retIter.hasNext()) {
			templates.add(retIter.next());
		}
		ret.put("templates", templates);
		JSONObject pageInfo = new JSONObject();
		ret.put("page", pageInfo);
		pageInfo.put("pageSize", responsePage.getSize());
		pageInfo.put("page", responsePage.getNumber());
		pageInfo.put("size", responsePage.getTotalElements());
		Sort sort = responsePage.getSort();
		Iterator<Sort.Order> sortOrders = sort.iterator();
		List<String> properties = new ArrayList<>();
		while(sortOrders.hasNext()) {
			Sort.Order order = sortOrders.next();
			properties.add(order.getProperty());
		}
		pageInfo.put("properties", properties);
		pageInfo.put("direction",  sort.getOrderFor(properties.get(0)));
		
		return ret;
	}
	
	private PageRequest getPagable(JSONObject page) {
		int pageSize, pageNumber,offset;
		List<String> sortBy;
		
		Sort.Direction direction;
		String directionStr;
		Sort sort = null;
		PageRequest pageReq = null;
		if(page != null) {
			directionStr = (String)page.get("direction");
			if(directionStr != null) {
				if(directionStr.equals("ASC")){
					direction = Sort.Direction.ASC;
				}else {
					direction = Sort.Direction.DESC;
				}
			}else {
				direction =Sort.Direction.DESC;
			}
			sortBy = (List<String>)page.get("properties");
			if(sortBy == null ) {
				sortBy= new ArrayList<String>();
				sortBy.add("_id");
			}
			
			sort = Sort.by(direction,sortBy.toArray(new String[sortBy.size()])); 
			offset = page.get("offset") != null ? (int)page.get("offset") : 0;
			pageNumber = page.get("pageNumber") != null ? (int)page.get("pageNumber") : 0;
			pageSize = page.get("pageSize") != null ? (int)page.get("pageSize") : 10;
			pageReq =  PageRequest.of(pageNumber, pageSize, sort);
		}else {
			sortBy= new ArrayList<String>();
			sortBy.add("_id");
			sort = Sort.by(Sort.Direction.ASC,sortBy.toArray(new String[sortBy.size()])); 
			offset = 0;
			pageNumber = 0;
			pageSize = 10;
			pageReq =  PageRequest.of(pageNumber, pageSize, sort);
		}
		return pageReq;
	}
	

	public void deleteTemplateFromQueue(JSONObject jsonObj) throws Exception{
		templateService.deleteTemplateFromQueue(jsonObj);
	}
	
	public void dropTenant(JSONObject jsonObj) throws Exception{	
		templateService.deleteTenant(jsonObj);
	}

}
