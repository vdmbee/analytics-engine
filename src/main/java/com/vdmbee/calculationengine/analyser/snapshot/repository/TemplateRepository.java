package com.vdmbee.calculationengine.analyser.snapshot.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;

@Component
@RepositoryRestResource(collectionResourceRel = "template", path = "template")
public interface TemplateRepository extends MongoRepository<Template, String> {
	List<Template> findFirstByTenantIdAndContext(String tenandId,String context); 
	
	@Query("{ '_id' : { '$in': ?0 }}")
	Page<Template> getTemplatesByDocument(String[] docIds, Pageable pageable);
	
	@Query("{ 'tenantId' : ?0 }")
	Page<Template> getTemplatesWithTenantId(String tenantId, Pageable pageable);
	
	@Query("{ 'tenantId' : ?0 }")
	List<Template> getTemplateWithTenantId(String tenantId);
	
	@Query( "{'tags.id': {'$in': [?0] }}" )
	List<Template> getTemplatesWithTag(String tagId);
}
