package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="library")
public class MeasureLibrary {
	@Id
    private String id;

	private String libraryId;

	private String tenantId;
	
	public String getLibraryId() {
		return libraryId;
	}

	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}
	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public List<JSONObject> getFileContent() {
		return fileContent;
	}

	public void setFileContent(List<JSONObject> fileContent) {
		this.fileContent = fileContent;
	}

	private List<JSONObject> fileContent;
}
