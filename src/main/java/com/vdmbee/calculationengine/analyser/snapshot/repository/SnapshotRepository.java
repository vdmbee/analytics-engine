package com.vdmbee.calculationengine.analyser.snapshot.repository;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;

@Component
@RepositoryRestResource(collectionResourceRel = "snapshot", path = "snapshot")
public interface SnapshotRepository extends MongoRepository<Snapshot, String> {
	List<Snapshot> findAllByTenantIdAndContext(String tenenatId,String context);
	Snapshot findOneByTenantIdAndContext(String tenenatId,String context);
	List<Snapshot> findByTenantIdAndContextAndUnitAndPeriod(String tenantId,String context,String Unit,String period);
	Snapshot findOneByTenantIdAndContextAndUnitAndPeriod(String tenenatId,String context,String Unit,String period);
	List<Snapshot> findByTenantIdAndContextAndUnit(String tenenatId,String context,String Unit);
	List<Snapshot> findByUnitAndPeriod(String unit,String period);
	List<Snapshot> findByContextAndUnitAndPeriod(String context,String unit,String period);
	List<Snapshot> findByContextAndUnit(String context,String unit);
	List<Snapshot> findByUnit(String unit);
	List<Snapshot> findByContext(String context);
	List<Snapshot> findByIdIn(List<String> ids);
	
	@Query("{'_id' : ?0}")
	Snapshot getSnapshotById(String id);

	@Aggregation(pipeline = {
			"{$unwind: {path: \"$fileContent\"}}",
			"{$match: {\"fileContent.ValueName\": ?0}}",
			"{$project: {fileContent: 1}}"
	})
	List<JSONObject> getSnapshotsByRubricExternalId(String rubricExternalId);
}

