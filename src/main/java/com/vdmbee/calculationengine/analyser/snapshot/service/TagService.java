package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.mongodb.client.result.DeleteResult;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Tag;
import com.vdmbee.calculationengine.analyser.snapshot.model.TagModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TagRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.repository.PermissionRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;


@Service
public class TagService {
	
	@Autowired
	private Environment env;
	
	@Autowired 
	private TagRepository tagRepository;
	
	@Autowired
	private PermissionService templateService;
	
	@Autowired
	private DocumentsRepository documentRepository;
	
	@Autowired
	private PermissionRepository permissionRepository;
	
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	
	@Autowired
	private SnapshotService snapshotService;
	
	@Autowired
	private PermissionService permissionService;
	
	public boolean checkAccessToTenant(String tenantId, String email) {
		if(tenantId != null) {
			boolean canAccess = permissionService.isMemberOfTenant(tenantId,email);
			if(!canAccess) {
				return false;
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				return false;
			}
		}
		return true;
	}
	public boolean checkAccessForDocument(String tenantId,String id, String email) throws AccessDeniedException {
		if(tenantId != null) {
			boolean canAccess = permissionService.checkPermission(tenantId, id, 0, email);
			if(!canAccess) {
				return false;
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				return false;
			}
		}
		return true;
	}
	
	public Tag getTagObject(String tenantId, String id) throws AccessDeniedException {
		if(!checkAccessForDocument(tenantId,id, null)) {
			return null;
		}
		Optional<Tag> tagObj = tagRepository.findById(id);
		Tag tag = null;
		if(tagObj.isPresent()) {
			tag = tagObj.get();
		}
		return tag;
	}
	
	public Tag saveTagObjects(TagModel tagModel, String tenantId,String email) throws AccessDeniedException {
		if(!this.checkAccessToTenant(tenantId,email)) {
			return null;
		}
		Tag existingTag = null;
		if(tagModel.getId() != null) {
			Optional<Tag> existTagObj = tagRepository.findById(tagModel.getId());
			if(existTagObj.isPresent()) {
				existingTag = existTagObj.get();
			}
		}
		/*else {
			existingTag = tagRepository.findOneByTenantIdAndContext(tag.getTenantId(),tag.getContext());	
		}*/
		
		if(existingTag != null) {
			if(permissionService.checkPermission(tenantId, tagModel.getId(), 1, null)) {
				existingTag.setName(tagModel.getName());
				existingTag.setType(tagModel.getType());
				existingTag.setValues(tagModel.getValues());
				tagRepository.save(existingTag);
				snapshotService.updateTagsInSnapshot(tagModel);
				return existingTag;
			}else {
				return null;
			}
		}else {
			ModelMapper mapper = new ModelMapper();
			mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			Tag tag = mapper.map(tagModel, Tag.class);
			if(tag.getId() == null) {
				tag.setId(UUID.randomUUID().toString());
			}else {
				tag.setId(tag.getId());
			}
			
			Documents document = new Documents();
			document.setRefdoc(tag.getId());
			document.setType("tag");
			templateService.addDocument(document,email);
			
			tagRepository.save(tag);
			return tag;
		}
	}
	public void deleteTag(String id, String tenantId) throws AccessDeniedException {
    	if(tenantId == null || (tenantId != null && permissionService.checkPermission(tenantId, id, 2, null))) {
	    	Query getSnapshots = new Query();
	    	getSnapshots.addCriteria(Criteria.where("tagId").is(id));
	    	List<Snapshot> snapshotList = mongoTemplate.find(getSnapshots, Snapshot.class);
	    	for(Snapshot s : snapshotList) {
	    		templateService.deleteDocument(s.getId());
	    	}
	    	DeleteResult deletedSnapshots = mongoTemplate.remove(getSnapshots, Snapshot.class);
	    	//System.out.println(deletedSnapshots);
	    	
	    	//tag deletion
	    	templateService.deleteDocument(id);
	    	Query tagDelete = new Query();
	    	tagDelete.addCriteria(Criteria.where("_id").is(id));
	    	DeleteResult deletedTag = mongoTemplate.remove(tagDelete, Tag.class);
	    	//System.out.println(deletedTag);
    	}
	}
	public Permission createNewPermission(String id, String tenantId, Permission permission,String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, id, 1, null)) {
			Documents document;
			String isLocalUserGuideId="";
			Documents doc = documentRepository.getDocumentByRefdoc(id);
	
			Tag tag = tagRepository.findTagById(id);
			
			String tenantIdForUsers = tag.getTenantId();
			String[] userIds = permission.getUserIds();
			for(int i=0;i<userIds.length ;i++) {
				isLocalUserGuideId = templateService.checkIsLocalUser(userIds[i],tenantIdForUsers,email);
				if(isLocalUserGuideId == null) {			
					return null;
				}
			}
			 
			if(doc == null) {		
				document = new Documents();
				document.setId(UUID.randomUUID().toString());
				document.setRefdoc(id);
				//document.setType(type);
				document.setType("tag");
				document.setName(tag.getName());
				documentRepository.save(document);
				//resourceAssembler.toResource(document);
			}else {
				document = doc;
			}
			
			if(permission.getId() == null) {
				permission.setId(UUID.randomUUID().toString());
			}else {
				permission.setId(permission.getId());
			}
			
			String[] userGuideId = {isLocalUserGuideId};
			permission.setUserIds(userGuideId);
			//permission.setUserId(email);
			//permission.setDocument(doc);
			permissionRepository.save(permission);
			return permission;
		}else {
			return null;
		}
	}
	
}
