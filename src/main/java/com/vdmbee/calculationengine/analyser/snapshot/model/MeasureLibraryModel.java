package com.vdmbee.calculationengine.analyser.snapshot.model;

import java.util.List;

import org.json.simple.JSONObject;

import lombok.Data;

@Data
public class MeasureLibraryModel {
	private String id;
	private String libraryId;
	private String tenantId;
	private List<JSONObject> fileContent;
}
