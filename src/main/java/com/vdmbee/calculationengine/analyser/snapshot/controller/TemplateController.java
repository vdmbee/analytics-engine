package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.MongoTransactionException;
import org.springframework.data.mongodb.UncategorizedMongoDbException;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.mongodb.MongoCommandException;
import com.mongodb.MongoException;
import com.vdmbee.calculationengine.analyser.formula.ValueFormula;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.model.TemplateModel;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.analyser.snapshot.service.TemplateService;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import lombok.RequiredArgsConstructor;


@RepositoryRestController
//@RequestMapping("/vdmbee/")
@RequiredArgsConstructor
public class TemplateController {
	/*private static Logger logger = LogManager.getLogger(TemplateController.class);
	@Autowired
	private TemplateService templateService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private SnapshotService snapshotService;
    
	@GetMapping(path = "/template/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public TemplateResource getTemplate(TemplateResourceAssembler templateResourceAssembler, @PathVariable("id") String id, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		
		Template template = templateService.getTemplateObject(id, tenantId, null);
		return template != null ? templateResourceAssembler.toModel(template): null;
	}*/
	
	/*@RequestMapping(path = "/template", method = RequestMethod.GET,consumes = "application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission('Address', 'create')")
	public PagedResources<TemplateResource> getAllTemplate(TemplateResourceAssembler templateResourceAssembler,Pageable pageable) {
    	if(permissionService.checkForAdmin()) {
    		Page<Template> templates = templateRepository.findAll(pageable);
        	PagedResources<TemplateResource> pagedTemplateResource = pagedAssembler.toModel(templates,templateResourceAssembler);
            return pagedTemplateResource;
    	}
		return null;
	}*/
	
	
	/*@PostMapping(path = "/template", consumes = "application/json",produces ="application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
	//@Transactional(value = "mongoTransactionManager", propagation = Propagation.REQUIRED)
    @Retryable(value = {MongoCommandException.class, MongoException.class}, exclude = {MongoTransactionException.class, UncategorizedMongoDbException.class},
            backoff = @Backoff(delay = 10), maxAttempts = 10)
    public @ResponseBody PersistentEntityResource addNewTemplate( PersistentEntityResourceAssembler resourceAssembler, @RequestBody TemplateModel templateModel,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException{
		try {
			//throw new MongoException("retry test");
			Template newTemplate = templateService.saveTemplateObject(templateModel,null);
			return newTemplate != null ? resourceAssembler.toModel(newTemplate): null;
        }catch (MongoTransactionException | UncategorizedMongoDbException ex){
            MongoException mongoException = null;
            if(ex.getCause() instanceof MongoException) {
                mongoException = (MongoException) ex.getCause();
            }else if(ex.getCause() instanceof MongoCommandException){
                mongoException = (MongoCommandException) ex.getCause();
            }
            if(mongoException!=null && mongoException.hasErrorLabel(MongoException.TRANSIENT_TRANSACTION_ERROR_LABEL)) {
                System.out.println("TransientTransactionError aborting transaction and retrying ...");
                throw mongoException;
            }else if(mongoException!=null &&  mongoException.hasErrorLabel(MongoException.UNKNOWN_TRANSACTION_COMMIT_RESULT_LABEL)){
                logger.debug("UnknownTransactionCommitResult, retrying commit operation ...");
                throw mongoException;
            }
            throw ex;
        }
		//Template newTemplate = templateService.saveTemplateObject(templateModel,null);
		//return newTemplate != null ? resourceAssembler.toModel(newTemplate): null;
    }
	
	@PostMapping(path = "/pythontemplate", consumes = "application/json",produces ="application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
    public @ResponseBody PersistentEntityResource addNewPythonTemplate( PersistentEntityResourceAssembler resourceAssembler, @RequestBody TemplateModel templateModel) throws AccessDeniedException, ParseException{
		JSONParser parser = new JSONParser();
		JSONObject data  =   (JSONObject) parser.parse(new Gson().toJson(templateModel));
		TemplateModel templateObj = templateService.fillTemplateObject(data);
		//String jsonStr = data.toString();
		//ObjectMapper objectMapper = new ObjectMapper();
		//Template templateObj = objectMapper.readValue(jsonStr,Template.class);
		Template newTemplate = templateService.saveTemplateObject(templateObj, null);
		
		//JSONObject jsonResponse = templateService.getJsonResponseTemplateObject(template);
		
		return newTemplate != null ? resourceAssembler.toModel(newTemplate): null;
    }
	
	@PutMapping(path = "/template/{id}", consumes = "application/json")
    public @ResponseBody PersistentEntityResource updateTemplate( PersistentEntityResourceAssembler resourceAssembler,@PathVariable("id") String id,@RequestBody TemplateModel templateModel, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
		
		Template newTemplate = templateService.updateTemplateObject(templateModel,tenantId,id,null);
		
		return newTemplate != null ? resourceAssembler.toModel(newTemplate): null;
    }
	
	@PatchMapping(path = "/template/{id}", consumes = "application/json")
	@PreAuthorize("hasPermission(#id,'template', null)")
	@ResponseBody
	public void patchTemplate(@PathVariable("id") String id) {};
	
    private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
    	for(int i=0;i<nodes.size();i++) {
    		JSONObject node = nodes.get(i);
    		if(node.get("id").equals(id)) {
    			return node;
    		}
    	}
    	return null;
    }
    
	@PostMapping(path = "/template/calculate/{value}", consumes = "application/json")
    public @ResponseBody String testValue(@RequestParam("tenantId") String tenantId, PersistentEntityResourceAssembler resourceAssembler, @PathVariable("value") String value, @RequestBody TemplateModel template)throws AccessDeniedException{
    	if(permissionService.checkPermission(tenantId, template.getId(), 0, null)) {
			List<JSONObject> templateData = template.getFileContent();
	    	JSONObject obj = new JSONObject((Map) getNodeWithId(templateData,value));
	    	JSONObject dataParameters = template.getDataParameters();
	    	if(obj != null) {
	    		ValueFormula formula = templateService.initialiseFormula(value, obj,dataParameters,template.getTags());
	    		Double result = formula.calculate();
	    		if(result != null) {
	    			return result.toString();
	    		}else {
	    			return null;
	    		}
	    	}
			return null;
    	}
    	else {
    		return null;
    	}
    }
	
	@PostMapping(path = "/template/aggregations/{value}", consumes = "application/json")
    public @ResponseBody String getAggregations( PersistentEntityResourceAssembler resourceAssembler, @PathVariable("value") String value, @RequestBody TemplateModel template, @RequestParam("tenantId") String tenantId) throws AccessDeniedException{
		if(!permissionService.checkPermission(tenantId, template.getId(), 1,null)) {
		      return null;
		} else {
			StringBuffer ret = new StringBuffer();
	    	List<JSONObject> templateData = template.getFileContent();
	    	JSONObject obj = new JSONObject((Map) getNodeWithId(templateData,value));
	    	JSONObject dataParameters = template.getDataParameters();
	    	if(obj != null) {
	    		ValueFormula formula = templateService.initialiseFormula(value,obj,dataParameters,template.getTags());
	    		Iterator<Document> docs = formula.getAggrigations().iterator();
	    		ret.append("{\"response\":[");
	    		boolean first = true;
	    		while(docs.hasNext()){
	    			if(!first) {
	    				ret.append(",");
	    			}else {
	    				first = false;
	    			}
	    			Document doc = docs.next();
	    			ret.append(doc.toJson());
	    		}
	    		ret.append("]}");
	    		return ret.toString();
	    	}
			return null;
		}
		//}
		//return null;
    }*/
	
	/*@RequestMapping(path = "/template/collection", method = RequestMethod.GET,consumes = "application/json")
    public @ResponseBody String getCollections( PersistentEntityResourceAssembler resourceAssembler){
		StringBuffer ret = new StringBuffer();
		String uri = env.getProperty("spring.data.mongodb.uri");
		String databaseName = uri.substring(uri.lastIndexOf("/") + 1);
		//MongoClientURI mongoClient = new MongoClientURI(uri);
		//String user = mongoClient.getUsername();
		//String passwd = mongoClient.getPassword().toString();
		//String host = mongoClient.getHosts().get(0);
		MongoClient mongo = new MongoClient(new MongoClientURI( uri ));
		MongoDatabase db = mongo.getDatabase(databaseName);
		MongoCursor<String> collections = db.listCollectionNames().iterator();
		
		ret.append("{\"response\":[");
		boolean first = true;
		while(collections.hasNext()) {
			String coll = collections.next();
			if(!first) {
				ret.append(",");
			}else {
				first = false;
			}
			
			ret.append("\"" + coll + "\"");
		}
		ret.append("]}");
		JSONParser parser = new JSONParser();
		JSONObject formulaObj;
		try {
			formulaObj = (JSONObject)parser.parse(ret.toString());	
		}catch(Exception e) {
			return e.toString();
		}finally{
			mongo.close();	
		}
		if(formulaObj != null) {
			return formulaObj.toString();	
		}else {
			return null;
		}
    }*/
	

    /*@GetMapping(path = "/template/all",produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public Page<JSONObject> getAccessableTemplates(@RequestParam("tenantId") String tenantId, Pageable pageable) throws ParseException {
		return templateService.getAccessableTemplates(tenantId, pageable);
    }
    
    @PostMapping(path = "/template/permission/{id}",  consumes = "application/json")
	@ResponseBody	
	public PersistentEntityResource addNewPermission(PersistentEntityResourceAssembler resourceAssembler,@RequestParam("tenantId") String tenantId, @PathVariable("id") String id, @RequestBody Permission permission, @RequestParam(value = "email", required = false) String email)throws AccessDeniedException {
    	if(permissionService.checkPermission(tenantId, permission.getId(), 1, email)) {
    		Permission newPermission = templateService.saveTemplateAndPermissionObject(permission,tenantId,id, null);
    		return newPermission != null ? resourceAssembler.toModel(newPermission): null;
    	}else {
    		return null;
    	}
	}
    
    @RequestMapping(value = "/template/{id}", method = RequestMethod.DELETE, consumes = "application/json")
    @ResponseBody
    public void deleteTemplate(@PathVariable("id") String id, @RequestParam("tenantId") String tenantId) throws AccessDeniedException {
    	templateService.deleteTemplateObject(id,tenantId, null);
    }
    
    @GetMapping(path = "/template/getAllValues/{tenantId}", consumes = "application/json")
    @ResponseBody
    public List<JSONObject> getTemplateValuesForCurrentUser(@PathVariable("tenantId") String tenantId) throws  ParseException {
    	return permissionService.getdocs(tenantId, "snapshot", null,null);
    }*/
    
}
