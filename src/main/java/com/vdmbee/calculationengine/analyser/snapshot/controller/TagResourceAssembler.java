package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Tag;


public class TagResourceAssembler extends RepresentationModelAssemblerSupport<Tag, TagResource> {

		public TagResourceAssembler() {
			super(TagController.class, TagResource.class);
		}
		 @Override
		 public TagResource toModel(Tag tag) {
		    TagResource resource = createModelWithId(tag.getId(), tag);
		    // … do further mapping
		    resource.setName(tag.getName());
		    resource.setTenantId(tag.getTenantId());
		    resource.setType(tag.getType());
		    resource.setValues(tag.getValues());
		    
		    return resource;
		 }	

}
