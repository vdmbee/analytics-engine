package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.client.result.DeleteResult;
import com.vdmbee.calculationengine.analyser.formula.ValueFormula;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import com.vdmbee.calculationengine.analyser.snapshot.model.TemplateModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.document.entity.Documents;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.repository.DocumentsRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;

@Service
public class TemplateService {
	
	@Autowired 
	private TemplateRepository templateRepository;

	@Autowired
	private PermissionService permissionService;
	@Autowired
	private DocumentsRepository documentRepository;
	@Autowired
	private MongoTenantTemplate mongoTemplate;
	@Autowired
	private Environment env;

	private static Logger logger = LogManager.getLogger(TemplateService.class);


	
	public Template getTemplateObject(String id, String tenantId, String email) throws AccessDeniedException {
		Template template =  null;
		if(permissionService.checkPermission(tenantId, id, 0, email)) {
			Optional<Template> templateObj = templateRepository.findById(id);			
			if(templateObj.isPresent()) {
				template = templateObj.get();
			}
		}
		return template;
	}

	public JSONObject saveTemplateFromQueue(JSONObject jsonObj) throws Exception{
		try{
			//String tenantId = (String)jsonObj.get("tenantId");
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			TemplateModel templateObj = fillTemplateObject(data);
			//String jsonStr = data.toString();
			//ObjectMapper objectMapper = new ObjectMapper();
			//Template templateObj = objectMapper.readValue(jsonStr,Template.class);

			Template template = saveTemplateObject(templateObj, email);
			
			JSONObject jsonResponse = getJsonResponseTemplateObject(template);
	        //String jsonString = objectMapper.writeValueAsString(template);
	        //JSONParser parser = new JSONParser();
	        //JSONObject jsonResponse = (JSONObject) parser.parse(jsonString);
	        //System.out.println("tempalte"+jsonResponse.toString());
	        return jsonResponse;
		}catch(Exception e) {
			logger.error("Error while performing saveTemplateFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public TemplateModel fillTemplateObject(JSONObject data) {
		TemplateModel template = new TemplateModel();
		if(!StringUtils.isEmpty(data.get("tags"))) {//for vdmbee
			List<JSONObject> tagList = new ArrayList<JSONObject>();
			tagList.addAll((List<JSONObject>)data.get("tags"));
			template.setTags(tagList);
		}
		template.setId((String) data.get("id"));
		template.setTenantId((String) data.get("tenantId"));
		template.setName((String) data.get("name"));
		template.setContext((String) data.get("context"));
		template.setMeasures((List<String>)data.get("measures"));
		List<JSONObject> fileContent = (List)data.get("fileContent");
		List<JSONObject> newFileContent = generateSingleScript(template.getId(),fileContent,template);
		template.setFileContent(newFileContent);
		return template;
	}
	
	private List<JSONObject> generateSingleScript(String templateId, List<JSONObject> fileContent,TemplateModel template) {
		List<JSONObject> ret = new ArrayList<>();
		JSONObject singleFormula = new JSONObject();
		singleFormula.put("ValueType", "Calculated");
		
		LinkedHashMap<String, TemplateValue> formulas = TemplateValue.getNodes(fileContent);
		String newTemplateId = "Template_" + UUID.randomUUID().toString().replaceAll("[-@]", "_");
		singleFormula.put("id",  newTemplateId);
		singleFormula.put("isSingleScript", true);
		JSONObject formula = new JSONObject();
		formula.put("expression", "pythonExecutor()");
		String script = TemplateValue.getSingleScript(newTemplateId,formulas);
		formula.put("pythonScript", script);
		TemplateValue benchmarkFormula = formulas.get("benchmark");
		if(benchmarkFormula != null && benchmarkFormula.getMeasures() != null && benchmarkFormula.getMeasures().size() > 0) {
			template.setMeasures(benchmarkFormula.getMeasures());
		}
		Set<String> dependentValues = new HashSet();
		Set<String> rubricsToCalculate = new HashSet();
		Iterator<String> keys = formulas.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			TemplateValue tempValue = formulas.get(key);
			List<String> valueDependents = tempValue.getDependentValues();
			if(valueDependents != null) {
				Iterator<String> valueDependentsIterator = valueDependents.iterator();
				while(valueDependentsIterator.hasNext()) {
					String valKey = valueDependentsIterator.next();
					TemplateValue valTemplate = formulas.get(valKey);
					if (valTemplate != null && !valTemplate.isCalculatedValue() && !dependentValues.contains(valKey)) {
						dependentValues.add(valTemplate.getId());	
					}
				}
			}else if (tempValue != null && !tempValue.isCalculatedValue() && !dependentValues.contains(key)) {
				dependentValues.add(tempValue.getId());	
			}
			if(tempValue.getRubricsToCalculate() != null && tempValue.getRubricsToCalculate().size() > 0) {
				rubricsToCalculate.addAll(tempValue.getRubricsToCalculate());
			}
		}
		formula.put("dependentValues", dependentValues);
		if(rubricsToCalculate.size() > 0) {
			formula.put("rubricsToCalculate", rubricsToCalculate);
		}
		singleFormula.put("Formula", formula);
		ret.add(singleFormula);
		//Add also input nodes to the template so we know which of them are inputs, skiping calculated values
		for(int i=0;i<fileContent.size();i++) {
			JSONObject node = fileContent.get(i);
			if(node.get("ValueType").equals("Input")) {
				ret.add(node);
			}
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject getJsonResponseTemplateObject(Template template) {
		  JSONObject jsonResponse = new JSONObject();
	      jsonResponse.put("tags", template.getTags());
	      jsonResponse.put("tenantId", template.getTenantId());
	      jsonResponse.put("context", template.getContext());
	      jsonResponse.put("name", template.getName());
	      jsonResponse.put("id", template.getId());
	      jsonResponse.put("lastModified", template.getLastModified().toString());
	      jsonResponse.put("fileContent", template.getFileContent());
	      return jsonResponse;
	}
	
	public JSONObject updateTemplateFromQueue(JSONObject jsonObj) throws Exception {//currently unsed for bms
		try {
			String tenantId = (String)jsonObj.get("tenantId");
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			String jsonStr = data.toString();
			ObjectMapper objectMapper = new ObjectMapper();
			TemplateModel templateObj = objectMapper.readValue(jsonStr, TemplateModel.class);
			Template template = updateTemplateObject(templateObj, tenantId, templateObj.getId(), email);
			
	        String jsonString = objectMapper.writeValueAsString(template);
	        JSONParser parser = new JSONParser();
	        JSONObject jsonResponse = (JSONObject) parser.parse(jsonString);
	        return jsonResponse;
		}catch(Exception e) {
			logger.error("Error while performing updateTemplateFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}
	public Template saveTemplateObject(TemplateModel templateModel, String email) throws AccessDeniedException {
		ModelMapper modelMapper = new ModelMapper();
        Template template = modelMapper.map(templateModel, Template.class);
		Template existingTemplate = null;
		if(template.getId() != null) {
			Optional<Template> templateObj = Optional.empty();
			try {
				templateObj = templateRepository.findById(template.getId());
			}catch(Exception e) {
				logger.error("Error while fetching template by id inside saveTemplateObject method: " + e.getMessage(), e);
			}
			if(templateObj != null && templateObj.isPresent()) {
				existingTemplate = templateObj.get();
			}
		}
		/*else {
			existingTemplate = templateRepository.findOneByTenantIdAndContext(template.getTenantId(),template.getContext());	
		}*/
		
		if(existingTemplate != null) {
			if(permissionService.checkPermission(template.getTenantId(), template.getId(), 1, email)) {
				if(!existingTemplate.getFileContent().equals(template.getFileContent()) || (template.getMeasures() != null && !(template.getMeasures().equals(existingTemplate.getMeasures())))) {
					existingTemplate.setName(template.getName());
					existingTemplate.setContext(template.getContext());
					
					existingTemplate.setFileContent(createcustomJsonList(template.getFileContent()));
					existingTemplate.setTags(template.getTags());
					existingTemplate.setMeasures(template.getMeasures());
					existingTemplate.setLastModified(""+new Date().getTime());
					templateRepository.save(existingTemplate);
				}				
				return existingTemplate;
			}else {
				logger.error("permission failed to update template for tenant:"+template.getTenantId()+" for email:"+email);
			}
		}else {
			if(permissionService.isMemberOfTenant(template.getTenantId(),email)) {
				if(template.getId() == null) {
					template.setId(UUID.randomUUID().toString());
				}else {
					template.setId(template.getId());
				}
				String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
	 			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
	 				template.setTenantId(null);//we don't use it
	 			} else {
	 				template.setTenantId(MongoTenantContext.getTenant());
				}
				
				Documents document = new Documents();
				document.setRefdoc(template.getId());
				document.setType("template");
				permissionService.addDocument(document, email);
				
				template.setFileContent(createcustomJsonList(template.getFileContent()));
				template.setLastModified(""+new Date().getTime());
				templateRepository.save(template);
				return template;
			}else {
				logger.error("permission failed to create template for tenant:"+template.getTenantId()+" for email:"+email);
			}
		}
		return existingTemplate;
	}
	
	private List<JSONObject> createcustomJsonList(List<JSONObject> contentList) {
		List<JSONObject> ret = new ArrayList();
		for(int i=0;i<contentList.size();i++) {
			ret.add(createcustomJson(contentList.get(i)));
		}
		return ret;
	}
	private JSONObject createcustomJson(JSONObject content) {
		JSONObject retJson  = new JSONObject();

		if (content instanceof JSONObject) {
	        for (Object key : content.keySet()) {
	        	Object str = content.get(key);
	        	if(str instanceof Map) {
	        		JSONObject jsonObject = new JSONObject();
	        		Map contentMap = (Map)content.get(key);
	        		Iterator contentIterator = contentMap.keySet().iterator();
		        	while(contentIterator.hasNext()) {
		        		String contentKey = (String) contentIterator.next();
		        		try {
		        			Map jsonmap = (Map) contentMap.get(contentKey);
		        			JSONObject valuejson = new JSONObject(jsonmap);
		        			JSONObject myjson = createcustomJson(valuejson);
		        			jsonObject.put(contentKey, myjson);
		        	    } catch (Exception ex) {
		        	    	jsonObject.put(contentKey, contentMap.get(contentKey));
		        	    }
		        	}
		        	retJson.put(key, jsonObject);
	        	}else {
	        		retJson.put(key, str);
	        	}
	        }

	    } else if(content != null){
	    	retJson  = new JSONObject(content);
	    }
		return retJson;
	}
	
	public ValueFormula initialiseFormula(String value, JSONObject obj,JSONObject dataParameters,List<JSONObject> tags) {
		ValueFormula valueFormula = null;
		try {
			Object inputObj = obj.get("Formula");
			JSONObject formulaObj;
			String formula;
			if(inputObj instanceof String) {
				formula = (String)inputObj;
				JSONParser parser = new JSONParser();
				formulaObj = (JSONObject)parser.parse(formula);	
			}else {
				formulaObj = new JSONObject((Map) inputObj);
				formula = formulaObj.toJSONString();
			}
			
			String expression = null;
			expression = (String)formulaObj.get("expression");	
			List<JSONObject> stages = (List<JSONObject>)formulaObj.get("Stages");
			List<String> sortFields = (List<String>)formulaObj.get("SortFields");
			String direction = (String)formulaObj.get("SortDirection");
			String outputField = (String)formulaObj.get("OutputField");
			String mongoJsonQuery = (String)formulaObj.get("MongoJsonQuery");
			String mongoValueField = (String)formulaObj.get("MongoValueField");
			String mongoFormula = (String)formulaObj.get("MongoFormula");
			
    		String snapshotId = (String)formulaObj.get("snapshotId");
    		ArrayList<String> snapshotIds = (ArrayList<String>)formulaObj.get("snapshotIds");
    		ArrayList<String> dependentValues = (ArrayList<String>)formulaObj.get("dependentValues");
    		String snapshotExpression = (String)formulaObj.get("snapshotExpression");
    		String snapshotValue = (String)formulaObj.get("snapshotValue");
    		String snapshotTenantId = (String)formulaObj.get("snapshotTenantId");
    		String snapshotContext = (String)formulaObj.get("snapshotContext");
    		String snapshotPeriod = (String)formulaObj.get("snapshotPeriod");
    		String snapshotUnit = (String)formulaObj.get("snapshotUnit");
    		List<JSONObject> rubricsToCalculate = (List<JSONObject>)formulaObj.get("rubricsToCalculate");
			/*if(formulaStr.equalsIgnoreCase("undefined") || formulaStr.equalsIgnoreCase("")) {
				continue;
			}*/
			Sort.Direction directionVal = Sort.Direction.ASC;
			if(direction != null) {
				if(direction.equals("1")) {
					directionVal = Sort.Direction.ASC;
				}else {
					directionVal = Sort.Direction.DESC;
				}
			}
			
			HashMap<String, ValueFormula> values = new HashMap<>();
			HashMap<String, Double> calculatedValues = new HashMap<>();
			valueFormula = new ValueFormula(value,null,expression,stages,sortFields,directionVal,outputField,mongoJsonQuery,mongoValueField,mongoFormula,snapshotId,snapshotTenantId,snapshotValue,snapshotContext,snapshotPeriod,snapshotUnit,snapshotIds,snapshotExpression,calculatedValues,dataParameters,tags,dependentValues,rubricsToCalculate);	
			
		}catch(ParseException pe) {
			logger.error(pe.getMessage());
		}
		return valueFormula;
    }
	
	public JSONObject fetchTemplateFromQueue(JSONObject jsonObj) throws Exception{//not needed
		try{
			String tenantId = (String)jsonObj.get("tenantId");
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			String templateId = (String)data.get("id");
			return fetchTemplate(templateId, tenantId, email);
		}catch(Exception e) {
			logger.error("Error while performing fetchTemplateFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}

	public JSONObject fetchTemplate(String id, String tenantId, String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, id, 2, email)) {	
			Optional<Template> existignTemplateObj = templateRepository.findById(id);
			Template existingTemplate = null;
			if(existignTemplateObj.isPresent()) {
				existingTemplate = existignTemplateObj.get();
				return getJsonResponseTemplateObject(existingTemplate);
			}
		}else {
			return null;
		}
		return null;
	}
	
	public void deleteTemplateFromQueue(JSONObject jsonObj) throws Exception{
		try{
			String email = (String)jsonObj.get("email");
			JSONObject data = (JSONObject)jsonObj.get("data");
			String templateId = (String)data.get("id");
			String tenantId = (String)data.get("tenantId");
			deleteTemplateObject(templateId, tenantId, email);
		}catch(Exception e) {
			logger.error("Error while performing deleteTemplateFromQueue method: " + e.getMessage(), e);
			throw e;
		}
	}

	public void deleteTemplatesFromQueue(JSONObject jsonObj) throws Exception {
        String tenantId = null;
        String email = null;
        try {
            tenantId = (String) jsonObj.get("tenantId");
            email = (String) jsonObj.get("email");
            JSONObject data = (JSONObject) jsonObj.get("data");
            ObjectMapper mapper = new ObjectMapper();
            if (data.containsKey("ids")) {
                org.json.simple.JSONArray idsArr = (org.json.simple.JSONArray) data.get("ids");
                if (idsArr != null && idsArr.size() > 0) {
                    List<String> templateIds = mapper.readValue(idsArr.toString(), new TypeReference<List<String>>() {

                    });
                    List<String> deletionFailedTemplates = new ArrayList<>();
                    for (String id : templateIds) {
                        try {
                            deleteTemplateObject(id, tenantId, email);
                        } catch (Exception e) {
                            deletionFailedTemplates.add(id);
                            logger.error(String.format("Error while performing deleteTemplateObject method for tenantId: %s, email: %s, templateId: %s", tenantId, email, id), e);
                        }
                    }
                    if (!deletionFailedTemplates.isEmpty()) {
                        throw new Exception(String.format("Error in deleteTemplatesFromQueue method while processing deletion for the templates: %s", deletionFailedTemplates));
                    }
                }
            }
        } catch (Exception e) {
            logger.error(String.format("Error while performing deleteTemplatesFromQueue method for tenantId: %s, email %s", tenantId, email), e);
            throw e;
        }
    }

	public void deleteTemplateObject(String id, String tenantId, String email) throws AccessDeniedException {
	    	//snapshot deletion
		if(permissionService.checkPermission(tenantId, id, 2, email)) {
	    	Query getSnapshots = new Query();
	    	getSnapshots.addCriteria(Criteria.where("templateId").is(id));
	    	List<Snapshot> snapshotList = mongoTemplate.find(getSnapshots, Snapshot.class);
	    	for(Snapshot s : snapshotList) {
	    		permissionService.deleteDocument(s.getId());
	    	}
	    	DeleteResult deletedSnapshots = mongoTemplate.remove(getSnapshots, Snapshot.class);
	    	//System.out.println(deletedSnapshots);
	    	
	    	//template deletion
	    	permissionService.deleteDocument(id);
	    	Query templateDelete = new Query();
	    	templateDelete.addCriteria(Criteria.where("_id").is(id));
	    	DeleteResult deletedTemplate = mongoTemplate.remove(templateDelete, Template.class);
	    	//System.out.println(deletedTemplate);		
		}
	}


	public Template updateTemplateObject(TemplateModel template, String tenantId, String id, String email) throws AccessDeniedException {
		Template existingTemplate =  null;
		if(permissionService.checkPermission(tenantId, template.getId(), 1, email)) {
			Optional<Template> templateObj = templateRepository.findById(id);
			if(templateObj.isPresent()) {
				existingTemplate = templateObj.get();
			}
			
			if(existingTemplate != null) {
				existingTemplate.setName(template.getName());
				existingTemplate.setContext(template.getContext());
				existingTemplate.setFileContent(template.getFileContent());
				existingTemplate.setTags(template.getTags());
				templateRepository.save(existingTemplate);
			}
		}
		return existingTemplate;
	}
	
	public void deleteTenant(JSONObject jsonObj) throws Exception {//VPBMS-3770
		try {
			ApplicationTransactionManager.getApplicationTransaction().rollbackTransaction();
			mongoTemplate.getDb().drop();
		}catch(Exception e){
			logger.error("Error deleteTenant: " + e.getMessage(), e);
		}
	}

	public Permission saveTemplateAndPermissionObject(Permission permission, String tenantId, String id, String email) throws AccessDeniedException {
		if(permissionService.checkPermission(tenantId, permission.getId(), 1, email)) {
			if(permission.getId() == null) {
				permission.setId(UUID.randomUUID().toString());
			}else {
				permission.setId(permission.getId());
			}
			List<Permission> documentPermissionList = new ArrayList<Permission>();
			documentPermissionList.add(permission);
			
			String RefDoc = id;
			String isLocalUserGuideId="";
			Documents existingDocument = documentRepository.getDocumentByRefdoc(RefDoc);
	
			Optional<Template> tempObj = templateRepository.findById(id);
			if(tempObj.isPresent()) {
				Template template = tempObj.get();	
				
				String tenantIdForUsers = template.getTenantId();
				String[] userIds = permission.getUserIds();
				for(int i = 0; i < userIds.length; i++) {
					isLocalUserGuideId = permissionService.checkIsLocalUser(userIds[i],tenantIdForUsers,email);
					if(isLocalUserGuideId == null) {			
						return null;
					}
				}
				 
				if(existingDocument == null) {		
					Documents document = new Documents();
					document.setId(UUID.randomUUID().toString());
					document.setRefdoc(id);
					document.setPermission(documentPermissionList);
					document.setType("template");
					document.setName(template.getName());
					documentRepository.save(document);
				} else {
					List<Permission> permissionList = existingDocument.getPermission();
					permissionList.add(permission);
					existingDocument.setPermission(permissionList);
					documentRepository.save(existingDocument);
				}
							
				
					/*permission.setDocument(document);
					String[] userGuideId = {isLocalUserGuideId};
					permission.setUserIds(userGuideId);
					//permission.setUserId(email);
					//permission.setDocument(doc);
					permissionRepository.save(permission);*/
			}
		}
    	return permission;
	}
	
	/*public static void main(String args[]) throws ParseException, IOException {
		TemplateService ts = new TemplateService();
		JSONParser parser  =new JSONParser();
		JSONObject template = (JSONObject)parser.parse(readJSONFiile("benchmarkTemplate.json"));
		List<JSONObject> newFileContent = ts.generateSingleScript("MyTemplate" , (List<JSONObject>)template.get("fileContent"));
		System.out.println(newFileContent);
	}
	
   private static String readJSONFiile(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		String ls = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		// delete the last new line separator
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		reader.close();
		return stringBuilder.toString();
	}*/
	
	public Page<JSONObject> getAccessableTemplates(String tenantId, Pageable pageable) throws ParseException {
		List<JSONObject> templateList = permissionService.getdocs(tenantId, "template", null,pageable);
		int start = (int) pageable.getOffset();
		int end = (start + pageable.getPageSize()) > templateList.size() ? templateList.size()
				: (start + pageable.getPageSize());
		return new PageImpl<JSONObject>(templateList.subList(start, end), pageable, templateList.size());
    }
}
