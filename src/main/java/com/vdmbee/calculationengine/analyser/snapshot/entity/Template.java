package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.util.List;

import javax.persistence.Transient;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="template")
public class Template {
	@Id
    private String id;

	private String tenantId;
	
	private String name;
	
	private String lastModified;
	
	private Boolean validator = Boolean.FALSE;
	
	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}

	private String context;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

	public List<JSONObject> getFileContent() {
		return fileContent;
	}

	public void setFileContent(List<JSONObject> fileContent) {
		this.fileContent = fileContent;
	}

	private List<JSONObject> fileContent;
	@Transient
	private org.json.simple.JSONObject dataParameters;

	public org.json.simple.JSONObject getDataParameters() {
		return dataParameters;
	}

	public void setDataParameters(org.json.simple.JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}
	private List<JSONObject> tags;
	private List<String> measures;

	public List<String> getMeasures() {
		return measures;
	}

	public void setMeasures(List<String> measures) {
		this.measures = measures;
	}

	public List<JSONObject> getTags() {
		return tags;
	}

	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}
	
	@Override
	public String toString() {
		 /*return String.format(
	                "Customer[id=%s, tenantId='%s', name='%s',validator='%b',context='%s']",
	                id, tenantId, name,validator,context);*/
		JSONObject obj= new JSONObject();
		obj.put("id",id);
		obj.put("tenantId",tenantId);
		obj.put("name",name);
		obj.put("context",context);
		return "[" + obj.toJSONString() + "]";
	}
	
}
