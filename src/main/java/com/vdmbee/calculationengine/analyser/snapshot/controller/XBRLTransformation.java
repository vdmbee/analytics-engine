package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.data.annotation.Id;

public class XBRLTransformation {

	@Id
    private String id;
	
	private String templateId;
	private String xbrlDocument;
	private String xsltDocument;
	private String period;
	private String unit;
	private String name;
	private String context;
	private String xsltParameters;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContext() {
		return context;
	}
	public String getXsltParameters() {
		return xsltParameters;
	}
	public void setXsltParameters(String xsltParameters) {
		this.xsltParameters = xsltParameters;
	}
	public void setContext(String context) {
		this.context = context;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	public String getXbrlDocument() {
		return xbrlDocument;
	}
	public void setXbrlDocument(String xbrlDocument) {
		this.xbrlDocument = xbrlDocument;
	}
	public String getXsltDocument() {
		return xsltDocument;
	}
	public void setXsltDocument(String xsltDocument) {
		this.xsltDocument = xsltDocument;
	}
	private List<JSONObject> tags;

	public List<JSONObject> getTags() {
		return tags;
	}

	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}

}
