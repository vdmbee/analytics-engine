package com.vdmbee.calculationengine.analyser.snapshot.controller;


import java.util.List;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Category;
import com.vdmbee.calculationengine.analyser.snapshot.model.CategoryModel;
import com.vdmbee.calculationengine.analyser.snapshot.service.CategoryService;
import com.vdmbee.calculationengine.document.entity.Permission;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;
import com.vdmbee.calculationengine.multitenant.MongoDBCredentials;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class CategoryController {

	@Autowired
	private CategoryService categoryService;

	@Autowired
	private Environment env;

	@Autowired
	private PermissionService templateService;

	@Autowired
	private PermissionService permissionService;

	@Autowired
	public MongoDBCredentials mongoDBCredentials;

	@GetMapping(path = "/category/{id}",consumes = "application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
	public @ResponseBody CategoryResource getCategory(CategoryResourceAssembler categoryResourceAssembler, @PathVariable("id") String id,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException {
		CategoryResource resource = null;
		Category category = categoryService.getCategoryObject(tenantId,id);
		if(category != null) {
			resource = categoryResourceAssembler.toModel(category);
		}
		return resource;
	}

	@GetMapping(path = "/category/all",consumes = "application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission('Address', 'create')")
	public Page<JSONObject> getAllCategory(CategoryResourceAssembler categoryResourceAssembler,Pageable pageable,@RequestParam(value = "tenantId", required = false) String tenantId) throws  ParseException {
		/*if(this.checkAccessToTenant(tenantId)) {
	    	if(templateService.checkForAdmin()) {
	    		Page<Category> categorys = categoryRepository.findAll(pageable);
	        	PagedResources<CategoryResource> pagedCategoryResource = pagedAssembler.toResource(categorys,categoryResourceAssembler);
	            return pagedCategoryResource;
	    	}
		}*/
		//return permissionService.getdocs(tenantId, pageable, "category");
		List<JSONObject> categoryList = permissionService.getdocs(tenantId, "category", null,pageable);
		int start = (int) pageable.getOffset();
		int end = (start + pageable.getPageSize()) > categoryList.size() ? categoryList.size()
				: (start + pageable.getPageSize());
		return new PageImpl<JSONObject>(categoryList.subList(start, end), pageable, categoryList.size());
		//return null;
	}

	@PostMapping(path = "/category", consumes = "application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
    public @ResponseBody PersistentEntityResource addNewCategory( PersistentEntityResourceAssembler resourceAssembler, @RequestBody CategoryModel categoryModel,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException{
		Category categoryobj = categoryService.saveCategoryObjects(categoryModel,tenantId,null);
		return categoryobj != null?resourceAssembler.toModel(categoryobj):null;
    }

	@PostMapping(path = "/category/{id}", consumes = "application/json")
    public @ResponseBody PersistentEntityResource updateCategory( PersistentEntityResourceAssembler resourceAssembler,@PathVariable("id") String id,@RequestBody CategoryModel categoryModel,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException{
		Category categoryobj = categoryService.saveCategoryObjects(categoryModel,tenantId,null);
		return categoryobj != null?resourceAssembler.toModel(categoryobj):null;
    }


	@GetMapping(path = "/category/collection", consumes = "application/json")
    public @ResponseBody String getCollections( PersistentEntityResourceAssembler resourceAssembler,@RequestParam(value = "tenantId", required = false) String tenantId)throws AccessDeniedException{
		if(categoryService.checkAccessToTenant(tenantId,null)) {

			StringBuffer ret = new StringBuffer();
			//String uri = env.getProperty("spring.data.mongodb.uri");
			//String databaseName = uri.substring(uri.lastIndexOf("/") + 1);
			/*MongoClientURI mongoClient = new MongoClientURI(uri);
			String user = mongoClient.getUsername();
			String passwd = mongoClient.getPassword().toString();
			String host = mongoClient.getHosts().get(0);*/

			/*String mongoUrl = mongoDBCredentials.getPath() + "/" + MongoTenantContext.getTenant();

			MongoClient mongo = MongoClients.create(mongoUrl );*/
			MongoClient mongo = mongoDBCredentials.getMongoClient(MongoTenantContext.getTenant(), null);
			MongoDatabase db = mongo.getDatabase(MongoTenantContext.getTenant());
			MongoCursor<String> collections = db.listCollectionNames().iterator();
			ret.append("{\"response\":[");
			boolean first = true;
			while(collections.hasNext()) {
				String coll = collections.next();
				if(!first) {
					ret.append(",");
				}else {
					first = false;
				}
				ret.append("\"" + coll + "\"");
			}
			ret.append("]}");
			JSONParser parser = new JSONParser();
			JSONObject formulaObj;
			try {
				formulaObj = (JSONObject)parser.parse(ret.toString());
			}catch(Exception e) {
				return e.toString();
			}finally{
				mongo.close();
			}
			if(formulaObj != null) {
				return formulaObj.toString();
			}else {
				return null;
			}
		}
		return null;

    }

    @GetMapping(path = "/category/accessDocs/{tenantId}", consumes = "application/json" )
    @ResponseBody
    public Page<JSONObject> getAccessableCategorys(@PathVariable("tenantId") String tenantId,Pageable pageable) throws ParseException {
		//return templateService.getdocs(tenantId, pageable, "category");
    	List<JSONObject> templateList = permissionService.getdocs(tenantId, "category", null,pageable);
		int start = (int) pageable.getOffset();
		int end = (start + pageable.getPageSize()) > templateList.size() ? templateList.size()
				: (start + pageable.getPageSize());
		return new PageImpl<JSONObject>(templateList.subList(start, end), pageable, templateList.size());
    }

    @PostMapping(path = "/category/permission/{id}", consumes = "application/json")
	@ResponseBody
	public PersistentEntityResource addNewPermission(@RequestParam("refdoc") String refdoc, @RequestParam("tenantId") String tenantId, PersistentEntityResourceAssembler resourceAssembler, @PathVariable("id") String id, @RequestBody Permission permission) throws AccessDeniedException {

		Permission newPermission = categoryService.createNewPermission(id, tenantId, permission, null);
		return newPermission!= null ? resourceAssembler.toModel(newPermission):null;
	}

    @DeleteMapping(value = "/category/{id}", consumes = "application/json")
    @ResponseBody
    public void deleteCategory(@PathVariable("id") String id,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException {
    	categoryService.deleteCategory(id, tenantId);
    }

}
