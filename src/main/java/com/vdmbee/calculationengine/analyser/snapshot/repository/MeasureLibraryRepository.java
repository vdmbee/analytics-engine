package com.vdmbee.calculationengine.analyser.snapshot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.MeasureLibrary;

@Component
@RepositoryRestResource(collectionResourceRel = "library", path = "library")
public interface MeasureLibraryRepository extends MongoRepository<MeasureLibrary, String> {
	MeasureLibrary findOneByTenantIdAndLibraryId(String tenandId,String libraryId); 
}
