package com.vdmbee.calculationengine.analyser.snapshot.model;

import com.vdmbee.calculationengine.analyser.snapshot.entity.CategoryType;
import lombok.Data;

@Data
public class CategoryModel {
	private String id;
	private String tenantId;
	private String name;
	private CategoryType categoryType;
	private String parent;
	private double upperBound;
	private double lowerBound;
	
}
