package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;


public class TemplateResourceAssembler extends RepresentationModelAssemblerSupport<Template, TemplateResource> {

		public TemplateResourceAssembler() {
			super(TemplateController.class, TemplateResource.class);
		}
		 @Override
		 public TemplateResource toModel(Template template) {
		    TemplateResource resource = createModelWithId(template.getId(), template);
		    // … do further mapping
		    resource.setName(template.getName());
		    resource.setTenantId(template.getTenantId());
		    resource.setContext(template.getContext());
		    resource.setFileContent(template.getFileContent());
		    resource.setTags(template.getTags());
		    return resource;
		 }	

}
