package com.vdmbee.calculationengine.analyser.snapshot.model;

import java.util.List;

import org.json.simple.JSONObject;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Template;
import lombok.Data;

@Data
public class SnapshotModel {
	private String id;

	private String tenantId;

	public String context;
	private String period;
    private String unit;
    
    private String errorCode;
    
    private String errorMessage;

	private String lastModified;
    
    private List<JSONObject> tags;
	private Template template;
	
	private Boolean contentChange;
	private Boolean persistSnapshot;
	private String name;
	private String templateId;
	private String validationTemplateId;
	private List<JSONObject> fileContent;
	private org.json.simple.JSONObject dataParameters;
    private String modifiedBy;
    private boolean useExistingData;
    private String rescaleRubric;
    private Double rescaleFactor;
}
