package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Given a Task with list of its dependencies and execute method. Run the task such that dependencies are executed first.
 * You are given x number of threads. Increase parallelism as much as possible.
 */
public class DependencyTaskExecutor {
	private static final Logger logger = LogManager.getLogger(DependencyTaskExecutor.class);
	private static ExecutorService executor = new ThreadPoolExecutor(
            1,        // Minimum number of threads
            10,         // Maximum number of threads
            1*60*1000L,                  // Keep alive time for idle threads (0L means keep alive indefinitely)
            java.util.concurrent.TimeUnit.MILLISECONDS, // Time unit for the keep alive time
            new java.util.concurrent.LinkedBlockingQueue<Runnable>() // Work queue for holding tasks before execution
        );

	
    private Map<String, CompletableFuture<Void>> taskTracker = new HashMap<>();
    private CompletableFuture<Void> future;
    public boolean isComplete() {
		return future.isDone();
	}

    
    public void scheduleTask(List<Task> tasks) {
        this.future = CompletableFuture.completedFuture(null);
        for (Task task : tasks) {
            future = future.thenAcceptBothAsync(scheduleTaskUtil(task, executor), (a, b) -> {}, executor);
        }
        /*future.thenRunAsync(() -> {
        	logger.debug("All tasks done. Closing executor "+ new Date()); 
        	executor.shutdown();
        	this.complete = true;
        });*/
    }

    private CompletableFuture<Void> scheduleTaskUtil(Task task, Executor executor) {
        CompletableFuture<Void> f = taskTracker.get(task.name());
        if (f != null) {
            return f;
        }
        if (task.dependencies().isEmpty()) {
            CompletableFuture<Void> future = CompletableFuture.runAsync(() -> task.execute(), executor);
            taskTracker.put(task.name(), future);
            return future;
        }
        CompletableFuture<Void> future = CompletableFuture.completedFuture(null);;
        for (Task upstreamTask : task.dependencies()) {
            future = future.thenAcceptBothAsync(scheduleTaskUtil(upstreamTask, executor), (a, b) -> {}, executor);
        }
        future = future.thenRunAsync(() -> task.execute(), executor);
        taskTracker.put(task.name(), future);
        return future;
    }

	public Map<String, CompletableFuture<Void>> getTaskTracker() {
		return taskTracker;
	}
	/*public void shutdown() throws InterruptedException {
		if(this.executor != null) {
			this.executor.shutdown();
			try {
				if(!this.executor.awaitTermination(1000, TimeUnit.MICROSECONDS)) {
					this.executor.shutdownNow();
			    }
			}catch (InterruptedException e){
				logger.error(e);
				//Thread.currentThread().interrupt();
			}
		}
		this.complete = true;
	}*/
	
	public Boolean awaitTermination(Long waitTime) throws InterruptedException, ExecutionException, TimeoutException {
		if(!this.future.isDone()) {
			//return this.future.awaitTermination(waitTime, TimeUnit.SECONDS);
			this.future.get(waitTime, TimeUnit.SECONDS);
			//this.future.completeOnTimeout(null, waitTime, TimeUnit.SECONDS);
			if(this.future.isDone()) {
				return true;
			}
			return this.future.cancel(false);
		}else {
			//logger.error("Thread not closed:"+Thread.currentThread().getName());
			if(this.isComplete()) {
				 taskTracker.forEach((key, completableFuture) -> completableFuture.cancel(true));
			     taskTracker.clear();
			}
			return true;
		}
	}
	public void cancel() {
		future.cancel(true);
	}
	
}

