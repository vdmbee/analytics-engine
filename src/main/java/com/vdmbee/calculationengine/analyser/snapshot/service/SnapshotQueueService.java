package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.transaction.Transactional;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;


@Service
public class SnapshotQueueService {
	@Autowired
	private SnapshotService snapshotService;
	
	@Autowired
	private PermissionService permissionService;
	
	public JSONObject getSnapshot(JSONObject body) throws Exception{
		JSONObject dataObj = (JSONObject) body.get("data");
		String snapshotId = null;
		if (dataObj != null) {
			snapshotId = (String) dataObj.get("id");
        }
		if(snapshotId == null) {
			throw new Exception("Snapshot Id can not be null");
		}
		return snapshotService.fetchSnapshotFromQueue(body);
	}

	public JSONObject getSnapshotsByRubricExternalId(JSONObject body) throws Exception{
		return snapshotService.fetchSnapshotsByRubricExternalIdFromQueue(body);
	}

	public JSONObject createSnapshot(JSONObject snapshotRequest) throws Exception{
		return snapshotService.saveSnapshotFromQueue(snapshotRequest);
	}
	

	public JSONObject updateSnapshot(JSONObject snapshotRequest) throws Exception{
		return snapshotService.updateSnapshotFromQueue(snapshotRequest);
	}
	

	public void deleteSnapshot(JSONObject snapshotRequest) throws Exception{
		JSONObject dataObj = (JSONObject) snapshotRequest.get("data");
		String snapshotId = null;
		if (dataObj != null) {
			snapshotId = (String) dataObj.get("id");
        }
		if(snapshotId == null) {
			throw new Exception("Snapshot Id can not be null");
		}
		snapshotService.deleteSnapshotFromQueue(snapshotRequest);
	}

	public JSONObject getSnapshots(JSONObject snapshotRequest) throws ParseException {
		JSONObject data = (JSONObject)snapshotRequest.get("data");
		String tenantId = (String) data.get("tenantId");
		JSONObject page = (JSONObject)data.get("page");
		PageRequest pageReq = getPagable(page);
		JSONObject ret = new JSONObject();
		List<JSONObject> snapshots = new ArrayList<JSONObject>();
		Page<JSONObject> responsePage = snapshotService.getAccessableSnapshots(tenantId, pageReq);
		Iterator<JSONObject> retIter = responsePage.get().iterator();
		while(retIter.hasNext()) {
			snapshots.add(retIter.next());
		}
		ret.put("snapshots", snapshots);
		JSONObject pageInfo = new JSONObject();
		ret.put("page", pageInfo);
		pageInfo.put("pageSize", responsePage.getSize());
		pageInfo.put("page", responsePage.getNumber());
		pageInfo.put("size", responsePage.getTotalElements());
		Sort sort = responsePage.getSort();
		Iterator<Sort.Order> sortOrders = sort.iterator();
		List<String> properties = new ArrayList<>();
		while(sortOrders.hasNext()) {
			Sort.Order order = sortOrders.next();
			properties.add(order.getProperty());
		}
		pageInfo.put("properties", properties);
		pageInfo.put("direction",  sort.getOrderFor(properties.get(0)));
	
		return ret;
	}
	
	private PageRequest getPagable(JSONObject page) {
		int pageSize, pageNumber;
		List<String> sortBy;
		
		Sort.Direction direction;
		String directionStr;
		Sort sort = null;
		PageRequest pageReq = null;
		if(page != null) {
			directionStr = (String)page.get("direction");
			if(directionStr != null) {
				if(directionStr.equals("ASC")){
					direction = Sort.Direction.ASC;
				}else {
					direction = Sort.Direction.DESC;
				}
			}else {
				direction =Sort.Direction.DESC;
			}
			sortBy = (List<String>)page.get("properties");
			if(sortBy == null ) {
				sortBy= new ArrayList<String>();
				sortBy.add("_id");
			}
			
			sort = Sort.by(direction,sortBy.toArray(new String[sortBy.size()])); 
			//offset = page.get("offset") != null ? (int)page.get("offset") : 0;
			pageNumber = page.get("pageNumber") != null ? (int)page.get("pageNumber") : 0;
			pageSize = page.get("pageSize") != null ? (int)page.get("pageSize") : 10;
			pageReq =  PageRequest.of(pageNumber, pageSize, sort);
		}else {
			sortBy= new ArrayList<String>();
			sortBy.add("_id");
			sort = Sort.by(Sort.Direction.ASC,sortBy.toArray(new String[sortBy.size()])); 
			//offset = 0;
			pageNumber = 0;
			pageSize = 10;
			pageReq =  PageRequest.of(pageNumber, pageSize, sort);
		}
		return pageReq;
	}

	public Page<JSONObject> getSnapshotsByTemplates(JSONObject snapshotRequest) throws  ParseException, AccessDeniedException {
		JSONObject data = (JSONObject)snapshotRequest.get("data");
		String tenantId = (String) data.get("tenantId");
		JSONObject page = (JSONObject)data.get("page");
		PageRequest pageReq = getPagable(page);
		String id = (String) data.get("id");
		return permissionService.getSnapshotsByTemplateId(id,tenantId, pageReq,(String)snapshotRequest.get("email"));
	} 
	
	public JSONObject getRubricValueInSnapshots(JSONObject snapshotRequest) throws  ParseException, AccessDeniedException {
		JSONObject data = (JSONObject)snapshotRequest.get("data");
		String tenantId = (String) data.get("tenantId");
		List<JSONObject> snapshots =  permissionService.getRubricValueInSnapshots2((String)data.get("rubricId"),(List<String>)data.get("snapshots"),tenantId,(String)snapshotRequest.get("email"));
		JSONObject ret = new JSONObject();
		ret.put("snapshots", snapshots);
		return ret;
	}
}
