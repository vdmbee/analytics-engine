package com.vdmbee.calculationengine.analyser.snapshot.controller;


import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.hateoas.RepresentationModel;

//@Relation(collectionRelation = "address")
public class SnapshotResource extends RepresentationModel<SnapshotResource>{
	
	private String context;
	private String tenantId;
    public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getContext() {
		return context;
	}
	public void setContext(String context) {
		this.context = context;
	}
	private String templateId;
	public String getTemplateId() {
		return templateId;
	}
	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String period;
    private String unit;
	private List<JSONObject> fileContent;
	private JSONObject dataParameters;
	
	private List<JSONObject> tags;
	
	public List<JSONObject> getTags() {
		return tags;
	}
	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}
	public JSONObject getDataParameters() {
		return dataParameters;
	}
	public void setDataParameters(JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public List<JSONObject> getFileContent() {
		return fileContent;
	}
	public void setFileContent(List<JSONObject> fileContent) {
		this.fileContent = fileContent;
	}
	

}
