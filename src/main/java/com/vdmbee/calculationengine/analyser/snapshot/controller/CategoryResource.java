package com.vdmbee.calculationengine.analyser.snapshot.controller;

import org.springframework.hateoas.RepresentationModel;

import com.vdmbee.calculationengine.analyser.snapshot.entity.CategoryType;

public class CategoryResource extends RepresentationModel<CategoryResource> {
	private String tenantId;
	private String name;
	private CategoryType categoryType;
	private String parent;
	private double upperBound;
	private double lowerBound;
	
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public CategoryType getCategoryType() {
		return categoryType;
	}
	public void setCategoryType(CategoryType categoryType) {
		this.categoryType = categoryType;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public double getUpperBound() {
		return upperBound;
	}
	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}
	public double getLowerBound() {
		return lowerBound;
	}
	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

}

