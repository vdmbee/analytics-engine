package com.vdmbee.calculationengine.analyser.snapshot.service;

import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.vdmbee.calculationengine.analyser.snapshot.entity.MeasureLibrary;
import com.vdmbee.calculationengine.analyser.snapshot.model.MeasureLibraryModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.MeasureLibraryRepository;
import com.vdmbee.calculationengine.document.service.PermissionService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;


@Service
public class MeasureLibraryService {
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired 
	private MeasureLibraryRepository libraryRepository;

	@Autowired
	private Environment env;
	
	public boolean checkAccessToTenant(String tenantId,String email)throws AccessDeniedException {
		if(tenantId != null) {
			boolean canAccess = permissionService.isMemberOfTenant(tenantId,email);
			if(!canAccess) {
				throw new AccessDeniedException(MeasureLibrary.class,"id","Permission Denied");
			}
		}else {
			String useMultiDatabase = env.getProperty("tenant.useMultiDatabase");
			if(useMultiDatabase != null && useMultiDatabase.equalsIgnoreCase("true")) {
				throw new AccessDeniedException(MeasureLibrary.class,"id","Not Permitted To Use MultiDatabase");
			}
		}
		return true;
	}

	public MeasureLibrary getMeasureLibraryObject(String id, String tenantId,String email) throws  AccessDeniedException {
		MeasureLibrary library = null;
		if(this.checkAccessToTenant(tenantId,email)) {
			Optional<MeasureLibrary> libObj = libraryRepository.findById(id);
			if(libObj.isPresent()) {
				library = libObj.get();
			}
		}
		return library;
	}

	public MeasureLibrary saveLibraryObject(MeasureLibraryModel libraryModel, String tenantId,String email) throws  AccessDeniedException {
		if(this.checkAccessToTenant(tenantId,email)) {
			MeasureLibrary existingMeasureLibrary = libraryRepository.findOneByTenantIdAndLibraryId(libraryModel.getTenantId(),libraryModel.getLibraryId());
			if(existingMeasureLibrary != null) {
				existingMeasureLibrary.setFileContent(libraryModel.getFileContent());
				libraryRepository.save(existingMeasureLibrary);
				return existingMeasureLibrary;
	
			}else {
				ModelMapper mapper = new ModelMapper();
				mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
				MeasureLibrary library = mapper.map(libraryModel, MeasureLibrary.class);
				if(library.getId() == null) { 
					library.setId(UUID.randomUUID().toString());
				}else {
					library.setId(library.getId());
				}
				libraryRepository.save(library);
				return library;
			}
		}else {
			return null;
		}
	}
	
}
