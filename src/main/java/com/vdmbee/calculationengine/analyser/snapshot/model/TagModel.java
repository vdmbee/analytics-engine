package com.vdmbee.calculationengine.analyser.snapshot.model;

import java.util.List;

import lombok.Data;

@Data
public class TagModel {
	    private String id;
		private String tenantId;
		private String name;
		private String type;
		private List<String> values;
}
