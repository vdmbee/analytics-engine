package com.vdmbee.calculationengine.analyser.snapshot.controller;


import java.util.List;

import org.springframework.hateoas.RepresentationModel;

public class TagResource extends RepresentationModel<TagResource> {
	private String tenantId;
	private String name;
	private String type;
	private List<String> values;
	public String getName() {
		return name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<String> getValues() {
		return values;
	}
	public void setValues(List<String> values) {
		this.values = values;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
