package com.vdmbee.calculationengine.analyser.snapshot.model;

import java.util.List;

import org.json.simple.JSONObject;

import lombok.Data;

@Data
public class TemplateModel {
	private String id;
	private String tenantId;
	private String name;
	private String lastModified;
	private Boolean validator = Boolean.FALSE;
	private List<JSONObject> fileContent;
	private JSONObject dataParameters;
	private List<JSONObject> tags;
	private String context;
	private List<String> measures;
	public TemplateModel() {
		
	}
	public TemplateModel(JSONObject tempJson) {
		this.setId((String)tempJson.get("id"));
		this.setTenantId((String)tempJson.get("tenantId"));
		this.setName((String)tempJson.get("name"));
		this.setLastModified((String)tempJson.get("lastModified"));
		this.setValidator((Boolean)tempJson.get("validator") != null ? (Boolean)tempJson.get("validator") : Boolean.FALSE);
		this.setFileContent((List<JSONObject>)tempJson.get("fileContent"));
		this.setDataParameters((JSONObject)tempJson.get("dataParameters"));
		this.setTags((List<JSONObject>)tempJson.get("tags"));
		this.setContext((String)tempJson.get("context"));
	}
}
