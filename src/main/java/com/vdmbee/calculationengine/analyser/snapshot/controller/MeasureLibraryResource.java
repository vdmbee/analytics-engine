package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.hateoas.RepresentationModel;

public class MeasureLibraryResource extends RepresentationModel<MeasureLibraryResource>  {
	private String tenantId;
	private String libraryId;
	
	public String getTenantId() {
		return tenantId;
	}
	public String getLibraryId() {
		return libraryId;
	}
	public void setLibraryId(String libraryId) {
		this.libraryId = libraryId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	private List<JSONObject> fileContent;
	public List<JSONObject> getFileContent() {
		return fileContent;
	}
	public void setFileContent(List<JSONObject> fileContent) {
		this.fileContent = fileContent;
	}
}
