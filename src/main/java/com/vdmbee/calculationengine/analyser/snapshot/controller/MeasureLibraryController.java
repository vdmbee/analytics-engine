package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vdmbee.calculationengine.analyser.snapshot.entity.MeasureLibrary;
import com.vdmbee.calculationengine.analyser.snapshot.model.MeasureLibraryModel;
import com.vdmbee.calculationengine.analyser.snapshot.repository.MeasureLibraryRepository;
import com.vdmbee.calculationengine.analyser.snapshot.service.MeasureLibraryService;
import com.vdmbee.calculationengine.errorhandler.AccessDeniedException;

@RepositoryRestController
//@RequestMapping("/vdmbee/")
public class MeasureLibraryController {
	@Autowired
	private PagedResourcesAssembler<MeasureLibrary> pagedAssembler;
	
	@Autowired
	MeasureLibraryService measureLibraryService;
	
	@Autowired 
	private MeasureLibraryRepository libraryRepository;
	
	@GetMapping(path = "/library/{id}", consumes = "application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
	public @ResponseBody MeasureLibraryResource getMeasureLibrary(MeasureLibraryResourceAssembler libraryResourceAssembler, @PathVariable("id") String id,@RequestParam(value = "tenantId") String tenantId) throws AccessDeniedException {
		if(measureLibraryService.checkAccessToTenant(tenantId, null)) {
		MeasureLibrary library = measureLibraryService.getMeasureLibraryObject(id, tenantId, null);
		return library != null ? libraryResourceAssembler.toModel(library): null;
        } else {
        	return null;
        }
	}

	@GetMapping(path = "/library",consumes = "application/json",produces = MediaType.APPLICATION_JSON_VALUE)
    //@PreAuthorize("hasPermission('Address', 'create')")
	public PagedModel<MeasureLibraryResource> getAllMeasureLibrary(MeasureLibraryResourceAssembler libraryResourceAssembler,Pageable pageable,@RequestParam(value = "tenantId") String tenantId) throws  AccessDeniedException {
		if(measureLibraryService.checkAccessToTenant(tenantId, null)) {
	    	List<MeasureLibrary> librarys = libraryRepository.findAll();
	    	return pagedAssembler.toModel(new PageImpl(librarys, pageable, librarys.size()), (RepresentationModelAssembler)this.pagedAssembler);
		}else {
        	return null;
        }
	}
	
	@PostMapping(path = "/library", consumes = "application/json")
    //@PreAuthorize("hasPermission('Address', 'create')")
    public @ResponseBody PersistentEntityResource addNewMeasureLibrary( PersistentEntityResourceAssembler resourceAssembler,@RequestBody MeasureLibraryModel libraryModel,@RequestParam(value = "tenantId") String tenantId) throws  AccessDeniedException{
		if(measureLibraryService.checkAccessToTenant(tenantId, null)) {
			MeasureLibrary newLibrary = measureLibraryService.saveLibraryObject(libraryModel,libraryModel.getTenantId(), null);
			return newLibrary != null ? resourceAssembler.toModel(newLibrary): null;
		}else {
			return null;
		}
    }
}
