package com.vdmbee.calculationengine.analyser.snapshot.controller;

import java.util.List;

public class RubricSnapshotsModel {
	private String rubricId;
	private List<String> snapshots;
	public String getRubricId() {
		return rubricId;
	}
	public void setRubricId(String rubricId) {
		this.rubricId = rubricId;
	}
	public List<String> getSnapshots() {
		return snapshots;
	}
	public void setSnapshots(List<String> snapshots) {
		this.snapshots = snapshots;
	}
	

}
