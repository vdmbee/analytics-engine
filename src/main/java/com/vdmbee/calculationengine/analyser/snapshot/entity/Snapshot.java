package com.vdmbee.calculationengine.analyser.snapshot.entity;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.persistence.EntityNotFoundException;
import javax.persistence.Transient;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import com.vdmbee.calculationengine.analyser.formula.ValueFormula;
import com.vdmbee.calculationengine.analyser.snapshot.repository.MeasureLibraryRepository;
import com.vdmbee.calculationengine.analyser.snapshot.repository.TemplateRepository;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.multitenant.TenantContext;
import com.vdmbee.calculationengine.util.Timming;

@Document(collection="snapshot")
public class Snapshot {
	private static final Logger logger = LogManager.getLogger(Snapshot.class);
	@Id
    private String id;

	private String tenantId;

	public String context;
	private String period;
    private String unit;
    
    private String errorCode;
    
    private String errorMessage;

	private String lastModified;
    
    private List<JSONObject> tags;
    
    private String rescaleRubric;
    private Double rescaleFactor;
    
    public String getRescaleRubric() {
		return rescaleRubric;
	}

	public void setRescaleRubric(String rescaleRubric) {
		this.rescaleRubric = rescaleRubric;
	}

	public Double getRescaleFactor() {
		return rescaleFactor;
	}

	public void setRescaleFactor(Double rescaleFactor) {
		this.rescaleFactor = rescaleFactor;
	}


	@Transient
    private HashMap<String, List<JSONObject>> aggregationsByAdmin;
    
    public HashMap<String, List<JSONObject>> getAggregationsByAdmin() {
		return aggregationsByAdmin;
	}

	public void setAggregationsByAdmin(HashMap<String, List<JSONObject>> aggregationsByAdmin) {
		this.aggregationsByAdmin = aggregationsByAdmin;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getLastModified() {
		return lastModified;
	}

	public void setLastModified(String lastModified) {
		this.lastModified = lastModified;
	}

	public List<JSONObject> getTags() {
		return tags;
	}

	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}


	@Transient
	private Template template;
	
	@Transient
	private Boolean contentChange;
	
	public Boolean getContentChange() {
		return contentChange;
	}

	public void setContentChange(Boolean contentChange) {
		this.contentChange = contentChange;
	}


	private String name;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	
	private String templateId;
	
    public Template getTemplate() {
		return template;
	}

	public void setTemplate(Template template) {
		this.template = template;
	}

	private String validationTemplateId;
	
	public String getValidationTemplateId() {
		return validationTemplateId;
	}

	public void setValidationTemplateId(String validationTemplateId) {
		this.validationTemplateId = validationTemplateId;
	}

	public String getContext() {
		return context;
	}

	public void setContext(String context) {
		this.context = context;
	}

    
    public String getPeriod() {
		return period;
	}

	public void setPeriod(String period) {
		this.period = period;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	private List<JSONObject> fileContent;
	private org.json.simple.JSONObject dataParameters;

	public org.json.simple.JSONObject getDataParameters() {
		return dataParameters;
	}

	public void setDataParameters(org.json.simple.JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}

	@CreatedBy
    private String modifiedBy;

    /*@CreatedDate
    private Date modifiedDate;*/


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

	public List<JSONObject> getFileContent() {
        return fileContent;
    }

    public void setFileContent(List<JSONObject> fileContent) {
        this.fileContent = fileContent;
    }

	public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /*public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }*/
    
    private static HashMap<String,HashMap<String,Snapshot>> snapshotsCache = new HashMap<String,HashMap<String,Snapshot>> ();

	public static Snapshot getSnapshotsCache(String currentSnapshotId,String referenceSnapshot) {
		HashMap<String,Snapshot> currentSnapshot = snapshotsCache.get(currentSnapshotId);
		if(currentSnapshot == null) {
			return null;
		}else {
			return currentSnapshot.get(referenceSnapshot);
		}
	}

	public static void putSnapshotsCache(String currentSnapshotId,String referenceSnapshot, Snapshot snapshot) {
		HashMap<String,Snapshot> currentSnapshotCache = snapshotsCache.get(currentSnapshotId);
		if(currentSnapshotCache == null) {
			currentSnapshotCache=new HashMap<String,Snapshot>();
			snapshotsCache.put(currentSnapshotId, currentSnapshotCache);
		}
		currentSnapshotCache.put(referenceSnapshot, snapshot);
	}
	public static void removeSnapshotsCache(String currentSnapshotId) {
		HashMap<String,Snapshot> cache = snapshotsCache.remove(currentSnapshotId);
		if(cache != null) {
			cache.clear();
		}
		cache = null;
	}
    
    /*public boolean validate(TemplateRepository repository, String benchmarkId, boolean updateDataset) throws ParseException, InterruptedException {
    	if(benchmarkId == null) {
    		throw new RuntimeException("BenchmarkId can not be empty");
    	}
    	SnapshotRepository snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
    	Snapshot benchmark = null;
    	Optional<Snapshot> benchmarkObj = snapshotRepository.findById(benchmarkId);
    	if(benchmarkObj.isPresent()) {
    		benchmark = benchmarkObj.get();
    	}
    	if(benchmark == null) {
    		throw new RuntimeException("Snapshot/Benchmark not found");
    	}
    	String validationTemplateId =benchmark.getValidationTemplateId(); 
    	if( validationTemplateId == null) {
    		return true;
    	}
    	Template validationTemplate = null;
    	Optional<Template> validationTemplateObj = repository.findById(validationTemplateId); 
    	if(validationTemplateObj.isPresent()) {
    		validationTemplate = validationTemplateObj.get();
    	}
    	if(validationTemplate == null) {
    		throw new RuntimeException("Validation Template not found");
    	}
    	if(updateDataset) {
    		this.update(repository,null,null,true);
    	}
    	HashMap<String, Double> calculatedValues = this.update(repository, validationTemplate,null,true);
    	Iterator keys = calculatedValues.keySet().iterator();
    	//boolean ret = false;
    	while(keys.hasNext()) {
    		String key = (String)keys.next();
    		Double value = calculatedValues.get(key);
    		if(value <= 0) {
    			return false;
    		}
    	}
    	return true;
    }*/
    
    public void update(TemplateRepository repository,Template validationTemplate, Snapshot existingSnapshot,boolean useExistingData,List<String> measures,boolean removeCache) throws ParseException, InterruptedException {
    	HashMap<String, Double> calculatedValues = new HashMap<>();
    	HashMap<String, List<org.bson.Document>> aggregatedValues = new HashMap<>();
    	List<JSONObject> templateData = null;
    	HashMap<String, ValueFormula> values = null;
    	Template template = validationTemplate != null ? validationTemplate : this.getTemplate(); 
    	if(template == null && this.getTemplateId() != null && StringUtils.hasText(this.getTemplateId())) {
    		//Date before = new Date();
        	Optional<Template> templateObj = repository.findById(this.getTemplateId());
        	//Date after = new Date();
			//Timming.addReadTime(after.getTime() - before.getTime());
        	if(!templateObj.isPresent()) {
        		Thread.sleep(5000);//to handle parallel queue
        		templateObj = repository.findById(this.getTemplateId());
        	}
        	if(templateObj.isPresent()) {
        		template = templateObj.get();
        	}
    	}
    	if(template == null) {
    		logger.error("There is no template with id:"+this.getTemplateId()+" for Snapshot:"+this.getId());
    		List<Template> templateList = repository.findFirstByTenantIdAndContext(this.getTenantId(), this.getContext());
    		if(!templateList.isEmpty()) {
        		template = templateList.get(0);
        	}
    	}	
    	try {
	    	if(template == null) {
	    		this.errorCode = "Error_No_Template";
	    		this.errorMessage = this.getName()+" cannot be calculated as there is no Template Id:"+this.getTemplateId()+ " found for tenant:"+this.getTenantId();
	    		return;
	    	}
	    	if(measures == null) {
	    		measures = template.getMeasures();
	    	}
	    	templateData = template.getFileContent();
	    	values = new HashMap<>();
	    	List<JSONObject> snapshotData = this.getFileContent();
	    	ArrayList<String> inputNaNValue = new ArrayList<String>();
	    	if(snapshotData!=null) {
	    		if(validationTemplate == null) {
	    			inputNaNValue = this.updateCalculatedValuesWithInputs(snapshotData,templateData, calculatedValues,existingSnapshot,useExistingData);	
	    		}
		    	this.initialiseFormulas(templateData, values,calculatedValues,validationTemplate == null ? false : true,measures);
		    	this.updateSnapshotDataBasedOnTemplate(snapshotData, templateData,values,calculatedValues,aggregatedValues,validationTemplate != null ? true:false);
		    	if(validationTemplate == null) {
		    		this.updateSnapshotWithCalculatedvalues(calculatedValues,aggregatedValues, templateData, snapshotData, inputNaNValue,values);	
		    	}
		    	snapshotData = null;
		    	inputNaNValue.clear();
		    	inputNaNValue = null;
		    	values.clear();
	    	}
    	}catch(InterruptedException e){
    		this.errorCode = "Error_interrupted_exception";
    		StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
    		this.errorMessage = sw.toString();
    		logger.error("ErrorCode:" + this.errorCode + " ErrorMessage:" + this.errorMessage, e);
    		//Thread.currentThread().interrupt();
    	}catch(ParseException e){
    		this.errorCode = "Error_parse_exception";
    		StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            this.errorMessage = sw.toString();
            logger.error("ErrorCode:" + this.errorCode + " ErrorMessage:" + this.errorMessage, e);
    	}catch(EntityNotFoundException e){
    		this.errorCode = "Error_no_record_found";
    		StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            this.errorMessage = sw.toString();
            logger.error("ErrorCode:" + this.errorCode + " ErrorMessage:" + this.errorMessage, e);
    	}catch(NullPointerException e){
    		this.errorCode = "Error_null_pointer_exception";
    		StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            this.errorMessage = sw.toString();
            logger.error("ErrorCode:" + this.errorCode + " ErrorMessage:" + this.errorMessage, e);
    	}catch(Exception e){
    		this.errorCode = "Error_exception";
    		StringWriter sw = new StringWriter();
            e.printStackTrace(new PrintWriter(sw));
            this.errorMessage = sw.toString();
            logger.error("ErrorCode:" + this.errorCode + " ErrorMessage:" + this.errorMessage, e);
    	}finally{
    		if(this.id != null && removeCache) {
	    		this.removeSnapshotsCache(this.id);
	    	}
    		aggregatedValues.clear();
    		calculatedValues.clear();
    		if(templateData != null) {
    			templateData.clear();
        		templateData = null;
    		} 
    		repository = null;
    		if(values != null) {
    			values.clear();
    			values = null;
    		}
    		
    	}
    	
    	//return calculatedValues;
    }

    private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
    	if(nodes == null) {
    		return null;
    	}
    	for(int i=0;i<nodes.size();i++) {
    		JSONObject node = nodes.get(i);
    		if(node.get("id").equals(id)) {
    			return node;
    		}
    	}
    	return null;
    }

    private List<JSONObject> removeNodeWithId(List<JSONObject> nodes,String id) {
    	int nodeToRemove = -1;
    	for(int i=0;i<nodes.size();i++) {
    		JSONObject node = nodes.get(i);
    		if(node.get("id").equals(id)) {
    			nodeToRemove = i;
    			break;
    		}
    	}
    	if(nodeToRemove >=0) {
    		nodes.remove(nodeToRemove);
    	}
    	return nodes;
    }

	private void updateSnapshotWithCalculatedvalues(HashMap<String, Double> calculatedValues,
			HashMap<String, List<org.bson.Document>> aggregatedValues, List<JSONObject> templateData,
			List<JSONObject> snapshotData, ArrayList<String> inputNaNValue,
			HashMap<String, ValueFormula> valueFormulas) throws ParseException {
		// Iterator<String> templateValues = templateData.keySet().iterator();
		// while(templateValues.hasNext()) {
 		for (int i = 0; i < templateData.size(); i++) {

			String valueStr = (String) templateData.get(i).get("id");
			Boolean isSingleScript = (Boolean)templateData.get(i).get("isSingleScript");
			
			String escValueStr = valueStr.replaceAll("[@-]", "_");
			escValueStr = escValueStr.replaceAll("[#]", "_");
			Double val = calculatedValues.get(escValueStr);
			ValueFormula formula = valueFormulas.get(escValueStr);
			HashMap<String, Double> returnValues = formula != null ? formula.getReturnValues() : null;

			Object snpObj = getNodeWithId(snapshotData, valueStr);

			JSONObject snapshotObj;
			if (snpObj != null) {
				snapshotObj = new JSONObject((Map) snpObj);
				//snapshotData.remove(valueStr);
				snapshotData = removeNodeWithId(snapshotData, valueStr);
			} else {
				snapshotObj = new JSONObject((Map) getNodeWithId(templateData, valueStr));
				snapshotObj.remove("value");
			}
			if ((isSingleScript  != null && isSingleScript ) || valueStr.startsWith("Template")) {//single script
				snapshotData.clear();// to remove existing json array(for multiple template it can cause issue)
				List<JSONObject> jsonReturnValues = new ArrayList();
				// snapshotData.put(valueStr, jsonReturnValues);
				if (returnValues != null && returnValues.size() > 0) {
					Iterator<String> keys = returnValues.keySet().iterator();
					while (keys.hasNext()) {
						String key = keys.next();
						if (!inputNaNValue.contains(key)) {
							Object objVal = returnValues.get(key);
							//if(objVal != null && (objVal instanceof Double && !Double.isNaN((Double) objVal)) || !(objVal instanceof Double)) {
								JSONObject valueNode = new JSONObject();
								valueNode.put("id", key);
								valueNode.put("Value", objVal);
								/*
								 * if(formula.getArguments().containsKey(key)) { valueNode.put("dep",1); }
								 */
								jsonReturnValues.add(valueNode);
							//}
						}
					}
				}
				// snapshotObj.put("Value", jsonReturnValues);
				snapshotData.addAll(jsonReturnValues);
				if(formula != null && formula.getExpressionStr().startsWith("mongodbAggregation") && formula.getResults().size() > 0) {
					JSONParser parser = new JSONParser();
					for(int j =0;j<formula.getResults().size();j++) {
						snapshotData.add((JSONObject)parser.parse(formula.getResults().get(j).toJson()));
					}
				}
				jsonReturnValues.clear();
			} else if (val != null) {
				//snapshotData.clear();// to remove existing json array(for multiple template it can cause issue)
				/*
				 * if(val == 0.0 && inputNaNValue.contains(valueStr)) {
				 * snapshotObj.put("Value", null); }else { snapshotObj.put("Value", val); }
				 */
				if (val.equals(Double.NaN)) {
					snapshotObj.put("Value", String.valueOf(val));
				} else {
					snapshotObj.put("Value", val);
				}
				snapshotObj.remove("Formula");
				if (!inputNaNValue.contains(valueStr)) {
					snapshotData.add(snapshotObj);
				}
			} else {
				List<org.bson.Document> values = aggregatedValues.get(escValueStr);
				if (values != null) {
					snapshotObj.put("Value", values);
				}
				snapshotObj.remove("Formula");
				if (!inputNaNValue.contains(valueStr)) {
					snapshotData.add(snapshotObj);
				}
			}
			if(returnValues != null) {
				returnValues.clear();
			}
		}
		this.setFileContent(snapshotData);
	}
    private JSONObject getJSONObjectFromArrayStr(Object obj) throws ParseException {
    	if(obj instanceof String) {
    		String str = (String)obj;
        	JSONParser parser = new JSONParser();
        	return (JSONObject)parser.parse(str);
    	}else if(obj instanceof LinkedHashMap){
    		return new JSONObject((Map)obj);
    	}else
    	{
    		return (JSONObject)obj;
    	}
    }
    private void initialiseFormulas(List<JSONObject> templateData,HashMap<String, ValueFormula> values,HashMap<String, Double> calculatedValues,boolean validating,List<String> calcMeasures) throws ParseException {
    	//Iterator<String> keys = templateData.keySet().iterator();
    	//while(keys.hasNext()) {
    	for(int i=0;i<templateData.size();i++) {
    		JSONObject templateNode = templateData.get(i);
    		String key = (String)templateNode.get("id");
    		String escKey = key.replaceAll("[#@-]","_");
    		//escKey = escKey.replaceAll("[#]", "_");
    		JSONObject obj = getJSONObjectFromArrayStr(templateNode);
    		String expression = null;
    		Object formulaVal = obj.get("Formula");
    		if(formulaVal == null || formulaVal instanceof String) {
    			continue;
    		}
    		JSONObject formulaObj = getJSONObjectFromArrayStr(formulaVal);
    		expression = (String)formulaObj.get("expression");
    		ValueFormula valueFormula = null;
    		if(expression == null || expression.equals("")) {
        		String libraryId = (String)formulaObj.get("library");
        		if(libraryId == null) {
        			continue;
        		}
        		String measureId = (String)formulaObj.get("measure");
        		String measurementId = (String)formulaObj.get("id");
        		JSONObject mestRelations = null;
        		if(formulaObj.get("relations") != null) {
        			mestRelations = new JSONObject((Map)formulaObj.get("relations"));	
        		}
        		
        		
        		MeasureLibraryRepository mesRep = BeanLoaderUtil.getBean(MeasureLibraryRepository.class);
        		MeasureLibrary library = mesRep.findOneByTenantIdAndLibraryId(tenantId, libraryId);
        		if(library != null) {
        			List<JSONObject> measures = library.getFileContent();
        			//JSONObject measure = null;
        			Object measureObj = getNodeWithId(measures,measureId);
        			if(measureObj != null) {
            			formulaObj = new JSONObject((Map)measureObj);
            			expression = (String)formulaObj.get("expression");
            			if(expression == null || expression.equals("") ) {
            				valueFormula = new ValueFormula(key,this.id, measurementId,mestRelations,formulaObj,calculatedValues,this.tenantId);	
            				valueFormula.setDbTenant(MongoTenantContext.getTenant());
            			}else {
            				valueFormula = this.getExpressionBasedValueFormula(key, expression, formulaObj, calculatedValues,validating);
            				valueFormula.setMeasures(calcMeasures);
            				valueFormula.setDbTenant(MongoTenantContext.getTenant());
            			}
        			}
        		}
    		}else {
    			valueFormula = this.getExpressionBasedValueFormula(key, expression, formulaObj, calculatedValues,validating);
    			valueFormula.setMeasures(calcMeasures);
    			valueFormula.setDbTenant(MongoTenantContext.getTenant());
    		}
    		
    		if(valueFormula != null) {
    			values.put(escKey, valueFormula);	
    		}
    	}
    }
    private ValueFormula getExpressionBasedValueFormula(String key,String expression,JSONObject formulaObj,HashMap<String, Double> calculatedValues,boolean validating) {

		String snapshotId = (String)formulaObj.get("snapshotId");
		String snapshotValue = (String)formulaObj.get("snapshotValue");
		String snapshotTenantId = (String)formulaObj.get("snapshotTenantId");
		String snapshotContext = (String)formulaObj.get("snapshotContext");
		String snapshotPeriod = (String)formulaObj.get("snapshotPeriod");
		String snapshotUnit = (String)formulaObj.get("snapshotUnit");
		ArrayList<String> snapshotIds = null;
		if(validating) {
			snapshotIds  =new ArrayList<String>();
			snapshotIds.add(this.getId());
		}else {
			snapshotIds = (ArrayList<String>)formulaObj.get("snapshotIds");	
		}
		
		String snapshotExpression = (String)formulaObj.get("snapshotExpression");
		
    	List<JSONObject> stages = (List<JSONObject>)formulaObj.get("Stages");
		List<String> sortFields = (List<String>)formulaObj.get("SortFields");
		String direction = (String)formulaObj.get("SortDirection");
		String outputField = (String)formulaObj.get("OutputField");
		String queryOrScript = (String)formulaObj.get("MongoJsonQuery");
		List<JSONObject> rubricsToCalculate = (List<JSONObject>)formulaObj.get("rubricsToCalculate");
		if(expression.indexOf("pythonExecutor") >=0) {
			queryOrScript = (String)formulaObj.get("pythonScript");
		}
		if(expression.indexOf("spelExecutor") >=0) {
			queryOrScript = (String)formulaObj.get("spelScript");
		}
		String mongoValueField = (String)formulaObj.get("MongoValueField");
		String mongoFormula = (String)formulaObj.get("MongoFormula");
		ArrayList<String> dependentValues = (ArrayList<String>)formulaObj.get("dependentValues");
		
		Sort.Direction directionVal = Sort.Direction.ASC;
		if(direction != null) {
			if(direction.equals("1")) {
				directionVal = Sort.Direction.ASC;
			}else {
				directionVal = Sort.Direction.DESC;
			}
		}	
		return new ValueFormula(key,this.id,expression,stages,sortFields,directionVal,outputField,queryOrScript,mongoValueField,mongoFormula,snapshotId,snapshotTenantId,snapshotValue,snapshotContext,snapshotPeriod,snapshotUnit,snapshotIds,snapshotExpression,calculatedValues,this.getDataParameters(),this.tags,dependentValues,rubricsToCalculate);
    }
    private ArrayList<String> updateCalculatedValuesWithInputs(List<JSONObject> snapshotData,List<JSONObject> templateData,HashMap<String, Double> calculatedValues,Snapshot existingSnapshot,boolean useExistingData) throws ParseException {
    	//Iterator<String> keys = templateData.keySet().iterator();
    	ArrayList<String> inputNaNValue = new ArrayList<String>();
    	//while(keys.hasNext()) {
    	List<JSONObject> existingContent = null;
    	if(existingSnapshot != null) {
    		existingContent = existingSnapshot.getFileContent();	
    	}
    	
    	for(int i=0;i<templateData.size();i++) {
    		JSONObject templateNode = templateData.get(i);
    		if(templateNode == null || templateNode.get("id") == null) {
    			continue;
    		}
    		String key = (String)templateNode.get("id");
    		String escKey = key.replaceAll("[@-]", "_");
    		escKey = escKey.replaceAll("[#]", "_");
    		Map inputValueJsonObj = (Map) getNodeWithId(snapshotData,key);
    		if(inputValueJsonObj != null) {
    			JSONObject valueData = new JSONObject(inputValueJsonObj);
        		String type = (String) valueData.get("ValueType");
        		if(type == null) {
        			JSONObject tempValueData = getJSONObjectFromArrayStr(templateNode);
        			type = (String) tempValueData.get("ValueType");
        		}
        		if(type != null && type.equalsIgnoreCase("Input") ) {
        			Object valObj = valueData.get("Value");
        			Double value;
        			if(valObj instanceof Long) {
        				value = ((Long) valObj).doubleValue();
        			}else {
        				if(valObj instanceof Integer){
        					value = ((Integer) valObj).doubleValue();
        				}else {
        					value = (Double) valObj;	
        				}
        				
        			}
        			if(value == null) {
        				value = Double.NaN;
					}
        			if(value != null) {
        				Boolean invertSign = (Boolean)valueData.get("invertSign");
        				Double val = Double.parseDouble(value.toString());
        				if(val != null && val != Double.NaN  && invertSign != null && invertSign) {
        					val = val*(-1);
        				}
        				calculatedValues.put(escKey, val);
        				
        			}else {
        				Boolean updateDataset = (Boolean)valueData.get("updateDataset");
        				Map existingDataNode = null;
        				if(updateDataset == null ||  updateDataset) {
        					existingDataNode = (Map) getNodeWithId(existingContent,key);	
        				}
        		    	if(existingDataNode != null && useExistingData) {
        		    		Double val = Double.parseDouble(existingDataNode.get("Value").toString());
        		    		Boolean invertSign = (Boolean)valueData.get("invertSign");
            				if(val != null && val != Double.NaN && invertSign != null && invertSign) {
            					val = val*(-1);
            				}
        		    		calculatedValues.put(escKey, val);
        		    	}else {
	        				calculatedValues.put(escKey, Double.parseDouble("NaN".toString()));
	        				inputNaNValue.add(key);
        		    	}
        			}
        		}
    		}else {
    			JSONObject tempValueData = getJSONObjectFromArrayStr(templateNode);
    		    String type = (String) tempValueData.get("ValueType");
    		    if(type != null && type.equalsIgnoreCase("Input") ) {
    		    	Map existingDataNode = (Map) getNodeWithId(existingContent,key);
    		    	if(existingDataNode != null && useExistingData) {
    		    		Double val = Double.parseDouble(existingDataNode.get("Value").toString());
    		    		calculatedValues.put(escKey, val);
    		    	}else {
        		    	calculatedValues.put(escKey, Double.parseDouble("NaN".toString()));
        		    	if(!inputNaNValue.contains(key)){
        		    		inputNaNValue.add(key);
        		    	}
    		    	}
    		    }else if(tempValueData.containsKey("Formula")){
    		    	JSONObject singleScriptFormula = new JSONObject((Map)tempValueData.get("Formula"));
    		    	if(singleScriptFormula != null) {
    		    		List<String> dependentValues = (List<String>) singleScriptFormula.get("dependentValues");
    		    		if(dependentValues != null) {
        		    		for(String dep:dependentValues) {
        		    			Map inputValueJsonObj1 = (Map) getNodeWithId(snapshotData,dep);
        		    			if(inputValueJsonObj1 != null) {
        		        			JSONObject valueData = new JSONObject(inputValueJsonObj1);
        		            		String type1 = (String) valueData.get("ValueType");
        		            		if(type1 != null && type1.equalsIgnoreCase("Input") ) {
        		            			String escDepKey = dep.replaceAll("[@-]", "_");
        		            			if(valueData.get("Value") != null) {
        		            				calculatedValues.put(escDepKey, Double.parseDouble(valueData.get("Value").toString()));	
        		            			}else {
        		            				calculatedValues.put(escDepKey, Double.parseDouble("NaN".toString()));
        		            				//calculatedValues.put(escKey, 0.0);
        		            				if(!inputNaNValue.contains(dep)){
        		            					inputNaNValue.add(dep);
        		            				}
        		            				//logger.debug("Assume null:" + dep);
        		            			}
        		            		}
        		        		}else {
    		        				String escDepKey = dep.replaceAll("[@-]", "_");
    		        		    	calculatedValues.put(escDepKey, Double.parseDouble("NaN".toString()));
    		        		    	/*for(int j=0;j<templateData.size();j++) {
    		        		    		JSONObject depTemplateNode = templateData.get(j);
    		        		    		if(depTemplateNode == null || depTemplateNode.get("id") == null) {
    		        		    			continue;
    		        		    		}
    		        		    		String depKey = (String)depTemplateNode.get("id");
    		        		    		if(depKey.equals(dep)) {
    		        		    			String depType = (String)depTemplateNode.get("ValueType");
    		        		    			if(depType.equals("Input")) {
    		        		    				Map existingDataNode = (Map) getNodeWithId(existingContent,depKey);
    		        		    		    	if(!(existingDataNode != null && useExistingData)) {
    		        		        		        inputNaNValue.add(key);
    		        		    		    	}
    		        		    			}else {
    		        		    				break;
    		        		    			}
    		        		    		}
    		        		    	}*/
    		        		        //inputNaNValue.add(dep);
        		        		}
            		    	}
    		    		}
    		    	}
    		    }
    		}
    		
    	}
    	return inputNaNValue;
    }
    private void updateSnapshotDataBasedOnTemplate(List<JSONObject> snapshotData,List<JSONObject> templateData,HashMap<String, ValueFormula> values,HashMap<String, Double> calculatedValues,HashMap<String, List<org.bson.Document>> aggregatedValues,boolean validating) throws ParseException, InterruptedException, TimeoutException, ExecutionException {
    	//ExecutorService executor = Executors.newFixedThreadPool(200);
    	DependencyTaskExecutor taskExecutor = new DependencyTaskExecutor();
    	//TenantContext.setTaskExecutor(new DependencyTaskExecutor());
    	//Iterator<String> keys = snapshotData.keySet().iterator();
    	ArrayList<Task> tasks = new ArrayList<>();
    	
    	if(!validating) {
        	//while(keys.hasNext()) {
    		for(int i=0;i<snapshotData.size();i++) {
    			JSONObject snapshotNode = snapshotData.get(i);
    			
        		
        		if(snapshotNode == null || snapshotNode.get("id") == null) {
        			continue;
        		}
        		
        		JSONObject valueData = new JSONObject((Map) snapshotNode);
        		String type = (String) valueData.get("ValueType");
    			if(type == null) {
    				String key = (String)snapshotNode.get("id");
            		String escKey = key.replaceAll("[@-]", "_");
            		escKey = escKey.replaceAll("[#]", "_");
        			JSONObject tempValueData = getJSONObjectFromArrayStr(getNodeWithId(templateData,key));
        			if(tempValueData == null) {
        				logger.error(key + " does not exists in template");
        				continue;
        			}
        			type = (String) tempValueData.get("ValueType");
        			if(type == null) {
        				//System.out.println("type missing for:" + valueData.toJSONString());	
        			}
    			}else if(type.equals("Aggregated")) {
    				String key = (String)snapshotNode.get("id");
            		String escKey = key.replaceAll("[@-]", "_");
            		escKey = escKey.replaceAll("[#]", "_");
    				ValueFormula valueFormula = values.get(escKey);
    				List<org.bson.Document> results = valueFormula.getAggrigations();
    				aggregatedValues.put(escKey, results);
    			}
        	}
    	}
    	Iterator<String> formulaKeys = values.keySet().iterator();
    	while(formulaKeys.hasNext()) {
    		ValueFormula formula = values.get(formulaKeys.next());
    		tasks.add(formula);
    	}
    	for(int i=0;i<tasks.size();i++) {
    		tasks.get(i).checkDependencies(tasks);
    	}
    	
    	Long threadTimelimit = (long) 30000;
    	if(tasks.size() == 1) {//benchmark creation
	    	if(this.dataParameters != null && this.dataParameters.get("snapshotIds") != null) {
	    		List<String> snapshotIdsList = (List<String>)dataParameters.get("snapshotIds");
	    		List<String> invalidSnapshotIdsList = (List<String>)dataParameters.get("invalidSnapshotIds");
	    		if(snapshotIdsList.size() > 50) {
	    			threadTimelimit = (long) snapshotIdsList.size() * 70;
	    		}
	    		Integer totalDatasets = snapshotIdsList.size()+invalidSnapshotIdsList.size();
	    		Timming.addDatasetCount(totalDatasets);
	    		logger.error(this.name+":"+totalDatasets);
			}
	    	//tasks.get(0).execute();
    	} 
    	logger.info("Please wait for "+TimeUnit.MILLISECONDS.toSeconds(threadTimelimit)+" sec for "+this.getName());
    	taskExecutor.scheduleTask(tasks);
    	try
    	{
    		/*long startTime = new Date().getTime();
        	while(!taskExecutor.isComplete() && ((new Date().getTime() - startTime) < totalWaitingTime)){
        		Thread.currentThread().sleep(500);
        	}*/
    		Boolean tasksDone = taskExecutor.awaitTermination(threadTimelimit);
        	if(!tasksDone) {
        		StringBuffer tasksNotDone = new StringBuffer();
        		Iterator taskInterator = tasks.iterator();
        		while(taskInterator.hasNext()) {
        			ValueFormula formula = (ValueFormula)taskInterator.next();
        			formula.setStopExecution(true);
        			if(!formula.isDoneCalculation()) {
        				tasksNotDone.append(formula.getName());
        				tasksNotDone.append(", ");
        			}
        		}
        		taskExecutor.cancel();
        		//Thread.currentThread().sleep(6000);	//to enable threads to free
        		String errormsg = "Following tasks were not done due to dependencies:" + tasksNotDone;
        		//snapshotData.put("error", tasksNotDone);
        		logger.error(errormsg);
        		throw new InterruptedException(errormsg);
        	}else {
        		//System.out.print("done all tasks");
        	}
    	}catch(InterruptedException e) {
    		//System.out.println(e);
    		logger.error(e.getMessage());
    		throw e;
    	}catch(TimeoutException te) {
    		logger.error(te.getMessage());
    		throw te;
    	}catch(ExecutionException te) {
    		logger.error(te.getMessage());
    		throw te;
    	}
    }
    
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		 /*return String.format(
	                "Customer[id=%s, tenantId='%s', name='%s',context='%s']",
	                id, tenantId, name,context);*/
		JSONObject obj= new JSONObject();
		obj.put("id",id);
		obj.put("tenantId",tenantId);
		obj.put("name",name);
		obj.put("context",context);
		return "[" + obj.toJSONString() + "]";
	}
	private List<JSONObject> getFileContentCopy(){
		List<JSONObject> copy = new ArrayList<>();
		Iterator<JSONObject> nodes = this.getFileContent().iterator();
		while(nodes.hasNext()) {
			copy.add((JSONObject)nodes.next().clone());
		}
		return copy;
	}
	public Snapshot copy() {
		Snapshot copySnapshot = new Snapshot();
		copySnapshot.setContentChange(this.getContentChange());
		copySnapshot.setContext(this.getContext());
		copySnapshot.setDataParameters((JSONObject)this.getDataParameters().clone());
		copySnapshot.setFileContent(this.getFileContentCopy());
		copySnapshot.setTemplate(this.getTemplate());
		copySnapshot.setTemplateId(this.getTemplateId());
		copySnapshot.setUnit(this.getUnit());
		return copySnapshot;
	}
}
