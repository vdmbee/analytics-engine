package com.vdmbee.calculationengine.analyser.snapshot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import com.vdmbee.calculationengine.analyser.snapshot.entity.SnapshotEvent;

@Component
@RepositoryRestResource(collectionResourceRel = "snapshotevent", path = "snapshotevent")
public interface SnapshotEventRepository extends MongoRepository<SnapshotEvent, String> {
	SnapshotEvent findOneByTenantIdAndEventId(String tenantId,String eventId);
	SnapshotEvent findOneByTenantIdAndId(String tenantId,String id);
}
