package com.vdmbee.calculationengine.analyser.formula;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.mariuszgromada.math.mxparser.PrimitiveElement;
import org.springframework.beans.factory.annotation.Autowired;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;


public class SnapshotExpressionValue implements FunctionExtension {
	@Autowired
	private SnapshotRepository snapshotRepository;
	
	@Autowired
	private SnapshotService snapshotService;
	
	private static Logger logger = LogManager.getLogger(SnapshotExpressionValue.class);
	
    private List<String> snapshotIds;

    private String context;
	private String period;
    private String unit;
    
    private String value;
    
    private String expression;
    private List<JSONObject> tags;
    
    private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination
    
    public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		SnapshotExpressionValue.valueFormulas = valueFormulas;//should this be changed to this?
	}
	public SnapshotExpressionValue() {
		
	}
	public SnapshotExpressionValue(List<String> snapshotIds, String value) {
    	this.snapshotIds = snapshotIds;
    	this.value = value;
    }
    public SnapshotExpressionValue(String context,String period,String unit, String value) {
    	this.context = context;
    	this.period = period;
    	this.unit = unit;
    	this.value = value;
    }

    private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
    	for(int i=0;i<nodes.size();i++) {
    		JSONObject node = nodes.get(i);
    		if(node.get("id").equals(id)) {
    			return node;
    		}
    	}
    	return null;
    }

    @Override
	public double calculate() {
    	if(this.snapshotRepository == null) {
    		this.snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
    	}
    	ArrayList<Double> values = new ArrayList<>();
		if(this.snapshotIds != null) {
			for(int i=0;i<this.snapshotIds.size();i++) {
	        	Optional<Snapshot> snapshotObj = this.snapshotRepository.findById(this.snapshotIds.get(i));
	        	Snapshot snapshot = null;
	        	Object obj = null;
	        	if(snapshotObj.isPresent()) {
	        		snapshot = snapshotObj.get();
					List<JSONObject> content = snapshot.getFileContent();
					obj = getNodeWithId(content,this.value);
	        	}
				if(obj != null) {
					JSONObject valueObj = new JSONObject((Map)obj);
					String val = valueObj.get("Value").toString();
					if(val != null) {
						values.add(Double.parseDouble(val));
					}	
				}
			}
		}else {
			List<Snapshot> snapshots = null;
			if(this.tags != null && this.tags.size() > 0) {
				if(this.snapshotService == null) {
					this.snapshotService = BeanLoaderUtil.getBean(SnapshotService.class);
				}
				snapshots = this.snapshotService.getSnapshotsWithTags(this.tags);
			}else if(this.context != null && this.unit != null && this.period != null) {
				snapshots = this.snapshotRepository.findByContextAndUnitAndPeriod(this.context,this.unit, this.period);
			}else if(this.context != null && this.unit != null && !this.unit.equals("null")){
				snapshots = this.snapshotRepository.findByContextAndUnit(this.context, this.unit);
			}else if(this.context == null && unit != null) {
				snapshots = this.snapshotRepository.findByUnit(this.unit);
			}else if(this.context != null ) {
				snapshots = this.snapshotRepository.findByContext(this.context);
			}
			Iterator<Snapshot>  snapshotIter = snapshots.iterator();
			while(snapshotIter.hasNext()) {
				Snapshot snapshot = snapshotIter.next();
				List<JSONObject> content = snapshot.getFileContent();
				if(content == null) {
					continue;
				}
				Object obj = getNodeWithId(content,this.value);
				if(obj != null) {
					JSONObject valueObj = new JSONObject((Map)obj);
					String val = valueObj.get("Value").toString();
					if(val != null) {
						values.add(Double.parseDouble(val));
					}
				}
			}
		}
		Class expressionCls = null;
		FunctionExtension functionInstance = null;
		PrimitiveElement elements[] = new PrimitiveElement[1];
		
		try {
			expressionCls = Class.forName(this.expression);
			functionInstance = (FunctionExtension)expressionCls.newInstance();
		}catch(ClassNotFoundException cnf) {
			logger.error(cnf.getMessage());
		} catch (InstantiationException e) {
			logger.error(e.getMessage());
		} catch (IllegalAccessException e) {
			logger.error(e.getMessage());
		}
		String expressionStr;
		Expression exp;
		if(expressionCls != null) {
			if(this.expression.lastIndexOf(".")>0) {
				this.expression = this.expression.substring(this.expression.lastIndexOf(".") + 1);	
			}
			Function f = new Function(this.expression, functionInstance);
			exp = new Expression(this.expression +  "(" + StringUtils.join(values,",")  + ")",f);
		}else {
			expressionStr = this.expression + "(" + StringUtils.join(values,",")  + ")";
			exp = new Expression(expressionStr);
		}
		return exp.calculate();
	}

	@Override
	public FunctionExtension clone() {
		if(this.snapshotIds != null) {
			return new SnapshotExpressionValue(snapshotIds, value);
		}else {
			return new SnapshotExpressionValue(context,period, unit, value);	
		}
	}

	@Override
	public String getParameterName(int arg0) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	@Override
	public void setParameterValue(int paramNumber, double hash) {
		// TODO Auto-generated method stub
		if(paramNumber == 0) {
			ValueFormula formula = valueFormulas.get(hash);
			if(formula != null) {
				this.snapshotIds = formula.getSnapshotIdsForExpression();
				this.context = formula.getSnapshotContext();
				this.value = formula.getSnapshotValue();
				this.period = formula.getSnapshotPeriod();
				this.unit = formula.getSnapshotUnit();
				this.expression = formula.getSnapshotValueExpression();
				this.tags = formula.getTags();
				
				JSONObject dataParameters;
				dataParameters = formula.getDataParameters();
				if(dataParameters != null && dataParameters.get("snapshotIds") != null) {
					this.snapshotIds = (List<String>)dataParameters.get("snapshotIds");
				}
				if(dataParameters != null && dataParameters.get("context") != null) {
					this.context = (String)dataParameters.get("context");
				}
				if(dataParameters != null && dataParameters.get("period") != null) {
					this.period = (String)dataParameters.get("period");
				}
				if(dataParameters != null && dataParameters.get("unit") != null) {
					this.unit = (String)dataParameters.get("unit");
				}
			}
		}
	}

}
