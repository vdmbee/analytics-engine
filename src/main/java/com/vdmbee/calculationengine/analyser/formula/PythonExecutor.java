package com.vdmbee.calculationengine.analyser.formula;


import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.python.core.PyDictionary;
import org.python.core.PyFloat;
import org.python.core.PyInteger;
import org.python.core.PyList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.util.StringUtils;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.expression.providers.PythonExecutorFactory;
import com.vdmbee.calculationengine.expression.providers.PythonExecutorPool;
import com.vdmbee.calculationengine.expression.providers.PythonScriptExecutor;
import com.vdmbee.calculationengine.expression.providers.TemplateType;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.util.PythonConfig;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;
import com.vdmbee.calculationengine.util.Timming;

public class PythonExecutor implements FunctionExtension{
	private static final Logger logger = LogManager.getLogger(PythonExecutor.class);
	//public static JSONObject snapshotError = new JSONObject();
	private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination 
	private static PythonExecutorPool pool;
	
	//@Autowired
	private SnapshotRepository snapshotRepository;
	
	//@Autowired
	private SnapshotService snapshotService;
	
    private List<String> snapshotIds;

    private String context;
	private String period;
    private String unit;
    
    private String value;
    
    private List<String> invalidSnapshotIds;
    
    private List<JSONObject> tags;
    private String pythonScript;
    private JSONObject dataParameters;
    
    private Snapshot snapshot;
    private String currentSnapshotId;
    private ValueFormula formula = null;
    private double hash;
    private String formulaId;
    private TemplateType template;
    
    @Autowired
	private MongoTenantTemplate mongoTemplate;
    
    public String getCurrentSnapshotId() {
		return currentSnapshotId;
	}
	public void setCurrentSnapshotId(String currentSnapshotId) {
		this.currentSnapshotId = currentSnapshotId;
	}
    HashMap<String, List<JSONObject>> snapshots = new HashMap<String, List<JSONObject>>();
    HashMap<String, List<JSONObject>> invalidSnapshots = new HashMap<String, List<JSONObject>>();
    
    public HashMap<String, List<JSONObject>> getInvalidSnapshots() {
		return invalidSnapshots;
	}
	public void setInvalidSnapshots(HashMap<String, List<JSONObject>> invalidSnapshots) {
		this.invalidSnapshots = invalidSnapshots;
	}
	PythonScriptExecutor executor = null;
    
	public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		PythonExecutor.valueFormulas = valueFormulas;
	}
	
	public PythonExecutor() {

	}
	public PythonExecutor(List<String> snapshotIds, String value,List<String> invalidSnapshotIds,String pythonScript) {
    	this.snapshotIds = snapshotIds;
    	this.value = value;
    	this.invalidSnapshotIds = invalidSnapshotIds;
    	this.pythonScript = pythonScript;
    }
    public PythonExecutor(String context,String period,String unit, String value,String pythonScript) {
    	this.context = context;
    	this.period = period;
    	this.unit = unit;
    	this.value = value;
    	this.pythonScript = pythonScript;
    }
    
	@Override
	public double calculate() {
		TemplateType instance = null;
		try {
	    	if(this.snapshotRepository == null) {
	    		this.snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
	    	}
	    	//ArrayList<Double> values = new ArrayList<>();
			if(this.snapshotIds != null || this.invalidSnapshotIds != null) {
				this.addToSnapshotCache(this.snapshotIds,true);
				if(this.invalidSnapshotIds != null) {
					this.addToSnapshotCache(this.invalidSnapshotIds,false);
				}
			}else {
				List<Snapshot> snapshots = null;
				if(this.tags != null && this.tags.size() > 0) {
					if(this.snapshotService == null) {
						this.snapshotService = BeanLoaderUtil.getBean(SnapshotService.class);
					}
					logger.error("snapshotRepository called for Tag Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					snapshots = this.snapshotService.getSnapshotsWithTags(this.tags);
				}else if(this.context != null && this.unit != null && this.period != null) {
					logger.error("snapshotRepository called for ContextAndUnitAndPeriod Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					snapshots = this.snapshotRepository.findByContextAndUnitAndPeriod(this.context,this.unit, this.period);
				}else if(this.context != null && this.unit != null && !this.unit.equals("null")){
					logger.error("snapshotRepository called for ContextAndUnit Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					snapshots = this.snapshotRepository.findByContextAndUnit(this.context, this.unit);
				}else if(this.context == null && unit != null) {
					logger.error("snapshotRepository called for Unit Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					snapshots = this.snapshotRepository.findByUnit(this.unit);
				}else if(this.context != null ) {
					logger.error("snapshotRepository called for context Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					snapshots = this.snapshotRepository.findByContext(this.context);
				}
				if(snapshots != null) {
					Iterator<Snapshot>  snapshotIter = snapshots.iterator();
					while(snapshotIter.hasNext()) {
						Snapshot snapshot = snapshotIter.next();
						List<JSONObject> content = snapshot.getFileContent();
						if(content == null || content.isEmpty()) {
							continue;
						}
						this.snapshots.put(snapshot.getId(), content);
					}
				}
			}
			//logger.info("start"+new Date()+"-"+this.value);
			if(StringUtils.isEmpty(this.formulaId) || StringUtils.isEmpty(this.pythonScript)) {
				logger.error("formulaId or pythonScript is empty"+this.currentSnapshotId);
				return Double.NaN;
			}
			executor = this.getExecutor();
			instance = executor.getTemplateInstanceForBigScripts(TemplateType.class, this.formulaId,this.pythonScript);
			//instance = executor.getTemplateInstance(TemplateType.class, this.formulaId,this.pythonScript);
			//logger.error("template:"+this.pythonScript);
			if(this.formula.getMeasures() != null) {
				instance.setMeasures(new PyList(this.formula.getMeasures()));	
			}
			setInputsDependencies(instance);
			//instance.setMeasures(new PyList(this.snapshot.getTemplate().getMeasures()));
			executor.setDataSets(this.snapshots,this.invalidSnapshots, this.value, formula);
			//executor.setDataSets(this.snapshots,this.value,true,this.formula);
			//executor.setDataSets(this.invalidSnapshots,this.value,false,this.formula);
			//logger.info("end"+new Date()+"-"+this.value);
			if(this.dataParameters != null) {
				Iterator<String> keys = this.dataParameters.keySet().iterator();
				while(keys.hasNext()) {
					String key = keys.next();
					if(key.equals("snapshotIds") || key.equals("invalidSnapshotIds")) {
						continue;
					}
					executor.setNamedValue(key, this.dataParameters.get(key));
				}
			}
			//this.pythonScript = "values = datasets.values() \n" + 
    		//		"returnValue = mean(map(lambda x: x['Value'], values)) ";
			//this.pythonScript = "values = datasets.values()";
			//executor.execute(this.pythonScript);
			//this.pythonScript = "returnValue = mean(map(lambda x: x['Value'], values)) ";
			//this.pythonScript = this.pythonScript.replaceAll("\\\\n", "\\n");
			//executor.execute("values = datasets.values() \nreturnValue = mean(map(lambda x: x['Value'], values))");
			//this.pythonScript = "values = datasets.values() " + System.lineSeparator() + "returnValue = mean(map(lambda x: x['Value'], values))";
			Object retVal= null;
			Date before = new Date();
			if(instance == null) {
				this.pythonScript = this.pythonScript.replaceAll("\\\\n ", System.lineSeparator());
				executor.execute(this.pythonScript);
			}else {
				instance.setProperty("rubricIds", this.setRubricsToBeCalculated());
				
				retVal = instance.executeValueFormula(this);
			}
			Date after = new Date();
			Timming.addPythonTime(after.getTime() - before.getTime());
			//Object retVal = executor.getNamedValue("returnValues");
			if(retVal != null && retVal instanceof PyDictionary) {
				HashMap<String, Double> retValues = new HashMap<>();
				PyDictionary values = (PyDictionary)retVal;
				
				PyList keyList = values.keys();
				
				for(int i=0;i<keyList.size();i++) {
					String keyId = (String) keyList.get(i);
					Object val = values.get(keyList.get(i));
					if(val instanceof PyDictionary) {
						//logger.error("result:"+values.toString());
						PyDictionary benchValues = (PyDictionary)val;
						PyList benchValueskeyList = benchValues.keys();
						for(int j=0;j<benchValueskeyList.size();j++) {
							String benchValueskeyId = (String) benchValueskeyList.get(j);
							Object benchValuesval = benchValues.get(benchValueskeyList.get(j));
							getRetValues(retValues,benchValuesval,benchValueskeyId);
						}
					}else {
						getRetValues(retValues,val,keyId);
					}
				}
				if(this.formula != null) {
					this.formula.setReturnValues(retValues);
				}
				retValues = null;
				values = null;
			}
			retVal = executor.getNamedValue("returnValue");
			if(retVal == null) {
				return Double.NaN;
			}
			if(retVal instanceof PyInteger) {
				return (((PyInteger)retVal)).getValue();
			}else {
				return ((PyFloat)retVal).getValue();
			}
		}catch(Throwable e) {
			if(executor != null && executor.getGenerateDeugScript()) {
				logger.info(executor.getDebugScript());
			}
			//e.printStackTrace();
			StringWriter sw = new StringWriter();
	        e.printStackTrace(new PrintWriter(sw));
	        String expString = sw.toString();
	        String result = expString.substring(0, Math.min(expString.length(), 300));
	        //String[] result = sw.toString().split(System.lineSeparator(), 4);
	        //String exceptionAsString = sw.toString();
			//if(e.getCause() != null) {
			//	logger.error("for Rubric Id:"+this.value+" "+e.getLocalizedMessage());
			//}
	        if(!StringUtils.isEmpty(result) && result.contains("SyntaxError")) {
	        	logger.error("Template is corrupt for Snapshot:"+ this.currentSnapshotId + " in tenant:"+this.formula.getDbTenant());
	        }
			logger.error("snapshotId:"+this.currentSnapshotId+" "+result, e);
	       //snapshotError.put(this.value, exceptionAsString);
			//throw new RuntimeException(e.getMessage());	// as the 0.0 value is required for errored values so no throwing error
			return Double.NaN;
	        //return 0.0;
		}finally {
			this.snapshotRepository = null;
			this.snapshotService = null;
			if(executor != null) {
				//this.cleanDataParameters(executor, dataParameters);
				if(instance != null) {
					instance.cleanUp();
				}
				executor.setTemplate(null);
				//executor.reset();
				//valueFormulas.clear();
				valueFormulas.remove(hash);
				instance = null;
				pool.returnObject(executor);
				executor = null;
				//System.out.println("returnidle"+pool.getNumIdle());
				//System.out.println("returnActive"+pool.getNumActive());
			}
			this.snapshots.clear();
			this.invalidSnapshots.clear();
		}
	}
	
	private void getRetValues(HashMap<String, Double> retValues,Object val, String keyId) {
			if(val == null || val.toString().equalsIgnoreCase("NaN")) {
				retValues.put(keyId, Double.NaN);
			}else {
				if(Integer.class.isInstance(val)) {
					retValues.put(keyId, new Double((((Integer)val))));
				}else {
					 if(!(val instanceof Double) && !val.toString().equalsIgnoreCase("NaN")){
						 logger.error("value is corrupt for Snapshot:"+ this.currentSnapshotId + " in tenant:"+this.formula.getDbTenant());
						 logger.error(keyId+"-"+val);
					 }
					retValues.put(keyId, (Double) val);
				}
			}
	}
	private ArrayList<String> setRubricsToBeCalculated() {
		ArrayList<String> rubricIds = new ArrayList<>();
		List<JSONObject> rubricObjs = this.formula.getRubricsToCalculate();
		if(rubricObjs != null) {
			for(int i=0;i<rubricObjs.size();i++) {
				JSONObject js = new JSONObject((Map)rubricObjs.get(i));
				String rub = (String)js.get("id");
				rubricIds.add(rub);
			}
		}
		return rubricIds;
	}
	private void setInputsDependencies(TemplateType instance) {
		Iterator<String> keys = this.formula.getArguments().keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			String var = "val_" + key;
			Double valDouble = this.formula.getCalculatedValues().get(key);
			if(valDouble == null) {
				valDouble = Double.NaN;
			}
			if(instance != null) {
				instance.setProperty(var, valDouble);
			}else {
				executor.setNamedValue(key, valDouble);
			}
		}
	}
	
	public void getDatasetValuesExternal(String kpiId){
		List<Object> extensions = formula.getFunExtensions();
		PythonExecutor executor = null;
		for(int i=0;i<extensions.size();i++) {
			if(extensions.get(i) instanceof PythonExecutor) {
				executor = (PythonExecutor)extensions.get(i);
				break;
			}
		}
		if(executor != null) {
			executor.getDatasetValues(kpiId,true);
			executor.getDatasetValues(kpiId,false);
		}
	}
	
	private void getDatasetValues(String kpiId,boolean valid){
		//List<JSONObject> ret = new ArrayList();
		this.template.clearDatasets();
		this.template.setDatasetValue(kpiId);
		List<String> ids = valid? this.snapshotIds : this.invalidSnapshotIds;
		int length = ids.size();
		int index = 0;
		List<String> cursorSnapshotList = new ArrayList<String>();
		while(index<length) {
			cursorSnapshotList.clear();
			cursorSnapshotList = ids.subList(index, (index + 5000 < length) ? index+5000 : length);		
			String idStr = cursorSnapshotList.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(","));
			String queryStr = "{\"_id\" : {$in:[" + idStr + "]},\"fileContent.id\":\"" + kpiId + "\"}";
			String projectionStr = "{\"fileContent.$\":1  }";
			
			BasicQuery queryObject = new BasicQuery(queryStr,projectionStr);
			if(mongoTemplate == null) {
				mongoTemplate = BeanLoaderUtil.getBean(MongoTenantTemplate.class);
			}
			//StringBuffer setScript = new StringBuffer();
			Iterator<Snapshot> values = mongoTemplate.find(queryObject, Snapshot.class,"snapshot").iterator();
			while(values.hasNext()) {
				Snapshot s = values.next();
				//ret.add(s.getFileContent().get(0));
				JSONObject valueNode = new JSONObject((Map)s.getFileContent().get(0));
				PyDictionary datasetDictionary = new PyDictionary();
				datasetDictionary.put((String)valueNode.get("id"), (Double)valueNode.get("Value"));
				/*if(valid) {
					//this.template.setDataset(s.getId(), datasetDictionary);	
				}else {
					//this.template.setInvalidDataset(s.getId(), datasetDictionary);	
				}*/
			}
			index = index + 5000;
		}
		//return ret;
	}
	
	/*private void cleanDataParameters(PythonScriptExecutor executor,JSONObject dataParameters) {
		try {
			if(this.dataParameters != null) {
				Iterator<String> keys = this.dataParameters.keySet().iterator();
				StringBuffer delScript = new StringBuffer();
				while(keys.hasNext()) {
					String key = keys.next();
					if(key.equals("snapshotIds") || key.equals("invalidSnapshotIds")) {
						continue;
					}
					delScript.append("del " + key+"\n");
				}
				if(!delScript.toString().equals("")) {
					executor.execute(delScript.toString());
				}
			}
		}catch(Exception e) {
			System.out.println("exception2xxxxx"+e);
			pool.returnObject(executor); 
			executor = null;
		}
	}*/
	
	private void addToSnapshotCache(List<String> snapshotList,boolean valid) {
		for(int i=0;i<snapshotList.size();i++) {
			if(StringUtils.hasLength(snapshotList.get(i))) {
				Snapshot existedSnapshotObj = Snapshot.getSnapshotsCache(this.currentSnapshotId, snapshotList.get(i));
				Snapshot snapshot = null;
				if(existedSnapshotObj == null) {
					//logger.error("snapshotRepository called for addToSnapshotCache for "+snapshotList.get(i)+" Snapshot:"+ this.currentSnapshotId+" thread:"+Thread.currentThread().getName());
					Optional<Snapshot> snapshotObj = this.snapshotRepository.findById(snapshotList.get(i));
					if(snapshotObj.isPresent()) {
						snapshot = snapshotObj.get();
						List<JSONObject> content = snapshot.getFileContent();
						if(valid) {
							this.snapshots.put(snapshot.getId(), content);
						}else {
							this.invalidSnapshots.put(snapshot.getId(), content);
						}
						Snapshot.putSnapshotsCache(this.currentSnapshotId, snapshotList.get(i), snapshot);
					}
				}else {
					snapshot = existedSnapshotObj;
					List<JSONObject> content = snapshot.getFileContent();
					if(valid) {
						this.snapshots.put(snapshot.getId(), content);	
					}else {
						this.invalidSnapshots.put(snapshot.getId(), content);
					}
				}
			}
		}
	}
	
	private PythonScriptExecutor getExecutor() throws Exception {
		PythonScriptExecutor ex = pool.borrowObject();
		//System.out.println("idle"+pool.getNumIdle());
		//System.out.println("Active"+pool.getNumActive());
		if(!ex.isInitialized()) {
			try {
			    String JYTHON_HOME = System.getenv("JYTHON_HOME") != null ? System.getenv("JYTHON_HOME") : "E:\\jython2.7.0";
			    String JYTHON_LIB = JYTHON_HOME + File.separator + "Lib";
			    
		        Properties properties = new Properties();
		        properties.setProperty("python.home", JYTHON_HOME);
		        properties.setProperty("python.path", JYTHON_LIB);
		        properties.put("python.console.encoding", "UTF-8"); // Used to prevent: console: Failed to install '': java.nio.charset.UnsupportedCharsetException: cp0.
		        //properties.put("python.security.respectJavaAccessibility", "false"); //don't respect java accessibility, so that we can access protected members on subclasses
		        //properties.put("python.import.site","false");
		        
		        ex.setInitProperties(properties);
		        ex.init();
			}catch(Exception e) {
				//logger.error("Initialization of Jython failed");
				//System.out.println("Initialization of Jython failed");
				logger.error("Initialization of Jython failed inside getExecutor method: " + e.getMessage(), e);
				//e.printStackTrace();
				throw e;
			}
		}else {

		}
        return ex;
	}
	@Override
	public FunctionExtension clone() {
		if(this.snapshotIds != null) {
			return new PythonExecutor(snapshotIds, value,invalidSnapshotIds,pythonScript);
		}else {
			return new PythonExecutor(context,period, unit, value,pythonScript);	
		}
	}

	@Override
	public String getParameterName(int arg0) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	@Override
	public void setParameterValue(int paramNumber, double hash) {
		if(paramNumber == 0) {
			this.hash = hash;
			formula = valueFormulas.get(hash);
			if(formula != null) {
				this.formulaId = formula.getName();
				if(this.formulaId == null) {
					logger.error("failed to fetch formulaId");
				}
				this.snapshotIds = formula.getSnapshotIdsForExpression();
				this.context = formula.getSnapshotContext();
				this.value = formula.getSnapshotValue();
				this.period = formula.getSnapshotPeriod();
				this.unit = formula.getSnapshotUnit();
				this.tags = formula.getTags();
				this.pythonScript = formula.getCustomScript();
				if(this.pythonScript == null) {
					logger.error("failed to fetch pythonScript");
				}
				this.dataParameters = formula.getDataParameters();
				this.currentSnapshotId = formula.getCurrentSnapshotId();
				if(dataParameters != null && dataParameters.get("snapshotIds") != null) {
					this.snapshotIds = (List<String>)dataParameters.get("snapshotIds");
				}
				if(dataParameters != null && dataParameters.get("invalidSnapshotIds") != null) {//TODO making generic later using invalidDataset from template
					this.invalidSnapshotIds = (List<String>)dataParameters.get("invalidSnapshotIds");
				}
				if(this.dataParameters != null && this.dataParameters.get("context") != null) {
					this.context = (String)this.dataParameters.get("context");
				}
				if(this.dataParameters != null && this.dataParameters.get("period") != null) {
					this.period = (String)this.dataParameters.get("period");
				}
				if(this.dataParameters != null && this.dataParameters.get("unit") != null) {
					this.unit = (String)this.dataParameters.get("unit");
				}
			}
		}
	}

	static {
    	GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        Integer maxTotal = 30;//default
        //Integer maxIdle = maxTotal;
        
        PythonConfig pythonConfig = (PythonConfig) SnapshotBeanUtil.getBean(PythonConfig.class);
		String executorPoolsStatic = pythonConfig.getExecutorPools();
        if(NumberUtils.isDigits(executorPoolsStatic)) {
        	maxTotal = Integer.parseInt(executorPoolsStatic);
        	//maxIdle = maxTotal;
    	}else {
    		logger.error("Default maxIdleExecutorPools set:"+maxTotal);
    	}
        config.setMinIdle(2);
        config.setBlockWhenExhausted(true);
        config.setMaxIdle(5);
        config.setMaxTotal(maxTotal);
        config.setMaxWait(Duration.ofMillis(5L * 60 * 1000));//5minutes
        config.setTimeBetweenEvictionRuns(Duration.ofMillis(5L * 60 * 1000));//Try to clean each 5 minutes
        config.setMinEvictableIdleTime(Duration.ofMillis(10L * 60 * 1000)); // 10 minutes 
        config.setNumTestsPerEvictionRun(-1); //Try all
        //config.setTestOnBorrow(true);
       //config.setTestOnReturn(true);
        PythonExecutor.pool = new PythonExecutorPool(new PythonExecutorFactory(), config);
	}
	
}
