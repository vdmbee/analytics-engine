package com.vdmbee.calculationengine.analyser.formula;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.mariuszgromada.math.mxparser.FunctionExtensionVariadic;
import org.mariuszgromada.math.mxparser.PrimitiveElement;
import org.springframework.data.domain.Sort;

import com.vdmbee.calculationengine.analyser.formula.Measure.Accumulator;
import com.vdmbee.calculationengine.analyser.formula.MeasureRelation.Operation;
import com.vdmbee.calculationengine.analyser.snapshot.entity.Task;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.transaction.ApplicationTransaction;
import com.vdmbee.calculationengine.transaction.ApplicationTransactionManager;

public class ValueFormula implements Task {
	private static final Logger logger = LogManager.getLogger(ValueFormula.class);
    String name;
    int sleepTimeInMillis;
    protected String dbTenant;
    private boolean stopExecution = false;
    private boolean doneCalculation  = false;
    private static boolean usingSingleScript = true;
    private List<Object> funExtensions= new ArrayList<>(); 
    private static Pattern p = Pattern.compile("@[@#0-9a-zA-Z\\-]+");
    private List<String> measures;
    
    public List<String> getMeasures() {
		return measures;
	}

	public void setMeasures(List<String> measures) {
		this.measures = measures;
	}

	public String getDbTenant() {
		return dbTenant;
	}

	public void setDbTenant(String dbTenant) {
		this.dbTenant = dbTenant;
	}
	List<Task> dependencies = new ArrayList<>();

    void addDependency(Task task) {
        dependencies.add(task);
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public List<Task> dependencies() {
        return dependencies;
    }

    @Override
    public void execute() {
    	MongoTenantContext.setTenant(this.getDbTenant());
       	this.calculate();
    }
    @Override
    public void checkDependencies(ArrayList<Task> tasks) {
    	Iterator<String> keys = this.arguments.keySet().iterator();
    	while(keys.hasNext()) {
    		String key = keys.next();
    		String keyName = this.arguments.get(key);
    		for(int i=0;i<tasks.size();i++) {
    			if(tasks.get(i).name().equals(keyName)) {
    				this.addDependency(tasks.get(i));
    				break;
    			}
    		}
    	}
    }
	//private String key;
	private String escKey;
	private String tenantId;
	private Function function;

	private String expressionStr;
	private LinkedHashMap<String, String> arguments = new LinkedHashMap<>();
	private HashMap<String, Double> calculatedValues;
	
	private String mongoJsonQuery;
	private String mongoValueField;
	private String mongoFormula;
	private String customScript;
	private String currentSnapshotId;
	private HashMap<String, Double> returnValues;
	private List<JSONObject> rubricsToCalculate;
	private List<Document> results = null;
	
	public HashMap<String, Double> getReturnValues() {
		return returnValues;
	}

	public List<JSONObject> getRubricsToCalculate() {
		return rubricsToCalculate;
	}

	public void setRubricsToCalculate(List<JSONObject> rubricsToCalculate) {
		this.rubricsToCalculate = rubricsToCalculate;
	}

	public void setResults(List<Document> results){
		this.results = results;
	}
	public List<Document> getResults(){
		return this.results;
	}
	
	public void setReturnValues(HashMap<String, Double> returnValues) {
		this.returnValues = returnValues;
		if(!this.usingSingleScript) {
			this.calculatedValues.putAll(returnValues);
		}
		Iterator<String> inputs = this.arguments.keySet().iterator();
		while(inputs.hasNext()) {
			String escKey = inputs.next();
			String key = this.arguments.get(escKey);
			if(!this.returnValues.containsKey(key)) {
				this.returnValues.put(key, this.calculatedValues.get(escKey));
			}
		}
	}

	public String getCurrentSnapshotId() {
		return currentSnapshotId;
	}

	public void setCurrentSnapshotId(String currentSnapshotId) {
		this.currentSnapshotId = currentSnapshotId;
	}

	public String getCustomScript() {
		return customScript;
	}

	public void setCustomScript(String customScript) {
		this.customScript = customScript;
	}

	public String getSnapshotTenantId() {
		return snapshotTenantId;
	}

	public void setSnapshotTenantId(String snapshotTenantId) {
		this.snapshotTenantId = snapshotTenantId;
	}

	public String getSnapshotId() {
		return snapshotId;
	}

	public void setSnapshotId(String snapshotId) {
		this.snapshotId = snapshotId;
	}

	public String getSnapshotValue() {
		return snapshotValue;
	}

	public void setSnapshotValue(String snapshotValue) {
		this.snapshotValue = snapshotValue;
	}

	public String getSnapshotContext() {
		return snapshotContext;
	}

	public void setSnapshotContext(String snapshotContext) {
		this.snapshotContext = snapshotContext;
	}

	public String getSnapshotPeriod() {
		return snapshotPeriod;
	}

	public void setSnapshotPeriod(String snapshotPeriod) {
		this.snapshotPeriod = snapshotPeriod;
	}

	public String getSnapshotUnit() {
		return snapshotUnit;
	}

	public void setSnapshotUnit(String snapshotUnit) {
		this.snapshotUnit = snapshotUnit;
	}
	private JSONObject dataParameters;
	private List<String> sortFields;
	private List<JSONObject> stages;
	private Sort.Direction direction;
	private String outputField;
	private FunctionExtension extension;
	
	private List<Double> weights;
	private List<MeasureRelation> mesRelationsList;
	private Accumulator acc;
	private Future<?> future;
	
	private String snapshotTenantId;
	private String snapshotId;
	private String snapshotValue;
	private String snapshotValueExpression;
	private String snapshotContext;
	private String snapshotPeriod;
	private String snapshotUnit;
	
	private ArrayList<String> snapshotIdsForExpression;
	private List<JSONObject> tags;
	public List<JSONObject> getTags() {
		return tags;
	}

	public void setTags(List<JSONObject> tags) {
		this.tags = tags;
	}
	
	
	public ArrayList<String> getSnapshotIdsForExpression() {
		return snapshotIdsForExpression;
	}

	public void setSnapshotIdsForExpression(ArrayList<String> snapshotIdsForExpression) {
		this.snapshotIdsForExpression = snapshotIdsForExpression;
	}

	public Future<?> getFuture() {
		return future;
	}
	public void setFuture(Future<?> future) {
		this.future = future;
	}
	private static ConcurrentHashMap<Double, ValueFormula> valueFormulas = new ConcurrentHashMap();	//To store parameter data for expression combination
	private double hash = 0.0;
	
	public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		ValueFormula.valueFormulas = valueFormulas;
	}
	public double getHash() {
		return hash;
	}
	public void setHash(double hash) {
		this.hash = hash;
	}
	static {
		MongodbAggregation.setValueFormulas(valueFormulas);
		MongodbBasicQuery.setValueFormulas(valueFormulas);
		SnapshotValue.setValueFormulas(valueFormulas);
		SnapshotExpressionValue.setValueFormulas(valueFormulas);
		PythonExecutor.setValueFormulas(valueFormulas);
		SpelExecutor.setValueFormulas(valueFormulas);
	}
	
	public HashMap<String, Double> getCalculatedValues() {
		return calculatedValues;
	}
	public void setCalculatedValues(HashMap<String, Double> calculatedValues,String tenantId) {
		this.calculatedValues = calculatedValues;
	}

	//Relations based
	public ValueFormula(String key,String currentSnapshotId, String measurementId, JSONObject mestRelations, JSONObject measure ,HashMap<String, Double> calculatedValues,String tenantId) {
		this.name = key;
		this.currentSnapshotId =currentSnapshotId;
		this.tenantId = tenantId;
		this.calculatedValues = calculatedValues;
		this.escKey = key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		if(measure != null) {
			String measureId = (String)measure.get("id");
			Iterator<String> mestRelationsIter = mestRelations.keySet().iterator();
			List<MeasurementRelation> mestRelationsList = new ArrayList<>();
			this.mesRelationsList = new ArrayList<>();
			this.acc = null;
			String accStr = (String)measure.get("accumulator");
			List<Double> weights = new ArrayList<Double>();
			if(accStr.equals("sum")) {
				this.acc = Accumulator.SUM;
			}else if(accStr.equals("product")) {
				this.acc = Accumulator.PRODUCT;
			}else if(accStr.equals("average")) {
				this.acc = Accumulator.AVG;
			}else if(accStr.equals("maximum")) {
				this.acc = Accumulator.MAX;
			}else if(accStr.equals("minimum")) {
				this.acc = Accumulator.MIN;
			}else if(accStr.equals("standardDeviation")) {
				this.acc = Accumulator.STANDARD_DEVIATION;
			}else {
				this.acc = Accumulator.WEIGHTED_AVERAGE;
			}
			
			while(mestRelationsIter.hasNext()) {
				String mestRelId = mestRelationsIter.next();
				JSONObject mestRelDetails = new JSONObject((Map)mestRelations.get(mestRelId));
				String baseMeasurementId = (String)mestRelDetails.get("baseMeasurement");
				String weightMeasurementId;
				double weightVal;
				String escId = null;
				if(this.acc == Accumulator.WEIGHTED_AVERAGE) {
					weightMeasurementId = (String)mestRelDetails.get("weightMeasurement");
					if(weightMeasurementId != null) {
						escId = weightMeasurementId.replaceAll("[@-]", "_");
						escId = escId.replaceAll("[#]", "_");
					}
					
					if(escId == null || calculatedValues.get(escId) == null) {
						this.acc = Accumulator.AVG;
					}else {
						if(escId != null) {
							weightVal = calculatedValues.get(escId);
							weights.add(weightVal);
						}
					}
				}
				String escBaseMeasurementId = baseMeasurementId.replaceAll("[@-]", "_");
				escBaseMeasurementId = escBaseMeasurementId.replaceAll("[#]", "_");
				this.arguments.put(escBaseMeasurementId, baseMeasurementId);
				
				String mesRelId = (String)mestRelDetails.get("measureRelationship");
				JSONObject mesRelations = new JSONObject((Map)measure.get("relations"));
				JSONObject relDetails = new JSONObject((Map)mesRelations.get(mesRelId));
				if(relDetails!= null) {
					Operation oper = Operation.NOOPERATION;
					String operStr = (String)relDetails.get("operation");
					if(operStr !=  null) {
						if(operStr.equals("Reciprocal")) {
							oper = Operation.RECIPROCAL;
						}else if(operStr.equals("Square")) {
							oper = Operation.SQUARE;
						}else if(operStr.equals("Square root")) {
							oper = Operation.SQUAREROOT;
						}
					}
					
					MeasureRelation mesrel = new MeasureRelation(Double.valueOf(relDetails.get("multiplier").toString()), Double.valueOf(relDetails.get("offset").toString()), oper);
					mesRelationsList.add(mesrel);
				}
			}

			Measure mes = new Measure(measureId, mesRelationsList, acc);
			if(weights != null && weights.size() > 0) {
				this.weights = weights;
			}
			
		}
		
	}
	
	//Expression based
	public ValueFormula(String key, String currentSnapshotId, String formulaStr,List<JSONObject> stages,List<String> sortFields,Sort.Direction direction,String outputField, String queryOrScript, String mongoValueField, String mongoFormula, String snapshotId,String snapshotTenantId, String snapshotValue,String snapshotContext,String snapshotPeriod,String snapshotUnit,ArrayList<String> snapshotIds,String snapshotExpression,HashMap<String, Double> calculatedValues,JSONObject dataParameters,List<JSONObject> tags,ArrayList<String> dependentValues,List<JSONObject> rubricsToCalculate) {
		this.name = key;
		this.currentSnapshotId=currentSnapshotId;
		this.calculatedValues = calculatedValues;
		this.escKey = key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		this.dataParameters = dataParameters;
		this.sortFields = sortFields;
		this.direction = direction;
		this.outputField = outputField;
		this.mongoJsonQuery = queryOrScript;
		this.mongoFormula = mongoFormula;
		this.mongoValueField = mongoValueField;
		this.rubricsToCalculate = rubricsToCalculate;
		if(stages != null) {
			this.stages = stages;	
		}
		this.snapshotTenantId = snapshotTenantId;
		this.snapshotId = snapshotId;
		this.snapshotIdsForExpression = snapshotIds;
		this.snapshotValueExpression = snapshotExpression;
		this.snapshotContext = snapshotContext;
		this.snapshotPeriod = snapshotPeriod;
		this.snapshotUnit = snapshotUnit;
		this.snapshotValue = snapshotValue;
		this.tags = tags;

		if(formulaStr == null || formulaStr.equalsIgnoreCase("undefined") || formulaStr.equalsIgnoreCase("")) {
			return;
		}
		SecureRandom random = new SecureRandom();
		this.hash =  random.nextDouble();
		if(formulaStr.indexOf("mongodbBasicQuery") >= 0 ) {
			//this.getMongodbBasicQueryFunction(formulaStr);
			formulaStr = formulaStr.replaceAll("mongodbBasicQuery\\(\\)", "mongodbBasicQuery(" + this.hash + ")");
		}
		if(formulaStr.indexOf("mongodbAggregation") >= 0){
			//this.getMongodbAggrigationFunction();
			formulaStr = formulaStr.replaceAll("mongodbAggregation\\(\\)", "mongodbAggregation(" + this.hash + ")");			
		}
		if(formulaStr.indexOf("snapshotValue") >= 0){
			formulaStr = formulaStr.replaceAll("snapshotValue\\(\\)", "snapshotValue(" + this.hash + ")");			
		}
		if(formulaStr.indexOf("snapshotExpressionValue") >= 0){
			//this.getMongodbAggrigationFunction();
			formulaStr = formulaStr.replaceAll("snapshotExpressionValue\\(\\)", "snapshotExpressionValue(" + this.hash + ")");			
		}
		if(formulaStr.indexOf("pythonExecutor") >= 0) {
			formulaStr = formulaStr.replaceAll("pythonExecutor\\(\\)", "pythonExecutor(" + this.hash + ")");
			this.customScript = this.mongoJsonQuery;
		}
		if(formulaStr.indexOf("spelExecutor") >= 0) {
			formulaStr = formulaStr.replaceAll("spelExecutor\\(\\)", "spelExecutor(" + this.hash + ")");
			this.customScript = this.mongoJsonQuery;
		}

		//if(this.customScript != null) {
			this.getVMPFunction(formulaStr,this.customScript,dependentValues);	
		//}
	}
	
	
	public String getSnapshotValueExpression() {
		return snapshotValueExpression;
	}

	public void setSnapshotValueExpression(String snapshotValueExpression) {
		this.snapshotValueExpression = snapshotValueExpression;
	}

	private void getMongodbAggrigationFunction() {
		this.extension = new MongodbAggregation(this.stages,this.dataParameters,this.outputField);
		this.function = new Function(this.escKey, this.extension);
	}
	private void getMongodbBasicQueryFunction(String formulaStr) {
		this.mongoJsonQuery = formulaStr.substring(formulaStr.indexOf("{"),formulaStr.lastIndexOf("}") + 1);
		String otherPart = formulaStr.substring(formulaStr.lastIndexOf("}") + 2);
		this.mongoValueField = otherPart.substring(0,otherPart.indexOf(","));
		this.mongoFormula = otherPart.substring(otherPart.lastIndexOf(',') + 1,otherPart.indexOf(")"));
		this.extension = new MongodbBasicQuery(this.mongoJsonQuery,this.mongoValueField,this.sortFields,this.direction,this.mongoFormula,this.dataParameters);
		this.function = new Function(this.escKey, this.extension);
	}
	public String getEscKey() {
		return escKey;
	}
	public void setEscKey(String escKey) {
		this.escKey = escKey;
	}
	public String getTenantId() {
		return tenantId;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public String getExpressionStr() {
		return expressionStr;
	}
	public void setExpressionStr(String expressionStr) {
		this.expressionStr = expressionStr;
	}
	public String getMongoJsonQuery() {
		return mongoJsonQuery;
	}
	public void setMongoJsonQuery(String mongoJsonQuery) {
		this.mongoJsonQuery = mongoJsonQuery;
	}
	public String getMongoValueField() {
		return mongoValueField;
	}
	public void setMongoValueField(String mongoValueField) {
		this.mongoValueField = mongoValueField;
	}
	public String getMongoFormula() {
		return mongoFormula;
	}
	public void setMongoFormula(String mongoFormula) {
		this.mongoFormula = mongoFormula;
	}
	public JSONObject getDataParameters() {
		return dataParameters;
	}
	public void setDataParameters(JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}
	public List<String> getSortFields() {
		return sortFields;
	}
	public void setSortFields(List<String> sortFields) {
		this.sortFields = sortFields;
	}
	public List<JSONObject> getStages() {
		return stages;
	}
	public void setStages(List<JSONObject> stages) {
		this.stages = stages;
	}
	public Sort.Direction getDirection() {
		return direction;
	}
	public void setDirection(Sort.Direction direction) {
		this.direction = direction;
	}
	public String getOutputField() {
		return outputField;
	}
	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}
	public FunctionExtension getExtension() {
		return extension;
	}
	public void setExtension(FunctionExtension extension) {
		this.extension = extension;
	}
	public List<Double> getWeights() {
		return weights;
	}
	public void setWeights(List<Double> weights) {
		this.weights = weights;
	}
	public List<MeasureRelation> getMesRelationsList() {
		return mesRelationsList;
	}
	public void setMesRelationsList(List<MeasureRelation> mesRelationsList) {
		this.mesRelationsList = mesRelationsList;
	}
	public Accumulator getAcc() {
		return acc;
	}
	public void setAcc(Accumulator acc) {
		this.acc = acc;
	}

	public void setCalculatedValues(HashMap<String, Double> calculatedValues) {
		this.calculatedValues = calculatedValues;
	}
	private void getVMPFunction(String formulaStr,String customScript, ArrayList<String> dependentValues) {
		HashSet<String> parameters = new HashSet<>();
		if(dependentValues != null) {
			parameters.addAll(dependentValues);
		}
		
		Matcher m = p.matcher(formulaStr);
		int group = 0;
		while(m.find()) {
			parameters.add(m.group(group));
		}
		Iterator<String> parametersIter = parameters.iterator();
		while(parametersIter.hasNext()) {
			String param = parametersIter.next();
			String escParam = param.replaceAll("[#@-]", "_");
			//escParam = escParam.replaceAll("[#]", "_");
			arguments.put(escParam, param);
		}
		if(!formulaStr.contains("pythonExecutor")) {
			Iterator<String> argKeys = arguments.keySet().iterator();
			while(argKeys.hasNext()) {
				String argKey = argKeys.next();
				formulaStr = formulaStr.replaceAll(arguments.get(argKey),argKey);
				if(customScript != null) {
					customScript = customScript.replaceAll(arguments.get(argKey),argKey);	
				}
			}
		}
		/*this.functionStr = new StringBuffer();
		this.functionStr.append(this.escKey + "(");
		Iterator<String> keys = this.arguments.keySet().iterator();
		boolean firstArg = true;
		while(keys.hasNext()) {
			String argkey = keys.next();
			if(!firstArg) {
				this.functionStr.append(",");
			}else {
				firstArg = false;
			}
			this.functionStr.append(argkey);
		}
		this.functionStr.append(") = ");*/
		this.expressionStr = formulaStr;
		if(customScript != null) {
			this.customScript = customScript;	
		}
		this.valueFormulas.put(this.hash, this);
		//System.out.println(this.name + " expression str : " + this.expressionStr);
		//this.expression = new Expression(this.expressionStr.toString()); 		
	}
	public Function getFunction() {
		return function;
	}

	public void setFunction(Function function) {
		this.function = function;
	}

	public LinkedHashMap<String, String> getArguments() {
		return arguments;
	}

	public void setArguments(LinkedHashMap<String, String> arguments) {
		this.arguments = arguments;
	}
	
	public Double calculate() {
		ApplicationTransaction transaction = ApplicationTransactionManager.getApplicationTransaction();
		transaction.initializeMongoTransaction();
		try {
			//long startTime = new Date().getTime();
			if(this.arguments.size() == 0) {
				if(this.getFunction() == null && this.expressionStr == null) {
					//System.out.println("done calc of : " + this.escKey + " value: " + "");
					calculatedValues.put(this.escKey,null);
					doneCalculation = true;
					transaction.commitTransaction();
					return null;
				}
				Double value;
				if(this.getFunction() != null) {
					value = this.getFunction().calculate();	
				}else {
					value = this.executeFunction(calculatedValues);
					/*if(value.equals(Double.NaN)) {
						logger.info(this.customScript+" for Rubric Id:"+this.escKey);
					}*/
					this.valueFormulas.remove(this.getHash());
				}
				this.valueFormulas.remove(this.getHash());
				calculatedValues.put(this.escKey,value);
				//System.out.println("done calc of : " + this.escKey + " value: " + value);
				doneCalculation = true;
				transaction.commitTransaction();
				return value;
			}else {
				while(!this.hasAllArgumentvalues(calculatedValues)) {
					if(stopExecution) {
						logger.error("Time out error:" + "not able to get required args for:" +  this.escKey);
						logger.error("Timeout. Not able to fine some dependent arguments for :" + this.name);
						this.valueFormulas.remove(this.getHash());
						transaction.rollbackTransaction();
						return Double.NaN;
						//throw new RuntimeException("Timeout. Not able to fine some dependent arguments for :" + this.name);
					}
					try {
						Thread.currentThread().sleep(10);
					}catch(Exception e) {
						logger.error("Error while performing thread sleep inside calculate method: " + e.getMessage(), e);
						//e.printStackTrace();
						Thread.currentThread().interrupt();
					}
				}
				//System.out.println("ready to calculate:" + this.escKey);
				Double value = this.executeFunction(calculatedValues);
				if(value.equals(Double.NaN)) {
					//logger.debug(this.customScript+" for Rubric Id:"+this.name);
				}
				this.valueFormulas.remove(this.getHash());
				calculatedValues.put(this.escKey,value);
				//System.out.println("done calc of : " + this.escKey + " value: " + value);
				doneCalculation = true;
				transaction.commitTransaction();
				return value;
			}			
		}catch(Exception e) {
			transaction.rollbackTransaction();
			throw e;
		}finally {
			transaction.rollbackTransaction();
		}
	}
	
	public String getName() {
		return name;
	}

	public void setDoneCalculation(boolean doneCalculation) {
		this.doneCalculation = doneCalculation;
	}

	public boolean isDoneCalculation() {
		return doneCalculation;
	}

	public boolean isStopExecution() {
		return stopExecution;
	}

	public void setStopExecution(boolean stopExecution) {
		this.stopExecution = stopExecution;
	}

	public List<Document> getAggrigations(){
		if(this.extension == null) {
			this.extension = new MongodbAggregation(this.stages, this.dataParameters, this.outputField);
		}
		return ((MongodbAggregation)this.extension).executeAggregation();
	}
	private Double executeFunction(HashMap<String, Double> calculatedValues) {
		if(this.expressionStr != null && !this.expressionStr.equals("")) {
			Iterator<String> keys = this.arguments.keySet().iterator();
			//Expression e1 = new Expression("area(2,4)",area);
			while(keys.hasNext()) {
				String key = keys.next();
				this.expressionStr = this.expressionStr.replaceAll(key,calculatedValues.get(key).toString());
			}
			int argCount = 0;
			if(expressionStr.indexOf("measureRelation")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("measurement")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("mongodbBasicQuery")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("mongodbAggregation")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("snapshotValue")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("snapshotExpressionValue")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("toFixed")>=0) {
				argCount++;
			}
			if(expressionStr.indexOf("pythonExecutor")>=0 ) {
				argCount++;
			}
			if(expressionStr.indexOf("spelExecutor")>=0 ) {
				argCount++;
			}
			int index = 0;
			PrimitiveElement elements[]= new PrimitiveElement[argCount];
			Object funExtension;
			if(expressionStr.indexOf("measureRelation")>=0) {
				funExtension = new MeasurementRelation();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("measureRelation", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("measurement")>=0) {
				funExtension = new Measurement();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("measurement", (FunctionExtensionVariadic)funExtension);
			}
			if(expressionStr.indexOf("mongodbBasicQuery")>=0) {
				funExtension = new MongodbBasicQuery();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("mongodbBasicQuery", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("mongodbAggregation")>=0) {
				funExtension = new MongodbAggregation();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("mongodbAggregation", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("snapshotValue")>=0) {
				funExtension = new SnapshotValue();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("snapshotValue", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("snapshotExpressionValue")>=0) {
				funExtension = new SnapshotExpressionValue();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("snapshotExpressionValue", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("toFixed")>=0) {
				funExtension = new ToFixed();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("toFixed", (FunctionExtension)funExtension);
			}
			if(expressionStr.indexOf("pythonExecutor")>=0) {
				funExtension = new PythonExecutor();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("pythonExecutor", (FunctionExtension)funExtension);
				/*keys = this.arguments.keySet().iterator();
				while(keys.hasNext()) {
					String key = "val_" + keys.next();
					Double valDouble = calculatedValues.get(key);
					String valDoubleStr = valDouble != null ? valDouble.toString():null;
					if(valDoubleStr == null || Double.isNaN(valDouble)) {
						valDoubleStr = "float(\"nan\")";
					}
					this.customScript = this.customScript.replaceAll(key,valDoubleStr);
				}*/
			}
			if(expressionStr.indexOf("spelExecutor")>=0) {
				funExtension = new SpelExecutor();
				this.funExtensions.add(funExtension);
				elements[index++] = new Function("spelExecutor", (FunctionExtension)funExtension);
				keys = this.arguments.keySet().iterator();
				while(keys.hasNext()) {
					String key = keys.next();
					this.customScript = this.customScript.replaceAll(key,calculatedValues.get(key).toString());
				}
			}
			Expression e = new Expression(expressionStr.toString(),elements);
			return e.calculate();
		}else if(this.mesRelationsList == null) {
			Iterator<String> keys = this.arguments.keySet().iterator();
			//Expression e1 = new Expression("area(2,4)",area);
			StringBuffer expStr = new StringBuffer();
			expStr.append(this.escKey + "(");
			boolean firstVal = true;
			while(keys.hasNext()) {
				String key = keys.next();
				if(!firstVal) {
					expStr.append(",");
				}else {
					firstVal = false;
				}
				expStr.append(calculatedValues.get(key));
			}
			expStr.append(")");
			//System.out.println(this.name + " expression : " + expStr.toString());
			int argCount = 1;
			if(expStr.indexOf("measureRelation")>=0) {
				argCount++;
			}
			if(expStr.indexOf("measurement")>=0) {
				argCount++;
			}
			int index = 0;
			PrimitiveElement elements[] = new PrimitiveElement[argCount];
			if(expStr.indexOf("measureRelation")>=0) {
				elements[index++] = new Function("measureRelation", new MeasurementRelation());
			}
			if(expStr.indexOf("measurement")>=0) {
				elements[index++] = new Function("measurement", new Measurement());
			}
			elements[index] = this.function;
			Expression e = new Expression(expStr.toString(),elements);
			return e.calculate();
		}else {
			List<Double> baseValues = new ArrayList<Double>();
			Iterator<String> keys = this.arguments.keySet().iterator();
			while(keys.hasNext()) {
				String baseKey = keys.next();
				baseValues.add(calculatedValues.get(baseKey));
			}
			StringBuffer measurementStr = new StringBuffer();
			measurementStr.append("measurement(").append(Double.valueOf(acc.getValue()));
			for(int i=0;i<this.mesRelationsList.size();i++) {
				this.mesRelationsList.get(i).setVal(baseValues.get(i));
				measurementStr.append(",").append(this.mesRelationsList.get(i).getExpressionValue());
				if(this.acc == Accumulator.WEIGHTED_AVERAGE) {
					measurementStr.append("," + weights.get(i));
				}
			}
			measurementStr.append(")");
			Expression exp = new Expression(measurementStr.toString(),new Function("measureRelation", new MeasurementRelation()),new Function("measurement", new Measurement()));
			return exp.calculate();
		}
	}

	public List<Object> getFunExtensions() {
		return funExtensions;
	}

	private boolean hasAllArgumentvalues(HashMap<String, Double> calculatedValues) {
		boolean hasAllValues = false;
		Iterator<String> keys = this.arguments.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			if(calculatedValues.get(key) == null) {
				//System.out.println(this.name + ": is waiting for " + key );
				return false;
			}
		}
		return true;
	}

}
