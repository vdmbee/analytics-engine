package com.vdmbee.calculationengine.analyser.formula;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

import com.mongodb.Block;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.multitenant.MongoDBCredentials;
import com.vdmbee.calculationengine.multitenant.MongoTenantContext;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;


public class MongodbAggregation implements FunctionExtension {

	@Autowired
	private Environment env;

	@Autowired
	private MongoDBCredentials mongoDBCredentials;

	private static Logger logger = LogManager.getLogger(MongodbAggregation.class);

	private List<JSONObject> stages;
	private List<Document> stagesWithParams = new ArrayList<>();

	private JSONObject dataParameters;
	private String outputField;
	private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination
	private ValueFormula formula = null;
	private List<Document> results = null;
	
	public Environment getEnv() {
		return env;
	}

	public void setEnv(Environment env) {
		this.env = env;
	}

	public List<JSONObject> getStages() {
		return stages;
	}

	public void setStages(List<JSONObject> stages) {
		this.stages = stages;
	}

	public List<Document> getStagesWithParams() {
		return stagesWithParams;
	}

	public void setStagesWithParams(List<Document> stagesWithParams) {
		this.stagesWithParams = stagesWithParams;
	}

	public JSONObject getDataParameters() {
		return dataParameters;
	}

	public void setDataParameters(JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}

	public String getOutputField() {
		return outputField;
	}

	public void setOutputField(String outputField) {
		this.outputField = outputField;
	}

	public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}

	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		MongodbAggregation.valueFormulas = valueFormulas;
	}

	public Block<Document> getPrintBlock() {
		return printBlock;
	}

	public void setPrintBlock(Block<Document> printBlock) {
		this.printBlock = printBlock;
	}

	public MongodbAggregation() {

	}

	public MongodbAggregation(List<JSONObject> stages,JSONObject dataParameters,String outputField){
		this.stages = stages;
		this.dataParameters = dataParameters;
		this.outputField = outputField;
		Pattern p = Pattern.compile("\\?[#0-9a-zA-Z]+");
		Iterator<JSONObject> stagesIter = stages.iterator();
		while(stagesIter.hasNext()) {
			JSONParser parser = new JSONParser();
			JSONObject stageJson = null;
			Object stageObj = null;
			try {
				stageObj = stagesIter.next();
				if(stageObj instanceof String) {
					stageJson = (JSONObject)parser.parse((String)stageObj);
				}else {
					stageJson = (JSONObject)stageObj;
				}
			}catch(Exception e) {
				logger.error("Error while parsing stages inside MongodbAggregation method: " + e.getMessage(), e);
			}

			//JSONObject stageJson = stagesIter.next();
			if (stageJson != null) {
				String jsonQuery = stageJson.toJSONString();
				Matcher m = p.matcher(jsonQuery);
				int group = 0;
				while(m.find()) {
					String param = m.group(group);
					jsonQuery = jsonQuery.replaceAll("\\" + param, (String)this.dataParameters.get(param.substring(1)));
				}
	
				try
				{
					JSONObject obj = (JSONObject) parser.parse(jsonQuery);
					Iterator<String> keys = obj.keySet().iterator();
					Document doc = new Document();
					while(keys.hasNext()) {
						String key = keys.next();
						doc.put(key, obj.get(key));
					}
					stagesWithParams.add(doc);
				}catch(ParseException pe) {
	
				}
			}
		}
	}

	@Override
	public double calculate() {
		this.results = this.executeAggregation();
		this.formula.setResults(this.results);
		if(this.results != null && this.results.size()>=1 && this.outputField != null) {
			return new Double(this.results.get(0).get(this.outputField).toString());
		}else {
			return 0.0;
		}
	}
	private String getQueryString(String uriStr) {
		if(uriStr.lastIndexOf("?") > 0) {
			return uriStr.substring(uriStr.lastIndexOf("?"));
		}else {
			return "";
		}

	}
	 public List<Document> executeAggregation(){
		 	List<Document> ret = new ArrayList<>();
			if(env == null) {
				env = BeanLoaderUtil.getBean(Environment.class);
			}
			if(mongoDBCredentials == null) {
				mongoDBCredentials = SnapshotBeanUtil.getBean(MongoDBCredentials.class);
			}

			//String uriStr = env.getProperty("spring.data.mongodb.uri");
			//String uriStr = mongoDBCredentials.getPath() + "/" + MongoTenantContext.getTenant() + this.getQueryString(this.mongoDBCredentials.getUri());
			//MongoClientURI uri = new MongoClientURI(uriStr);
			//MongoClient mongoClient = new MongoClient(uri);
			//MongoClient mongoClient = MongoClients.create(uriStr);
		    MongoClient mongoClient = mongoDBCredentials.getMongoClient(MongoTenantContext.getTenant(), this.getQueryString(mongoDBCredentials.getUri()));
		 	String databaseStr = null;
			String collection = null;
			if(this.dataParameters != null) {
				databaseStr = (String)this.dataParameters.get("database");
				collection = (String)this.dataParameters.get("collection");
			}
			if(databaseStr == null) {
				//databaseStr = uriStr.substring(uriStr.lastIndexOf("/")+1);
				databaseStr = MongoTenantContext.getTenant();
				//databaseStr = "admin";
			}
			MongoDatabase database = mongoClient.getDatabase(databaseStr);
			if(collection == null) {
				collection = "event";
			}
			try {
				MongoCollection<Document> eventCollection = database.getCollection(collection);
				AggregateIterable<Document> results = eventCollection.aggregate(this.stagesWithParams);
				//results.forEach((Consumer<? super Document>) printBlock);
				Iterator<Document> iter = results.iterator();
				/*while(iter.hasNext()) {
					printDocument(iter.next());
				}*/
				results.into(ret);

			}catch(Exception e){
				logger.error("Error while performing mongo db event collection inside executeAggregation method: " + e.getMessage(), e);
				throw e;
			}finally {
				mongoClient.close();
			}
			return ret;
	 }
	 Block<Document> printBlock = new Block<Document>() {
	        @Override
	        public void apply(final Document document) {
	        	logger.info("document json value: " + document.toJson());
	        }
	    };
	private void printDocument(Document doc) {
		/*Iterator<String> keys = doc.keySet().iterator();
		while(keys.hasNext()) {
			System.out.println(((Document)doc.get(keys.next())).toJson());
		}*/
		logger.info("Document in JSON format: " + doc.toJson());
	}

	/*private AggregationOperation getOperation(JSONObject config) {
		AggregationOperation operation = null;
		return operation;
	}*/
	@Override
	public FunctionExtension clone() {
		MongodbAggregation aggr = new MongodbAggregation(this.stages, this.dataParameters,this.outputField);
		return aggr;
	}

	@Override
	public String getParameterName(int arg0) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	private String replaceParameters(Object stageObj) {
		Pattern p = Pattern.compile("\\?[#0-9a-zA-Z]+");
		String jsonQuery;
		if(stageObj instanceof String) {
			jsonQuery = (String)stageObj;
		}else {
			jsonQuery = ((JSONObject)stageObj).toJSONString();
		}
		Matcher m = p.matcher(jsonQuery);
		int group = 0;
		while(m.find()) {
			String param = m.group(group);
			jsonQuery = jsonQuery.replaceAll("\\" + param, this.dataParameters.get(param.substring(1)).toString());
		}
		return jsonQuery;
	}
	@Override
	public void setParameterValue(int paramNumber, double hash) {
		if(paramNumber == 0) {
			this.formula = valueFormulas.get(hash);
			if(this.formula != null) {
				this.stages = this.formula.getStages();
				this.outputField = this.formula.getOutputField();
				this.dataParameters = this.formula.getDataParameters();
				Pattern p = Pattern.compile("\\?[#0-9a-zA-Z]+");
				Iterator<JSONObject> stagesIter = stages.iterator();
				while(stagesIter.hasNext())
				{
					JSONParser parser = new JSONParser();
					Object stageObj = stagesIter.next();
					String jsonQuery = this.replaceParameters(stageObj);
					try
					{
						JSONObject obj = (JSONObject) parser.parse(jsonQuery);
						Iterator<String> keys = obj.keySet().iterator();
						Document doc = new Document();
						while(keys.hasNext()) {
							String key = keys.next();
							doc.put(key, obj.get(key));
						}
						stagesWithParams.add(doc);
					}catch(ParseException pe) {
						logger.error("Error while parsing inside setParameterValue method: " + pe.getMessage(), pe);
					}
				}
			}
		}
	}

}
