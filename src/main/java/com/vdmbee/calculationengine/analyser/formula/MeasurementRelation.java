package com.vdmbee.calculationengine.analyser.formula;


public class MeasurementRelation extends MeasureRelation{

	MeasurementRelation(){
		this.key = "measurementRelation";
		this.escKey = this.key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
	}

}
