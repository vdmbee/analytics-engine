package com.vdmbee.calculationengine.analyser.formula;

import java.math.BigDecimal;

import org.mariuszgromada.math.mxparser.FunctionExtension;


public class ToFixed implements FunctionExtension {
	private double value;
	private int precision;
	public ToFixed(double value,int precision) {
		this.value = value;
		this.precision = precision;
	}
	public ToFixed() {
	}
	@Override
	public double calculate() {
		if(Double.isNaN(this.value) || Double.isInfinite(this.value) || Double.MAX_VALUE == this.value){
			return Double.MAX_VALUE;
		}
		BigDecimal numberBigDecimal = BigDecimal.valueOf(this.value);
		return numberBigDecimal.setScale(this.precision, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

	@Override
	public FunctionExtension clone() {

		return new ToFixed(this.value,this.precision);
	}

	@Override
	public String getParameterName(int paramNumber) {
		switch(paramNumber) {
		case 0:
			return "value";
		default:
			return "precision";
		}
	}

	@Override
	public int getParametersNumber() {
		return 2;
	}

	@Override
	public void setParameterValue(int paramNumber, double value) {
		switch(paramNumber) {
		case 0:
			this.value = value;
			break;
		case 1:
			this.precision = (int) value;
			break;
		}	
	}

}
