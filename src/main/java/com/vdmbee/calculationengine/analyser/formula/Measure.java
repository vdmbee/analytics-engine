package com.vdmbee.calculationengine.analyser.formula;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import org.mariuszgromada.math.mxparser.FunctionExtensionVariadic;
import org.mariuszgromada.math.mxparser.PrimitiveElement;

public class Measure implements FunctionExtensionVariadic{
	protected String key;
	protected String escKey;
	private List<MeasureRelation> mesRelations;
	
	public enum Accumulator{
		SUM(0),MAX(1),MIN(2),AVG(3),PRODUCT(4),STANDARD_DEVIATION(5),WEIGHTED_AVERAGE(6);
		private int val;
		private Accumulator(int val) {
			this.val = val;
		}
		public int getValue() {
			return this.val;
		}
		public static Accumulator getInstance(int val) {
			switch(val) {
			case 0:
				return Accumulator.SUM;
			case 1:
				return Accumulator.MAX;
			case 2:
				return Accumulator.MIN;
			case 3:
				return Accumulator.AVG;
			case 4:
				return Accumulator.PRODUCT;
			case 5:
				return Accumulator.STANDARD_DEVIATION;
			default:
				return Accumulator.WEIGHTED_AVERAGE;
			}
		}
	};
	private Accumulator accumulator;
	private double[] relationValues;
	private double[] weights;
	
	public Measure() {
		this.key = "measure";
		this.escKey = "measure";
	}
	
	public double[] getWeights() {
		return weights;
	}

	public void setWeights(double[] weights) {
		this.weights = weights;
	}

	public Measure(String key, List<MeasureRelation> mesRelations,Accumulator accumulator){
		this.key = key;
		this.escKey = key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		this.mesRelations = mesRelations;
		this.accumulator = accumulator;
	}

	public Measure(Accumulator accumulator,double[]  relationValues){
		//this.key = key;
		this.escKey = key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		this.relationValues = relationValues;
		this.accumulator = accumulator;
	}
	
	/*public Expression getExpression() {
		Expression exp = new Expression(this.escKey + "()",this.getFunction() );
		return exp;
	}*/
	public Expression getExpression(List<Double> baseValues) {
		StringBuffer funStr = new StringBuffer();
		if(this.accumulator == Accumulator.SUM) {
			funStr.append("add(");
		}else if(this.accumulator == Accumulator.MAX) {
			funStr.append("max(");
		}else if(this.accumulator == Accumulator.MIN) {
			funStr.append("min(");
		}else if(this.accumulator == Accumulator.AVG) {
			funStr.append("mean(");
		}else if(this.accumulator == Accumulator.PRODUCT) {
			funStr.append("multi(");
		}else if(this.accumulator == Accumulator.STANDARD_DEVIATION) {
			funStr.append("std(");
		}else {
			funStr.append("(");
		}
		List<PrimitiveElement> funs = new ArrayList<>();
		double totalWeights = 0.0;
		for(int i=0;i<mesRelations.size();i++) {
			if(i>0) {
				if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
					funStr.append(" + ");
				}else {
					funStr.append(",");	
				}
			}
			funStr.append(mesRelations.get(i).getEscKey() + "(" + baseValues.get(i) + ")");
			if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
				funStr.append(" * " + this.weights[i] + " "  );
				totalWeights = totalWeights + this.weights[i];
			}
			funs.add(mesRelations.get(i).getFunction());
		}
		funStr.append(")");
		
		if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
			funStr.append(" / " + totalWeights);
		}
		PrimitiveElement funObjs[] = new PrimitiveElement[funs.size()];
		for(int i=0;i<mesRelations.size();i++) {
			funObjs[i] = mesRelations.get(i).getFunction();
		}
		return new Expression(funStr.toString(),funObjs);
	}
	
	public Expression getExpression() {
		StringBuffer funStr = new StringBuffer();
		if(this.accumulator == Accumulator.SUM) {
			funStr.append("add(");
		}else if(this.accumulator == Accumulator.MAX) {
			funStr.append("max(");
		}else if(this.accumulator == Accumulator.MIN) {
			funStr.append("min(");
		}else if(this.accumulator == Accumulator.AVG) {
			funStr.append("mean(");
		}else if(this.accumulator == Accumulator.PRODUCT) {
			funStr.append("multi(");
		}else if(this.accumulator == Accumulator.STANDARD_DEVIATION) {
			funStr.append("std(");
		}else {
			funStr.append("(");
		}
		List<PrimitiveElement> funs = new ArrayList<>();
		double totalWeights = 0.0;
		for(int i=0;i<relationValues.length;i++) {
			if(i>0) {
				if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
					funStr.append(" + ");
				}else {
					funStr.append(",");	
				}
			}
			funStr.append(this.relationValues[i]);
			if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
				funStr.append(" * " + this.weights[i] + " "  );
				totalWeights = totalWeights + this.weights[i];
			}
		}
		funStr.append(")");
		if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
			funStr.append(" / " + totalWeights);
		}
		return new Expression(funStr.toString());
	}
	

	public double[] getRelationValues() {
		return this.relationValues;
	}

	public void setRelationValues(double[] relationValues) {
		this.relationValues = relationValues;
	}

	//For relation values already calculated
	public Double calculate() {
		return this.getExpression().calculate();
	}
	
	//For base measure values 
	public Double calculate(List<Double> baseValues) {
		return this.getExpression(baseValues).calculate();
	}

	/*public Function getFunction() {
		StringBuffer funStr = new StringBuffer();
		funStr.append(this.escKey + "(");
		for(int i=0;i<mesRelations.size();i++) {
			if(i>0) {
				funStr.append(",");
			}
			funStr.append("x" + i);
			
		}
		funStr.append(") = ");
		if(this.accumulator == Accumulator.SUM) {
			funStr.append("add(");
		}else if(this.accumulator == Accumulator.MAX) {
			funStr.append("max(");
		}else if(this.accumulator == Accumulator.MIN) {
			funStr.append("min(");
		}else if(this.accumulator == Accumulator.AVG) {
			funStr.append("mean(");
		}else if(this.accumulator == Accumulator.PRODUCT) {
			funStr.append("multi(");
		}
		List<PrimitiveElement> funs = new ArrayList<>();
		
		for(int i=0;i<mesRelations.size();i++) {
			if(i>0) {
				funStr.append(",");
			}
			funStr.append(mesRelations.get(i).getEscKey() + "(x" + i + ")");
			funs.add(mesRelations.get(i).getFunction());
		}
		funStr.append(")");
		
		PrimitiveElement funObjs[] = new PrimitiveElement[funs.size()];
		for(int i=0;i<mesRelations.size();i++) {
			funObjs[i] = mesRelations.get(i).getFunction();
		}
		return new Function(funStr.toString(),funObjs);
	}*/
	
	public Function getFunction() {
		StringBuffer funStr = new StringBuffer();
		funStr.append(this.escKey + "(");
		for(int i=0;i<mesRelations.size();i++) {
			if(i>0) {
				funStr.append(",");
			}
			funStr.append("x" + i);
			
		}
		funStr.append(") = ");
		if(this.accumulator == Accumulator.SUM) {
			funStr.append("add(");
		}else if(this.accumulator == Accumulator.MAX) {
			funStr.append("max(");
		}else if(this.accumulator == Accumulator.MIN) {
			funStr.append("min(");
		}else if(this.accumulator == Accumulator.AVG) {
			funStr.append("mean(");
		}else if(this.accumulator == Accumulator.PRODUCT) {
			funStr.append("multi(");
		}else if(this.accumulator == Accumulator.STANDARD_DEVIATION) {
			funStr.append("std(");
		}else {
			funStr.append("(");
		}
		double totalWeights = 0.0;
		for(int i=0;i<mesRelations.size();i++) {
			if(i>0) {
				if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
					funStr.append(" + ");
				}else {
					funStr.append(",");	
				}
			}
			funStr.append("x" + i );
			if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
				funStr.append(" * " + this.weights[i] + " "  );
				totalWeights = totalWeights + this.weights[i];
			}		
		}
		funStr.append(")");
		if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
			funStr.append(" / " + totalWeights);
		}
		return new Function(funStr.toString());
	}
	
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getEscKey() {
		return escKey;
	}

	public void setEscKey(String escKey) {
		this.escKey = escKey;
	}

	public List<MeasureRelation> getRelations() {
		return mesRelations;
	}

	public void setRelations(List<MeasureRelation> mesRelations) {
		this.mesRelations = mesRelations;
	}

	public Accumulator getAccumulator() {
		return accumulator;
	}

	public void setAccumulator(Accumulator accumulator) {
		this.accumulator = accumulator;
	}
	@Override
	public double calculate(double... data) {
		if(data.length < 1) {
			return 0;
		}
		this.accumulator = Accumulator.getInstance(((Double)(new Double(data[0]))).intValue());
		if(this.accumulator == Accumulator.WEIGHTED_AVERAGE) {
			int relations = (data.length-1)/2;
			this.relationValues = new double[relations];
			this.weights = new double[relations];
			for(int i=1,r=0;i<data.length;i++) {
				if(i % 2 == 0) {
					this.weights[r] = data[i];
					r++;
				}else {
					this.relationValues[r] = data[i];
				}
			}
		}else {
			this.relationValues =  Arrays.copyOfRange(data,1, data.length);	
		}
		
		
		return this.getExpression().calculate();
	}
	@Override
	public FunctionExtensionVariadic clone() {
		return new Measure(this.accumulator, this.relationValues);
	}
}

