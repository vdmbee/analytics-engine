package com.vdmbee.calculationengine.analyser.formula;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.Expression;
import org.mariuszgromada.math.mxparser.Function;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;

import com.vdmbee.calculationengine.analyser.snapshot.entity.SnapshotEvent;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;

public class MongodbBasicQuery implements FunctionExtension {

	@Autowired
	private MongoTenantTemplate mongoTemplate;
	private String jsonQuery;
	private LinkedHashMap<String, String> parameters = new LinkedHashMap();
	private static Logger logger = LogManager.getLogger(MongodbBasicQuery.class);
	private String valueField;
	private String formula;
	private Function function;
	private JSONObject dataParameters;
	private List<String> sortdFields;
	private Sort.Direction direction;
	
	private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination 

	public MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}
	public void setMongoTemplate(MongoTenantTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}
	public LinkedHashMap<String, String> getParameters() {
		return parameters;
	}
	public void setParameters(LinkedHashMap<String, String> parameters) {
		this.parameters = parameters;
	}
	public Function getFunction() {
		return function;
	}
	public void setFunction(Function function) {
		this.function = function;
	}
	public JSONObject getDataParameters() {
		return dataParameters;
	}
	public void setDataParameters(JSONObject dataParameters) {
		this.dataParameters = dataParameters;
	}
	public List<String> getSortdFields() {
		return sortdFields;
	}
	public void setSortdFields(List<String> sortdFields) {
		this.sortdFields = sortdFields;
	}
	public Sort.Direction getDirection() {
		return direction;
	}
	public void setDirection(Sort.Direction direction) {
		this.direction = direction;
	}
	public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		MongodbBasicQuery.valueFormulas = valueFormulas;
	}
	public MongodbBasicQuery() {
	}
	public MongodbBasicQuery(String jsonQuery,String valueField,List<String> sortFields,Sort.Direction direction, String formula,JSONObject dataParameters) {
		this.jsonQuery = jsonQuery;
		this.valueField = valueField;
		this.formula = formula;
		this.dataParameters = dataParameters;
		this.sortdFields = sortFields;
		this.direction = direction;
		
		/*Document queryDoc = Document.parse(jsonQuery);
		Set<String> parms = queryDoc.keySet();
		Iterator<String> parmsIter = parms.iterator();
		while(parmsIter.hasNext()) {
			System.out.println(parmsIter.next());	
		}*/
		
		
		Pattern p = Pattern.compile("\\?[#0-9a-zA-Z]+");
		Matcher m = p.matcher(jsonQuery);
		int group = 0;
		while(m.find()) {
			String param = m.group(group);
			if(!this.parameters.containsKey(param)) {
				this.parameters.put(param, "");	
			}
		}
	}
	
	@Override
	public double calculate() {
		String collection = null;
		Iterator<String> params = this.parameters.keySet().iterator();
		while(params.hasNext()){
			String param = params.next();
			this.jsonQuery = this.jsonQuery.replaceAll("\\" + param, this.dataParameters.get(param.substring(1)).toString());
		}
		BasicQuery query =null;
		try {
			query = new BasicQuery(this.jsonQuery);
		}catch(Exception e) {
			//e.printStackTrace();
			logger.error("Error while performing query inside calculate method: " + e.getMessage(), e);
			logger.error(e.getMessage(), e);
		}
		if(mongoTemplate == null) {
			mongoTemplate = BeanLoaderUtil.getBean(MongoTenantTemplate.class);
		}
		if(query != null && this.sortdFields != null && this.sortdFields.size() > 0) {
			query.with(Sort.by(this.direction,(String[]) this.sortdFields.toArray(new String[0])));
		}
		if(this.dataParameters != null) {
			collection = (String)this.dataParameters.get("collection");
			if(collection == null) {
				collection = "event";
			}
		}
		Iterator<SnapshotEvent> events = mongoTemplate.find(query, SnapshotEvent.class,collection).iterator();
		ArrayList values = new ArrayList<Double>();
		while(events.hasNext()) {
			SnapshotEvent event = events.next();
			Double val = Double.parseDouble(event.getData().get(this.valueField).toString());
			values.add(val);
		}
		if(this.formula == null || this.formula.equals("") && values.size() == 1) {
			return (Double)values.get(0);
		}
		this.function = new Function(this.getFunctionStr(values));
		Expression exp = new Expression(this.getExpressionStr(values),this.function);
		return exp.calculate();
	}
	
	private String getExpressionStr(ArrayList values) {
		StringBuffer expStr = new StringBuffer();
		expStr.append("f1").append("(");
		boolean firstParam = true;
		for(int i=0;i<values.size();i++){
			if(!firstParam) {
				expStr.append(",");
			}else {
				firstParam = false;
			}
			expStr.append(values.get(i));
		}
		expStr.append(")");
		return expStr.toString();
	}
	private String getFunctionStr(ArrayList values) {
		StringBuffer functionStr = new StringBuffer();
		functionStr.append("f1").append("(");
		int size = values.size();
		boolean firstParam = true;
		for(int i=0;i<size;i++) {
			if(!firstParam) {
				functionStr.append(",");
			}else {
				firstParam = false;
			}
			functionStr.append("v" + (i + 1));
		}
		functionStr.append(") = ");
		functionStr.append(this.formula).append("(");
		firstParam = true;
		for(int j = 0;j<size;j++) {
			if(!firstParam) {
				functionStr.append(",");
			}else {
				firstParam = false;
			}
			functionStr.append("v" + (j + 1));
		}
		functionStr.append(")");
		return functionStr.toString();
	}
	
	@Override
	public FunctionExtension clone() {
		MongodbBasicQuery handler = new MongodbBasicQuery(this.getJsonQuery(),this.getValueField(),this.sortdFields,this.direction,this.getFormula(),this.dataParameters);
		handler.setFormula(this.getFormula());
		handler.setJsonQuery(this.getJsonQuery());
		handler.setValueField(this.getValueField());
		handler.parameters = new LinkedHashMap<>(this.parameters);
		return handler;
	}

	public String getJsonQuery() {
		return jsonQuery;
	}

	public void setJsonQuery(String jsonQuery) {
		this.jsonQuery = jsonQuery;
	}

	public String getValueField() {
		return valueField;
	}

	public void setValueField(String valueField) {
		this.valueField = valueField;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	@Override
	public String getParameterName(int pos) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	@Override
	public void setParameterValue(int paramNumber, double hash) {
		if(paramNumber == 0) {
			ValueFormula formula = valueFormulas.get(hash);
			if(formula != null) {
				this.jsonQuery = formula.getMongoJsonQuery();
				this.valueField = formula.getMongoValueField();
				this.formula = formula.getMongoFormula();
				this.sortdFields = formula.getSortFields();
				this.direction = formula.getDirection();
				this.dataParameters = formula.getDataParameters();
				Pattern p = Pattern.compile("\\?[#0-9a-zA-Z]+");
				Matcher m = p.matcher(jsonQuery);
				int group = 0;
				while(m.find()) {
					String param = m.group(group);
					if(!this.parameters.containsKey(param)) {
						this.parameters.put(param, "");	
					}
				}
			}
		}
	}

}
