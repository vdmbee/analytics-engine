package com.vdmbee.calculationengine.analyser.formula;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;

/**
 * can not evaluate this as parameters for this are String values.. 
 * @author mudig
 *
 */
//@Component//unable to remove this
public class SnapshotValue implements FunctionExtension {
	@Autowired
	private SnapshotRepository snapshotRepository;
	
	
    private String snapshotId;

	private String tenantId;

	private String context;
	private String period;
    private String unit;
    
    private String value;
    
    private Snapshot snapshot;
    private String currentSnapshotId;
    
    private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination
    
    public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		SnapshotValue.valueFormulas = valueFormulas;
	}
	public SnapshotValue() {
		
	}
	public SnapshotValue(String snapshotId, String value) {
    	this.snapshotId = snapshotId;
    	this.value = value;
    }
    public SnapshotValue(String tenantId,String context,String period,String unit, String value) {
    	this.tenantId = tenantId;
    	this.context = context;
    	this.period = period;
    	this.unit = unit;
    	this.value = value;
    }

    public void init() {
    	if(this.snapshotRepository == null) {
    		this.snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
    	}
    	if(this.snapshotId != null && !this.snapshotId.equals("")) {
    		Snapshot existedSnapshotObj = snapshot.getSnapshotsCache(this.currentSnapshotId, this.snapshotId);
    		if(existedSnapshotObj == null) {
    			Optional<Snapshot> snapshotObj = snapshotRepository.findById(this.snapshotId);
	    		if(snapshotObj.isPresent()) {
	    			this.snapshot = snapshotObj.get();
	    			snapshot.putSnapshotsCache(this.currentSnapshotId, this.snapshotId, this.snapshot);
	    		}
    		}else {
    			this.snapshot = existedSnapshotObj;
    		}
    		return;
    	}else if(this.context != null && this.tenantId != null){
    		if(this.unit != null && this.period != null) {
    			this.snapshot = this.snapshotRepository.findOneByTenantIdAndContextAndUnitAndPeriod(this.tenantId, this.context, this.unit, this.period);	
    		}else {
    			this.snapshot = this.snapshotRepository.findOneByTenantIdAndContext(this.tenantId, this.context);
    		}
    	}
    }
    private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
    	for(int i=0;i<nodes.size();i++) {
    		JSONObject node = nodes.get(i);
    		if(node.get("id").equals(id)) {
    			return node;
    		}
    	}
    	return null;
    }
    
    @Override
	public double calculate() {
    	
		if(this.snapshot != null) {
			List<JSONObject> content = this.snapshot.getFileContent();
			JSONObject valueObj = new JSONObject((Map)getNodeWithId(content,this.value));
			String val = valueObj.get("Value").toString();
			if(val != null) {
				return Double.parseDouble(val);
			}
		}
		return 0;
	}

	@Override
	public FunctionExtension clone() {
		if(this.snapshotId != null) {
			return new SnapshotValue(snapshotId, value);
		}else {
			return new SnapshotValue(tenantId, context, period, unit, value);	
		}
	}

	@Override
	public String getParameterName(int arg0) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	@Override
	public void setParameterValue(int paramNumber, double hash) {
		// TODO Auto-generated method stub
		if(paramNumber == 0) {
			ValueFormula formula = valueFormulas.get(hash);
			if(formula != null) {
				this.currentSnapshotId = formula.getCurrentSnapshotId();
				this.snapshotId = formula.getSnapshotId();
				this.tenantId = formula.getSnapshotTenantId();
				this.context = formula.getSnapshotContext();
				this.value = formula.getSnapshotValue();
				this.period = formula.getSnapshotPeriod();
				this.unit = formula.getSnapshotUnit();
				JSONObject dataParameters = formula.getDataParameters();
				this.currentSnapshotId = formula.getCurrentSnapshotId();
				if(dataParameters != null && dataParameters.get("currentSnapshotId") != null) {
					this.currentSnapshotId = (String)dataParameters.get("currentSnapshotId");
				}
				this.init();
			}
		}
	}
	public String getCurrentSnapshotId() {
		return currentSnapshotId;
	}
	public void setCurrentSnapshotId(String currentSnapshotId) {
		this.currentSnapshotId = currentSnapshotId;
	}

}
