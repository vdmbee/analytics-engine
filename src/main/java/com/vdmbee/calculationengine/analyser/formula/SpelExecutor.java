package com.vdmbee.calculationengine.analyser.formula;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.mariuszgromada.math.mxparser.FunctionExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import com.vdmbee.calculationengine.analyser.snapshot.entity.Snapshot;
import com.vdmbee.calculationengine.analyser.snapshot.repository.SnapshotRepository;
import com.vdmbee.calculationengine.analyser.snapshot.service.SnapshotService;
import com.vdmbee.calculationengine.analyser.util.BeanLoaderUtil;
import com.vdmbee.calculationengine.expression.providers.SpelScriptExecutor;
import com.vdmbee.calculationengine.multitenant.MongoTenantTemplate;
import com.vdmbee.calculationengine.util.SnapshotBeanUtil;

public class SpelExecutor implements FunctionExtension {
	private static ConcurrentHashMap<Double, ValueFormula> valueFormulas;	//To store parameter data for expression combination 

	@Autowired
	private SnapshotRepository snapshotRepository;

	@Autowired
	private SnapshotService snapshotService;

	@Autowired
	MongoTenantTemplate mongoTemplate;
	private static Logger logger = LogManager.getLogger(SpelExecutor.class);


	private List<String> snapshotIds;

	private String context;
	private String period;
	private String unit;

	private String value;

	private List<JSONObject> tags;
	private String SpelScript;
	private JSONObject dataParameters;
	HashMap<String, List<JSONObject>> snapshots = new HashMap<String, List<JSONObject>>();

	public static ConcurrentHashMap<Double, ValueFormula> getValueFormulas() {
		return valueFormulas;
	}
	public static void setValueFormulas(ConcurrentHashMap<Double, ValueFormula> valueFormulas) {
		SpelExecutor.valueFormulas = valueFormulas;
	}

	public SpelExecutor() {

	}
	public SpelExecutor(List<String> snapshotIds, String value,String SpelScript) {
		this.snapshotIds = snapshotIds;
		this.value = value;
		this.SpelScript = SpelScript;
	}
	public SpelExecutor(String context,String period,String unit, String value,String SpelScript) {
		this.context = context;
		this.period = period;
		this.unit = unit;
		this.value = value;
		this.SpelScript = SpelScript;
	}

	private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
		for(int i=0;i<nodes.size();i++) {
			JSONObject node = nodes.get(i);
			if(node.get("id").equals(id)) {
				return node;
			}
		}
		return null;
	}

	@Override
	public double calculate() {
		try {
			SpelScriptExecutor executor = this.getExecutor();

			if(this.snapshotRepository == null) {
				this.snapshotRepository = BeanLoaderUtil.getBean(SnapshotRepository.class);
			}
			ArrayList<Double> values = new ArrayList<>();
			if(this.snapshotIds != null) {
				for(int i=0;i<this.snapshotIds.size();i++) {
					Optional<Snapshot> snapshotObj = this.snapshotRepository.findById(this.snapshotIds.get(i));
					Snapshot snapshot = null;
					if(snapshotObj.isPresent()) {
						snapshot = snapshotObj.get();
						List<JSONObject> content = snapshot.getFileContent();
						this.snapshots.put(snapshot.getId(), content);
					}
				}
			}else {
				List<Snapshot> snapshots = null;
				if(this.tags != null && this.tags.size() > 0) {
					if(this.snapshotService == null) {
						this.snapshotService = BeanLoaderUtil.getBean(SnapshotService.class);
					}
					snapshots = this.snapshotService.getSnapshotsWithTags(this.tags);
				}else if(this.context != null && this.unit != null && this.period != null) {
					snapshots = this.snapshotRepository.findByContextAndUnitAndPeriod(this.context,this.unit, this.period);
				}else if(this.context != null && this.unit != null && !this.unit.equals("null")){
					snapshots = this.snapshotRepository.findByContextAndUnit(this.context, this.unit);
				}else if(this.context == null && unit != null) {
					snapshots = this.snapshotRepository.findByUnit(this.unit);
				}else if(this.context != null ) {
					snapshots = this.snapshotRepository.findByContext(this.context);
					//snapshots = this.findByContext(this.context);
				}
				if(snapshots != null) {
					Iterator<Snapshot>  snapshotIter = snapshots.iterator();
					while(snapshotIter.hasNext()) {
						Snapshot snapshot = snapshotIter.next();
						List<JSONObject> content = snapshot.getFileContent();
						if(content == null) {
							continue;
						}
						this.snapshots.put(snapshot.getId(), content);
					}
				}
			}
			executor.setDataSets(this.snapshots, this.value, null, this.snapshots.size(), this.snapshots.size());
			if(this.dataParameters != null) {
				Iterator<String> keys = this.dataParameters.keySet().iterator();
				while(keys.hasNext()) {
					String key = keys.next();
					executor.setNamedValue(key, this.dataParameters.get(key));
				}
			}
			//this.SpelScript = "values = datasets.values() \n" + 
			//		"returnValue = mean(map(lambda x: x['Value'], values)) ";
			//this.SpelScript = "values = datasets.values()";
			//executor.execute(this.SpelScript);
			//this.SpelScript = "returnValue = mean(map(lambda x: x['Value'], values)) ";
			//this.SpelScript = this.SpelScript.replaceAll("\\\\n", "\\n");
			//executor.execute("values = datasets.values() \nreturnValue = mean(map(lambda x: x['Value'], values))");
			return (Double)executor.execute(this.SpelScript);
		}catch(Exception e) {
			logger.error("Error while performing calculate method: " + e.getMessage(), e);
			//throw new RuntimeException(e.getMessage());
			//e.printStackTrace();
			return 0.0;
		}
	}

	private List<Snapshot> findByContext(String context){
		Query contextQuery = new Query();
		Criteria criteria = Criteria.where("context").is(context);
		contextQuery.addCriteria(criteria);
		if(mongoTemplate == null) {
			mongoTemplate = SnapshotBeanUtil.getBean(MongoTenantTemplate.class);
		}
		List<Snapshot> ret = mongoTemplate.find(contextQuery, Snapshot.class,"Snapshot");
		return ret;
	}
	private SpelScriptExecutor getExecutor() throws Exception {
		SpelScriptExecutor ex = new SpelScriptExecutor();
		if(!ex.isInitialized()) {
			ex.init();
		}
		return ex;
	}
	@Override
	public FunctionExtension clone() {
		if(this.snapshotIds != null) {
			return new SpelExecutor(snapshotIds, value,SpelScript);
		}else {
			return new SpelExecutor(context,period, unit, value,SpelScript);
		}
	}

	@Override
	public String getParameterName(int arg0) {
		return "valueFormulaHash";
	}

	@Override
	public int getParametersNumber() {
		return 1;
	}

	@Override
	public void setParameterValue(int paramNumber, double hash) {
		if(paramNumber == 0) {
			ValueFormula formula = valueFormulas.get(hash);
			if(formula != null) {
				this.snapshotIds = formula.getSnapshotIdsForExpression();
				this.context = formula.getSnapshotContext();
				this.value = formula.getSnapshotValue();
				this.period = formula.getSnapshotPeriod();
				this.unit = formula.getSnapshotUnit();
				this.tags = formula.getTags();
				this.SpelScript = formula.getCustomScript();
				this.dataParameters = formula.getDataParameters();
			}
		}
	}

}
