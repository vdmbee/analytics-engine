package com.vdmbee.calculationengine.analyser.formula;
import org.mariuszgromada.math.mxparser.Function;
import org.mariuszgromada.math.mxparser.FunctionExtension;


public class MeasureRelation implements FunctionExtension{

	protected String key;
	protected String escKey;
	private Double multiplier = 1.0;
	private Double offset = 0.0;
	private Double val = Double.NaN;
		
	public enum Operation {
		RECIPROCAL(0),SQUARE(1),SQUAREROOT(2),NOOPERATION(3);
		private final int value;
		private Operation(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		public static Operation getInstance(int val) {
			switch(val) {
			case 0:
				return Operation.RECIPROCAL;
			case 1:
				return Operation.SQUARE;
			case 2:
				return Operation.SQUAREROOT;
			default:
				return Operation.NOOPERATION;
			}
		}
	};
	private Operation operation = Operation.NOOPERATION;

	MeasureRelation(){
		this.key = "measureRelation";
		this.escKey = this.key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
	}
	MeasureRelation(Double multiplier,Double offset, Operation operation){
		this.key = "measureRelation";
		this.escKey = this.key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		this.multiplier = multiplier;
		this.offset = offset;
		this.operation = operation;
	}

	MeasureRelation(Double multiplier,Double offset, Operation operation,Double val){
		this.key = "measureRelation";
		this.escKey = this.key.replaceAll("[@-]", "_");
		this.escKey = this.escKey.replaceAll("[#]", "_");
		this.multiplier = multiplier;
		this.offset = offset;
		this.operation = operation;
		this.val = val;
	}
	
	public String getExpressionValue() {
		return this.escKey + "(" + Double.valueOf(this.multiplier.toString()) + "," + Double.valueOf(this.offset.toString()) + "," + Double.valueOf(this.operation.getValue()) +"," + this.val +  ")";
	}
	public Function getFunction() {
		if(this.operation == Operation.NOOPERATION) {
			return new Function(this.escKey + "(x) = " + this.multiplier + " * x + (" + this.offset  + ")");
		}else if(this.operation == Operation.RECIPROCAL) {
			return new Function(this.escKey + "(x) = 1/(" + this.multiplier + " * x + (" + this.offset  + "))");
		}else if(this.operation == Operation.SQUARE) {
			return new Function(this.escKey + "(x) = (" + this.multiplier + " * x + (" + this.offset  + "))^2");
		}else {
			return new Function(this.escKey + "(x) = sqrt(" + this.multiplier + " * x + (" + this.offset  + "))");
		}
	}

	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getEscKey() {
		return escKey;
	}
	public void setEscKey(String escKey) {
		this.escKey = escKey;
	}
	public Double getMultiplier() {
		return multiplier;
	}
	public void setMultiplier(Double multiplier) {
		this.multiplier = multiplier;
	}
	public Double getOffset() {
		return offset;
	}
	public void setOffset(Double offset) {
		this.offset = offset;
	}
	public Double getVal() {
		return val;
	}
	public void setVal(Double val) {
		this.val = val;
	}
	public Operation getOperation() {
		return operation;
	}
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	public double calculate(double val) {
		return this.getFunction().calculate(val );
	}
	public double calculate() {
		return this.getFunction().calculate(this.val);
	}
	public FunctionExtension clone() {
		return new MeasureRelation(this.multiplier, this.offset, this.operation,this.val);
	}
	@Override
	public String getParameterName(int paramNumber) {
		switch(paramNumber) {
		case 0:
			return "multiplier";
		case 1:
			return "offset";
		default:
			return "operation";

		}
	}
	@Override
	public int getParametersNumber() {
		return 4;
	}
	@Override
	public void setParameterValue(int paramNumber, double val) {
		switch(paramNumber) {
		case 0:
			this.multiplier = val;
			break;
		case 1:
			this.offset = val;
			break;
		case 2:
			this.operation = Operation.getInstance(((Double)(new Double(val))).intValue());
		default:
			this.val = val;

		}	
		
	}
}
