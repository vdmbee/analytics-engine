package com.vdmbee.calculationengine.util;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="vmp.python")
public class PythonConfig {

	private String executorPools;
	


	public String getExecutorPools() {
		return executorPools;
	}

	public void setExecutorPools(String executorPools) {
		this.executorPools = executorPools;
	}

	
}
