package com.vdmbee.calculationengine.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

public class Timming {

	private static Boolean set = false;

	@Value("${print.statistics}")
	public void setBool(Boolean val){
		set = val;
	}
	private static Logger logger = LogManager.getLogger(Timming.class);

	static long numberOfUploads = 0;
	static long totalUpdateTime = 0;
	//static long numberOfUpdates = 0;
	static long totalReadTime = 0;
	//static long numberOfReads = 0;
	static long totalPythonExecution = 0;
	//static long numberOfPythonExecutions = 0;
	static long totalRequestHandlingTime = 0;
	static long pythonInitialisationTime = 0;
	static long pythonInitialisationCount = 0;
	static long pythonResetTime = 0;
	static long pythonResetCount = 0;
	static long pythonDestroyCount = 0;
	static long dataSetCount = 0;
	
	static Boolean isBenchmark = false;
	
	public static synchronized void addUpdateTime(long time) {
		totalUpdateTime = totalUpdateTime + time;
	}
	
	public static synchronized void addReadTime(long time) {
		totalReadTime = totalReadTime + time;
	}
	
	public static synchronized void addPythonTime(long time) {
		totalPythonExecution = totalPythonExecution + time;
	}
	
	public static synchronized void addPythonExecutorInitTime(long time) {
		pythonInitialisationCount++;
		pythonInitialisationTime = pythonInitialisationTime + time;
	}
	
	public static synchronized void addPythonExecutorResetTime(long time) {
		pythonResetCount++;
		pythonResetTime = pythonResetTime + time;
	}
	
	public static synchronized void addHandlingTime(long time) {
		totalRequestHandlingTime = totalRequestHandlingTime + time;
	}
	
	public static synchronized void addPythonDestroy() {
		pythonDestroyCount++;
	}
	
	public static synchronized void addDatasetCount(int size) {
		dataSetCount = dataSetCount + size;
	}
	
	public static synchronized void incrementUploads() {
		numberOfUploads++;
		if(numberOfUploads % 100 == 0) {
			printStatistics(null);
		}
	}
	
	public static synchronized void printStatistics(String header) {
			//numberOfUploads++;
		if(set){
			if(header != null) {
				logger.error("header:"+header);
				if(numberOfUploads == 0) {
					numberOfUploads = 1;
				}
				logger.error("dataSetCount for Benchmark:"+dataSetCount);
			}
			logger.error("Number Uploads:"+numberOfUploads);
			logger.error("Average python Time:"+totalPythonExecution/numberOfUploads);
			logger.error("Average read Time:"+totalReadTime/numberOfUploads);
			logger.error("Average update Time:"+totalUpdateTime/numberOfUploads);
			logger.error("Average Handling Time:"+totalRequestHandlingTime/numberOfUploads);
			logger.error("Total Handling Time:"+totalRequestHandlingTime);
			logger.error("Total read Time:"+totalReadTime);
			logger.error("Total update Time:"+totalUpdateTime);
			logger.error("Total Python:"+totalPythonExecution);
			logger.error("Total Python Init executorCount:"+pythonInitialisationCount);
			logger.error("Total Python Init executorTime:"+pythonInitialisationTime);
			logger.error("Total Python Reset executorCount:"+pythonResetCount);
			logger.error("Total Python Reset executorTime:"+pythonResetTime);
			logger.error("Total Python Destroy executorCount:"+pythonDestroyCount);
		}
	}
}
