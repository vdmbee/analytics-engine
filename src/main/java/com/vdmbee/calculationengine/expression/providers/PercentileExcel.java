package com.vdmbee.calculationengine.expression.providers;

import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.commons.math3.stat.ranking.NaNStrategy;
import org.apache.commons.math3.util.KthSelector;
import org.apache.commons.math3.util.MedianOf3PivotingStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PercentileExcel extends Percentile {

	private static final Logger LOGGER = LoggerFactory.getLogger(PercentileExcel.class);

	public PercentileExcel() throws MathIllegalArgumentException {

		super(50.0,
				EstimationType.R_7, // use excel style interpolation
				NaNStrategy.REMOVED,
				new KthSelector(new MedianOf3PivotingStrategy()));
	}
	public static double percentileInc(double[] values, double percentage) {
		PercentileExcel ep;
		try {
			ep = new PercentileExcel();
			Double val = ep.evaluate(values,percentage);
			return val;
		} catch(Exception e) {
			LOGGER.error("Values -- "+values+" percentage --"+percentage);
			LOGGER.error("Exception in percentileInc() ", e);
			throw e;
		} finally {
			ep = null;
		}
	}
	/*public static void main(String[] args) {
	   double arr[] = new double[] {15,14,18,-2,6,-78,31,21,98,-54,-2,-36,5,2,46,-72,3,-2,7,9,34};
	   System.out.println((""+percentile(arr,18)));
   }*/
}
