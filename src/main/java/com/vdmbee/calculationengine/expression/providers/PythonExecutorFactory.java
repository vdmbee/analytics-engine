package com.vdmbee.calculationengine.expression.providers;

import org.apache.commons.pool2.BasePooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;

public class PythonExecutorFactory extends BasePooledObjectFactory<PythonScriptExecutor> {

	@Override
	public PythonScriptExecutor create() throws Exception {
		return new PythonScriptExecutor();
	}

	@Override
	public PooledObject<PythonScriptExecutor> wrap(PythonScriptExecutor executor) {
		return new DefaultPooledObject<PythonScriptExecutor>(executor);
	}

    @Override
    public void destroyObject(final PooledObject<PythonScriptExecutor> executor)
        throws Exception  {
    	executor.getObject().destroy();
    }
    @Override
    public void passivateObject(PooledObject<PythonScriptExecutor> executor) throws Exception {
    	executor.getObject().reset();
    }
    @Override
    public boolean validateObject(PooledObject<PythonScriptExecutor> executor) {
    	return true;
    }
}
