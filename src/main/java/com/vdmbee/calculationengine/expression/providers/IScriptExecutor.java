package com.vdmbee.calculationengine.expression.providers;

import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.json.simple.JSONObject;

import com.vdmbee.calculationengine.analyser.formula.ValueFormula;

public interface IScriptExecutor {
	public void init();
	public void setInitProperties(Properties properties);
	public Properties getInitProperties();
	public Object execute(String script);
	public void setNamedValue(String key,Object value);
	public Object getNamedValue(String key);
	public void setDataset(String name,JSONObject dataset,boolean valid);
	public void setDataset(String name,JSONObject dataset);

	//public JSONObject getDataset(String name);
	//public void setDataSets(Map<String,List<JSONObject>> datasets,String value,ValueFormula formula,int maxSize);
	//public Map<String,JSONObject> getDataSets();
	public void reset();
	public boolean isInitialized();
	public void destroy();
	public void compile(String code);
	public void generateDebugScript(Boolean generate);
	void setDataSets(Map<String, List<JSONObject>> datasets, String value, boolean valid, ValueFormula formula,
			int maxValidValues, int maxValues);
	void setDataSets(Map<String, List<JSONObject>> datasets, String value, ValueFormula formula, int maxValidValues,
			int maxValues);
}
