package com.vdmbee.calculationengine.expression.providers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.math3.stat.StatUtils;
import org.apache.commons.math3.stat.descriptive.SummaryStatistics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;

import com.vdmbee.calculationengine.analyser.formula.ValueFormula;

public class SpelScriptExecutor implements IScriptExecutor {

	private static final Logger LOGGER = LogManager.getLogger(SpelScriptExecutor.class);
	private ExpressionParser parser;
	private StandardEvaluationContext context;

	private Map<String,JSONObject> datasets = new HashMap<>();

	@Override
	public void init() {
		try {
			this.parser = new SpelExpressionParser();
			context = new StandardEvaluationContext(this);
			context.registerFunction("geometricMean",StatUtils.class.getDeclaredMethod("geometricMean", double[].class));
			context.registerFunction("max",StatUtils.class.getDeclaredMethod("max", double[].class));
			context.registerFunction("mean",StatUtils.class.getDeclaredMethod("mean", double[].class));
			//context.registerFunction("mode",StatUtils.class.getDeclaredMethod("mode", double[].class));
			context.registerFunction("normalize",StatUtils.class.getDeclaredMethod("normalize", double[].class));
			context.registerFunction("percentile",StatUtils.class.getDeclaredMethod("percentile", double[].class,double.class));
			context.registerFunction("populationVariance",StatUtils.class.getDeclaredMethod("populationVariance", double[].class));
			context.registerFunction("product",StatUtils.class.getDeclaredMethod("product", double[].class));
			context.registerFunction("sum",StatUtils.class.getDeclaredMethod("sum", double[].class));
			context.registerFunction("sumLog",StatUtils.class.getDeclaredMethod("sumLog", double[].class));
			context.registerFunction("sumSq",StatUtils.class.getDeclaredMethod("sumSq", double[].class));
			context.registerFunction("geometricMean",StatUtils.class.getDeclaredMethod("geometricMean", double[].class));
			context.registerFunction("variance",StatUtils.class.getDeclaredMethod("variance", double[].class));

			context.registerFunction("getSummaryStatistics", SpelScriptExecutor.class.getDeclaredMethod("getSummaryStatistics", List.class));
			context.registerFunction("getValues", SpelScriptExecutor.class.getDeclaredMethod("getValues", JSONObject.class,String.class));
			context.registerFunction("hasIntersection", SpelScriptExecutor.class.getDeclaredMethod("hasIntersection", List.class,List.class));
		}catch(NoSuchMethodException nme) {
			throw new RuntimeException(nme);
		}
	}
	@Override
	public void reset() {
		this.datasets.clear();
	}

	@Override
	public boolean isInitialized() {
		return this.parser != null;
	}

	@Override
	public void setInitProperties(Properties properties) {

	}

	@Override
	public Properties getInitProperties() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object execute(String script) {
		if(!this.isInitialized()) {
			throw new RuntimeException("Script Executor not initialized");
		}
		return this.parser.parseExpression(script).getValue(this.context);
	}

	@Override
	public void setNamedValue(String key, Object value) {
		this.context.setVariable(key, value);
	}

	@Override
	public Object getNamedValue(String key) {
		return this.context.lookupVariable(key);
	}

	@Override
	public void setDataset(String name, JSONObject dataset) {
		this.datasets.put(name, dataset);
	}

	
	/*@Override
	public JSONObject getDataset(String name) {
		return this.datasets.get(name);
	}*/

	/*@Override
	public void setDataSets(Map<String, JSONObject> datasets) {
		this.datasets.clear();
		this.datasets.putAll(datasets);
	}*/

	/*@Override
	public Map<String, JSONObject> getDataSets() {
		return this.datasets;
	}*/

	public static List<Double> getValues(JSONObject dataset,String valueKey){
		List<Double> values = new ArrayList<Double>();
		Iterator<String> keys = dataset.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			values.add((Double)((JSONObject)dataset.get(key)).get(valueKey));
		}
		return values;
	}
	public List<Double> getValues(String valueKey){
		List<Double> values = new ArrayList<Double>();
		Iterator<String> keys = this.datasets.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			Object val = this.datasets.get(key);
			if(val != null) {
				values.add((Double)((JSONObject)this.datasets.get(key)).get(valueKey));
			}
		}
		return values;
	}
	public static SummaryStatistics getSummaryStatistics(List<Double> values) {
		SummaryStatistics stats = new SummaryStatistics();
		for(double value: values) {
			stats.addValue(value);
		}
		return stats;
	}

	public static Double hasIntersection(List<String> list1,List<String> list2) {
		Set<String> list1Set = new HashSet<String>(list1);
		Set<String> list2Set = new HashSet<String>(list2);
		list1Set.retainAll(list2Set);
		return new Double(list1Set.size()) ;
	}

	public static void main(String[] args) {
		SpelScriptExecutor spelExec = new SpelScriptExecutor();
		spelExec.init();
		List<String> bs = new ArrayList<>(Arrays.asList("a", "d")) ;
		spelExec.setNamedValue("bs", bs);

		//ExpressionParser parser = new SpelExpressionParser();

		//Expression exp = parser.parseExpression("new ArrayList(T(java.util.Arrays).asList('a', 'd'))");

		//Double val = (Double)spelExec.execute("#hasIntersection(new ArrayList(T(java.util.Arrays).asList('a', 'd')),#bs)");
		Object val = spelExec.execute("new ArrayList(T(java.util.Arrays).asList('a', 'd'))");
		/*System.out.print("3 > 2: " + spelExec.execute("3 > 2"));
		
        JSONObject e1 = new JSONObject();
        e1.put("Value", 100.0);
        JSONObject e2 = new JSONObject();
        e2.put("Value", 200.0);
        JSONObject e3 = new JSONObject();
        e3.put("Value", 300.0);
        JSONObject e4 = new JSONObject();
        e4.put("Value", 500.0);
        

        JSONObject dataset= new JSONObject();
        dataset.put("1", e1);
        dataset.put("2", e2);
        dataset.put("3", e3);
        dataset.put("4", e4);
        spelExec.setDataset("ds1", dataset);
        SummaryStatistics stats = (SummaryStatistics)spelExec.execute("#getSummaryStatistics(#getValues(get('ds1'),'Value'))");
        System.out.print("mean:" + stats.getMean());
        System.out.print("std:" + stats.getStandardDeviation());
        System.out.print("sum:" + spelExec.execute("#sum(#getValues(get('ds1'),'Value'))"));*/
	}
	@Override
	public void destroy() {
		this.datasets.clear();
		this.parser = null;

	}
	private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
		for(int i=0;i<nodes.size();i++) {
			JSONObject node = nodes.get(i);
			if(node.get("id").equals(id)) {
				return node;
			}
		}
		return null;
	}
	@Override
	public void setDataSets(Map<String, List<JSONObject>> datasets, String value,ValueFormula formula,int maxValidValues,int maxSize) {
		if(datasets == null) {
			return;
		}
		this.datasets.clear();
		Iterator<String> keys = datasets.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			if(value != null) {
				List<JSONObject> snapshotValues =  ((List<JSONObject>)datasets.get(key));
				try {
					JSONObject valueNode = getNodeWithId(snapshotValues,value);
					if(valueNode != null) {
						JSONParser parser = new JSONParser();
						JSONObject valueData = (JSONObject)parser.parse(valueNode.toJSONString());
						this.setDataset(key, valueData);
					}
				}catch(Exception e) {
					LOGGER.error("Error while parsing snapshot value inside setDataSets method: " + e.getMessage(), e);
				}


			}else {
				//this.setDataset(key, datasets.get(key));
				JSONObject newObj = new JSONObject();
				List<JSONObject> dataset = datasets.get(key);
				for(int i=0;i<dataset.size();i++) {
					JSONObject keyValue = new JSONObject((Map)dataset.get(i));
					if(keyValue.get("Value") != null && !keyValue.get("Value").toString().equalsIgnoreCase("NaN")) {
						keyValue.remove("ValueName");
						newObj.put(keyValue.get("id"), keyValue);
					}
				}
				this.setDataset(key, newObj);

			}

		}


	}
	@Override
	public void compile(String code) {
		// TODO Auto-generated method stub
	}
	@Override
	public void generateDebugScript(Boolean generate) {
		// TODO Auto-generated method stub

	}
	@Override
	public void setDataset(String name, JSONObject dataset, boolean valid) {
		// TODO Auto-generated method stub
		this.setDataset(name, dataset);

	}
	@Override
	public void setDataSets(Map<String, List<JSONObject>> datasets, String value, boolean valid,ValueFormula formula,int maxValidValues,int maxSize) {
		// TODO Auto-generated method stub
		this.setDataSets(datasets, value, formula, maxValidValues, maxSize);
	}
}
