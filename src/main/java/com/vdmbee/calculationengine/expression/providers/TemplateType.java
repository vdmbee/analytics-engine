package com.vdmbee.calculationengine.expression.providers;

import org.json.simple.JSONObject;
import org.python.core.PyDictionary;
import org.python.core.PyList;

import com.vdmbee.calculationengine.analyser.formula.PythonExecutor;
public interface TemplateType {
	public PyDictionary executeValueFormula(PythonExecutor executor);
	public void setDataset(String name,PyList dataset);
	public PyDictionary getDataset(String name);
	public void setInvalidDataset(String name,PyList dataset);
	public PyDictionary getInvalidDataset(String name);
	public void clearDatasets();
	public void setDatasetValue(String rubric);
	public String getDatasetValue();
	public void clearDatasetValue();
	public void setProperty(String property, Object value);
	public void setJSONProperty(String property, JSONObject value);
	public Object getProperty(String property);
	public Double getRetValue();
	public void setRetValue(Double val);
	public void cleanUp();
	public void setExecutor(PythonExecutor executor);
	public PythonExecutor getExecutor();
	public void setMinimumValue(String rubricId,Double val);
	public void setMaximumValue(String rubricId,Double val);
	public void setAverageValue(String rubricId,Double val);
	public PyList getMeasures();
	public void setMeasures(PyList measures);
	public void clearMeasures();
}
