package com.vdmbee.calculationengine.expression.providers;

import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PythonExecutorPool extends GenericObjectPool<PythonScriptExecutor> {
	
	private static final Logger logger = LogManager.getLogger(PythonExecutorPool.class);

	public PythonExecutorPool(PooledObjectFactory<PythonScriptExecutor> factory) {
		super(factory);
	}
    public PythonExecutorPool(PooledObjectFactory<PythonScriptExecutor> factory,
            GenericObjectPoolConfig config) {
        super(factory, config);
    }
    
    public static void main(String[] args) throws Exception {
    	GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxIdle(1);
        config.setMaxTotal(1);
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);
        try (PythonExecutorPool pool = new PythonExecutorPool(new PythonExecutorFactory(), config)){
        	PythonScriptExecutor executor = pool.borrowObject();
        	logger.info(executor.isInitialized());
        }        
    }
}
