package com.vdmbee.calculationengine.expression.providers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.python.core.CompilerFlags;
import org.python.core.PyCode;
import org.python.core.PyList;
import org.python.core.PyObject;
import org.python.util.PythonInterpreter;

import com.vdmbee.calculationengine.analyser.formula.ValueFormula;
import com.vdmbee.calculationengine.util.Timming;




public class PythonScriptExecutor implements IScriptExecutor {
	class RubricDataElements	{
		private double arrayOfValidValues[]=null;
		private double arrayOfInvalidValues[] = null;
		int cntValidValues, cntInvalidValues, maxValues, maxValidValues;
		boolean countEmptyAsZero,isPrepared;
		double min, max, sum, avarage;

		public RubricDataElements(boolean countEmptyAsZero, int maxValidValues, int maxValues)	{
			this.countEmptyAsZero = countEmptyAsZero;
			this.maxValidValues = maxValidValues;
			this.maxValues = maxValues;
			this.isPrepared = false;
		}

		public void addValidValue(Double value)	{
			if ( value == null || value.isNaN() )	{
				// don't do anything
				return;
			}
			if ( this.arrayOfValidValues == null)	{
				this.arrayOfValidValues = new double[this.maxValidValues];
			}

			this.arrayOfValidValues[this.cntValidValues] = value;
			this.cntValidValues += 1;
			this.sum += value;
			if ( value < this.min )	{
				this.min = value;
			} else 	if ( value > this.max )	{
				this.max = value;
			}
		}

		public void addInvalidValue(Double value)	{
			if ( value == null || value.isNaN() )	{
				// don't do anything
				return;
			}
			if ( this.arrayOfInvalidValues  == null)	{
				this.arrayOfInvalidValues = new double[this.maxValues];
			}

			this.arrayOfInvalidValues[this.cntInvalidValues] = value;
			this.cntInvalidValues += 1;
		}

		public double[] getValidValues()	{
			prepareForCalculation();
			return this.arrayOfValidValues;
		}

		public double[] getTotalValues()	{
			//prepareForCalculation(false);
			int totalValues = this.maxValues;
			int elementsToCopy = this.cntValidValues;
			this.cntInvalidValues = this.countEmptyAsZero ? totalValues - elementsToCopy : this.cntInvalidValues;
			if ( this.cntInvalidValues > 0 )	{
				// merge valid into Invalid;
				if (!this.countEmptyAsZero) {
					if (this.arrayOfInvalidValues  == null) {
						return null;
					} else {
						double[] newArrayOfInvalidValues = new double[this.cntInvalidValues + this.cntValidValues];
						for (int i = 0; i < this.cntInvalidValues; i++) {
							newArrayOfInvalidValues[i] = this.arrayOfInvalidValues[i];
						}
						this.arrayOfInvalidValues = newArrayOfInvalidValues;
					}
				}
				if ( this.arrayOfInvalidValues  == null) {
					this.arrayOfInvalidValues = new double[totalValues];
				}
				while ( elementsToCopy > 0)	{
					elementsToCopy -= 1;
					this.arrayOfInvalidValues[this.cntInvalidValues + elementsToCopy] = this.arrayOfValidValues[elementsToCopy];
				}
				if ( this.arrayOfInvalidValues.length > 1 )	{
					Arrays.sort(this.arrayOfInvalidValues, 0, this.arrayOfInvalidValues.length - 1);
				}
				return (this.arrayOfInvalidValues);
			}
			return null;
		}

		private void prepareForCalculation() {
			if ( !this.isPrepared )	{
				this.isPrepared = true;
				if ( this.arrayOfValidValues == null )	{
					/*if ( !this.countEmptyAsZero || this.cntValidValues == 0)	{

					}*/
					return;
					//this.arrayOfValidValues = new double[this.maxValues];
				}
				// add zero's to complete the array.
				int zerosToBeAdded = this.maxValidValues - (this.cntValidValues);

				if ( zerosToBeAdded > 0 && this.countEmptyAsZero )	{
					//commenting because of arrayOfValidValues intialisation
					//Arrays.fill(this.arrayOfValidValues, this.cntValidValues, this.cntValidValues + zerosToBeAdded, 0.0);
					this.cntValidValues += zerosToBeAdded;
				}

				if ( this.cntValidValues > 1 )	{
					Arrays.sort(this.arrayOfValidValues, 0, this.cntValidValues - 1);
				}
			}
		}

		public Double getMin()	{
			if ( this.cntValidValues > 0 )	{
				return (this.min);
			}
			return null;
		}

		public Double getMax()	{
			if ( this.cntValidValues > 0 )	{
				return (this.max);
			}
			return null;
		}

		public Double getAvarage()	{
			if ( this.cntValidValues > 0 )	{
				return ( this.sum / this.cntValidValues );
			}
			return null;
		}
		public void clean() {
			this.arrayOfInvalidValues = null;
			this.arrayOfValidValues = null;
		}
	}

	private static final Logger logger = LogManager.getLogger(PythonScriptExecutor.class);

	private Boolean generateDeugScript = false;

	private Properties properties;
	private PythonInterpreter interpretor;
	private static final String baseScript = "import sys \n" +
			"from statistics import stdev \n" +
			"from statistics import mean \n" +
			"from com.vdmbee.calculationengine.expression.providers.PercentileExcel import percentileInc \n" +
			"import json \n" +
			"import math \n" +
			//"import Template \n" +
			"from customFunctions import *\n"+
			"from Template import *\n";

	//interp.execfile("C:/Users/A/workspace/LeaerningPyDev/customFunctions.py");
	//private Map<String,JSONObject> datasets = new HashMap<>();
	private StringBuffer datasetsScript;
	private StringBuffer nameValueScript;
	private String execScript;
	private TemplateType template;
	@Override
	public void init() {
		Date before = new Date();
		String[] args = {};
		PythonInterpreter.initialize(System.getProperties(), properties, args);
		interpretor = new PythonInterpreter(){
			{
				cflags = new CompilerFlags(CompilerFlags.PyCF_SOURCE_IS_UTF8);
			}
		};
		interpretor.exec(baseScript);
		//interpretor.execfile("E:/pythonscript.py");
        /*String cust = "i=1+1+1+1+1+1;";
        StringBuffer ex = new StringBuffer();
        for(int i=0;i<2000;i++) {
        	ex.append(cust+"\n");
        }
        ex.append("print(i)");
        interpretor.exec(ex.toString());
        String path1 = "E:/pythonscript2.py";
        PyCode py1 = interpretor.compile(Application.getBufferedString(path1));
        String path2 = "E:/pythonscript1.py";
        PyCode py2 = interpretor.compile(Application.getBufferedString(path2));
        interpretor.exec(py1);
        interpretor.exec(py2);*/
		if(generateDeugScript) {
			this.datasetsScript = new StringBuffer();
			this.datasetsScript.append("datasets= {}" + " \n");
			this.nameValueScript = new StringBuffer();
			this.execScript = null;
		}
		Date after = new Date();
		Timming.addPythonExecutorInitTime(after.getTime() - before.getTime());
	}
	public TemplateType getTemplateInstanceForBigScripts(Object interfaceType, String moduleName,String code) {
		if(code.indexOf("class ")< 0){
			return null;
		}
		String header = null;
		int headerIndex = 0;

		int maxLength= 75000;//max def fn limit 500*57=28500
		int totalCodeLength = code.length();
		if(totalCodeLength > maxLength) {
			PyObject pyObject = this.interpretor.get(moduleName);
			StringBuffer subClassCode = null;
			if(pyObject == null) {
				headerIndex =  code.indexOf(moduleName + "(Template):") + moduleName.length() + "(Template):".length();
				header = code.substring(0,headerIndex);
				//System.out.println("Header:");
				//System.out.println(header);
				int parentCount = 0;
				int currentCodeIndex = headerIndex+1;
				while(currentCodeIndex < totalCodeLength) {
					int remainingCodeIndex = code.indexOf("\tdef ", currentCodeIndex + maxLength);
					subClassCode = new StringBuffer();
					String subClassHeader = null;
					if(parentCount>0 && remainingCodeIndex < 0) {
						subClassHeader = header.replace("(Template)", "(" + moduleName + "Parent" + (parentCount-1) + ")");
					}else {
						if(parentCount == 0) {
							if(remainingCodeIndex < 0) {
								subClassHeader = header;
							}else {
								subClassHeader = header.replace(moduleName, moduleName + "Parent" + parentCount);
							}
						}else {
							subClassHeader = header.replace(moduleName, moduleName + "Parent" + parentCount);
							subClassHeader = subClassHeader.replace("(Template)", "(" + moduleName + "Parent" + (parentCount-1) + ")" );
						}
					}
					subClassCode = subClassCode.append(subClassHeader);

					String currentClassCode;
					if(remainingCodeIndex < 0) {
						currentClassCode = code.substring(currentCodeIndex);
						subClassCode = subClassCode.append("\n" + currentClassCode);
						currentCodeIndex = totalCodeLength ;
					}else {
						int codeEndIndex = remainingCodeIndex;
						currentClassCode = code.substring(currentCodeIndex,codeEndIndex);
						subClassCode = subClassCode.append("\n" + currentClassCode);
						currentCodeIndex = codeEndIndex;
						interpretor.exec(interpretor.compile(subClassCode.toString(), moduleName + "Parent" + parentCount));
					}
					//System.out.println("subclass Code:");
					//System.out.println(subClassCode);
					parentCount++;
				}
			}
			return getTemplateInstance(interfaceType, moduleName,subClassCode != null ? subClassCode.toString(): null);
		}else {
			return getTemplateInstance( interfaceType, moduleName, code);
		}
	}

	public TemplateType getTemplate() {
		return template;
	}
	public void setTemplate(TemplateType template) {
		this.template = template;
	}
	public TemplateType getTemplateInstance(Object interfaceType, String moduleName,String code) {
		JythonObjectFactory factory = JythonObjectFactory.getInstance();
		this.template = (TemplateType) factory.createObject(this.interpretor, interfaceType, moduleName, code);

		/*try {
			InputStream in = this.getClass().getResourceAsStream("/customFunctions.py");
			//String customFunctionCode = readPythonFiile("customFunctions.py");
			String customFunctionCode = readPythonFile(in);
			this.interpretor.exec(this.interpretor.compile(customFunctionCode, "customFunctions"));
		} catch (IOException e) {
			logger.error("Unable to load CustomFunction.py file");
			e.printStackTrace();
		}*/

		return this.template;
	}

	private static String readPythonFile(InputStream in) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		//BufferedReader reader = new BufferedReader(new FileReader(fileName));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		String ls = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		// delete the last new line separator
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		reader.close();
		return stringBuilder.toString();
	}

	/*private static String readPythonFiile(String fileName) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		StringBuilder stringBuilder = new StringBuilder();
		String line = null;
		String ls = System.getProperty("line.separator");
		while ((line = reader.readLine()) != null) {
			stringBuilder.append(line);
			stringBuilder.append(ls);
		}
		// delete the last new line separator
		stringBuilder.deleteCharAt(stringBuilder.length() - 1);
		reader.close();
		return stringBuilder.toString();
	}*/

	@Override
	public void destroy() {
		this.interpretor.cleanup();
		this.interpretor.close();
		this.interpretor = null;
		Timming.addPythonDestroy();
		//this.datasets.clear();
	}
	@Override
	public void reset() {
		Date before = new Date();
		//exec("datasets = {}");
		//this.interpretor.exec("returnValues = {}");
		//this.interpretor.exec("del returnValues");
		if(generateDeugScript) {
			this.datasetsScript = new StringBuffer();
			this.datasetsScript.append("datasets= {}" + " \n");
			this.nameValueScript = new StringBuffer();
			this.execScript = null;
		}
		if(this.interpretor != null) {
			this.interpretor.cleanup();
		}
		this.template=null;
		Date after = new Date();
		Timming.addPythonExecutorResetTime(after.getTime() - before.getTime());
	}
	@Override
	public boolean isInitialized() {
		return this.interpretor != null;
	}
	@Override
	public void setInitProperties(Properties properties) {
		this.properties = properties;
	}

	@Override
	public Properties getInitProperties() {
		return this.properties;
	}

	@Override
	public Object execute(String script) {
		if(!this.isInitialized()) {
			throw new RuntimeException("Script Executor not initialized");
		}
		if(generateDeugScript) {
			this.execScript = script;
		}
		return this.executeScript(script);
	}


	@Override
	public void setNamedValue(String key, Object value) {
		if(this.generateDeugScript) {
			nameValueScript.append("from " + value.getClass().getPackage().getName() + " import " + value.getClass().getSimpleName() + "\n");
			nameValueScript.append(key + " = " + value.getClass().getSimpleName() + "(" + value.toString() + ")\n");
		}

		if(value instanceof ArrayList<?>) {
			if(this.template == null) {
				interpretor.set(key,this.getArrayListValues((ArrayList)value));
			}else {
				this.template.setProperty(key, this.getArrayListValues((ArrayList)value));
			}
		}else if(value instanceof JSONObject) {
			if(this.template == null) {
				String script = key + " = " + "json.loads('" + ((JSONObject)value).toJSONString() + "','UTF-8')";
				this.executeScript(script);
			}else {
				this.template.setJSONProperty(key,(JSONObject)value);
			}
		}
		else {
			if(this.template == null) {
				interpretor.set(key, value);
			}else {
				this.template.setProperty(key, value);
			}
		}
	}

	private PyList getArrayListValues(ArrayList value) {
		PyList pyList = new PyList();
		Iterator valueIterator = value.iterator();
		while(valueIterator.hasNext()) {
			Object item = valueIterator.next();
			if(item instanceof ArrayList) {
				PyList innerList = this.getArrayListValues((ArrayList)item);
				pyList.append(innerList);
			}else {
				pyList.add(item);
			}
		}
		return pyList;
	}

	@Override
	public Object getNamedValue(String key) {
		return interpretor.get(key);
	}
	@Override
	public void setDataset(String name,JSONObject dataset) {
		this.setDataset(name, dataset,true);
	}
	private PyCode setDatasetPyCode = null;
	private PyCode setInvalidDatasetPyCode = null;

	@Override
	public void setDataset(String name,JSONObject dataset,boolean valid) {
		/*if(dataset == null || name == null) {
			return;
		}
		name = name.replaceAll("-", "_");
		if(valid) {
			this.template.setDataset(name, dataset);
		}else {
			this.template.setInvalidDataset(name, dataset);
		}
		if(this.generateDeugScript) {
			String script;
			if(valid) {
				script = "datasets['" + name + "'] = " + "json.loads('" + dataset.toJSONString() + "','UTF-8')";
			}else {
				script = "invalidDatasets['" + name + "'] = " + "json.loads('" + dataset.toJSONString() + "','UTF-8')";
			}
			this.datasetsScript.append(script + " \n");
		}*/
	}
	private JSONObject getNodeWithId(List<JSONObject> nodes,String id) {
		for(int i=0;i<nodes.size();i++) {
			JSONObject node = nodes.get(i);
			if(node.get("id").equals(id)) {
				return node;
			}
		}
		return null;
	}
	private HashMap<String, JSONObject> getCountEmptyAsZeroMap(ValueFormula formula) {
		HashMap<String, JSONObject> ret = new HashMap<>();
		List<JSONObject> rubrics = formula.getRubricsToCalculate();
		if(rubrics != null) {
			for(int i=0;i<rubrics.size();i++) {
				JSONObject rubric = new JSONObject((Map)rubrics.get(i));
				if((boolean)rubric.get("countEmptyAsZero")) {
					ret.put((String)rubric.get("id"), rubric);
				}
			}
		}
		return ret;
	}
	private JSONObject getRubricToCalculate(ValueFormula formula,String rubricId) {
		if(rubricId == null) {
			return null;
		}
		List<JSONObject> rubrics = formula.getRubricsToCalculate();
		for(int i=0;i<rubrics.size();i++) {
			JSONObject rubric =rubrics.get(i);
			if(rubric.get("id").equals(rubricId)) {
				return rubric;
			}
		}
		return null;
	}

	private RubricDataElements  getRubricDataElementsForRubric(HashMap<String, RubricDataElements> rubricDictionaries,String rubricId,boolean countEmptyAsZero,int maxValidValues,int maxValues) {
		RubricDataElements ret = rubricDictionaries.get(rubricId) ;
		if(ret == null) {
			ret = new RubricDataElements(countEmptyAsZero, maxValidValues, maxValues);
			rubricDictionaries.put(rubricId, ret);
		}
		return ret;
	}
	HashMap<String, RubricDataElements> rubricDictionaries;// = new HashMap<String, PythonScriptExecutor.RubricDataElements>();

	public void setDataSets(Map<String,List<JSONObject>> datasets,Map<String,List<JSONObject>> invalidDatasets, String value,ValueFormula formula) {
		this.rubricDictionaries = new HashMap<String, PythonScriptExecutor.RubricDataElements>();
		int maxValues = datasets.size() + invalidDatasets.size();
		this.setDataSets(datasets, value, true,formula,datasets.size(),maxValues);
		this.setDataSets(invalidDatasets, value, false,formula,datasets.size(),maxValues);
		Iterator rubricIterator = rubricDictionaries.keySet().iterator();
		while(rubricIterator.hasNext()) {
			String rubricId = (String)rubricIterator.next();
			RubricDataElements rubricDataElement = rubricDictionaries.get(rubricId);
			double []validValues = rubricDataElement.getValidValues();
			double []totalVaues = rubricDataElement.getTotalValues();
			if(validValues != null) {
				//ArrayList dvValues = Arrays.stream(validValues).boxed().collect(Collectors.toCollection(ArrayList::new));
				//PyList values = new PyList(dvValues);
				//this.template.setDataset(rubricId, values);
				ArrayList listValidValues = Arrays.stream(validValues).boxed().collect(Collectors.toCollection(ArrayList::new));
				//PyList pyValues = new PyList(listValidValues);
				PyList pyValues = new PyList(listValidValues.subList(0, rubricDataElement.cntValidValues));
				this.template.setDataset(rubricId, pyValues);
			}else {
				this.template.setDataset(rubricId, new PyList());
			}
			if(totalVaues != null) {
				ArrayList listTotalValues = Arrays.stream(totalVaues).boxed().collect(Collectors.toCollection(ArrayList::new));
				int totalValues = rubricDataElement.countEmptyAsZero ? maxValues : rubricDataElement.cntValidValues + rubricDataElement.cntInvalidValues;
				PyList pyTotalValues = new PyList(listTotalValues.subList(0, totalValues));
				this.template.setInvalidDataset(rubricId, pyTotalValues);
			}else {
				PyList pyValues = new PyList();
				if(validValues != null) {
					ArrayList listValidValues = Arrays.stream(validValues).boxed().collect(Collectors.toCollection(ArrayList::new));
					pyValues = new PyList(listValidValues.subList(0, rubricDataElement.cntValidValues));
				}
				this.template.setInvalidDataset(rubricId, pyValues);
			}
			rubricDataElement = null;
			//this.template.setAverageValue(rubricId,rubricDataElement.getAvarage());later check with benchmark mode
			//this.template.setMaximumValue(rubricId,rubricDataElement.getMax());
			//this.template.setMinimumValue(rubricId,rubricDataElement.getMin());
		}
		Set<String> rubDicts = this.rubricDictionaries.keySet();
		rubDicts.forEach(key->{
			this.rubricDictionaries.get(key).clean();
		});
		this.rubricDictionaries.clear();
		this.rubricDictionaries = null;
	}

	@Override
	public void setDataSets(Map<String,List<JSONObject>> datasets,String value,boolean valid,ValueFormula formula, int maxValidValues, int maxValues) {
		if(datasets == null) {
			return;
		}
		if(this.generateDeugScript) {
			if(valid) {
				this.datasetsScript.append("datasets= {}" + " \n");
			}else{
				this.datasetsScript.append("invalidDatasets= {}" + " \n");
			}
		}
		/*if(valid) {
			this.interpretor.set("datasets",new PyDictionary());
		}else {
			this.interpretor.set("invalidDatasets",new PyDictionary());
		}*/
		//HashMap<String, RubricDataElements> rubricDataElements = new HashMap<String, RubricDataElements>();
		//JSONObject rubric = this.getRubricToCalculate(formula, value);
		HashMap<String, JSONObject> countEmptyRubrics = this.getCountEmptyAsZeroMap(formula);
		//HashMap<String, RubricDataElements> rubricDictionaries = new HashMap<String, PythonScriptExecutor.RubricDataElements>();
		Iterator<String> keys = datasets.keySet().iterator();
		while(keys.hasNext()) {
			String key = keys.next();
			//PyDictionary datasetDictionary = new PyDictionary();
			if(value != null) {
				List<JSONObject> snapshotValues =  ((List<JSONObject>)datasets.get(key));
				if(snapshotValues != null && getNodeWithId(snapshotValues,value) != null) {
					JSONObject valueNode = getNodeWithId(snapshotValues,value);
					if(valueNode != null) {
						JSONObject obj = new JSONObject();
						if(valueNode instanceof Map) {
							obj = new JSONObject((Map)valueNode);
						}else {
							obj = (JSONObject)valueNode;
						}
						boolean countEmptyAsZero = (boolean)countEmptyRubrics.get(obj.get("id")).get("countEmptyAsZero");
						RubricDataElements rubricDataElement = this.getRubricDataElementsForRubric(this.rubricDictionaries, (String)obj.get("id"),countEmptyAsZero,maxValidValues,countEmptyRubrics.size());
						if(valid) {
							rubricDataElement.addValidValue((Double)obj.get("Value"));
						}else {
							rubricDataElement.addInvalidValue((Double)obj.get("Value"));
						}
					}
				}
			}else {//for benchmark calc
				List<JSONObject> dataset = datasets.get(key);
				List<String> processedIds = new ArrayList<String>();
				for(int i=0;i<dataset.size();i++) {
					JSONObject keyValue = new JSONObject((Map)dataset.get(i));
					Double rubValue = keyValue.get("Value").equals("NaN") ? Double.NaN : Double.valueOf(keyValue.get("Value").toString());
					String rubricId = (String) keyValue.get("id");
					if(processedIds.contains(rubricId)) {
						continue;
					}
					else {
						processedIds.add(rubricId);
					}
					boolean countEmptyAsZero = false;
					if(countEmptyRubrics.get(rubricId) != null) {
						countEmptyAsZero = (boolean)countEmptyRubrics.get(rubricId).get("countEmptyAsZero");
					}
					RubricDataElements rubricDataElement = this.getRubricDataElementsForRubric(this.rubricDictionaries, rubricId,countEmptyAsZero, maxValidValues, maxValues);
					if(valid) {
						rubricDataElement.addValidValue(rubValue);
					}else {
						rubricDataElement.addInvalidValue(rubValue);
					}
				}
			}
		}
	}
	@Override
	public void setDataSets(Map<String,List<JSONObject>> datasets,String value,ValueFormula formula, int maxValidValues, int maxValues) {
		this.setDataSets(datasets, value, true,formula, maxValidValues, maxValues);
	}
	private Object executeScript(String script) {
		/*InputStream scriptStream = new ByteArrayInputStream(script.getBytes());
		interpretor.execfile(scriptStream );
		return interpretor.get("returnValue");*/
		if(this.generateDeugScript) {
			this.execScript = script;
		}
		//Date before = new Date();
		interpretor.exec(script);
		//Date after = new Date();
		//Timming.addPythonTime(after.getTime() - before.getTime());
		return interpretor.get("returnValue");
	}

	@Override
	public void compile(String code) {
		this.interpretor.compile(code);
	}

	public String getDebugScript() {
		if(this.generateDeugScript) {
			StringBuffer debugScript = new StringBuffer();
			debugScript.append(baseScript).append(" \n");
			debugScript.append(datasetsScript);
			debugScript.append(nameValueScript);
			debugScript.append(execScript);
			return debugScript.toString();
		}
		else {
			return null;
		}
	}

	public Boolean getGenerateDeugScript() {
		return generateDeugScript;
	}
	public void setGenerateDeugScript(Boolean generateDeugScript) {
		this.generateDeugScript = generateDeugScript;
		if(this.generateDeugScript) {
			datasetsScript = new StringBuffer();
			nameValueScript = new StringBuffer();
		}
	}
	@Override
	public void generateDebugScript(Boolean generate) {
		this.setGenerateDeugScript(generate);

	}

	public static void main(String[] args) {

		String JYTHON_HOME = "E:\\jython2.7.0";
		String JYTHON_LIB = JYTHON_HOME + "\\Lib";

		Properties properties = new Properties();
		properties.setProperty("python.home", JYTHON_HOME);
		properties.setProperty("python.path", JYTHON_LIB);
		properties.put("python.console.encoding", "UTF-8");
		PythonScriptExecutor ex = new PythonScriptExecutor();
		ex.setGenerateDeugScript(true);
		ex.setInitProperties(properties);
		ex.init();

		try {
			InputStream in = PythonScriptExecutor.class.getResourceAsStream("/BenchmarkTemplate.py");
			//String customFunctionCode = readPythonFiile("customFunctions.py");
			String customFunctionCode = readPythonFile(in);
			//ex.interpretor.exec(ex.interpretor.compile(customFunctionCode, "BenchmarkTemplate"));
			JythonObjectFactory factory = JythonObjectFactory.getInstance();
			TemplateType template = (TemplateType) factory.createObject(ex.interpretor, TemplateType.class, "BenchmarkTemplate", customFunctionCode);
		} catch (IOException e) {
			logger.error("Unable to load CustomFunction.py file" + e.getMessage(), e);
			//e.printStackTrace();
		}
        /*ArrayList test = new ArrayList<ArrayList<String>>();
        ArrayList list1 = new ArrayList<String>();
        list1.add("s11");
        list1.add("s12");
        test.add(list1);
        ArrayList list2 = new ArrayList<String>();
        list2.add("s21");
        list2.add("s22");
        test.add(list2);
        ex.setNamedValue("test", test);*/
		JSONObject e1 = new JSONObject();
		e1.put("Value", "Financiële vaste activa");
		ex.setNamedValue("test", e1);
		Object t1 = ex.getNamedValue("test");
		//ex.executeScript("s1 =set(test)");
		//ex.getNamedValue("s1");
        /*JSONObject e1 = new JSONObject();
        e1.put("Value", 100);
        JSONObject e2 = new JSONObject();
        e2.put("Value", 200);
        JSONObject e3 = new JSONObject();
        e3.put("Value", 300);
        JSONObject e4 = new JSONObject();
        e4.put("Value", 500);



        JSONObject dataset= new JSONObject();
        dataset.put("e1", e1);
        dataset.put("e2", e2);
        dataset.put("e3", e3);
        dataset.put("e4", e4);
        System.out.println(dataset.toJSONString());
        JSONObject eo = (JSONObject)dataset.get("e1");*/

		//ex.setDataset("test", dataset);

		//ex.executeScript("from statistics import stdev ");
		//ex.executeScript("from statistics import mean ");
		//ex.executeScript("from numpy import percentile ");

		//ex.setNamedValue("dataset", dataset);

		//PyObject ds = (PyObject)ex.getNamedValue("dataset");

        /*ex.executeScript("e1Val = dataset.get('e1').get('Value')");
        PyObject m = (PyObject)ex.getNamedValue("e1Val");
        System.out.println(m);*/
		//ex.executeScript("returnValue = stdev(values)");
		//PyObject s = (PyObject)ex.getNamedValue("returnValue");
		//System.out.println(s);

        /*String script = "from statistics import stdev \n" +
        				"from statistics import mean \n" +
        				"values = dataset.getValues() \n" +
        				"returnValue = stdev(values)";
        ex.executeScript(script);*/

        /*String script = "pjson = json.loads(dataset.toJSONString()) \n" +
        				"e1 = pjson['e1'] \n" +
						"val = e1['Value']";
        try {
            ex.compile(script);
            ex.executeScript(script);
            PyObject s = (PyObject)ex.getNamedValue("val");
            System.out.println(s);
            System.out.println("done");
            ex.compile("val=10");
            ex.executeScript(script);
            System.out.println(ex.getDebugScript());
        }catch(Exception e) {
        	e.printStackTrace();
        }*/
		System.exit(0);
        
        
        /*ex.executeScript("returnValue = mean(values)");
        PyObject m = (PyObject)ex.getNamedValue("returnValue");
        System.out.println(m);*/

        /*ex.executeScript("returnValue = percentile(values,100)");
        PyObject p = (PyObject)ex.getNamedValue("returnValue");
        System.out.println(p);*/
	}


}
