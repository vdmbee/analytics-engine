package com.vdmbee.server.auth;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ApplicationIdConstants {


    private static final String VMP = "MUU3QTQzQ0YtMTI2Qy00MUUxLTgxQUMtQzg3NTU4NUEwMUVF";

    private static final String Internal = "NDMwMjFBQTUtRjBENC00Njc3LUJCRTAtMDA2RTI3MjYxQzFD";

    static List<String> getListOfRegisteredApplicationIds() {
        String[] arrayOfApplicationIds = { VMP, Internal };
        return new ArrayList<>(Arrays.asList(arrayOfApplicationIds));
    }
}

