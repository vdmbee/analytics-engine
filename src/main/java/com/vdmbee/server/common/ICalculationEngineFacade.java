package com.vdmbee.server.common;

import org.json.simple.JSONObject;

public interface ICalculationEngineFacade {
	public JSONObject handleCalculateEngineRequest(JSONObject body);
}
