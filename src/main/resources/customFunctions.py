import math
from com.vdmbee.calculationengine.expression.providers.PercentileExcel import percentileInc
from statistics import *
import datetime

def getCalculatedValues(rubricId,dataset,invalidSets,measureList,returnValues):
	#print("calculating for rubric:" + rubricId)
	#print("before filtering data from list for rubric:")
	#print(datetime.datetime.now())
	#filteredList = list(filter(lambda y:rubricId in y,dataset.values()))
	if rubricId not in dataset and rubricId not in invalidSets :
		return
	if rubricId not in dataset :
		valList = []
	else :
		valList = list(dataset[rubricId])
	if rubricId not in invalidSets :
		valList = []
	else :
		totalList = list(invalidSets[rubricId])
	#totalList = list(map(lambda x: x[rubricId], filteredList + invalidLists))
	#print("after filtering data from list:")
	#print(datetime.datetime.now())
	
	valListLen = getSampleSizeVal(valList)
	totalListLen = getSampleSizeVal(totalList)
	standardDev = getSTDVal(valListLen,valList)
	valListAvg = getAvgVal(valListLen,valList)
    
	for measure in measureList:
		if measure == 'minimum':
			returnValues[measure+'_'+rubricId]=getMinVal(valListLen,valList)
		elif measure == 'maximum':
			returnValues[measure+'_'+rubricId]=getMaxVal(valListLen,valList)
		elif measure == 'standardDeviation':
			returnValues[measure+'_'+rubricId]=standardDev
		elif measure == 'sampleSize':
			returnValues[measure+'_'+rubricId]=valListLen
		elif measure == 'average':
			returnValues[measure+'_'+rubricId]=valListAvg
		elif measure.startswith('standardDeviationPoint'):
			stdNumber = measure.partition('-')[2]
			stdNumberString = stdNumber.replace(".", "-")
			returnValues['standardDeviationPoint'+stdNumberString+'_'+rubricId]=getSTDPointVal(valListAvg,stdNumber,standardDev)
		elif measure.startswith('percentilePrevalidation'):
			percentileNumber = measure.partition('-')[2]
			percentileNumberString = percentileNumber.replace(".", "-")
			returnValues['percentilePrevalidation'+percentileNumberString+'_'+rubricId]=getPercentilePrevalidationVal(totalListLen,totalList,percentileNumber)
		elif measure.startswith('percentile'):
			percentileNumber = measure.partition('-')[2]
			percentileNumberString = percentileNumber.replace(".", "-")
			returnValues['percentile'+percentileNumberString+'_'+rubricId]=getPercentileVal(valListLen,valList,percentileNumber)
	#print("after calculating benchmark values for rubric:")
	#print(datetime.datetime.now())
		
def getMinVal(valListLen,valList):
	if valListLen > 0: return min(valList)
	else: return float('NaN')
	
def getMaxVal(valListLen,valList):
	if valListLen > 0: return max(valList)
	else: return float('NaN')
    
def getSTDVal(valListLen,valList):
	if valListLen > 2: return stdev(valList)
	else: return float('NaN')
	
def getSampleSizeVal(valList):
	return len(valList)
	
def getAvgVal(valListLen,valList):
	if valListLen > 0: return mean(valList)
	else: return float('NaN')
	
def getSTDPointVal(valListAvg,stdNumber,standardDev):
	if math.isnan(standardDev): return float('NaN')
	else: return valListAvg + float(stdNumber) * standardDev
	
def getPercentileVal(valListLen,valList,percentile):
	if valListLen > 0: return percentileInc(valList,float(percentile))
	else: return float('NaN')
	
def getPercentilePrevalidationVal(totalListLen,totalList,percentile):
	if totalListLen > 0: return percentileInc(totalList,float(percentile))
	else: return float('NaN')

def kpiInit(val):
	if(math.isnan(val)):
		return 0;
	else:
		return val;
	   
def allNuls(valList):
	return all([math.isnan(x) for x in valList]);

def get_key(dic,key):
	if(key in dic):
		return dic[key]
	else:
		return 0