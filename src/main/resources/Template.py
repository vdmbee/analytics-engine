from com.vdmbee.calculationengine.expression.providers import TemplateType
import org.json.simple.JSONObject
import json
import sys
import traceback
import com.vdmbee.calculationengine.analyser.formula.PythonExecutor

class Template(TemplateType):
	def __init__(self):
		self.ds = {}
		self.invalidDs = {}
		self.rubricValue = None
		self.returnValues = {}
		self.returnValue = None
		self.exec = None
		self.min = {}
		self.max = {}
		self.avg = {}
		self.mes = []
	
	def setMeasures(self,measures):
		del self.mes[:]
		self.mes.extend(measures)
	
	def getMeasures(self):
		return self.mes
		
	def clearMeasures(self):
		self.mes.clear()

	def setDataset(self,name,dataset):
		self.ds[name] = dataset

	def getDataset(self,name):
		return self.ds[name]

	def setInvalidDataset(self,name,dataset):
		self.invalidDs[name] = dataset

	def getInvalidDataset(self,name):
		return self.invalidDs[name]

	def setDatasetValue(self,rubric):
		self.rubricValue = rubric
	
	def getDatasetValue(self):
		return self.rubricValue
	
	def getMinimumValue(self,rubricId):
		return self.min[rubricId]
		
	def setMinimumValue(self,rubricId,val):
		self.min[rubricId] = val
		
	def getMaximumValue(self,rubricId):
		return self.max[rubricId]
		
	def setMaximumValue(self,rubricId,val):
		self.max[rubricId] = val

	def getAverageValue(self,rubricId):
		return self.avg[rubricId]
		
	def setAverageValue(self,rubricId,val):
		self.avg[rubricId] = val
		
	def clearDatasetValue(self):
		self.rubricValue = None
		
	def setJSONProperty(self,property,value):
		self.property = json.loads(value.toString(),'UTF-8')
	
	def setProperty(self,property,value):
		self.__dict__[property] = value
	
	def getProperty(self,property):
		return self.__dict__[property]

	def clearDatasets(self):
		self.ds.clear()
		self.invalidDs.clear()
				
	def executeValueFormula(self,executor):
		return self.returnValues
		
	def getRetValue(self):
		return self.returnValue
	
	def setRetValue(self,val):
		self.returnValue = val
	
	def getExecutor(self):
		return self.exec
	
	def setExecutor(self,obj):
		self.exec = obj	
	
	def cleanUp(self):
		self.ds.clear()
		self.invalidDs.clear()
		self.rubricValue = None
		self.returnValues.clear()
		self.rubricIds = None
		self.returnValue = None
		self.min.clear()
		self.max.clear()
		self.avg.clear()
		if hasattr(self, 'vb') and type(self.vb) is dict:
			self.vb.clear()
		self.__dict__.clear()
		
	def __del__(self):
		self.cleanUp()
		
	def hc(self,fc,key,shortKey):
		try:
			fc(self)
		except (ArithmeticError) as error:
			self.returnValues[key]=float('NaN')
			self.__dict__[shortKey]=float('NaN')
		except:
			print(sys.exc_info()[0])
			print(key)
			print(shortKey)
			traceback.print_exc()
			self.returnValues[key]=float('NaN')
			self.__dict__[shortKey]=0