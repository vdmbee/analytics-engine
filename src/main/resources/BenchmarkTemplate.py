import org.json.simple.JSONObject
import json
import com.vdmbee.calculationengine.analyser.formula.PythonExecutor

class BenchmarkTemplate(Template):
	def executeValueFormula(self,executor):
		Template.hc(self,self.__class__.fv957,\"benchmark\",\"v957\")
		return self.returnValues

	def fv957(self):
		self.v957 = {}
		for x in self.rubricIds:
			getCalculatedValues(x,self.ds,self.invalidDs,self.measures,self.v957)
		self.returnValues[\"benchmark\"] = self.v957
