FROM docker.io/library/eclipse-temurin:17.0.12_7-jre

# Create a non-root user
#RUN groupadd -g 1000 username && useradd -r -u 1000 -g username username

# Set the user
#USER username

####################### JYTHON ################################

RUN apt-get purge -y python.*

ENV JYTHON_VERSION="2.7.3"

RUN curl -fSL -o jython_installer.jar "http://search.maven.org/remotecontent?filepath=org/python/jython-installer/${JYTHON_VERSION}/jython-installer-${JYTHON_VERSION}.jar" \
 && java -jar jython_installer.jar -s -d /usr/local/jython \
 && rm jython_installer.jar \
 && ln -s /usr/local/jython/bin/jython /usr/local/jython/bin/python \
 && ln -s /usr/local/jython/bin/jython /usr/local/jython/bin/python2

ENV PATH="/usr/local/jython/bin:$PATH"
ENV JYTHON_HOME=/usr/local/jython

RUN export PATH=/usr/local/jython/bin:$PATH


#RUN /usr/local/jython/bin/jython -m ensurepip
#RUN /usr/local/jython/bin/jython -m pip install --upgrade setuptools==65.5.1
#RUN mkdir -p ~/.conf/pip
#RUN echo '[global]\n index-url=https://pypi.python.org/simple/ ' >> ~/.conf/pip/pip.conf
#RUN /usr/local/jython/bin/jython -m pip install --index-url=https://pypi.org/simple/ statistics --log ./pip.log ; cat  ./pip.log
#RUN /usr/local/jython/bin/jython -m pip install statistics --log ./pip.log ; cat  ./pip.log
####################### JYTHON ################################

RUN mkdir -p /usr/local/app
WORKDIR /usr/local/app

ARG CUSTOM_FUNCTIONS=./src/main/resources/customFunctions.py
COPY ${CUSTOM_FUNCTIONS} /usr/local/jython/Lib/site-packages/
#RUN cp customFunctions.py /usr/local/jython/Lib/site-packages/

RUN mkdir -p /usr/local/jython/Lib/site-packages/statistics
ARG STATS_INIT=./src/main/resources/statistics/__init__.py
COPY ${STATS_INIT} /usr/local/jython/Lib/site-packages/statistics

ARG CUSTOM_TEMPLATE=./src/main/resources/Template.py
COPY ${CUSTOM_TEMPLATE} /usr/local/jython/Lib/site-packages/

ARG JAR_FILE=./target/gs-vmp-analysis-0.1.0.jar

ENV APPLICATIONINSIGHT_AGENT_VERSION=3.2.3
RUN curl -Lo applicationinsights-agent.jar  https://github.com/microsoft/ApplicationInsights-Java/releases/download/${APPLICATIONINSIGHT_AGENT_VERSION}/applicationinsights-agent-${APPLICATIONINSIGHT_AGENT_VERSION}.jar

#COPY ${JAR_FILE} gs-snapshot-server.jar
ADD ${JAR_FILE} .
EXPOSE ${appPort} ${BMS_ADDITIONAL_PORTS}

ENTRYPOINT ["java","-javaagent:applicationinsights-agent.jar","-server","-XX:+UseG1GC","-XX:+UseStringDeduplication", "-jar","gs-vmp-analysis-0.1.0.jar"]


#docker exec -it b2350f80762f /bin/bash
#root@<containerName>:/usr/local/app# cd ..
#root@<containerName>:/usr/local# cd jython/Lib/site-packages/
#root@<containerName>:/usr/local/jython/Lib/site-packages# jython -m pip install statistics
